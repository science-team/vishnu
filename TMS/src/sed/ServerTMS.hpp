/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file ServerTMS.hpp
 * \brief This file presents the implementation of the TMS server.
 * \author Daouda Traore (daouda.traore@sysfera.com)
 * \date April
*/

#ifndef _TMS_SERVER_H_
#define _TMS_SERVER_H_

#include <string>

#include "DIET_server.h"
#include "DbConfiguration.hpp"
#include "utilVishnu.hpp"
#include "TMSMapper.hpp"
#include "MapperRegistry.hpp"

class Database;

/**
 *  * \brief Number of service in TMS
 *   */
#define NB_SRV 10 

static const char* SERVICES[NB_SRV] = {
  "jobSubmit_",
  "jobCancel_",
  "jobInfo_",
  "getListOfJobs_",
  "getJobsProgression_",
  "getListOfQueues_",
  "jobOutputGetResult_",
  "jobOutputGetCompletedJobs_",
  "getListOfJobs_all",
  "jobSubmit_autom"
};

/**
 * \class ServerTMS
 * \brief This class describes the g server
 */
class ServerTMS {
public :

  /**
   * \brief To get the unique instance of the server
   */
  static ServerTMS*  getInstance();
  /**
   * \brief To get the unique instance of the database
   */
  Database* getDatabaseVishnu();

  /**
   * \brief to get the VishnuId
   * \return the path of the configuration file
   */
  int
  getVishnuId() const;
  /**
  * \brief To get the batchType
  * \return the type of the underlying batch scheduler
  */
  BatchType
  getBatchType();

 /**
  * \brief To get the machine id of the TMS server
  * \return the machine id
  */
  std::string
  getMachineId();

  /**
   * \brief To get the slave binary directory
   * \return path to the binary tmsSlave
   */
  std::string
  getSlaveDirectory();

   /**
   * \brief To initialize the TMS Server class
   * \param vishnuId The identifier of the vishnu instance
   * \param dbConfig  The configuration of the database
   * \param machineId the id of the machine
   * \param batchType the type of batch scheduler
   * \param slaveBinDir  the directory that contains the slave binary
   * \return raises an exception on error
   */
  int
  init(int vishnuId,
       DbConfiguration dbConfig,
       std::string machineId,
       BatchType batchType,
       std::string slaveBinDir);

  /**
   * \brief Destructor, raises an exception on error
   */
  ~ServerTMS();

private :

  /**
   * \brief Constructor, private because class is singleton
   */
  ServerTMS();

  /**
   * \brief operator=
   */
  ServerTMS& operator=(const ServerTMS&);

  /**
   * \brief Function to compute the batch load performance (number of waiting jobs, running jobs and total jobs)
   * \param pb the resquest profile
   * \param perfValues The vector contain the estimation load performance (number of waiting jobs, running jobs and total jobs)
   */
  static void
    setBatchLoadPerformance(diet_profile_t* pb, estVector_t perfValues);
  

  /////////////////////////////////
  // Attributes
  /////////////////////////////////
  /**
   * \brief The singleton reference
   */
  static ServerTMS *minstance;
  /**
   * \brief Instance of TMSMapper
   */
  static TMSMapper *mmapper;
  /**
   * \brief The vishnu id
   */
  int mvishnuId;
  /**
  * \brief represents The batch Type
  */
  BatchType mbatchType;
  /**
  * \brief represents The batch Type
  */
  std::string mmachineId;
  /**
  * \brief Structure representing a profile description
  */
  diet_profile_desc_t* mprofile;
  /**
  * \brief Instance of Database
  */
  Database *mdatabaseVishnu;
  /**
   * \brief Directory containing the slave binary
   */
  std::string mslaveBinDir;
};
#endif // SERVER
