/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
  * \file JobServer.hpp
  * \brief This file contains the VISHNU JobServer class.
  * \author Daouda Traore (daouda.traore@sysfera.com)
  * \date April 2011
  */

#ifndef _JOB_SERVER_H
#define _JOB_SERVER_H

#include "TMS_Data.hpp"
#include "SessionServer.hpp"
#include "MachineServer.hpp"
/**
 * \class JobServer
 * \brief JobServer class implementation
 */
class JobServer
{
 
public:
	/**
  * \param sessionServer The object which encapsulates the session information
  * \param machineId The machine identifier 
  * \param job The job data structure
  * \param batchType The batch scheduler type
  * \brief Constructor
  */
  explicit JobServer(const SessionServer& sessionServer,
                     const std::string& machineId,
                     const TMS_Data::Job& job,
                     const BatchType& batchType);
  /**
   * \brief Function to submit job
   * \param scriptContent the content of the script
   * \param options the options to submit job
   * \param vishnuId The VISHNU identifier
   * \param slaveDirectory the path to the TMS slave executable
   * \return raises an exception on error
   */
	int submitJob(const std::string& scriptContent, 
                const TMS_Data::SubmitOptions& options,
                const int& vishnuId,
                const std::string& slaveDirectory);

  /**
   * \brief Function to cancel job
   * \param slaveDirectory the path to the TMS slave executable
   * \return raises an exception on error
   */
	int cancelJob(const std::string& slaveDirectory);

  /**
   * \brief Function to get job information
   * \return The job data structure
   */
  TMS_Data::Job getJobInfo(); 

  /**
   * \brief Function to get job data 
   * \return The job data structure
   */
  TMS_Data::Job getData();

  /**
   * \brief Destructor
   */
  ~JobServer(); 
  

  private:

  /**
   * \brief Function to scan VISHNU error message 
   * \param errorInfo the error information to scan
   * \param code The code The code of the error
   * \param message The message associeted to the error code
   * \return raises an exception on erroor
   */
  void scanErrorMessage(const std::string& errorInfo, int& code, std::string& message);
  
  /**
   * \brief Function to convert a given date into correspondant long value
   * \fn long long convertToTimeType(std::string date)
   * \param date The date to convert
   * \return The converted value
   */
  long long convertToTimeType(std::string date);  

  /**
   * \brief job data structure 
   */ 
  TMS_Data::Job mjob;

  /**
   * \brief The object which encapsulates the session information 
   */
  SessionServer msessionServer;

  /**
   * \brief The machine identifier 
   */   
  std::string mmachineId;
  
  /**
   * \brief The type of the batch scheduler 
   */
  BatchType mbatchType;
  
  /**
   * \brief An instance of vishnu database
   */
  Database *mdatabaseVishnu;
};

#endif
