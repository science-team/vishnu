/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file ListQueuesServer.hpp
 * \brief This file contains the VISHNU QueryServer class.
 * \author Daouda Traore (daouda.traore@sysfera.com)
 * \date February 2011
 */
#ifndef _QUERY_SERVER_H_
#define _QUERY_SERVER_H_

#include <string>

#include "utilVishnu.hpp"
#include "BatchServer.hpp"
#include "BatchFactory.hpp"
#include "ListQueuesServer.hpp"


/**
 * \fn ListQueuesServer(const SessionServer session)
 * \param session The object which encapsulates the session information (ex: identifier of the session)
 * \brief Constructor, raises an exception on error
 */
ListQueuesServer::ListQueuesServer(const SessionServer& session,
                                   const std::string& machineId,
                                   const BatchType& batchType,
                                   const std::string& option)
 : moption(option), mlistQueues(NULL)
{
   BatchFactory factory;
   mbatchServer = factory.getBatchServerInstance(batchType);
}

/**
 * \brief Function to list machines information
 * \fn TMS_Data::ListQueues* list()
 * \return The pointer to the TMS_Data::ListQueues containing users information
 * \return raises an exception on error
 */
TMS_Data::ListQueues* ListQueuesServer::list()
{
  return mbatchServer->listQueues(moption);
}

/**
 * \fn ~ListQueuesServer()
 * \brief Destructor, raises an exception on error
 */
ListQueuesServer::~ListQueuesServer()
{
}

#endif
