/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file Env.cpp
 * \brief This file contains the VISHNU Env class.
 * \author Daouda Traore (daouda.traore@sysfera.com)
 * \date December 2011
 */

#include "Env.hpp"

/**
 * \param batchType The batch scheduler type
 * \brief Constructor
 */
Env::Env(const BatchType& batchType): mbatchType(batchType) {
}

/**
 * \brief Function to replace all occurences in a string
 * \param scriptContent The string to modify
 * \param oldValue The value to replace
 * \param newValue The new value
 */

void Env::replaceAllOccurences(std::string& scriptContent,
    const std::string& oldValue,
    const std::string& newValue) {

  size_t pos = scriptContent.find(oldValue);
  while(pos!=std::string::npos) {
    scriptContent.replace(pos, oldValue.size(), newValue, 0, newValue.size());
    pos = scriptContent.find(oldValue, pos+newValue.size());
  }

}

/**
 * \brief Function to replace some environment variables in a string
 * \param scriptContent The string content to modify
 */
void Env::replaceEnvVariables(std::string& scriptContent) {

  std::string numNodes;
  std::string cpuPerNode;
  size_t pos;
  switch(mbatchType) {

    case TORQUE:
      //To replace VISHNU_BATCHJOB_ID
      replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_ID", "$PBS_JOBID");
      replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_ID}", "$PBS_JOBID");
      //To replace VISHNU_BATCHJOB_NAME
      replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_NAME", "$PBS_JOBNAME");
      replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_NAME}", "$PBS_JOBNAME");
      //To replace VISHNU_BATCHJOB_NODEFILE
      replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_NODEFILE", "$PBS_NODEFILE");
      replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_NODEFILE}", "$PBS_NODEFILE");
      //To replace VISHNU_BATCHJOB_NUM_NODES
      replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_NUM_NODES", "$(cat  $PBS_NODEFILE | sort | uniq | wc -l)");
      replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_NUM_NODES}", "$(cat  $PBS_NODEFILE | sort | uniq | wc -l)");
      break;
    case LOADLEVELER:
      //To replace VISHNU_BATCHJOB_ID
      replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_ID", "$LOADL_STEP_ID"); 
      replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_ID}", "$LOADL_STEP_ID"); 
      //To replace VISHNU_BATCHJOB_NAME
      replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_NAME", "$LOADL_JOB_NAME"); 
      replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_NAME}", "$LOADL_JOB_NAME"); 
      //To replace VISHNU_BATCHJOB_NODEFILE
      replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_NODEFILE", "$LOADL_HOSTFILE"); 
      replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_NODEFILE}", "$LOADL_HOSTFILE");
      //To replace VISHNU_BATCHJOB_NUM_NODES
      replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_NUM_NODES", "$(cat  $LOADL_HOSTFILE | sort | uniq | wc -l)");
      replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_NUM_NODES}", "$(cat  $LOADL_HOSTFILE | sort | uniq | wc -l)"); 
      break;
    case SLURM:
      //To replace VISHNU_BATCHJOB_ID
      replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_ID", "$SLURM_JOB_ID");
      replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_ID}", "$SLURM_JOB_ID");
      //To replace VISHNU_BATCHJOB_NAME
      replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_NAME", "$SLURM_JOB_NAME");
      replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_NAME}", "$SLURM_JOB_NAME");
      //To replace SLURM_JOB_NUM_NODES
      replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_NUM_NODES", "$SLURM_JOB_NUM_NODES");
      replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_NUM_NODES}", "$SLURM_JOB_NUM_NODES");

      //To replace VISHNU_BATCHJOB_NODEFILE
      replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_NODEFILE}", "$VISHNU_BATCHJOB_NODEFILE");      
      pos = scriptContent.find("$VISHNU_BATCHJOB_NODEFILE");
      if(pos!=std::string::npos) {
        std::string fileName = "/tmp/NODELIST_XXXXXX";
        vishnu::createTmpFile(const_cast<char*>(fileName.c_str()));
        pos = scriptContent.rfind("\n", pos-1);
        scriptContent.insert(pos+1, "echo $SLURM_JOB_NODELIST > "+fileName+"\n");
        std::string tmp = "echo $SLURM_JOB_NODELIST > "+fileName+"\n";
        scriptContent.insert(pos+1+tmp.size(), "sed -i 's/,/\\n/g' "+fileName+"\n");
        replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_NODEFILE", fileName);
        replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_NODEFILE}", fileName);
        scriptContent.insert(scriptContent.size()-1, "\n rm "+fileName+"\n");
      }

     break;
    case LSF:
      //To replace VISHNU_BATCHJOB_ID
      replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_ID", "$LSB_JOBID");
      replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_ID}", "$LSB_JOBID");
      //To replace VISHNU_BATCHJOB_NAME
      replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_NAME", "$LSB_JOBNAME");
      replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_NAME}", "$LSB_JOBNAME");
      //To replace VISHNU_BATCHJOB_NODEFILE
      replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_NODEFILE}", "$VISHNU_BATCHJOB_NODEFILE");      
      pos = scriptContent.find("$VISHNU_BATCHJOB_NODEFILE");
      if(pos!=std::string::npos) {
        std::string fileName = "/tmp/LSF_NODELIST_XXXXXX";
        vishnu::createTmpFile(const_cast<char*>(fileName.c_str()));
        pos = scriptContent.rfind("\n", pos-1);
        scriptContent.insert(pos+1, "echo $LSB_HOSTS > "+fileName+"\n");
        std::string tmp = "echo $LSB_HOSTS > "+fileName+"\n";
        scriptContent.insert(pos+1+tmp.size(), "sed -i 's/ /\\n/g' "+fileName+"\n");
        replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_NODEFILE", fileName);
        replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_NODEFILE}", fileName);
        //To replace VISHNU_BATCHJOB_NUM_NODES
        replaceAllOccurences(scriptContent, "$VISHNU_BATCHJOB_NUM_NODES", "$(cat "+fileName+" | sort | uniq | wc -l)");
        replaceAllOccurences(scriptContent, "${VISHNU_BATCHJOB_NUM_NODES}", "$(cat "+fileName+" | sort | uniq | wc -l)"); 
        scriptContent.insert(scriptContent.size()-1, "\n rm "+fileName+"\n");
      }
    default:
      break;

  }
}
