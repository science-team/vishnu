/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
  * \file JobOutputServer.hpp
  * \brief This file contains the VISHNU JobOutputServer class.
  * \author Daouda Traore (daouda.traore@sysfera.com)
  * \date April 2011
  */

#ifndef _JOB_OUTPUT_SERVER_H
#define _JOB_OUTPUT_SERVER_H

#include "TMS_Data.hpp"
#include "SessionServer.hpp"
#include "MachineServer.hpp"
/**
 * \class JobOutputServer
 * \brief JobOutputServer class implementation
 */
class JobOutputServer
{
 
public:
   /**
   * \param sessionServer The object which encapsulates the session information
   * \param machineId The machine identifier 
   * \brief Constructor
   */
  JobOutputServer(const SessionServer& sessionServer,
                  const std::string& machineId);

  /**
   * \param sessionServer The object which encapsulates the session information
   * \param machineId The machine identifier 
   * \param jobResult The job result data structure
   * \brief Constructor
   */
  JobOutputServer(const SessionServer& sessionServer,
                  const std::string& machineId,
                  const TMS_Data::JobResult& jobResult);
  /**
   * \brief Function to get the job results
   * \return The job results data structure
   */
  TMS_Data::JobResult
  getJobOutput();  

  /**
   * \brief Function to get the all completed jobs results
   * \return The lits of job results data structure
   */
  TMS_Data::ListJobResults_ptr
  getCompletedJobsOutput();

  /**
   * \brief Destructor
   */
  ~JobOutputServer(); 
  

  private:
 
  /**
   * \brief job results data structure 
   */ 
  TMS_Data::JobResult mjobResult;
 
  /**
   * \brief The object which encapsulates the session information 
   */ 
  SessionServer msessionServer;
 
  /**
   * \brief The machine identifier 
   */ 
  std::string mmachineId;
 
  /**                   
   * \brief The lits of job results data structure 
   */ 
  TMS_Data::ListJobResults_ptr mlistJobsResult;
  
  /**
   * \brief An instance of vishnu database
   */
  Database *mdatabaseVishnu;  
};

#endif
