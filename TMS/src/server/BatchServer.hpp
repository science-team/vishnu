/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file BatchServer.hpp
 * \brief This file contains the VISHNU BatchServer class.
 * \author Daouda Traore (daouda.traore@sysfera.com)
 * \date April 2011
 */

#ifndef TMS_BATCH_SERVER_H 
#define TMS_BATCH_SERVER_H

#include <string>
#include <iostream>

//EMF
#include <ecore.hpp> // Ecore metamodel
#include <ecorecpp.hpp> // EMF4CPP utils
#include "TMS_Data.hpp"

using namespace std;

/**
 * \class BatchServer
 * \brief BatchServer class implementation
 */
class BatchServer
{
public:

  /**
   * \brief Constructor
   */
  BatchServer();
   
  /**
   * \brief Function to submit a job
   * \param scriptPath the path to the script containing the job characteristique
   * \param options the options to submit job
   * \param job The job data structure
   * \param envp The list of environment variables used by submission function 
   * \return raises an exception on error
   */
  virtual int 
  submit(const char* scriptPath, 
         const TMS_Data::SubmitOptions& options, 
         TMS_Data::Job& job, char** envp=NULL)=0;

  /**
   * \brief Function to cancel job
   * \param jobId the identifier of the job to cancel
   * \return raises an exception on error
   */  
  virtual int 
  cancel(const char* jobId)=0;

  /**
   * \brief Function to get the status of the job
   * \param jobId the identifier of the job 
   * \return -1 if the job is unknown or server not  unavailable 
   */
  virtual int 
  getJobState(const std::string& jobId)=0;
  
  /**
   * \brief Function to get the start time of the job
   * \param jobId the identifier of the job 
   * \return 0 if the job is unknown or server not  unavailable
   */
  virtual time_t 
  getJobStartTime(const std::string& jobId)=0;
  
  /**
   * \brief Function to request the status of queues 
   * \param optQueueName (optional) the name of the queue to request 
   * \return The requested status in to ListQueues data structure 
   */
  virtual TMS_Data::ListQueues* 
  listQueues(const std::string& optQueueName=std::string())=0;

   /**
   * \brief Function to get a list of submitted jobs
   * \param listOfJobs the ListJobs structure to fill
   * \param ignoredIds the list of job ids to ignore 
   */
  virtual void fillListOfJobs(TMS_Data::ListJobs*& listOfJobs,
                      const std::vector<string>& ignoredIds=std::vector<string>())=0;
   
  /**
   * \brief Destructor
   */
  virtual ~BatchServer();

private:
};

#endif
