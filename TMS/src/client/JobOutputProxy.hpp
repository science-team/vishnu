/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
* \file JobOutputProxy.hpp
* \brief This file contains the VISHNU JobOutputProxy class.
* \author Eugène PAMBA CAPO-CHICHI (eugene.capochichi@sysfera.com)
* \date March 2011
*/

#ifndef _JOB_OUT_PUT_PROXY_H
#define _JOB_OUT_PUT_PROXY_H

#include "TMS_Data.hpp"
#include "SessionProxy.hpp"
#include "MachineProxy.hpp"

/**
 * \class JobOutputProxy
 * \brief JobOutputProxy class implementation
 */

class JobOutputProxy {

public:

  /**
  * \param session The object which encapsulates the session information
  * \param machineId The machine identifier where has been submitted
  * \param outDir The output directory where the files will be stored 
  * (default is current directory) 
  * \brief Constructor, raises an exception on error
  */
	explicit
	JobOutputProxy( const SessionProxy& session,
                  const std::string& machineId,
                  const std::string& outDir);

  /**
  * \brief Function to get the job results
  * \param jobId The Id of the
  * \return The job results data structure
  */
  //TMS_Data::JobResult_ptr
  TMS_Data::JobResult
	getJobOutPut(const std::string& jobId);

  /**
  * \brief Function to get the results of all job submitted
  * \return The list of the job results
  */
	TMS_Data::ListJobResults_ptr
	getCompletedJobsOutput();

  /**
  * \brief Destructor, raises an exception on error
  */
  ~JobOutputProxy();

private:

  /////////////////////////////////
  // Attributes
  /////////////////////////////////

  /**
  * \brief The object to manipulate the session data
  */
  SessionProxy msessionProxy;
  /**
  * \brief The id of the machine
  */
  std::string mmachineId;

  /**
  * \brief The output directory where the files will be stored
  */ 
  std::string moutDir;
};

#endif
