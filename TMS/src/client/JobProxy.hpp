/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
  * \file JobProxy.hpp
  * \brief This file contains the VISHNU JobProxy class.
  * \author Eugène PAMBA CAPO-CHICHI (eugene.capochichi@sysfera.com)
  * \date March 2011
  */

#ifndef _JOB_PROXY_H
#define _JOB_PROXY_H

#include "TMS_Data.hpp"
#include "SessionProxy.hpp"
#include "MachineProxy.hpp"

/**
 * \class JobProxy
 * \brief JobProxy class implementation
 */
class JobProxy {

  public:

  /**
  * \param session The object which encapsulates the session information
  * \param machineId The id of the machine
  * \param job The job data structure
  * \brief Constructor
  */
  explicit JobProxy(const SessionProxy& session,
                    const std::string& machineId,
                    TMS_Data::Job& job);
  /**
  * \brief Function to submit job
  * \param scriptContent the content of the script
  * \param options the options to submit job
  * \return raises an exception on error
  */
  int
  submitJob(const std::string& scriptContent,
            const TMS_Data::SubmitOptions& options);

  
  /**
  * \brief Function to cancel job
  * \return raises an exception on error
  */
  int
  cancelJob();

  /**
   * \brief Function to get job information
   * \return raises an exception on error
   */
  TMS_Data::Job  
  getJobInfo(); 

  /**
  * \brief Function to get job information
  * \return The job data structure
  */
  TMS_Data::Job
  getData() const;

  /**
    * \brief Destructor
    */
  ~JobProxy();

  private:

  /////////////////////////////////
  // Attributes
  /////////////////////////////////

  /**
  * \brief The object to manipulate the session data
  */
  SessionProxy msessionProxy;
  /**
  * \brief The id of the machine
  */
  std::string mmachineId;
  /**
  * \brief The job data structure
  */
  TMS_Data::Job mjob;

};
#endif //_JOB_PROXY_H
