/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file submit_job.cpp
 * This file defines the VISHNU submit job command 
 * \author Coulomb Kevin (kevin.coulomb@sysfera.com) and Daouda Traore (daouda.traore@sysfera.com)
 */


#include "CLICmd.hpp"
#include "utilVishnu.hpp"
#include "cliError.hpp"
#include "cliUtil.hpp"
#include "api_ums.hpp"
#include "api_tms.hpp"
#include "sessionUtils.hpp"
#include "displayer.hpp"
#include <boost/bind.hpp>

namespace po = boost::program_options;

using namespace std;
using namespace vishnu;

/**
 * \brief To build options for the VISHNU submit job command
 * \param pgName : The name of the command
 * \param fname: The name of the job
 * \param fqueue : The queue to set the job
 * \param fwallTime : The wall time for the job
 * \param fmemory : The memory needed by the job
 * \param fnbCpu : The number of cpu per node needed 
 * \param fnbNodeAndCpu : The number of node and processor per node
 * \param foutput : The output path
 * \param ferr : The error path
 * \param fmailNotif : The type of the nofication (BEGIN, END, ERROR, and ALL)
 * \param fmailUser : The name of user to receive email notification
 * \param fgroup : The job group name
 * \param fworkingDir : The job working directory
 * \param fcpuTime : The cpu time limit of the job
 * \param loadCriterionStr : The load value to use (for now three criterions are be used: 
 *  minimum number of waiting jobs, minimum number of running jobs and the total number of job )
 * \param dietConfig: Represents the VISHNU config file 
 * \param dietConfig: Represents the VISHNU config file
 * \return The description of all options allowed by the command
 */
boost::shared_ptr<Options>
makeSubJobOp(string pgName, 
	     boost::function1<void, string>& fname,
	     boost::function1<void, string>& fqueue,
	     boost::function1<void, int>& fmemory, 
	     boost::function1<void, int>& fnbCpu,
	     boost::function1<void, string>& fnbNodeAndCpu,
	     boost::function1<void, string>& foutput,
	     boost::function1<void, string>& ferr,
	     boost::function1<void, string>& fmailNotif, 
	     boost::function1<void, string>& fmailUser,
	     boost::function1<void, string>& fgroup,
	     boost::function1<void, string>& fworkingDir,
	     boost::function1<void, string>& fcpuTime,
       string& loadCriterionStr,
       string& walltime,
       string& dietConfig){
  boost::shared_ptr<Options> opt(new Options(pgName));

  // Environement option
  opt->add("dietConfig,c",
           "The diet config file",
           ENV,
           dietConfig);

  // All cli options
  opt->add("name,n",
	   "The name of the job",
	   CONFIG,
	   fname);
  opt->add("queue,q",
	   "The queue to submit the job",
	   CONFIG,
	   fqueue);
  opt->add("wallTime,t",
	   "The wall time for the job (in secondes or in the format [[HH:]MM:]SS)",
	   CONFIG,
	   walltime);
  opt->add("memory,m",
	   "The memory needed by the job (in MegaBytes)",
	   CONFIG,
	   fmemory);
  opt->add("nbCpu,P",
	   "The number of cpu per node needed by the job",
	   CONFIG,
	   fnbCpu);
  opt->add("nbNodesAndCpuPerNode,N",
	   "The number of node and processor per node",
	   CONFIG,
	   fnbNodeAndCpu);
  opt->add("outPutPath,o",
	   "The outputh path on the remote machine of the job",
	   CONFIG,
	   foutput);
  opt->add("errorPath,e",
	   "The error path on the remote machine of the job",
	   CONFIG,
	   ferr);
  
  opt->add("mailNotification,M",
	   "Assigns the notification type of the job. Valid type values are " 
     "BEGIN, END, ERROR, and ALL (any state change).",
	   CONFIG,
	   fmailNotif);
  opt->add("mailNotifyUser,u",
	   "The name of user to receive email notification of state changes as defined " 
     "by the option mailNotification. The default value is the submitting user.",
	   CONFIG,
	   fmailUser);
  opt->add("group,g",
	   "Assigns a job group name.",
	   CONFIG,
	   fgroup);
  opt->add("workingDir,D",
	   "Assigns a job remote working dir",
	   CONFIG,
	   fworkingDir);
  opt->add("cpuTime,T",
	   "Assigns a job cpu limit time (in secondes or in the format [[HH:]MM:]SS)",
	   CONFIG,
	   fcpuTime);
  opt->add("useLoad,L",
      " The criterion to aumatically submit a job (for now three criterions are used:"
      " minimum number of waiting jobs, minimum number of running jobs and the total"
      "number of jobs). Predefined values are:\n"
      "0 or W: To select a machine that has minimum number of waiting jobs\n"
      "1 or T: To select a machine that has minimum number of total jobs (wainting and running)\n"
      "2 or R: To select a machine that has minimum number of running jobs",
      CONFIG,
      loadCriterionStr);

  return opt;
}


int main (int argc, char* argv[]){
  
  int ret; // Return value

  /******* Parsed value containers ****************/
  string dietConfig;
  string sessionKey;
  string machineId;
  string scriptPath;
  string walltime;

  /********** EMF data ************/
  TMS_Data::SubmitOptions subOp;

  /******** Callback functions ******************/
  boost::function1<void,string> fname(boost::bind(&TMS_Data::SubmitOptions::setName,boost::ref(subOp),_1));
  boost::function1<void,string> fqueue(boost::bind(&TMS_Data::SubmitOptions::setQueue,boost::ref(subOp),_1));
  boost::function1<void,int> fmemory(boost::bind(&TMS_Data::SubmitOptions::setMemory,boost::ref(subOp),_1));
  boost::function1<void,int> fnbCpu(boost::bind(&TMS_Data::SubmitOptions::setNbCpu,boost::ref(subOp),_1));
  boost::function1<void,string> fnbNodeAndCpu(boost::bind(&TMS_Data::SubmitOptions::setNbNodesAndCpuPerNode,boost::ref(subOp),_1));
  boost::function1<void,string> foutput(boost::bind(&TMS_Data::SubmitOptions::setOutputPath,boost::ref(subOp),_1));
  boost::function1<void,string> ferr(boost::bind(&TMS_Data::SubmitOptions::setErrorPath,boost::ref(subOp),_1));
  boost::function1<void,string> fmailNotif(boost::bind(&TMS_Data::SubmitOptions::setMailNotification,boost::ref(subOp),_1));
  boost::function1<void,string> fmailUser(boost::bind(&TMS_Data::SubmitOptions::setMailNotifyUser,boost::ref(subOp),_1));
  boost::function1<void,string> fgroup(boost::bind(&TMS_Data::SubmitOptions::setGroup,boost::ref(subOp),_1));
  boost::function1<void,string> fworkingDir(boost::bind(&TMS_Data::SubmitOptions::setWorkingDir,boost::ref(subOp),_1));
  boost::function1<void,string> fcpuTime(boost::bind(&TMS_Data::SubmitOptions::setCpuTime,boost::ref(subOp),_1));
  std::string loadCriterionStr; 
  /*********** Out parameters *********************/
  Job job;

  /**************** Describe options *************/
  boost::shared_ptr<Options> opt=makeSubJobOp(argv[0],fname,fqueue,
                fmemory, fnbCpu, fnbNodeAndCpu,
                foutput, ferr, fmailNotif, fmailUser, fgroup, fworkingDir, fcpuTime, loadCriterionStr, walltime, dietConfig);
 
  opt->add("selectQueueAutom,Q",
      "allows to select automatically a queue which has the number of nodes requested by the user.",
      CONFIG);
  
  opt->add("machineId,i",
	   "represents the id of the machine",
	   HIDDEN,
	   machineId,1);
  opt->setPosition("machineId",1);

  opt->add("scriptPath,p",
	   "represents the script of submission",
	   HIDDEN,
	   scriptPath,1);
  opt->setPosition("scriptPath",1);

  CLICmd cmd = CLICmd (argc, argv, opt);

  // Process command
  try {

    // Parse the cli and setting the options found
    ret = cmd.parse(env_name_mapper());

    if (ret != CLI_SUCCESS){
      helpUsage(*opt,"[options] machineId script");
      return ret;
    }

    // PreProcess (adapt some parameters if necessary)
    checkVishnuConfig(*opt);  
    if ( opt->count("help")){
      helpUsage(*opt,"[options] machineId script");
      return 0;
    }

    if(walltime.size()!=0) {
      subOp.setWallTime(convertStringToWallTime(walltime));
    }

    //To set the load criterion
    int loadCriterionType;
    TMS_Data::LoadCriterion_ptr loadCriterion_ptr = new TMS_Data::LoadCriterion();
    if(loadCriterionStr.size()!=0) {
      size_t pos = loadCriterionStr.find_first_not_of("0123456789");
      if(pos!=std::string::npos) {
        if(loadCriterionStr.size()==1) {
          switch(loadCriterionStr[0]) {
            case 'W' :
              loadCriterionType = 0;
              break;
            case 'T' :
              loadCriterionType = 1;
              break;
            case 'R' :
              loadCriterionType = 2;
              break;
            default:
              loadCriterionType = 0;
              break;
          }
        }
        if ((loadCriterionStr.size() > 1)) {
          std::cerr << "Unknown load criterion " << loadCriterionStr << std::endl;
          return 0;
        }
      } else {
        loadCriterionType = convertToInt(loadCriterionStr);
      }
      loadCriterion_ptr->setLoadType(loadCriterionType);
      subOp.setCriterion(loadCriterion_ptr);
    }

    if(opt->count("selectQueueAutom")) {
      subOp.setSelectQueueAutom(true);
    }
 
    // initializing DIET
    if (vishnuInitialize(const_cast<char*>(dietConfig.c_str()), argc, argv)) {
      errorUsage(argv[0],dietErrorMsg,EXECERROR);
      return  CLI_ERROR_DIET ;
    }

    // get the sessionKey
    sessionKey=getLastSessionKey(getppid());

    // DIET call : submit
    if(false==sessionKey.empty()){
      printSessionKeyMessage();
      submitJob(sessionKey, machineId, scriptPath, job, subOp);
    }
    displaySubmit(job);
    printSuccessMessage();
  } catch(VishnuException& except){// catch all Vishnu runtime error
    std::string  submitErrmsg = except.getMsg()+" ["+except.getMsgComp()+"]";
    errorUsage(argv[0], submitErrmsg,EXECERROR);
    //check the bad session key
    if (checkBadSessionKeyError(except)){
      removeBadSessionKeyFromFile(getppid());
    }
    return except.getMsgI() ;
  } catch(std::exception& except){// catch all std runtime error
    errorUsage(argv[0],except.what());
    return CLI_ERROR_RUNTIME;
  }

  return 0;
}
