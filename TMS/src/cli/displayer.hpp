/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

#include "api_tms.hpp"

/**
 * \brief To display the output of all job
 * \param j: the list of jobs output to display
 */
void 
displayAllJobOutput(TMS_Data::ListJobResults& j);

/**
 * \brief To display the output of a job
 * \param j: the job output result
 */
void 
displayJobOutput(TMS_Data::JobResult_ptr j);

/**
 * \brief To display the job info
 * \param j: The job to display the info
 */
void 
displayJob(TMS_Data::Job& j);

/**
 * \brief To display the job info in a list
 * \param j: The progression to display the info
 */
void 
displayJobProgress(ListProgression& j);

/**
 * \brief To display the job info
 * \param j: The progression to display the info
 */
void
displayProgress(Progression& j);

/**
 * \brief To display the list of jobs
 * \param j: The list of job to display 
 */
void 
displayListJobs(ListJobs& j);

/**
 * \brief To display the queues info
 * \param j: The list of queues to display the info
 */
void 
displayQueues(ListQueues& j);


/**
 * \brief To display the info about a queue
 * \param j: The list of queues to display the info
 */
void
displayQueue(Queue& j);

/**
 * \brief To display some basic job info after submit
 * \param job: The job to submit data
 */
void
displaySubmit(TMS_Data::Job job);

/**
 * \brief  function to convert job status into string 
 * \param state: The state of job
 * \return The converted state value
 */
std::string convertJobStateToString(const int& state);

/**
 * \brief  function to convert job priority into string 
 * \param state: The state of job
 * \return The converted state value
 */
std::string convertJobPriorityToString(const int& prio);

/**
 * \brief Helper function to display a list of queues
 * \param os: The output stream in which the list will be printed 
 * \param lsQueues: The list to display
 * \return The output stream in which the list of users has been printed
 */
std::ostream&
operator<<(std::ostream& os, ListQueues& lsQueues);

/**
 * \brief Helper function to display a list of jobs 
 * \param os: The output stream in which the list will be printed 
 * \param ListJobs: The list to display
 * \return The output stream in which the list of users has been printed
 */
std::ostream&
operator<<(std::ostream& os, ListJobs& listJobs);

/**
 * \brief Helper function to display a list of jobs progression
 * \param os: The output stream in which the list will be printed 
 * \param listProgress: The list to display
 * \return The output stream in which the list of users has been printed
 */
std::ostream&
operator<<(std::ostream& os, ListProgression& listProgress);

