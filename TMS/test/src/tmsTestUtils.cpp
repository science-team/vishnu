/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

#include <boost/assign/list_inserter.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/assign/std/vector.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/process/all.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/thread.hpp>
#include "tmsTestUtils.hpp"
using namespace std;
namespace bp = boost::process;

namespace ba = boost::assign;
namespace bfs = boost::filesystem;
namespace bs = boost::system;

VishnuConnexion::VishnuConnexion(const string& uid, const string& upwd, const UMS_Data::ConnectOptions& co):muid(uid),mupwd(upwd),mco(co),open(false){

}

VishnuConnexion::~VishnuConnexion(){
  if(true==open){
    try {
      BOOST_REQUIRE_EQUAL(vishnu::close(msession.getSessionKey()),0);
      BOOST_TEST_MESSAGE("The session is closed");
    } catch (VishnuException& e) {
      BOOST_MESSAGE(e.what());
      BOOST_CHECK(false);
    }
  }
}

string VishnuConnexion::getConnexion(){
  try {
    BOOST_REQUIRE(vishnu::connect(muid,mupwd,msession,mco)==0);
    open=true;
    BOOST_TEST_MESSAGE("The session is open");
    return msession.getSessionKey();
  } catch (VishnuException& e) {
    BOOST_MESSAGE(e.what());
    BOOST_CHECK(false);
  }
  return "";
}


bool operator== (const Job& lJob,const Job& rJob ){

  return ( (lJob.getJobId() == rJob.getJobId())
        && (lJob.getSubmitMachineId() == rJob.getSubmitMachineId())
        && (lJob.getJobPath() == rJob.getJobPath())
        && (lJob.getJobName() == rJob.getJobName())
        && (lJob.getJobPrio() == rJob.getJobPrio())
        && (lJob.getOwner() == rJob.getOwner() )
        && (lJob.getJobQueue() == rJob.getJobQueue() )
        && (lJob.getWallClockLimit() == rJob.getWallClockLimit() )


      );



}

std::string findValue(const std::string& content, const std::string& key) {
 
  size_t pos = content.rfind(key);
  std::string tmp = content.substr(pos+key.size());
  std::istringstream iss(tmp);
  std::string value;
  iss >> value;
 return value;
}

std::string
getFileContent(const std::string& filePath){

  bfs::path file (filePath);

  // Check the existence of file
  if (((false==bfs::exists(file)) || (true==bfs::is_empty(file)))
    || (false==bfs::is_regular_file(file))) {
    throw UserException(ERRCODE_INVALID_PARAM, "can not read the file: " + filePath);
  }

  bfs::ifstream ifs (file);

  // Read the whole file into string

  std::stringstream ss;
  ss << ifs.rdbuf();

  return ss.str();

}





