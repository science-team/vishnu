
package com.sysfera.vishnu.api.ums;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="data" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="session" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                           &lt;attribute name="sessionId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="userId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="dateLastConnect" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="dateCreation" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="dateClosure" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="status" type="{urn:ResourceProxy}StatusType" />
 *                           &lt;attribute name="closePolicy" type="{urn:ResourceProxy}SessionCloseType" />
 *                           &lt;attribute name="timeout" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="authenId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data"
})
@XmlRootElement(name = "listSessionsResponse")
public class ListSessionsResponse {

    protected ListSessionsResponse.Data data;

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link ListSessionsResponse.Data }
     *     
     */
    public ListSessionsResponse.Data getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListSessionsResponse.Data }
     *     
     */
    public void setData(ListSessionsResponse.Data value) {
        this.data = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="session" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *                 &lt;attribute name="sessionId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="userId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="dateLastConnect" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="dateCreation" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="dateClosure" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="status" type="{urn:ResourceProxy}StatusType" />
     *                 &lt;attribute name="closePolicy" type="{urn:ResourceProxy}SessionCloseType" />
     *                 &lt;attribute name="timeout" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="authenId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "session"
    })
    public static class Data {

        @XmlElement(nillable = true)
        protected List<ListSessionsResponse.Data.Session> session;

        /**
         * Gets the value of the session property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the session property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSession().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ListSessionsResponse.Data.Session }
         * 
         * 
         */
        public List<ListSessionsResponse.Data.Session> getSession() {
            if (session == null) {
                session = new ArrayList<ListSessionsResponse.Data.Session>();
            }
            return this.session;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *       &lt;/sequence>
         *       &lt;attribute name="sessionId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="userId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="dateLastConnect" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="dateCreation" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="dateClosure" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="status" type="{urn:ResourceProxy}StatusType" />
         *       &lt;attribute name="closePolicy" type="{urn:ResourceProxy}SessionCloseType" />
         *       &lt;attribute name="timeout" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="authenId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Session {

            @XmlAttribute(name = "sessionId")
            protected String sessionId;
            @XmlAttribute(name = "userId")
            protected String userId;
            @XmlAttribute(name = "sessionKey")
            protected String sessionKey;
            @XmlAttribute(name = "dateLastConnect")
            protected Long dateLastConnect;
            @XmlAttribute(name = "dateCreation")
            protected Long dateCreation;
            @XmlAttribute(name = "dateClosure")
            protected Long dateClosure;
            @XmlAttribute(name = "status")
            protected StatusType status;
            @XmlAttribute(name = "closePolicy")
            protected SessionCloseType closePolicy;
            @XmlAttribute(name = "timeout")
            protected Long timeout;
            @XmlAttribute(name = "authenId")
            protected String authenId;

            /**
             * Gets the value of the sessionId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSessionId() {
                return sessionId;
            }

            /**
             * Sets the value of the sessionId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSessionId(String value) {
                this.sessionId = value;
            }

            /**
             * Gets the value of the userId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUserId() {
                return userId;
            }

            /**
             * Sets the value of the userId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUserId(String value) {
                this.userId = value;
            }

            /**
             * Gets the value of the sessionKey property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSessionKey() {
                return sessionKey;
            }

            /**
             * Sets the value of the sessionKey property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSessionKey(String value) {
                this.sessionKey = value;
            }

            /**
             * Gets the value of the dateLastConnect property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getDateLastConnect() {
                return dateLastConnect;
            }

            /**
             * Sets the value of the dateLastConnect property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setDateLastConnect(Long value) {
                this.dateLastConnect = value;
            }

            /**
             * Gets the value of the dateCreation property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getDateCreation() {
                return dateCreation;
            }

            /**
             * Sets the value of the dateCreation property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setDateCreation(Long value) {
                this.dateCreation = value;
            }

            /**
             * Gets the value of the dateClosure property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getDateClosure() {
                return dateClosure;
            }

            /**
             * Sets the value of the dateClosure property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setDateClosure(Long value) {
                this.dateClosure = value;
            }

            /**
             * Gets the value of the status property.
             * 
             * @return
             *     possible object is
             *     {@link StatusType }
             *     
             */
            public StatusType getStatus() {
                return status;
            }

            /**
             * Sets the value of the status property.
             * 
             * @param value
             *     allowed object is
             *     {@link StatusType }
             *     
             */
            public void setStatus(StatusType value) {
                this.status = value;
            }

            /**
             * Gets the value of the closePolicy property.
             * 
             * @return
             *     possible object is
             *     {@link SessionCloseType }
             *     
             */
            public SessionCloseType getClosePolicy() {
                return closePolicy;
            }

            /**
             * Sets the value of the closePolicy property.
             * 
             * @param value
             *     allowed object is
             *     {@link SessionCloseType }
             *     
             */
            public void setClosePolicy(SessionCloseType value) {
                this.closePolicy = value;
            }

            /**
             * Gets the value of the timeout property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getTimeout() {
                return timeout;
            }

            /**
             * Sets the value of the timeout property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setTimeout(Long value) {
                this.timeout = value;
            }

            /**
             * Gets the value of the authenId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAuthenId() {
                return authenId;
            }

            /**
             * Sets the value of the authenId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAuthenId(String value) {
                this.authenId = value;
            }

        }

    }

}
