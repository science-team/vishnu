/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package com.sysfera.vishnu.api.ums.data;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Session Close Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see UMS_Data.UMS_DataPackage#getSessionCloseType()
 * @model instanceClass="UMS_Data.SessionCloseType"
 *        annotation="Description content='The SessionCloseType is an option for closing session automatically'"
 * @generated
 */
public enum SessionCloseType implements Enumerator {
	/**
	 * The '<em><b>UNDEFINED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNDEFINED_VALUE
	 * @generated
	 * @ordered
	 */
	UNDEFINED(0, "UNDEFINED", "UNDEFINED"),

	/**
	 * The '<em><b>CLOSE ON TIMEOUT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CLOSE_ON_TIMEOUT_VALUE
	 * @generated
	 * @ordered
	 */
	CLOSE_ON_TIMEOUT(1, "CLOSE_ON_TIMEOUT", "CLOSE_ON_TIMEOUT"),

	/**
	 * The '<em><b>CLOSE ON DISCONNECT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CLOSE_ON_DISCONNECT_VALUE
	 * @generated
	 * @ordered
	 */
	CLOSE_ON_DISCONNECT(2, "CLOSE_ON_DISCONNECT", "CLOSE_ON_DISCONNECT");

	/**
	 * The '<em><b>UNDEFINED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UNDEFINED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNDEFINED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UNDEFINED_VALUE = 0;

	/**
	 * The '<em><b>CLOSE ON TIMEOUT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CLOSE ON TIMEOUT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CLOSE_ON_TIMEOUT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CLOSE_ON_TIMEOUT_VALUE = 1;

	/**
	 * The '<em><b>CLOSE ON DISCONNECT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CLOSE ON DISCONNECT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CLOSE_ON_DISCONNECT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CLOSE_ON_DISCONNECT_VALUE = 2;

	/**
	 * An array of all the '<em><b>Session Close Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final SessionCloseType[] VALUES_ARRAY =
		new SessionCloseType[] {
			UNDEFINED,
			CLOSE_ON_TIMEOUT,
			CLOSE_ON_DISCONNECT,
		};

	/**
	 * A public read-only list of all the '<em><b>Session Close Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<SessionCloseType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Session Close Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SessionCloseType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SessionCloseType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Session Close Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SessionCloseType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SessionCloseType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Session Close Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SessionCloseType get(int value) {
		switch (value) {
			case UNDEFINED_VALUE: return UNDEFINED;
			case CLOSE_ON_TIMEOUT_VALUE: return CLOSE_ON_TIMEOUT;
			case CLOSE_ON_DISCONNECT_VALUE: return CLOSE_ON_DISCONNECT;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private SessionCloseType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //SessionCloseType
