
package com.sysfera.vishnu.api.fms;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="data" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="direntry" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                           &lt;attribute name="path" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="owner" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="group" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="perms" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                           &lt;attribute name="size" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="creationTime" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="type" type="{urn:ResourceProxy}FileType" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data"
})
@XmlRootElement(name = "listDirResponse")
public class ListDirResponse {

    protected ListDirResponse.Data data;

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link ListDirResponse.Data }
     *     
     */
    public ListDirResponse.Data getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListDirResponse.Data }
     *     
     */
    public void setData(ListDirResponse.Data value) {
        this.data = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="direntry" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *                 &lt;attribute name="path" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="owner" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="group" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="perms" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *                 &lt;attribute name="size" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="creationTime" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="type" type="{urn:ResourceProxy}FileType" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "direntry"
    })
    public static class Data {

        @XmlElement(nillable = true)
        protected List<ListDirResponse.Data.Direntry> direntry;

        /**
         * Gets the value of the direntry property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the direntry property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDirentry().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ListDirResponse.Data.Direntry }
         * 
         * 
         */
        public List<ListDirResponse.Data.Direntry> getDirentry() {
            if (direntry == null) {
                direntry = new ArrayList<ListDirResponse.Data.Direntry>();
            }
            return this.direntry;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *       &lt;/sequence>
         *       &lt;attribute name="path" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="owner" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="group" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="perms" type="{http://www.w3.org/2001/XMLSchema}integer" />
         *       &lt;attribute name="size" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="creationTime" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="type" type="{urn:ResourceProxy}FileType" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Direntry {

            @XmlAttribute(name = "path")
            protected String path;
            @XmlAttribute(name = "owner")
            protected String owner;
            @XmlAttribute(name = "group")
            protected String group;
            @XmlAttribute(name = "perms")
            protected BigInteger perms;
            @XmlAttribute(name = "size")
            protected Long size;
            @XmlAttribute(name = "creationTime")
            protected String creationTime;
            @XmlAttribute(name = "type")
            protected FileType type;

            /**
             * Gets the value of the path property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPath() {
                return path;
            }

            /**
             * Sets the value of the path property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPath(String value) {
                this.path = value;
            }

            /**
             * Gets the value of the owner property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOwner() {
                return owner;
            }

            /**
             * Sets the value of the owner property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOwner(String value) {
                this.owner = value;
            }

            /**
             * Gets the value of the group property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroup() {
                return group;
            }

            /**
             * Sets the value of the group property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroup(String value) {
                this.group = value;
            }

            /**
             * Gets the value of the perms property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getPerms() {
                return perms;
            }

            /**
             * Sets the value of the perms property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setPerms(BigInteger value) {
                this.perms = value;
            }

            /**
             * Gets the value of the size property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getSize() {
                return size;
            }

            /**
             * Sets the value of the size property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setSize(Long value) {
                this.size = value;
            }

            /**
             * Gets the value of the creationTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCreationTime() {
                return creationTime;
            }

            /**
             * Sets the value of the creationTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCreationTime(String value) {
                this.creationTime = value;
            }

            /**
             * Gets the value of the type property.
             * 
             * @return
             *     possible object is
             *     {@link FileType }
             *     
             */
            public FileType getType() {
                return type;
            }

            /**
             * Sets the value of the type property.
             * 
             * @param value
             *     allowed object is
             *     {@link FileType }
             *     
             */
            public void setType(FileType value) {
                this.type = value;
            }

        }

    }

}
