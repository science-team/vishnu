
package com.sysfera.vishnu.api.tms;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="data" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="progression" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                           &lt;attribute name="jobId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="jobName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="wallTime" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                           &lt;attribute name="startTime" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="endTime" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="percent" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                           &lt;attribute name="status" type="{urn:ResourceProxy}JobStatus" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="nbJobs" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data",
    "nbJobs"
})
@XmlRootElement(name = "getJobProgressResponse")
public class GetJobProgressResponse {

    protected GetJobProgressResponse.Data data;
    @XmlElement(required = true)
    protected BigInteger nbJobs;

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link GetJobProgressResponse.Data }
     *     
     */
    public GetJobProgressResponse.Data getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetJobProgressResponse.Data }
     *     
     */
    public void setData(GetJobProgressResponse.Data value) {
        this.data = value;
    }

    /**
     * Gets the value of the nbJobs property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNbJobs() {
        return nbJobs;
    }

    /**
     * Sets the value of the nbJobs property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNbJobs(BigInteger value) {
        this.nbJobs = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="progression" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *                 &lt;attribute name="jobId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="jobName" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="wallTime" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *                 &lt;attribute name="startTime" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="endTime" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="percent" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *                 &lt;attribute name="status" type="{urn:ResourceProxy}JobStatus" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "progression"
    })
    public static class Data {

        @XmlElement(nillable = true)
        protected List<GetJobProgressResponse.Data.Progression> progression;

        /**
         * Gets the value of the progression property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the progression property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProgression().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GetJobProgressResponse.Data.Progression }
         * 
         * 
         */
        public List<GetJobProgressResponse.Data.Progression> getProgression() {
            if (progression == null) {
                progression = new ArrayList<GetJobProgressResponse.Data.Progression>();
            }
            return this.progression;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *       &lt;/sequence>
         *       &lt;attribute name="jobId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="jobName" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="wallTime" type="{http://www.w3.org/2001/XMLSchema}integer" />
         *       &lt;attribute name="startTime" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="endTime" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="percent" type="{http://www.w3.org/2001/XMLSchema}integer" />
         *       &lt;attribute name="status" type="{urn:ResourceProxy}JobStatus" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Progression {

            @XmlAttribute(name = "jobId")
            protected String jobId;
            @XmlAttribute(name = "jobName")
            protected String jobName;
            @XmlAttribute(name = "wallTime")
            protected BigInteger wallTime;
            @XmlAttribute(name = "startTime")
            protected Long startTime;
            @XmlAttribute(name = "endTime")
            protected Long endTime;
            @XmlAttribute(name = "percent")
            protected BigInteger percent;
            @XmlAttribute(name = "status")
            protected JobStatus status;

            /**
             * Gets the value of the jobId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getJobId() {
                return jobId;
            }

            /**
             * Sets the value of the jobId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setJobId(String value) {
                this.jobId = value;
            }

            /**
             * Gets the value of the jobName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getJobName() {
                return jobName;
            }

            /**
             * Sets the value of the jobName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setJobName(String value) {
                this.jobName = value;
            }

            /**
             * Gets the value of the wallTime property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getWallTime() {
                return wallTime;
            }

            /**
             * Sets the value of the wallTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setWallTime(BigInteger value) {
                this.wallTime = value;
            }

            /**
             * Gets the value of the startTime property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getStartTime() {
                return startTime;
            }

            /**
             * Sets the value of the startTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setStartTime(Long value) {
                this.startTime = value;
            }

            /**
             * Gets the value of the endTime property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getEndTime() {
                return endTime;
            }

            /**
             * Sets the value of the endTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setEndTime(Long value) {
                this.endTime = value;
            }

            /**
             * Gets the value of the percent property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getPercent() {
                return percent;
            }

            /**
             * Sets the value of the percent property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setPercent(BigInteger value) {
                this.percent = value;
            }

            /**
             * Gets the value of the status property.
             * 
             * @return
             *     possible object is
             *     {@link JobStatus }
             *     
             */
            public JobStatus getStatus() {
                return status;
            }

            /**
             * Sets the value of the status property.
             * 
             * @param value
             *     allowed object is
             *     {@link JobStatus }
             *     
             */
            public void setStatus(JobStatus value) {
                this.status = value;
            }

        }

    }

}
