
package com.sysfera.vishnu.api.ums;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="data" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="authaccount" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                           &lt;attribute name="authSystemId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="userId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="acLogin" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data"
})
@XmlRootElement(name = "listAuthAccountsResponse")
public class ListAuthAccountsResponse {

    protected ListAuthAccountsResponse.Data data;

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link ListAuthAccountsResponse.Data }
     *     
     */
    public ListAuthAccountsResponse.Data getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListAuthAccountsResponse.Data }
     *     
     */
    public void setData(ListAuthAccountsResponse.Data value) {
        this.data = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="authaccount" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *                 &lt;attribute name="authSystemId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="userId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="acLogin" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "authaccount"
    })
    public static class Data {

        @XmlElement(nillable = true)
        protected List<ListAuthAccountsResponse.Data.Authaccount> authaccount;

        /**
         * Gets the value of the authaccount property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the authaccount property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAuthaccount().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ListAuthAccountsResponse.Data.Authaccount }
         * 
         * 
         */
        public List<ListAuthAccountsResponse.Data.Authaccount> getAuthaccount() {
            if (authaccount == null) {
                authaccount = new ArrayList<ListAuthAccountsResponse.Data.Authaccount>();
            }
            return this.authaccount;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *       &lt;/sequence>
         *       &lt;attribute name="authSystemId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="userId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="acLogin" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Authaccount {

            @XmlAttribute(name = "authSystemId", required = true)
            protected String authSystemId;
            @XmlAttribute(name = "userId")
            protected String userId;
            @XmlAttribute(name = "acLogin", required = true)
            protected String acLogin;

            /**
             * Gets the value of the authSystemId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAuthSystemId() {
                return authSystemId;
            }

            /**
             * Sets the value of the authSystemId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAuthSystemId(String value) {
                this.authSystemId = value;
            }

            /**
             * Gets the value of the userId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUserId() {
                return userId;
            }

            /**
             * Sets the value of the userId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUserId(String value) {
                this.userId = value;
            }

            /**
             * Gets the value of the acLogin property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcLogin() {
                return acLogin;
            }

            /**
             * Sets the value of the acLogin property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcLogin(String value) {
                this.acLogin = value;
            }

        }

    }

}
