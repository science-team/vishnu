
package com.sysfera.vishnu.api.fms;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sysfera.vishnu.api.fms package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sysfera.vishnu.api.fms
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ContentOfFileResponse }
     * 
     */
    public ContentOfFileResponse createContentOfFileResponse() {
        return new ContentOfFileResponse();
    }

    /**
     * Create an instance of {@link CopyFileRequest }
     * 
     */
    public CopyFileRequest createCopyFileRequest() {
        return new CopyFileRequest();
    }

    /**
     * Create an instance of {@link StopFileTransferResponse }
     * 
     */
    public StopFileTransferResponse createStopFileTransferResponse() {
        return new StopFileTransferResponse();
    }

    /**
     * Create an instance of {@link CreateFileResponse }
     * 
     */
    public CreateFileResponse createCreateFileResponse() {
        return new CreateFileResponse();
    }

    /**
     * Create an instance of {@link MoveAsyncFileRequest }
     * 
     */
    public MoveAsyncFileRequest createMoveAsyncFileRequest() {
        return new MoveAsyncFileRequest();
    }

    /**
     * Create an instance of {@link UNKNOWNUSERIDFault }
     * 
     */
    public UNKNOWNUSERIDFault createUNKNOWNUSERIDFault() {
        return new UNKNOWNUSERIDFault();
    }

    /**
     * Create an instance of {@link SESSIONKEYEXPIREDFault }
     * 
     */
    public SESSIONKEYEXPIREDFault createSESSIONKEYEXPIREDFault() {
        return new SESSIONKEYEXPIREDFault();
    }

    /**
     * Create an instance of {@link UNKNOWNFILETRANSFERFault }
     * 
     */
    public UNKNOWNFILETRANSFERFault createUNKNOWNFILETRANSFERFault() {
        return new UNKNOWNFILETRANSFERFault();
    }

    /**
     * Create an instance of {@link ListDirRequest }
     * 
     */
    public ListDirRequest createListDirRequest() {
        return new ListDirRequest();
    }

    /**
     * Create an instance of {@link RemoveFileRequest }
     * 
     */
    public RemoveFileRequest createRemoveFileRequest() {
        return new RemoveFileRequest();
    }

    /**
     * Create an instance of {@link CopyAsyncFileResponse }
     * 
     */
    public CopyAsyncFileResponse createCopyAsyncFileResponse() {
        return new CopyAsyncFileResponse();
    }

    /**
     * Create an instance of {@link GetFileInfoRequest }
     * 
     */
    public GetFileInfoRequest createGetFileInfoRequest() {
        return new GetFileInfoRequest();
    }

    /**
     * Create an instance of {@link ListFileTransfersResponse.Data.Filetransfer }
     * 
     */
    public ListFileTransfersResponse.Data.Filetransfer createListFileTransfersResponseDataFiletransfer() {
        return new ListFileTransfersResponse.Data.Filetransfer();
    }

    /**
     * Create an instance of {@link UNKNOWNLOCALACCOUNTFault }
     * 
     */
    public UNKNOWNLOCALACCOUNTFault createUNKNOWNLOCALACCOUNTFault() {
        return new UNKNOWNLOCALACCOUNTFault();
    }

    /**
     * Create an instance of {@link CreateDirRequest }
     * 
     */
    public CreateDirRequest createCreateDirRequest() {
        return new CreateDirRequest();
    }

    /**
     * Create an instance of {@link ListDirResponse.Data.Direntry }
     * 
     */
    public ListDirResponse.Data.Direntry createListDirResponseDataDirentry() {
        return new ListDirResponse.Data.Direntry();
    }

    /**
     * Create an instance of {@link ChModRequest }
     * 
     */
    public ChModRequest createChModRequest() {
        return new ChModRequest();
    }

    /**
     * Create an instance of {@link DBERRFault }
     * 
     */
    public DBERRFault createDBERRFault() {
        return new DBERRFault();
    }

    /**
     * Create an instance of {@link SESSIONKEYNOTFOUNDFault }
     * 
     */
    public SESSIONKEYNOTFOUNDFault createSESSIONKEYNOTFOUNDFault() {
        return new SESSIONKEYNOTFOUNDFault();
    }

    /**
     * Create an instance of {@link TailOfFileRequest }
     * 
     */
    public TailOfFileRequest createTailOfFileRequest() {
        return new TailOfFileRequest();
    }

    /**
     * Create an instance of {@link CopyAsyncFileRequest }
     * 
     */
    public CopyAsyncFileRequest createCopyAsyncFileRequest() {
        return new CopyAsyncFileRequest();
    }

    /**
     * Create an instance of {@link RemoveDirRequest }
     * 
     */
    public RemoveDirRequest createRemoveDirRequest() {
        return new RemoveDirRequest();
    }

    /**
     * Create an instance of {@link GetFileInfoResponse }
     * 
     */
    public GetFileInfoResponse createGetFileInfoResponse() {
        return new GetFileInfoResponse();
    }

    /**
     * Create an instance of {@link MoveAsyncFileResponse }
     * 
     */
    public MoveAsyncFileResponse createMoveAsyncFileResponse() {
        return new MoveAsyncFileResponse();
    }

    /**
     * Create an instance of {@link HeadOfFileResponse }
     * 
     */
    public HeadOfFileResponse createHeadOfFileResponse() {
        return new HeadOfFileResponse();
    }

    /**
     * Create an instance of {@link INVALIDPARAMFault }
     * 
     */
    public INVALIDPARAMFault createINVALIDPARAMFault() {
        return new INVALIDPARAMFault();
    }

    /**
     * Create an instance of {@link UNKNOWNMACHINEFault }
     * 
     */
    public UNKNOWNMACHINEFault createUNKNOWNMACHINEFault() {
        return new UNKNOWNMACHINEFault();
    }

    /**
     * Create an instance of {@link INVALIDPATHFault }
     * 
     */
    public INVALIDPATHFault createINVALIDPATHFault() {
        return new INVALIDPATHFault();
    }

    /**
     * Create an instance of {@link ListDirResponse.Data }
     * 
     */
    public ListDirResponse.Data createListDirResponseData() {
        return new ListDirResponse.Data();
    }

    /**
     * Create an instance of {@link CONFIGNOTFOUNDFault }
     * 
     */
    public CONFIGNOTFOUNDFault createCONFIGNOTFOUNDFault() {
        return new CONFIGNOTFOUNDFault();
    }

    /**
     * Create an instance of {@link ChGrpRequest }
     * 
     */
    public ChGrpRequest createChGrpRequest() {
        return new ChGrpRequest();
    }

    /**
     * Create an instance of {@link NOADMINFault }
     * 
     */
    public NOADMINFault createNOADMINFault() {
        return new NOADMINFault();
    }

    /**
     * Create an instance of {@link MoveFileResponse }
     * 
     */
    public MoveFileResponse createMoveFileResponse() {
        return new MoveFileResponse();
    }

    /**
     * Create an instance of {@link ListFileTransfersRequest }
     * 
     */
    public ListFileTransfersRequest createListFileTransfersRequest() {
        return new ListFileTransfersRequest();
    }

    /**
     * Create an instance of {@link ListDirResponse }
     * 
     */
    public ListDirResponse createListDirResponse() {
        return new ListDirResponse();
    }

    /**
     * Create an instance of {@link RUNTIMEERRORFault }
     * 
     */
    public RUNTIMEERRORFault createRUNTIMEERRORFault() {
        return new RUNTIMEERRORFault();
    }

    /**
     * Create an instance of {@link RemoveFileResponse }
     * 
     */
    public RemoveFileResponse createRemoveFileResponse() {
        return new RemoveFileResponse();
    }

    /**
     * Create an instance of {@link TailOfFileResponse }
     * 
     */
    public TailOfFileResponse createTailOfFileResponse() {
        return new TailOfFileResponse();
    }

    /**
     * Create an instance of {@link MoveFileRequest }
     * 
     */
    public MoveFileRequest createMoveFileRequest() {
        return new MoveFileRequest();
    }

    /**
     * Create an instance of {@link ListFileTransfersResponse.Data }
     * 
     */
    public ListFileTransfersResponse.Data createListFileTransfersResponseData() {
        return new ListFileTransfersResponse.Data();
    }

    /**
     * Create an instance of {@link ChModResponse }
     * 
     */
    public ChModResponse createChModResponse() {
        return new ChModResponse();
    }

    /**
     * Create an instance of {@link RemoveDirResponse }
     * 
     */
    public RemoveDirResponse createRemoveDirResponse() {
        return new RemoveDirResponse();
    }

    /**
     * Create an instance of {@link ChGrpResponse }
     * 
     */
    public ChGrpResponse createChGrpResponse() {
        return new ChGrpResponse();
    }

    /**
     * Create an instance of {@link CopyFileResponse }
     * 
     */
    public CopyFileResponse createCopyFileResponse() {
        return new CopyFileResponse();
    }

    /**
     * Create an instance of {@link ContentOfFileRequest }
     * 
     */
    public ContentOfFileRequest createContentOfFileRequest() {
        return new ContentOfFileRequest();
    }

    /**
     * Create an instance of {@link DIETFault }
     * 
     */
    public DIETFault createDIETFault() {
        return new DIETFault();
    }

    /**
     * Create an instance of {@link StopFileTransferRequest }
     * 
     */
    public StopFileTransferRequest createStopFileTransferRequest() {
        return new StopFileTransferRequest();
    }

    /**
     * Create an instance of {@link CreateFileRequest }
     * 
     */
    public CreateFileRequest createCreateFileRequest() {
        return new CreateFileRequest();
    }

    /**
     * Create an instance of {@link ListFileTransfersResponse }
     * 
     */
    public ListFileTransfersResponse createListFileTransfersResponse() {
        return new ListFileTransfersResponse();
    }

    /**
     * Create an instance of {@link HeadOfFileRequest }
     * 
     */
    public HeadOfFileRequest createHeadOfFileRequest() {
        return new HeadOfFileRequest();
    }

    /**
     * Create an instance of {@link CreateDirResponse }
     * 
     */
    public CreateDirResponse createCreateDirResponse() {
        return new CreateDirResponse();
    }

    /**
     * Create an instance of {@link UNDEFINEDFault }
     * 
     */
    public UNDEFINEDFault createUNDEFINEDFault() {
        return new UNDEFINEDFault();
    }

}
