
package com.sysfera.vishnu.api.ums;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="data" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="command" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                           &lt;attribute name="commandId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="sessionId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="machineId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="cmdDescription" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="cmdStartTime" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="cmdEndTime" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="status" type="{urn:ResourceProxy}CommandStatusType" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data"
})
@XmlRootElement(name = "listHistoryCmdResponse")
public class ListHistoryCmdResponse {

    protected ListHistoryCmdResponse.Data data;

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link ListHistoryCmdResponse.Data }
     *     
     */
    public ListHistoryCmdResponse.Data getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListHistoryCmdResponse.Data }
     *     
     */
    public void setData(ListHistoryCmdResponse.Data value) {
        this.data = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="command" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *                 &lt;attribute name="commandId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="sessionId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="machineId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="cmdDescription" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="cmdStartTime" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="cmdEndTime" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="status" type="{urn:ResourceProxy}CommandStatusType" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "command"
    })
    public static class Data {

        @XmlElement(nillable = true)
        protected List<ListHistoryCmdResponse.Data.Command> command;

        /**
         * Gets the value of the command property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the command property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCommand().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ListHistoryCmdResponse.Data.Command }
         * 
         * 
         */
        public List<ListHistoryCmdResponse.Data.Command> getCommand() {
            if (command == null) {
                command = new ArrayList<ListHistoryCmdResponse.Data.Command>();
            }
            return this.command;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *       &lt;/sequence>
         *       &lt;attribute name="commandId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="sessionId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="machineId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="cmdDescription" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="cmdStartTime" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="cmdEndTime" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="status" type="{urn:ResourceProxy}CommandStatusType" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Command {

            @XmlAttribute(name = "commandId")
            protected String commandId;
            @XmlAttribute(name = "sessionId")
            protected String sessionId;
            @XmlAttribute(name = "machineId")
            protected String machineId;
            @XmlAttribute(name = "cmdDescription")
            protected String cmdDescription;
            @XmlAttribute(name = "cmdStartTime")
            protected Long cmdStartTime;
            @XmlAttribute(name = "cmdEndTime")
            protected Long cmdEndTime;
            @XmlAttribute(name = "status")
            protected CommandStatusType status;

            /**
             * Gets the value of the commandId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCommandId() {
                return commandId;
            }

            /**
             * Sets the value of the commandId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCommandId(String value) {
                this.commandId = value;
            }

            /**
             * Gets the value of the sessionId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSessionId() {
                return sessionId;
            }

            /**
             * Sets the value of the sessionId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSessionId(String value) {
                this.sessionId = value;
            }

            /**
             * Gets the value of the machineId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMachineId() {
                return machineId;
            }

            /**
             * Sets the value of the machineId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMachineId(String value) {
                this.machineId = value;
            }

            /**
             * Gets the value of the cmdDescription property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCmdDescription() {
                return cmdDescription;
            }

            /**
             * Sets the value of the cmdDescription property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCmdDescription(String value) {
                this.cmdDescription = value;
            }

            /**
             * Gets the value of the cmdStartTime property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getCmdStartTime() {
                return cmdStartTime;
            }

            /**
             * Sets the value of the cmdStartTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setCmdStartTime(Long value) {
                this.cmdStartTime = value;
            }

            /**
             * Gets the value of the cmdEndTime property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getCmdEndTime() {
                return cmdEndTime;
            }

            /**
             * Sets the value of the cmdEndTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setCmdEndTime(Long value) {
                this.cmdEndTime = value;
            }

            /**
             * Gets the value of the status property.
             * 
             * @return
             *     possible object is
             *     {@link CommandStatusType }
             *     
             */
            public CommandStatusType getStatus() {
                return status;
            }

            /**
             * Sets the value of the status property.
             * 
             * @param value
             *     allowed object is
             *     {@link CommandStatusType }
             *     
             */
            public void setStatus(CommandStatusType value) {
                this.status = value;
            }

        }

    }

}
