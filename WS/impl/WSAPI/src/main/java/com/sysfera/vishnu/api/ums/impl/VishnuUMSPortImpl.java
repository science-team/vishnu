package com.sysfera.vishnu.api.ums.impl;

import java.math.BigInteger;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.transaction.SystemException;

import com.sysfera.vishnu.api.ums.AddLocalAccountRequest;
import com.sysfera.vishnu.api.ums.AddLocalAccountResponse;
import com.sysfera.vishnu.api.ums.COMMANDRUNNINGFault;
import com.sysfera.vishnu.api.ums.COMMANDRUNNINGMessage;
import com.sysfera.vishnu.api.ums.ChangePasswordRequest;
import com.sysfera.vishnu.api.ums.ChangePasswordResponse;
import com.sysfera.vishnu.api.ums.CloseRequest;
import com.sysfera.vishnu.api.ums.CloseResponse;
import com.sysfera.vishnu.api.ums.ConfigureOptionRequest;
import com.sysfera.vishnu.api.ums.ConfigureOptionResponse;
import com.sysfera.vishnu.api.ums.ConnectRequest;
import com.sysfera.vishnu.api.ums.ConnectResponse;
import com.sysfera.vishnu.api.ums.DBCONNFault;
import com.sysfera.vishnu.api.ums.DBCONNMessage;
import com.sysfera.vishnu.api.ums.DBERRFault;
import com.sysfera.vishnu.api.ums.DBERRMessage;
import com.sysfera.vishnu.api.ums.DIETFault;
import com.sysfera.vishnu.api.ums.DIETMessage;
import com.sysfera.vishnu.api.ums.DeleteLocalAccountRequest;
import com.sysfera.vishnu.api.ums.DeleteLocalAccountResponse;
import com.sysfera.vishnu.api.ums.INCORRECTTIMEOUTFault;
import com.sysfera.vishnu.api.ums.INCORRECTTIMEOUTMessage;
import com.sysfera.vishnu.api.ums.LOCALACCOUNTEXISTFault;
import com.sysfera.vishnu.api.ums.LOCALACCOUNTEXISTMessage;
import com.sysfera.vishnu.api.ums.ListHistoryCmdRequest;
import com.sysfera.vishnu.api.ums.ListHistoryCmdResponse;
import com.sysfera.vishnu.api.ums.ListLocalAccountsRequest;
import com.sysfera.vishnu.api.ums.ListLocalAccountsResponse;
import com.sysfera.vishnu.api.ums.ListMachinesRequest;
import com.sysfera.vishnu.api.ums.ListMachinesResponse;
import com.sysfera.vishnu.api.ums.ListOptionsRequest;
import com.sysfera.vishnu.api.ums.ListOptionsResponse;
import com.sysfera.vishnu.api.ums.ListSessionsRequest;
import com.sysfera.vishnu.api.ums.ListSessionsResponse;
import com.sysfera.vishnu.api.LoadLibrary;
import com.sysfera.vishnu.api.ums.AUTHACCOUNTEXISTMessage;
import com.sysfera.vishnu.api.ums.AddAuthAccountRequest;
import com.sysfera.vishnu.api.ums.AddAuthAccountResponse;
import com.sysfera.vishnu.api.ums.DeleteAuthAccountRequest;
import com.sysfera.vishnu.api.ums.DeleteAuthAccountResponse;
import com.sysfera.vishnu.api.ums.LOGINALREADYUSEDMessage;
import com.sysfera.vishnu.api.ums.ListAuthAccountsRequest;
import com.sysfera.vishnu.api.ums.ListAuthAccountsResponse;
import com.sysfera.vishnu.api.ums.ListAuthSystemsRequest;
import com.sysfera.vishnu.api.ums.ListAuthSystemsResponse;
import com.sysfera.vishnu.api.ums.MACHINELOCKEDFault;
import com.sysfera.vishnu.api.ums.MACHINELOCKEDMessage;
import com.sysfera.vishnu.api.ums.NOADMINFault;
import com.sysfera.vishnu.api.ums.NOADMINMessage;
import com.sysfera.vishnu.api.ums.ReconnectRequest;
import com.sysfera.vishnu.api.ums.ReconnectResponse;
import com.sysfera.vishnu.api.ums.SESSIONKEYEXPIREDFault;
import com.sysfera.vishnu.api.ums.SESSIONKEYEXPIREDMessage;
import com.sysfera.vishnu.api.ums.SESSIONKEYNOTFOUNDFault;
import com.sysfera.vishnu.api.ums.SESSIONKEYNOTFOUNDMessage;
import com.sysfera.vishnu.api.ums.SYSTEMFault;
import com.sysfera.vishnu.api.ums.SYSTEMMessage;
import com.sysfera.vishnu.api.ums.SessionCloseType;
import com.sysfera.vishnu.api.ums.StatusType;
import com.sysfera.vishnu.api.ums.AuthType;
import com.sysfera.vishnu.api.ums.EncryptionMethod;
import com.sysfera.vishnu.api.ums.UNDEFINEDFault;
import com.sysfera.vishnu.api.ums.UNDEFINEDMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNAUTHACCOUNTMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNAUTHSYSTEMMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNAUTHSYSTEMTYPEMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNLOCALACCOUNTFault;
import com.sysfera.vishnu.api.ums.UNKNOWNLOCALACCOUNTMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNMACHINEFault;
import com.sysfera.vishnu.api.ums.UNKNOWNMACHINEMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNOPTIONFault;
import com.sysfera.vishnu.api.ums.UNKNOWNOPTIONMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNSESSIONIDFault;
import com.sysfera.vishnu.api.ums.UNKNOWNSESSIONIDMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNUSERFault;
import com.sysfera.vishnu.api.ums.UNKNOWNUSERIDFault;
import com.sysfera.vishnu.api.ums.UNKNOWNUSERIDMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNUSERMessage;
import com.sysfera.vishnu.api.ums.UNUSABLEMACHINEFault;
import com.sysfera.vishnu.api.ums.UNUSABLEMACHINEMessage;
import com.sysfera.vishnu.api.ums.USERLOCKEDFault;
import com.sysfera.vishnu.api.ums.USERLOCKEDMessage;
import com.sysfera.vishnu.api.ums.UpdateAuthAccountRequest;
import com.sysfera.vishnu.api.ums.UpdateAuthAccountResponse;
import com.sysfera.vishnu.api.ums.UpdateLocalAccountRequest;
import com.sysfera.vishnu.api.ums.UpdateLocalAccountResponse;
import com.sysfera.vishnu.api.ums.VishnuUMSPortType;
import com.sysfera.vishnu.api.vishnu.internal.AuthAccount;
import com.sysfera.vishnu.api.vishnu.internal.ListAuthAccOptions;
import com.sysfera.vishnu.api.vishnu.internal.ListAuthAccounts;
import com.sysfera.vishnu.api.vishnu.internal.ListAuthSysOptions;
import com.sysfera.vishnu.api.vishnu.internal.ListAuthSystems;
import com.sysfera.vishnu.api.vishnu.internal.ListCmdOptions;
import com.sysfera.vishnu.api.vishnu.internal.ListCommands;
import com.sysfera.vishnu.api.vishnu.internal.ListLocalAccOptions;
import com.sysfera.vishnu.api.vishnu.internal.ListLocalAccounts;
import com.sysfera.vishnu.api.vishnu.internal.ListMachineOptions;
import com.sysfera.vishnu.api.vishnu.internal.ListMachines;
import com.sysfera.vishnu.api.vishnu.internal.ListOptOptions;
import com.sysfera.vishnu.api.vishnu.internal.ListOptionsValues;
import com.sysfera.vishnu.api.vishnu.internal.ListSessionOptions;
import com.sysfera.vishnu.api.vishnu.internal.ListSessions;
import com.sysfera.vishnu.api.vishnu.internal.LocalAccount;
import com.sysfera.vishnu.api.vishnu.internal.OptionValue;
import com.sysfera.vishnu.api.vishnu.internal.InternalUMSException;
import com.sysfera.vishnu.api.vishnu.internal.ConnectOptions;
import com.sysfera.vishnu.api.vishnu.internal.Session;
import com.sysfera.vishnu.api.vishnu.internal.VISHNU;
import com.sysfera.vishnu.api.vishnu.internal.InternalUMSException;

@WebService(targetNamespace = "urn:ResourceProxy", name = "VishnuUMS", serviceName = "VishnuUMSService")
@Remote(VishnuUMSPortType.class)
@Stateless
public class VishnuUMSPortImpl implements VishnuUMSPortType {

    // DIET value for already diet_init done
	static final int GRPC_ALREADY_INITIALIZED = 15;

    // Load the vishnu library
	static {
		LoadLibrary.LoadVishnuLibrary();
	}

    // To init the UMS module
	private void initUMS() {
		int res = 0;
		String VISHNU_CONFIG_FILE = System.getenv("VISHNU_CONFIG_FILE");
		try {
			res = VISHNU.vishnuInitialize(VISHNU_CONFIG_FILE);
		} catch (InternalUMSException e) {

		}
		if ((res != 0) && (res != GRPC_ALREADY_INITIALIZED)) {
			throw new RuntimeException("Cannot initialize VISHNU Java Library");
		}
	}

	public ConnectResponse connect(ConnectRequest parameters)
			throws UNKNOWNUSERMessage, NOADMINMessage, USERLOCKEDMessage,
			INCORRECTTIMEOUTMessage, DBCONNMessage, UNKNOWNUSERIDMessage,
			DIETMessage, UNDEFINEDMessage, SYSTEMMessage, DBERRMessage {
		initUMS();
		Session session = new Session();
		ConnectOptions options = new ConnectOptions();
		if (parameters.getUserId() == null) {
			parameters.setUserId("");
		}
		if (parameters.getPassword() == null) {
			parameters.setPassword("");
		}
		if (parameters.getClosePolicy() != null) {
			options.setClosePolicy(com.sysfera.vishnu.api.ums.data.SessionCloseType
					.getByName(parameters.getClosePolicy().value()).getValue());
		}
		if (parameters.getSessionInactivityDelay() != null) {
			options.setSessionInactivityDelay(parameters
				.getSessionInactivityDelay().intValue());
		}
		if (parameters.getSubstituteUserId() != null) {
			options.setSubstituteUserId(parameters.getSubstituteUserId());
		}
		
		if ((parameters.getUserId().isEmpty()) && 
				(parameters.getPassword().isEmpty())) {
			throw new UNKNOWNUSERMessage("It is not possible to connect without password and userId.", new UNKNOWNUSERFault());
		}
		
		try {
			VISHNU.connect(parameters.getUserId(),
					parameters.getPassword(), session, options);
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 20 : 
				throw new UNKNOWNUSERMessage(msg, new UNKNOWNUSERFault());
			case 21 :
				throw new UNKNOWNUSERIDMessage(msg, new UNKNOWNUSERIDFault());
			case 24 :
				throw new USERLOCKEDMessage(msg, new USERLOCKEDFault());
			case 25 :
				throw new NOADMINMessage(msg, new NOADMINFault());
			case 43 :
				throw new INCORRECTTIMEOUTMessage(msg, new INCORRECTTIMEOUTFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		ConnectResponse resp = new ConnectResponse();
		resp.setUserId(session.getUserId());
		resp.setClosePolicy(SessionCloseType
				.fromValue(com.sysfera.vishnu.api.ums.data.SessionCloseType
						.get(session.getClosePolicy()).getLiteral()));
		resp.setSessionId(session.getSessionId());
		resp.setSessionKey(session.getSessionKey());
		resp.setStatus(StatusType
				.fromValue(com.sysfera.vishnu.api.ums.data.StatusType.get(
						session.getStatus()).getLiteral()));
		resp.setTimeout(session.getTimeout());
		resp.setDateCreation(session.getDateCreation());
		return resp;
	}

	public UpdateLocalAccountResponse updateLocalAccount(
			UpdateLocalAccountRequest parameters) throws UNKNOWNMACHINEMessage,
			SESSIONKEYNOTFOUNDMessage, DBCONNMessage,
			UNKNOWNLOCALACCOUNTMessage, UNKNOWNUSERIDMessage, DIETMessage,
			UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage,
			DBERRMessage {
		UpdateLocalAccountResponse res = new UpdateLocalAccountResponse();
		LocalAccount acc = new LocalAccount();
		if (parameters.getSessionKey() == null) {
			parameters.setSessionKey("");
		}
		if (parameters.getAcLogin() != null){
			acc.setAcLogin(parameters.getAcLogin());
		}
		if (parameters.getHomeDirectory() != null){
			acc.setHomeDirectory((parameters.getHomeDirectory()));
		}
		
		if (parameters.getMachineId() != null){
			acc.setMachineId(parameters.getMachineId());
		}
		if (parameters.getSshKeyPath() != null){
			acc.setSshKeyPath(parameters.getSshKeyPath());
		}
		if (parameters.getUserId() != null){
			acc.setUserId(parameters.getUserId());
		}
		try {
			VISHNU.updateLocalAccount(parameters.getSessionKey(), acc);
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 21 :
				throw new UNKNOWNUSERIDMessage(msg, new UNKNOWNUSERIDFault());
			case 28 :
				throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
				throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38 :
				throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

	public ReconnectResponse reconnect(ReconnectRequest parameters)
			throws UNKNOWNUSERMessage, UNKNOWNSESSIONIDMessage,
			SESSIONKEYNOTFOUNDMessage, USERLOCKEDMessage, DBCONNMessage,
			DIETMessage, UNUSABLEMACHINEMessage, UNDEFINEDMessage,
			SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		initUMS();
		Session s = new Session();
		ReconnectResponse res = new ReconnectResponse();
		if (parameters.getUserId() == null) {
			parameters.setUserId("");
		}
		if (parameters.getPassword() == null) {
			parameters.setPassword("");
		}
		if (parameters.getSessionId() == null) {
			parameters.setSessionId("");
		}
		try {
			VISHNU.reconnect(parameters.getUserId(),
					parameters.getPassword(), parameters.getSessionId(), s);
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 20 : 
				throw new UNKNOWNUSERMessage(msg, new UNKNOWNUSERFault());
			case 24 :
				throw new USERLOCKEDMessage(msg, new USERLOCKEDFault());
			case 28 :
				throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
				throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 30 :
				throw new UNKNOWNSESSIONIDMessage(msg, new UNKNOWNSESSIONIDFault());
			case 36 :
				throw new UNUSABLEMACHINEMessage(msg, new UNUSABLEMACHINEFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		res.setClosePolicy(SessionCloseType
				.fromValue(com.sysfera.vishnu.api.ums.data.SessionCloseType
						.get(s.getClosePolicy()).getLiteral()));
		res.setDateClosure(s.getDateClosure());
		res.setDateCreation(s.getDateCreation());
		res.setDateLastConnect(s.getDateLastConnect());
		res.setSessionId(s.getSessionId());
		res.setSessionKey(s.getSessionKey());
		res.setStatus(StatusType
				.fromValue(com.sysfera.vishnu.api.ums.data.StatusType.get(
						s.getStatus()).getLiteral()));
		res.setTimeout(s.getTimeout());
		res.setUserId(s.getUserId());
		return res;
	}

	public ListMachinesResponse listMachines(ListMachinesRequest parameters)
			throws SESSIONKEYNOTFOUNDMessage, DBCONNMessage,
			UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage,
			SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		int i;
		ListMachinesResponse res = new ListMachinesResponse();
		ListMachinesResponse.Data dat = new ListMachinesResponse.Data();
		ListMachines li = new ListMachines();
		ListMachineOptions op = new ListMachineOptions();
		if (parameters.getSessionKey() == null) {
			parameters.setSessionKey("");
		}
		if (parameters.isListAllMachine() != null){
			op.setListAllMachine(parameters.isListAllMachine());
		}
		if (parameters.getMachineId() != null){
			op.setMachineId(parameters.getMachineId());
		}
		if (parameters.getUserId() != null){
			op.setUserId(parameters.getUserId());
		}
		try {
			VISHNU.listMachines(parameters.getSessionKey(), li, op);
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 21 :
				throw new UNKNOWNUSERIDMessage(msg, new UNKNOWNUSERIDFault());
			case 28 :
				throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
				throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		for (i = 0; i < li.getMachines().size(); i++) {
			ListMachinesResponse.Data.Machine opva = new ListMachinesResponse.Data.Machine();
			opva.setLanguage(li.getMachines().get(i).getLanguage());
			opva.setMachineDescription(li.getMachines().get(i)
					.getMachineDescription());
			opva.setMachineId(li.getMachines().get(i).getMachineId());
			opva.setName(li.getMachines().get(i).getName());
			opva.setSite(li.getMachines().get(i).getSite());
			opva.setSshPublicKey(li.getMachines().get(i).getSshPublicKey());
			opva.setStatus(StatusType
					.fromValue(com.sysfera.vishnu.api.ums.data.StatusType.get(
							li.getMachines().get(i).getStatus()).getLiteral()));
			dat.getMachine().add(opva);
		}
		res.setData(dat);
		return res;
	}

	public ListHistoryCmdResponse listHistoryCmd(
			ListHistoryCmdRequest parameters) throws SESSIONKEYNOTFOUNDMessage,
			DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage,
			SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		ListHistoryCmdResponse res = new ListHistoryCmdResponse();
		ListHistoryCmdResponse.Data dat = new ListHistoryCmdResponse.Data();
		ListCommands li = new ListCommands();
		ListCmdOptions op = new ListCmdOptions();
		int i;
		if (parameters.getSessionKey() == null) {
			parameters.setSessionKey("");
		}
		if (parameters.isAdminListOption() != null){
			op.setAdminListOption(parameters.isAdminListOption());
		}
		if (parameters.getEndDateOption() != null){
			op.setEndDateOption(parameters.getEndDateOption().intValue());
		}
		if (parameters.getSessionId() != null){
			op.setSessionId(parameters.getSessionId());
		}
		if (parameters.getStartDateOption() != null){
			op.setStartDateOption(parameters.getStartDateOption().intValue());
		}
		if (parameters.getUserId() != null){
			op.setUserId(parameters.getUserId());
		}
		try {
			VISHNU.listHistoryCmd(parameters.getSessionKey(), li, op);
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 21 :
				throw new UNKNOWNUSERIDMessage(msg, new UNKNOWNUSERIDFault());
			case 28 :
				throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
				throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		for (i = 0; i < li.getCommands().size(); i++) {
			ListHistoryCmdResponse.Data.Command opva = new ListHistoryCmdResponse.Data.Command();
			opva.setCmdDescription(li.getCommands().get(i).getCmdDescription());
			opva.setCmdEndTime(li.getCommands().get(i).getCmdEndTime());
			opva.setCmdStartTime(li.getCommands().get(i).getCmdStartTime());
			opva.setCommandId(li.getCommands().get(i).getCommandId());
			opva.setMachineId(li.getCommands().get(i).getMachineId());
			opva.setSessionId(li.getCommands().get(i).getSessionId());
			dat.getCommand().add(opva);
		}
		res.setData(dat);
		return res;
	}

	public ListOptionsResponse listOptions(ListOptionsRequest parameters)
			throws SESSIONKEYNOTFOUNDMessage, NOADMINMessage, DBCONNMessage,
			UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage,
			SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage,
			UNKNOWNOPTIONMessage {
		int i;
		ListOptionsResponse.Data dat = new ListOptionsResponse.Data();
		ListOptionsResponse res = new ListOptionsResponse();
		ListOptionsValues li = new ListOptionsValues();
		ListOptOptions op = new ListOptOptions();
		if (parameters.getSessionKey() == null) {
			parameters.setSessionKey("");
		}
		if (parameters.isListAllDeftValue() != null){
			op.setListAllDeftValue(parameters.isListAllDeftValue());
		}
		if (parameters.getOptionName() != null){
			op.setOptionName(parameters.getOptionName());
		}
		if (parameters.getUserId() != null){
			op.setUserId(parameters.getUserId());
		}
		try {
			VISHNU.listOptions(parameters.getSessionKey(), li, op);
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 21 :
				throw new UNKNOWNUSERIDMessage(msg, new UNKNOWNUSERIDFault());
			case 25 :
				throw new NOADMINMessage(msg, new NOADMINFault());
			case 28 :
				throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
				throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 41:
				throw new UNKNOWNOPTIONMessage(msg, new UNKNOWNOPTIONFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		for (i = 0; i < li.getOptionValues().size(); i++) {
			ListOptionsResponse.Data.Optionvalue opva = new ListOptionsResponse.Data.Optionvalue();
			opva.setOptionName(li.getOptionValues().get(i).getOptionName());
			opva.setValue(li.getOptionValues().get(i).getValue());
			dat.getOptionvalue().add(opva);
		}
		res.setData(dat);
		return res;
	}

	public ConfigureOptionResponse configureOption(
			ConfigureOptionRequest parameters)
			throws SESSIONKEYNOTFOUNDMessage, DBCONNMessage, DIETMessage,
			UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage,
			DBERRMessage, UNKNOWNOPTIONMessage {
		ConfigureOptionResponse res = new ConfigureOptionResponse();
		OptionValue op = new OptionValue();
		if (parameters.getSessionKey() == null) {
			parameters.setSessionKey("");
		}
		if (parameters.getOptionName() != null){
			op.setOptionName(parameters.getOptionName());
		}
		if (parameters.getValue()!= null){
			op.setValue(parameters.getValue());
		}
		try {
			VISHNU.configureOption(parameters.getSessionKey(), op);
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 28 :
				throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
				throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 41:
				throw new UNKNOWNOPTIONMessage(msg, new UNKNOWNOPTIONFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

	public ListLocalAccountsResponse listLocalAccounts(
			ListLocalAccountsRequest parameters)
			throws SESSIONKEYNOTFOUNDMessage, DBCONNMessage,
			UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage,
			SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		int i;
		ListLocalAccountsResponse res = new ListLocalAccountsResponse();
		ListLocalAccounts li = new ListLocalAccounts();
		ListLocalAccountsResponse.Data dat = new ListLocalAccountsResponse.Data();
		ListLocalAccOptions op = new ListLocalAccOptions();
		if (parameters.getSessionKey() == null) {
			parameters.setSessionKey("");
		}
		if (parameters.isAdminListOption() != null){
			op.setAdminListOption(parameters.isAdminListOption());
		}
		if (parameters.getMachineId()!=null){
			op.setMachineId(parameters.getMachineId());
		}
		if (parameters.getUserId() != null){
			op.setUserId(parameters.getUserId());
		}
		try {
			VISHNU.listLocalAccounts(parameters.getSessionKey(), li, op);
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 21 :
				throw new UNKNOWNUSERIDMessage(msg, new UNKNOWNUSERIDFault());
			case 28 :
				throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
				throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		for (i = 0; i < li.getAccounts().size(); i++) {
			ListLocalAccountsResponse.Data.Localaccount opva = new ListLocalAccountsResponse.Data.Localaccount();
			opva.setAcLogin(li.getAccounts().get(i).getAcLogin());
			opva.setHomeDirectory(li.getAccounts().get(i).getHomeDirectory());
			opva.setMachineId(li.getAccounts().get(i).getMachineId());
			opva.setSshKeyPath(li.getAccounts().get(i).getSshKeyPath());
			opva.setUserId(li.getAccounts().get(i).getUserId());
			dat.getLocalaccount().add(opva);
		}
		res.setData(dat);
		return res;
	}

	public DeleteLocalAccountResponse deleteLocalAccount(
			DeleteLocalAccountRequest parameters)
			throws SESSIONKEYNOTFOUNDMessage, DBCONNMessage,
			UNKNOWNLOCALACCOUNTMessage, DIETMessage, UNDEFINEDMessage,
			SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		DeleteLocalAccountResponse res = new DeleteLocalAccountResponse();
		if (parameters.getSessionKey() == null) {
			parameters.setSessionKey("");
		}
		if (parameters.getUserId() == null) {
			parameters.setUserId("");
		}
		if (parameters.getMachineId() == null) {
			parameters.setMachineId("");
		}
		try {
			VISHNU.deleteLocalAccount(parameters.getSessionKey(),
					parameters.getUserId(), parameters.getMachineId());
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 28 :
				throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
				throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 38 :
				throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

	public CloseResponse close(CloseRequest parameters)
			throws SESSIONKEYNOTFOUNDMessage, DBCONNMessage, DIETMessage,
			UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, COMMANDRUNNINGMessage,
			SYSTEMMessage, DBERRMessage {
		CloseResponse res = new CloseResponse();
		if (parameters.getSessionKey() == null) {
			parameters.setSessionKey("");
		}
		try {
			VISHNU.close(parameters.getSessionKey());
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 28 :
				throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
				throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 31 :
				throw new COMMANDRUNNINGMessage(msg, new COMMANDRUNNINGFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

	public ListSessionsResponse listSessions(ListSessionsRequest parameters)
			throws SESSIONKEYNOTFOUNDMessage, DBCONNMessage,
			UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage,
			SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		ListSessionsResponse res = new ListSessionsResponse();

		int i;
		ListSessions li = new ListSessions();
		ListSessionsResponse.Data dat = new ListSessionsResponse.Data();
		ListSessionOptions op = new ListSessionOptions();
		if (parameters.getSessionKey() == null) {
			parameters.setSessionKey("");
		}
		if (parameters.isAdminListOption() != null){
			op.setAdminListOption(parameters.isAdminListOption());
		}
		if (parameters.getEndDateOption() != null){
			op.setEndDateOption(parameters.getEndDateOption().intValue());
		}
		if (parameters.getMachineId() != null){
			op.setMachineId(parameters.getMachineId());
		}
		if (parameters.getSessionClosePolicy() != null){
			op.setSessionClosePolicy(com.sysfera.vishnu.api.ums.data.SessionCloseType
					.getByName(parameters.getSessionClosePolicy().value())
					.getValue());
		}
		if (parameters.getSessionId() != null){
			op.setSessionId(parameters.getSessionId());
		}
		if (parameters.getSessionInactivityDelay() != null){
			op.setSessionInactivityDelay(parameters.getSessionInactivityDelay()
					.intValue());
		}
		if (parameters.getStartDateOption() != null){
			op.setStartDateOption(parameters.getStartDateOption().intValue());
		}
		if (parameters.getStatus() != null){
			op.setStatus(com.sysfera.vishnu.api.ums.data.StatusType.getByName(
					parameters.getStatus().toString()).getValue());
		}
		if (parameters.getUserId() != null){
			op.setUserId(parameters.getUserId());
		}
		try {
			VISHNU.listSessions(parameters.getSessionKey(), li, op);
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 21 :
				throw new UNKNOWNUSERIDMessage(msg, new UNKNOWNUSERIDFault());
			case 28 :
				throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
				throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		for (i = 0; i < li.getSessions().size(); i++) {
			ListSessionsResponse.Data.Session opva = new ListSessionsResponse.Data.Session();
			opva.setClosePolicy(SessionCloseType
					.fromValue(com.sysfera.vishnu.api.ums.data.SessionCloseType
							.get(li.getSessions().get(i).getClosePolicy())
							.getLiteral()));
			opva.setDateClosure(li.getSessions().get(i).getDateClosure());
			opva.setDateCreation(li.getSessions().get(i).getDateCreation());
			opva.setDateLastConnect(li.getSessions().get(i).getDateLastConnect());
			opva.setSessionId(li.getSessions().get(i).getSessionId());
			opva.setSessionKey(li.getSessions().get(i).getSessionKey());
			opva.setStatus(StatusType
					.fromValue(com.sysfera.vishnu.api.ums.data.StatusType.get(
							li.getSessions().get(i).getStatus()).getLiteral()));
			opva.setTimeout(li.getSessions().get(i).getTimeout());
			opva.setUserId(li.getSessions().get(i).getUserId());
			dat.getSession().add(opva);
		}
		res.setData(dat);

		return res;
	}

	public AddLocalAccountResponse addLocalAccount(
			AddLocalAccountRequest parameters) throws LOCALACCOUNTEXISTMessage,
			UNKNOWNMACHINEMessage, SESSIONKEYNOTFOUNDMessage, DBCONNMessage,
			UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage,
			SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage,
			MACHINELOCKEDMessage, UNUSABLEMACHINEMessage {
		String key[] = { "" };
		AddLocalAccountResponse res = new AddLocalAccountResponse();
		LocalAccount acc = new LocalAccount();
		if (parameters.getSessionKey() == null) {
			parameters.setSessionKey("");
		}
		if (parameters.getAcLogin() != null){
			acc.setAcLogin(parameters.getAcLogin());
		}
		if (parameters.getHomeDirectory() != null){
			acc.setHomeDirectory(parameters.getHomeDirectory());
		}
		if (parameters.getMachineId() != null){
			acc.setMachineId(parameters.getMachineId());
		}
		if (parameters.getUserId() != null){
			acc.setUserId(parameters.getUserId());
		}
		if (parameters.getSshKeyPath() != null){
			acc.setSshKeyPath(parameters.getSshKeyPath());
		}
		try {
			VISHNU.addLocalAccount(parameters.getSessionKey(), acc, key);
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 21 :
				throw new UNKNOWNUSERIDMessage(msg, new UNKNOWNUSERIDFault());
			case 28 :
				throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
				throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 34 :
				throw new MACHINELOCKEDMessage(msg, new MACHINELOCKEDFault());
			case 36 :
				throw new UNUSABLEMACHINEMessage(msg, new UNUSABLEMACHINEFault());
			case 37 :
				throw new LOCALACCOUNTEXISTMessage(msg, new LOCALACCOUNTEXISTFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}

		res.setSshPublicKey(key[0]);
		return res;
	}

	public ChangePasswordResponse changePassword(
			ChangePasswordRequest parameters) throws UNKNOWNUSERMessage,
			USERLOCKEDMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage,
			SYSTEMMessage, DBERRMessage {
		initUMS();
		ChangePasswordResponse res = new ChangePasswordResponse();
		if (parameters.getUserId() == null) {
			parameters.setUserId("");
		}
		if (parameters.getPassword() == null) {
			parameters.setPassword("");
		}
		if (parameters.getPasswordNew() == null) {
			parameters.setPasswordNew("");
		}
		try {
			VISHNU.changePassword(parameters.getUserId(),
					parameters.getPassword(), parameters.getPasswordNew());
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 20 : 
				throw new UNKNOWNUSERMessage(msg, new UNKNOWNUSERFault());
			case 24 :
				throw new USERLOCKEDMessage(msg, new USERLOCKEDFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

   	/**
	 * Returns the arguments of the exception provided in its serialized form
	 * @param s1	the exception string (serialized)
	 * @param array	contains two elements: array[0] = the exception code / array[1] = the full message (generic and detailed message concatenated)
	 */
	private void splitExcep (String s1, String[] array ){
		
		Integer i = s1.indexOf('#');
		Integer j = s1.lastIndexOf('#');
		
		if(i!=-1){
			array[0] = s1.substring(0, i);
			array[1] = s1.substring(j+1);
			if(j!=i+1){
				array[1] += ": ";
				array[1] += s1.substring(i+1, j);
			}
		}else{
			array[0] = "7"; // 7 is invalid system exception in C++
			array[1] = "Error ! Invalid exception gotten";
		}
	}

	public ListAuthSystemsResponse listAuthSystems(
			ListAuthSystemsRequest parameters)
			throws SESSIONKEYNOTFOUNDMessage, NOADMINMessage, DBCONNMessage,
			UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage,
			SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage,
			UNKNOWNAUTHSYSTEMTYPEMessage {
		ListAuthSystemsResponse res = new ListAuthSystemsResponse();
		int i;
		ListAuthSystems li = new ListAuthSystems();
		ListAuthSystemsResponse.Data dat = new ListAuthSystemsResponse.Data();
		ListAuthSysOptions op = new ListAuthSysOptions();
		
		if (parameters.getUserId()!=null){
			op.setUserId(parameters.getUserId());
		}
		if (parameters.getSessionKey()==null){
			parameters.setSessionKey("");
		}
		if (parameters.isListAllAuthSystems()!=null){
			op.setListAllAuthSystems(parameters.isListAllAuthSystems());
		}
		if (parameters.isListFullInfo()!=null){
			op.setListFullInfo(parameters.isListFullInfo());
		}
		if (parameters.getAuthSystemId()!=null){
			op.setAuthSystemId(parameters.getAuthSystemId());
		}
		try {
			VISHNU.listAuthSystems(parameters.getSessionKey(), li, op);
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 25 :
				throw new NOADMINMessage(msg, new NOADMINFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		for (i = 0; i < li.getAuthSystems().size(); i++) {
			ListAuthSystemsResponse.Data.Authsystem opva = new ListAuthSystemsResponse.Data.Authsystem();
			opva.setAuthLogin(li.getAuthSystems().get(i).getAuthLogin());
			opva.setAuthPassword(li.getAuthSystems().get(i).getAuthPassword());
			opva.setAuthSystemId(li.getAuthSystems().get(i).getAuthSystemId());
			opva.setLdapBase(li.getAuthSystems().get(i).getLdapBase());
			opva.setName(li.getAuthSystems().get(i).getName());
			opva.setStatus(StatusType
					.fromValue(com.sysfera.vishnu.api.ums.data.StatusType.get(
							li.getAuthSystems().get(i).getStatus()).getLiteral()));
			opva.setType(AuthType
					.fromValue(com.sysfera.vishnu.api.ums.data.AuthType.get(
							li.getAuthSystems().get(i).getType()).getLiteral()));
			opva.setURI(li.getAuthSystems().get(i).getURI());
			opva.setUserPasswordEncryption(EncryptionMethod
					.fromValue(com.sysfera.vishnu.api.ums.data.EncryptionMethod.get(
							li.getAuthSystems().get(i).getUserPasswordEncryption()).getLiteral()));

			dat.getAuthsystem().add(opva);
		}
		res.setData(dat);
		return res;
	}

	public DeleteAuthAccountResponse deleteAuthAccount(
			DeleteAuthAccountRequest parameters)
			throws UNKNOWNAUTHACCOUNTMessage, UNKNOWNAUTHSYSTEMMessage,
			SESSIONKEYNOTFOUNDMessage, NOADMINMessage, USERLOCKEDMessage,
			DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage,
			SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		DeleteAuthAccountResponse res = new DeleteAuthAccountResponse();
		
		if (parameters.getAuthSystemId()==null) {
			parameters.setAuthSystemId("");
		}
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		if (parameters.getUserId()==null) {
			parameters.setUserId("");
		}

		try {
			VISHNU.deleteAuthAccount(parameters.getSessionKey(), parameters.getAuthSystemId(), parameters.getUserId());
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 25 :
				throw new NOADMINMessage(msg, new NOADMINFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		
		return res;
	}

	public ListAuthAccountsResponse listAuthAccounts(
			ListAuthAccountsRequest parameters)
			throws UNKNOWNAUTHACCOUNTMessage, UNKNOWNAUTHSYSTEMMessage,
			SESSIONKEYNOTFOUNDMessage, NOADMINMessage, USERLOCKEDMessage,
			DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage,
			SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		ListAuthAccountsResponse res = new ListAuthAccountsResponse();

		int i;
		ListAuthAccounts li = new ListAuthAccounts();
		ListAuthAccountsResponse.Data dat = new ListAuthAccountsResponse.Data();
		ListAuthAccOptions op = new ListAuthAccOptions();
		
		if (parameters.getAuthSystemId()!=null){
			op.setAuthSystemId(parameters.getAuthSystemId());
		}
		if (parameters.getSessionKey()==null){
			parameters.setSessionKey("");
		}
		if (parameters.getUserId()!=null){
			op.setUserId(parameters.getUserId());
		}
		if (parameters.isListAll()!=null){
			op.setListAll(parameters.isListAll());
		}
		
		try {
			VISHNU.listAuthAccounts(parameters.getSessionKey(), li, op);
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 25 :
				throw new NOADMINMessage(msg, new NOADMINFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		for (i = 0; i < li.getAuthAccounts().size(); i++) {
			ListAuthAccountsResponse.Data.Authaccount opva = new ListAuthAccountsResponse.Data.Authaccount();
			opva.setAcLogin(li.getAuthAccounts().get(i).getAcLogin());
			opva.setAuthSystemId(li.getAuthAccounts().get(i).getAuthSystemId());
			opva.setUserId(li.getAuthAccounts().get(i).getUserId());
			dat.getAuthaccount().add(opva);
		}
		res.setData(dat);
		return res;
	}

	public UpdateAuthAccountResponse updateAuthAccount(
			UpdateAuthAccountRequest parameters)
			throws UNKNOWNAUTHACCOUNTMessage, UNKNOWNAUTHSYSTEMMessage,
			SESSIONKEYNOTFOUNDMessage, NOADMINMessage, USERLOCKEDMessage,
			DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage,
			SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		UpdateAuthAccountResponse res = new UpdateAuthAccountResponse();
		AuthAccount auth = new AuthAccount();
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		if (parameters.getAcLogin()!=null){
			auth.setAcLogin(parameters.getAcLogin());
		}
		if (parameters.getAuthSystemId()!=null) {
			auth.setAuthSystemId(parameters.getAuthSystemId());
		}
		if (parameters.getUserId()!=null) {
			auth.setUserId(parameters.getUserId());
		}
		try {
			VISHNU.updateAuthAccount(parameters.getSessionKey(), auth);
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 25 :
				throw new NOADMINMessage(msg, new NOADMINFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

	public AddAuthAccountResponse addAuthAccount(
			AddAuthAccountRequest parameters) throws UNKNOWNAUTHSYSTEMMessage,
			SESSIONKEYNOTFOUNDMessage, LOGINALREADYUSEDMessage,
			AUTHACCOUNTEXISTMessage, NOADMINMessage, USERLOCKEDMessage,
			DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage,
			SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		AddAuthAccountResponse res = new AddAuthAccountResponse();
		AuthAccount auth = new AuthAccount();
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		if (parameters.getAcLogin()!=null){
			auth.setAcLogin(parameters.getAcLogin());
		}
		if (parameters.getAuthSystemId()!=null) {
			auth.setAuthSystemId(parameters.getAuthSystemId());
		}
		if (parameters.getUserId()!=null) {
			auth.setUserId(parameters.getUserId());
		}

		try {
			VISHNU.addAuthAccount(parameters.getSessionKey(), auth);
		} catch (InternalUMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 3  :
				throw new DBCONNMessage(msg, new DBCONNFault());
			case 4 :
				throw new SYSTEMMessage(msg, new SYSTEMFault());
			case 25 :
				throw new NOADMINMessage(msg, new NOADMINFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

}
