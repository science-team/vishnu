
package com.sysfera.vishnu.api.fms;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="src" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dest" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="isRecursive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="trCommand" type="{urn:ResourceProxy}TransferCommand" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sessionKey",
    "src",
    "dest",
    "isRecursive",
    "trCommand"
})
@XmlRootElement(name = "copyFileRequest")
public class CopyFileRequest {

    @XmlElement(required = true)
    protected String sessionKey;
    @XmlElement(required = true)
    protected String src;
    @XmlElement(required = true)
    protected String dest;
    protected Boolean isRecursive;
    protected TransferCommand trCommand;

    /**
     * Gets the value of the sessionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * Sets the value of the sessionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionKey(String value) {
        this.sessionKey = value;
    }

    /**
     * Gets the value of the src property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrc() {
        return src;
    }

    /**
     * Sets the value of the src property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrc(String value) {
        this.src = value;
    }

    /**
     * Gets the value of the dest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDest() {
        return dest;
    }

    /**
     * Sets the value of the dest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDest(String value) {
        this.dest = value;
    }

    /**
     * Gets the value of the isRecursive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRecursive() {
        return isRecursive;
    }

    /**
     * Sets the value of the isRecursive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRecursive(Boolean value) {
        this.isRecursive = value;
    }

    /**
     * Gets the value of the trCommand property.
     * 
     * @return
     *     possible object is
     *     {@link TransferCommand }
     *     
     */
    public TransferCommand getTrCommand() {
        return trCommand;
    }

    /**
     * Sets the value of the trCommand property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferCommand }
     *     
     */
    public void setTrCommand(TransferCommand value) {
        this.trCommand = value;
    }

}
