package com.sysfera.vishnu.api.fms.test;


import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sysfera.vishnu.api.fms.CONFIGNOTFOUNDMessage;
import com.sysfera.vishnu.api.fms.ChGrpRequest;
import com.sysfera.vishnu.api.fms.ChGrpResponse;
import com.sysfera.vishnu.api.fms.ChModRequest;
import com.sysfera.vishnu.api.fms.ChModResponse;
import com.sysfera.vishnu.api.fms.ContentOfFileRequest;
import com.sysfera.vishnu.api.fms.ContentOfFileResponse;
import com.sysfera.vishnu.api.fms.CopyAsyncFileRequest;
import com.sysfera.vishnu.api.fms.CopyAsyncFileResponse;
import com.sysfera.vishnu.api.fms.CopyFileRequest;
import com.sysfera.vishnu.api.fms.CopyFileResponse;
import com.sysfera.vishnu.api.fms.CreateDirRequest;
import com.sysfera.vishnu.api.fms.CreateDirResponse;
import com.sysfera.vishnu.api.fms.CreateFileRequest;
import com.sysfera.vishnu.api.fms.CreateFileResponse;
import com.sysfera.vishnu.api.fms.DBERRMessage;
import com.sysfera.vishnu.api.fms.DIETMessage;
import com.sysfera.vishnu.api.fms.GetFileInfoRequest;
import com.sysfera.vishnu.api.fms.GetFileInfoResponse;
import com.sysfera.vishnu.api.fms.HeadOfFileRequest;
import com.sysfera.vishnu.api.fms.HeadOfFileResponse;
import com.sysfera.vishnu.api.fms.INVALIDPARAMMessage;
import com.sysfera.vishnu.api.fms.INVALIDPATHMessage;
import com.sysfera.vishnu.api.fms.ListDirRequest;
import com.sysfera.vishnu.api.fms.ListDirResponse;
import com.sysfera.vishnu.api.fms.ListFileTransfersRequest;
import com.sysfera.vishnu.api.fms.ListFileTransfersResponse;
import com.sysfera.vishnu.api.fms.MoveAsyncFileRequest;
import com.sysfera.vishnu.api.fms.MoveAsyncFileResponse;
import com.sysfera.vishnu.api.fms.MoveFileRequest;
import com.sysfera.vishnu.api.fms.MoveFileResponse;
import com.sysfera.vishnu.api.fms.NOADMINMessage;
import com.sysfera.vishnu.api.fms.RUNTIMEERRORMessage;
import com.sysfera.vishnu.api.fms.RemoveDirRequest;
import com.sysfera.vishnu.api.fms.RemoveDirResponse;
import com.sysfera.vishnu.api.fms.RemoveFileRequest;
import com.sysfera.vishnu.api.fms.RemoveFileResponse;
import com.sysfera.vishnu.api.fms.SESSIONKEYEXPIREDMessage;
import com.sysfera.vishnu.api.fms.StopFileTransferRequest;
import com.sysfera.vishnu.api.fms.StopFileTransferResponse;
import com.sysfera.vishnu.api.fms.TailOfFileRequest;
import com.sysfera.vishnu.api.fms.TailOfFileResponse;
import com.sysfera.vishnu.api.fms.UNDEFINEDMessage;
import com.sysfera.vishnu.api.fms.UNKNOWNFILETRANSFERMessage;
import com.sysfera.vishnu.api.fms.UNKNOWNLOCALACCOUNTMessage;
import com.sysfera.vishnu.api.fms.UNKNOWNMACHINEMessage;
import com.sysfera.vishnu.api.fms.UNKNOWNUSERIDMessage;
import com.sysfera.vishnu.api.fms.VishnuFMSPortType;
import com.sysfera.vishnu.api.fms.VishnuFMSService;
import com.sysfera.vishnu.api.ims.INVALIDPARAMETERMessage;
import com.sysfera.vishnu.api.tms.PERMISSIONDENIEDMessage;
import com.sysfera.vishnu.api.ums.ConnectRequest;
import com.sysfera.vishnu.api.ums.ConnectResponse;
import com.sysfera.vishnu.api.ums.SessionCloseType;
import com.sysfera.vishnu.api.ums.VishnuUMSPortType;
import com.sysfera.vishnu.api.ums.VishnuUMSService;

public class VishnuFMSPortImplTest {

	private static final String 	JBOSS_SERVER_NAME = "localhost";
	private static final Integer 	JBOSS_SERVER_PORT = 8080;
	private static final String		TEST_USER_LOGIN = "root";
	private static final String		TEST_USER_PASSWORD = "vishnu_user";
	
	private static final String 	MACHINEID = "machine_1";
	private static final String		prefix = MACHINEID + ":";
	
	private static VishnuUMSPortType 	UMSPort;
	private static VishnuFMSPortType 	FMSPort;
	private static String				sessionKey;
	private static String				sessionId;
	private static int                  fid;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("INIT TESTS");
		
		// Initialize web service interface
		UMSPort = getUMSPort("http://" + JBOSS_SERVER_NAME + ":" + JBOSS_SERVER_PORT + "/WSAPI/VishnuUMS?wsdl");
		FMSPort = getFMSPort("http://" + JBOSS_SERVER_NAME + ":" + JBOSS_SERVER_PORT + "/WSAPI/VishnuFMS?wsdl");
		
		// Initialize Vishnu session
		ConnectRequest request = new ConnectRequest();
		request.setUserId(TEST_USER_LOGIN);
		request.setPassword(TEST_USER_PASSWORD);
		request.setSessionInactivityDelay(new BigInteger("200"));
		System.out.println("Valeur du delai :"+request.getSessionInactivityDelay());
		request.setClosePolicy(SessionCloseType.valueOf(new String("CLOSE_ON_TIMEOUT")));
		ConnectResponse response = UMSPort.connect(request);
		sessionKey = response.getSessionKey();
		sessionId = response.getSessionId();
		System.out.println("SESSION KEY=" + sessionKey);
		System.out.println("SESSION ID=" + sessionId);
	}
	
	@Test
	public void testChGrp() throws  INVALIDPATHMessage,  com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage {
		ChGrpRequest req = new ChGrpRequest();
		req.setSessionKey(sessionKey);
		req.setPath(prefix + "/tmp/toto");
		req.setGroup("keo");
		ChGrpResponse resp = FMSPort.chGrp(req);
	}
	
	@Test
	public void testChMod() throws  INVALIDPATHMessage,com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, DIETMessage, UNKNOWNLOCALACCOUNTMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage {
		ChModRequest req = new ChModRequest();
		req.setSessionKey(sessionKey);
		req.setMode(new BigInteger("777"));
		req.setPath(prefix + "/tmp/toto");
		ChModResponse resp = FMSPort.chMod(req);
	}
	
	@Test
	public void testContentOfFile() throws  INVALIDPATHMessage,    com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage {
		ContentOfFileRequest req = new ContentOfFileRequest();
		req.setSessionKey(sessionKey);
		req.setPath(prefix + "/tmp/toto");
		ContentOfFileResponse resp = FMSPort.contentOfFile(req);
	}
	
	@Test
	public void testCopyAsyncFile() throws  INVALIDPATHMessage,    com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage {
		CopyAsyncFileRequest req = new CopyAsyncFileRequest();
		req.setSessionKey(sessionKey);
		req.setDest(prefix + "/tmp/titi");
		req.setIsRecursive(true);
		req.setSrc(prefix + "/tmp/toto");
		CopyAsyncFileResponse resp = FMSPort.copyAsyncFile(req);
	}
	
	@Test
	public void testCopyFile() throws  INVALIDPATHMessage,  com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage {
		CopyFileRequest req = new CopyFileRequest();
		req.setSessionKey(sessionKey);
		req.setDest(prefix + "/tmp/tata");
		req.setIsRecursive(true);
		req.setSrc(prefix + "/tmp/toto");
		CopyFileResponse resp = FMSPort.copyFile(req);
	}

	@Test
	public void testCreateDir() throws  INVALIDPATHMessage,     com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage {
		CreateDirRequest req = new CreateDirRequest();
		req.setSessionKey(sessionKey);
		req.setPath(prefix + "/tmp/lapin");
		CreateDirResponse resp = FMSPort.createDir(req);
	}

	@Test
	public void testCreateDirRecursive() throws  INVALIDPATHMessage,     com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage {
		CreateDirRequest req = new CreateDirRequest();
		req.setSessionKey(sessionKey);
		req.setPath(prefix + "/tmp/rep1/rep2");
		req.setIsRecursive(true);
		CreateDirResponse resp = FMSPort.createDir(req);
	}
	
	@Test
	public void testCreateFile() throws  INVALIDPATHMessage,     com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage, UNKNOWNUSERIDMessage {
		CreateFileRequest req = new CreateFileRequest();
		req.setSessionKey(sessionKey);
		req.setPath(prefix + "/tmp/titi");
		CreateFileResponse resp = FMSPort.createFile(req);
	}
	
	@Test
	public void testGetFilesInfo() throws  INVALIDPATHMessage,     com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage {
		GetFileInfoRequest req = new GetFileInfoRequest();
		req.setSessionKey(sessionKey);
		req.setPath(prefix + "/tmp/titi");
		GetFileInfoResponse resp = FMSPort.getFileInfo(req);
		System.out.println("Droits: "+resp.getPerms());
	}
	
	@Test
	public void testHeadOfFile() throws  INVALIDPATHMessage,     com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage {
		HeadOfFileRequest req = new HeadOfFileRequest();
		req.setSessionKey(sessionKey);
		req.setPath(prefix + "/tmp/titi");
		HeadOfFileResponse resp = FMSPort.headOfFile(req);
	}
	
	@Test
	public void testListDir() throws  INVALIDPATHMessage,     com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage {
		ListDirRequest req = new ListDirRequest();
		req.setSessionKey(sessionKey);
		req.setPath(prefix + "/tmp");
		req.setAllFiles(true);
		req.setLongFormat(true);
	
		ListDirResponse resp = FMSPort.listDir(req);
		for (int i = 0; i < resp.getData().getDirentry().size(); i++) {
			System.out.println("Path: "+resp.getData().getDirentry().get(i).getPath());
			System.out.println("Owner: "+resp.getData().getDirentry().get(i).getOwner());
			System.out.println("Group: "+resp.getData().getDirentry().get(i).getGroup());
			System.out.println("Size: "+resp.getData().getDirentry().get(i).getSize());
			System.out.println("Perms: "+resp.getData().getDirentry().get(i).getPerms()+"\n");
		}
	}
	
	@Test
	public void testListFileTransfers() throws  INVALIDPATHMessage,     com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, UNKNOWNUSERIDMessage, DBERRMessage {
		ListFileTransfersRequest req = new ListFileTransfersRequest();
		req.setSessionKey(sessionKey);
		req.setFromMachineId(MACHINEID);
		ListFileTransfersResponse resp = FMSPort.listFileTransfers(req);
		System.out.println("Nombre de transfert: "+resp.getData().getFiletransfer().size());
	}
	
	@Test
	public void testMoveAsyncFile() throws  INVALIDPATHMessage,     com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage {
		MoveAsyncFileRequest req = new MoveAsyncFileRequest();
		req.setSessionKey(sessionKey);
		req.setDest(prefix + "/tmp/tutu");
		req.setIsRecursive(true);
		req.setSrc(prefix + "/tmp/titi");
		MoveAsyncFileResponse resp = FMSPort.moveAsyncFile(req);
	}
	
	@Test
	public void testMovesFile() throws  INVALIDPATHMessage,     com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage {
		MoveFileRequest req = new MoveFileRequest();
		req.setSessionKey(sessionKey);
		req.setDest(prefix + "/tmp/titi");
		req.setIsRecursive(true);
		req.setSrc(prefix + "/tmp/tutu");
		MoveFileResponse resp = FMSPort.moveFile(req);
	}
	
	@Test
	public void testRemoveDir() throws  INVALIDPATHMessage,     com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage {
		RemoveDirRequest req = new RemoveDirRequest();
		req.setSessionKey(sessionKey);
		req.setPath(prefix + "/tmp/lapin");
		RemoveDirResponse resp = FMSPort.removeDir(req);
	}
	
	@Test
	public void testRemoveFile() throws  INVALIDPATHMessage,     com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage {
		RemoveFileRequest req = new RemoveFileRequest();
		req.setSessionKey(sessionKey);
		req.setPath(prefix + "/tmp/tata");
		RemoveFileResponse resp = FMSPort.removeFile(req);
	}
	
	@Test
	public void testStopFileTransfer() throws  INVALIDPATHMessage,     com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, UNKNOWNUSERIDMessage, DBERRMessage {
		StopFileTransferRequest req = new StopFileTransferRequest();
		req.setSessionKey(sessionKey);
		req.setFromMachineId(MACHINEID);
		StopFileTransferResponse resp = FMSPort.stopFileTransfer(req);
	}
	
	@Test
	public void testTailOfFile() throws  INVALIDPATHMessage,     com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage, CONFIGNOTFOUNDMessage, UNKNOWNFILETRANSFERMessage, UNKNOWNMACHINEMessage, NOADMINMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, RUNTIMEERRORMessage, PERMISSIONDENIEDMessage, INVALIDPARAMETERMessage, DBERRMessage {
		TailOfFileRequest req = new TailOfFileRequest();
		req.setSessionKey(sessionKey);
		req.setPath(prefix + "/tmp/toto");
		TailOfFileResponse resp = FMSPort.tailOfFile(req);
	}
	
	private static VishnuUMSPortType getUMSPort(String endpointURI) throws MalformedURLException   {

		URL wsdlURL = new URL(endpointURI);
		VishnuUMSService service = new VishnuUMSService(wsdlURL);
		return service.getVishnuUMSPort();
	}
	private static VishnuFMSPortType getFMSPort(String endpointURI) throws MalformedURLException   {

		URL wsdlURL = new URL(endpointURI);
		VishnuFMSService service = new VishnuFMSService(wsdlURL);
		return service.getVishnuFMSPort();
	}

}
