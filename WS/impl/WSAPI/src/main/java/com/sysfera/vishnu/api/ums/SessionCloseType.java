
package com.sysfera.vishnu.api.ums;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SessionCloseType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SessionCloseType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DEFAULT"/>
 *     &lt;enumeration value="CLOSE_ON_TIMEOUT"/>
 *     &lt;enumeration value="CLOSE_ON_DISCONNECT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SessionCloseType")
@XmlEnum
public enum SessionCloseType {

    DEFAULT,
    CLOSE_ON_TIMEOUT,
    CLOSE_ON_DISCONNECT;

    public String value() {
        return name();
    }

    public static SessionCloseType fromValue(String v) {
        return valueOf(v);
    }

}
