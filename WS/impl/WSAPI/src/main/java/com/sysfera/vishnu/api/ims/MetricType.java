
package com.sysfera.vishnu.api.ims;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MetricType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MetricType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UNDEFINED"/>
 *     &lt;enumeration value="CPUUSE"/>
 *     &lt;enumeration value="FREEDISKSPACE"/>
 *     &lt;enumeration value="FREEMEMORY"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MetricType")
@XmlEnum
public enum MetricType {

    UNDEFINED,
    CPUUSE,
    FREEDISKSPACE,
    FREEMEMORY;

    public String value() {
        return name();
    }

    public static MetricType fromValue(String v) {
        return valueOf(v);
    }

}
