package com.sysfera.vishnu.api.ims.test;


import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sysfera.vishnu.api.ims.DBERRORMessage;
import com.sysfera.vishnu.api.ims.ExportCommandsRequest;
import com.sysfera.vishnu.api.ims.ExportCommandsResponse;
import com.sysfera.vishnu.api.ims.GetMetricCurrentValueRequest;
import com.sysfera.vishnu.api.ims.GetMetricCurrentValueResponse;
import com.sysfera.vishnu.api.ims.GetMetricHistoryRequest;
import com.sysfera.vishnu.api.ims.GetMetricHistoryResponse;
import com.sysfera.vishnu.api.ims.GetSystemInfoRequest;
import com.sysfera.vishnu.api.ims.GetSystemInfoResponse;
import com.sysfera.vishnu.api.ims.GetUpdateFrequencyRequest;
import com.sysfera.vishnu.api.ims.GetUpdateFrequencyResponse;
import com.sysfera.vishnu.api.ims.INVALIDPARAMETERMessage;
import com.sysfera.vishnu.api.ims.VishnuIMSPortType;
import com.sysfera.vishnu.api.ims.VishnuIMSService;
import com.sysfera.vishnu.api.tms.DBCONNMessage;
import com.sysfera.vishnu.api.tms.DBERRMessage;
import com.sysfera.vishnu.api.tms.DIETMessage;
import com.sysfera.vishnu.api.tms.SESSIONKEYEXPIREDMessage;
import com.sysfera.vishnu.api.tms.SYSTEMMessage;
import com.sysfera.vishnu.api.tms.UNDEFINEDMessage;
import com.sysfera.vishnu.api.tms.UNKNOWNMACHINEMessage;
import com.sysfera.vishnu.api.ums.ConnectRequest;
import com.sysfera.vishnu.api.ums.ConnectResponse;
import com.sysfera.vishnu.api.ums.SESSIONKEYNOTFOUNDMessage;
import com.sysfera.vishnu.api.ums.SessionCloseType;
import com.sysfera.vishnu.api.ums.VishnuUMSPortType;
import com.sysfera.vishnu.api.ums.VishnuUMSService;

public class VishnuIMSPortImplTest {

	private static final String 	JBOSS_SERVER_NAME = "localhost";
	private static final Integer 	JBOSS_SERVER_PORT = 8080;
	private static final String		TEST_USER_LOGIN = "root";
	private static final String		TEST_USER_PASSWORD = "vishnu_user";
	
	private static final String 	MACHINEID = "machine_1";
	
	private static VishnuUMSPortType 	UMSPort;
	private static VishnuIMSPortType 	IMSPort;
	private static String				sessionKey;
	private static String				sessionId;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("INIT TESTS");
		
		// Initialize web service interface
		UMSPort = getUMSPort("http://" + JBOSS_SERVER_NAME + ":" + JBOSS_SERVER_PORT + "/WSAPI/VishnuUMS?wsdl");
		IMSPort = getIMSPort("http://" + JBOSS_SERVER_NAME + ":" + JBOSS_SERVER_PORT + "/WSAPI/VishnuIMS?wsdl");
		
		// Initialize Vishnu session
		ConnectRequest request = new ConnectRequest();
		request.setUserId(TEST_USER_LOGIN);
		request.setPassword(TEST_USER_PASSWORD);
		request.setSessionInactivityDelay(new BigInteger("200"));
		System.out.println("Valeur du delai :"+request.getSessionInactivityDelay());
		request.setClosePolicy(SessionCloseType.valueOf(new String("CLOSE_ON_TIMEOUT")));
		ConnectResponse response = UMSPort.connect(request);
		sessionKey = response.getSessionKey();
		sessionId = response.getSessionId();
		System.out.println("SESSION KEY=" + sessionKey);
		System.out.println("SESSION ID=" + sessionId);
	}
	@Test (expected=INVALIDPARAMETERMessage.class)
	public void testExportCommands() throws DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, NumberFormatException,UNKNOWNMACHINEMessage, DBERRORMessage, INVALIDPARAMETERMessage, com.sysfera.vishnu.api.ums.DBERRMessage, com.sysfera.vishnu.api.ums.SYSTEMMessage, SESSIONKEYNOTFOUNDMessage, com.sysfera.vishnu.api.ums.SESSIONKEYEXPIREDMessage, com.sysfera.vishnu.api.ims.SESSIONKEYEXPIREDMessage, com.sysfera.vishnu.api.ims.UNDEFINEDMessage, com.sysfera.vishnu.api.ims.SESSIONKEYNOTFOUNDMessage, com.sysfera.vishnu.api.ims.DBERRMessage {
		ExportCommandsRequest req = new ExportCommandsRequest();
		req.setSessionKey(sessionKey);
		req.setOldSessionId(sessionId);
		ExportCommandsResponse resp = IMSPort.exportCommands(req);
	}
	@Test
	public void testGetMetricCurrentValue() throws DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, NumberFormatException,UNKNOWNMACHINEMessage, DBERRORMessage, INVALIDPARAMETERMessage, com.sysfera.vishnu.api.ums.DBERRMessage, com.sysfera.vishnu.api.ums.SYSTEMMessage, SESSIONKEYNOTFOUNDMessage, com.sysfera.vishnu.api.ums.SESSIONKEYEXPIREDMessage, com.sysfera.vishnu.api.ims.SESSIONKEYEXPIREDMessage, com.sysfera.vishnu.api.ims.UNDEFINEDMessage, com.sysfera.vishnu.api.ims.SESSIONKEYNOTFOUNDMessage, com.sysfera.vishnu.api.ims.DBERRMessage {
		GetMetricCurrentValueRequest req = new GetMetricCurrentValueRequest();
		req.setSessionKey(sessionKey);
		req.setMachineId(MACHINEID);
		GetMetricCurrentValueResponse resp = IMSPort.getMetricCurrentValue(req);
	}
	@Test
	public void testGetMetricHistory() throws DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, NumberFormatException,UNKNOWNMACHINEMessage, DBERRORMessage, INVALIDPARAMETERMessage, com.sysfera.vishnu.api.ums.DBERRMessage, com.sysfera.vishnu.api.ums.SYSTEMMessage, SESSIONKEYNOTFOUNDMessage, com.sysfera.vishnu.api.ums.SESSIONKEYEXPIREDMessage, com.sysfera.vishnu.api.ims.SESSIONKEYEXPIREDMessage, com.sysfera.vishnu.api.ims.UNDEFINEDMessage, com.sysfera.vishnu.api.ims.SESSIONKEYNOTFOUNDMessage, com.sysfera.vishnu.api.ims.DBERRMessage {
		GetMetricHistoryRequest req = new GetMetricHistoryRequest();
		req.setSessionKey(sessionKey);
		req.setMachineId(MACHINEID);
		GetMetricHistoryResponse resp = IMSPort.getMetricHistory(req);
	}
	@Test
	public void testGetMetricHistoryLoop() throws DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, NumberFormatException,UNKNOWNMACHINEMessage, DBERRORMessage, INVALIDPARAMETERMessage, com.sysfera.vishnu.api.ums.DBERRMessage, com.sysfera.vishnu.api.ums.SYSTEMMessage, SESSIONKEYNOTFOUNDMessage, com.sysfera.vishnu.api.ums.SESSIONKEYEXPIREDMessage, com.sysfera.vishnu.api.ims.SESSIONKEYEXPIREDMessage, com.sysfera.vishnu.api.ims.UNDEFINEDMessage, com.sysfera.vishnu.api.ims.SESSIONKEYNOTFOUNDMessage, com.sysfera.vishnu.api.ims.DBERRMessage {
		GetMetricHistoryRequest req = new GetMetricHistoryRequest();
		req.setSessionKey(sessionKey);
		req.setMachineId(MACHINEID);
		for (int i = 0 ; i<10 ; i++) {
			GetMetricHistoryResponse resp = IMSPort.getMetricHistory(req);
		}
	}
	@Test
	public void testGetSystemInfo() throws DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, NumberFormatException,UNKNOWNMACHINEMessage, DBERRORMessage, INVALIDPARAMETERMessage, com.sysfera.vishnu.api.ums.DBERRMessage, com.sysfera.vishnu.api.ums.SYSTEMMessage, SESSIONKEYNOTFOUNDMessage, com.sysfera.vishnu.api.ums.SESSIONKEYEXPIREDMessage, com.sysfera.vishnu.api.ims.SESSIONKEYEXPIREDMessage, com.sysfera.vishnu.api.ims.UNDEFINEDMessage, com.sysfera.vishnu.api.ims.SESSIONKEYNOTFOUNDMessage, com.sysfera.vishnu.api.ims.DBERRMessage {
		GetSystemInfoRequest req = new GetSystemInfoRequest();
		req.setSessionKey(sessionKey);
		req.setMachineId(MACHINEID);
		GetSystemInfoResponse resp = IMSPort.getSystemInfo(req);
	}
	@Test
	public void testGetUpdateFrequency() throws DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, NumberFormatException,UNKNOWNMACHINEMessage, DBERRORMessage, INVALIDPARAMETERMessage, com.sysfera.vishnu.api.ums.DBERRMessage, com.sysfera.vishnu.api.ums.SYSTEMMessage, SESSIONKEYNOTFOUNDMessage, com.sysfera.vishnu.api.ums.SESSIONKEYEXPIREDMessage, com.sysfera.vishnu.api.ims.SESSIONKEYEXPIREDMessage, com.sysfera.vishnu.api.ims.UNDEFINEDMessage, com.sysfera.vishnu.api.ims.SESSIONKEYNOTFOUNDMessage, com.sysfera.vishnu.api.ims.DBERRMessage {
		GetUpdateFrequencyRequest req = new GetUpdateFrequencyRequest();
		req.setSessionKey(sessionKey);
		GetUpdateFrequencyResponse resp = IMSPort.getUpdateFrequency(req);
		System.out.println("freq:"+resp.getFreq());
	}
	private static VishnuUMSPortType getUMSPort(String endpointURI) throws MalformedURLException   {

		URL wsdlURL = new URL(endpointURI);
		VishnuUMSService service = new VishnuUMSService(wsdlURL);
		return service.getVishnuUMSPort();
	}
	private static VishnuIMSPortType getIMSPort(String endpointURI) throws MalformedURLException   {

		URL wsdlURL = new URL(endpointURI);
		VishnuIMSService service = new VishnuIMSService(wsdlURL);
		return service.getVishnuIMSPort();
	}
}
