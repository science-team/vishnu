
package com.sysfera.vishnu.api.ums;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sysfera.vishnu.api.ums package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sysfera.vishnu.api.ums
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RESTORECONFIGERRORFault }
     * 
     */
    public RESTORECONFIGERRORFault createRESTORECONFIGERRORFault() {
        return new RESTORECONFIGERRORFault();
    }

    /**
     * Create an instance of {@link TEMPORARYPASSWORDFault }
     * 
     */
    public TEMPORARYPASSWORDFault createTEMPORARYPASSWORDFault() {
        return new TEMPORARYPASSWORDFault();
    }

    /**
     * Create an instance of {@link ListLocalAccountsResponse }
     * 
     */
    public ListLocalAccountsResponse createListLocalAccountsResponse() {
        return new ListLocalAccountsResponse();
    }

    /**
     * Create an instance of {@link UNKNOWNOPTIONFault }
     * 
     */
    public UNKNOWNOPTIONFault createUNKNOWNOPTIONFault() {
        return new UNKNOWNOPTIONFault();
    }

    /**
     * Create an instance of {@link ListHistoryCmdResponse }
     * 
     */
    public ListHistoryCmdResponse createListHistoryCmdResponse() {
        return new ListHistoryCmdResponse();
    }

    /**
     * Create an instance of {@link CloseRequest }
     * 
     */
    public CloseRequest createCloseRequest() {
        return new CloseRequest();
    }

    /**
     * Create an instance of {@link AUTHACCOUNTEXISTFault }
     * 
     */
    public AUTHACCOUNTEXISTFault createAUTHACCOUNTEXISTFault() {
        return new AUTHACCOUNTEXISTFault();
    }

    /**
     * Create an instance of {@link ListOptionsResponse }
     * 
     */
    public ListOptionsResponse createListOptionsResponse() {
        return new ListOptionsResponse();
    }

    /**
     * Create an instance of {@link USERLOCKEDFault }
     * 
     */
    public USERLOCKEDFault createUSERLOCKEDFault() {
        return new USERLOCKEDFault();
    }

    /**
     * Create an instance of {@link ListLocalAccountsResponse.Data }
     * 
     */
    public ListLocalAccountsResponse.Data createListLocalAccountsResponseData() {
        return new ListLocalAccountsResponse.Data();
    }

    /**
     * Create an instance of {@link ListSessionsResponse }
     * 
     */
    public ListSessionsResponse createListSessionsResponse() {
        return new ListSessionsResponse();
    }

    /**
     * Create an instance of {@link MACHINEALREADYLOCKEDFault }
     * 
     */
    public MACHINEALREADYLOCKEDFault createMACHINEALREADYLOCKEDFault() {
        return new MACHINEALREADYLOCKEDFault();
    }

    /**
     * Create an instance of {@link ListOptionsResponse.Data }
     * 
     */
    public ListOptionsResponse.Data createListOptionsResponseData() {
        return new ListOptionsResponse.Data();
    }

    /**
     * Create an instance of {@link UNKNOWNUSERIDFault }
     * 
     */
    public UNKNOWNUSERIDFault createUNKNOWNUSERIDFault() {
        return new UNKNOWNUSERIDFault();
    }

    /**
     * Create an instance of {@link ListLocalAccountsRequest }
     * 
     */
    public ListLocalAccountsRequest createListLocalAccountsRequest() {
        return new ListLocalAccountsRequest();
    }

    /**
     * Create an instance of {@link ListAuthAccountsResponse.Data }
     * 
     */
    public ListAuthAccountsResponse.Data createListAuthAccountsResponseData() {
        return new ListAuthAccountsResponse.Data();
    }

    /**
     * Create an instance of {@link ReconnectResponse }
     * 
     */
    public ReconnectResponse createReconnectResponse() {
        return new ReconnectResponse();
    }

    /**
     * Create an instance of {@link UNUSABLEMACHINEFault }
     * 
     */
    public UNUSABLEMACHINEFault createUNUSABLEMACHINEFault() {
        return new UNUSABLEMACHINEFault();
    }

    /**
     * Create an instance of {@link ListAuthSystemsResponse }
     * 
     */
    public ListAuthSystemsResponse createListAuthSystemsResponse() {
        return new ListAuthSystemsResponse();
    }

    /**
     * Create an instance of {@link ListMachinesResponse }
     * 
     */
    public ListMachinesResponse createListMachinesResponse() {
        return new ListMachinesResponse();
    }

    /**
     * Create an instance of {@link AddAuthAccountResponse }
     * 
     */
    public AddAuthAccountResponse createAddAuthAccountResponse() {
        return new AddAuthAccountResponse();
    }

    /**
     * Create an instance of {@link ListMachinesRequest }
     * 
     */
    public ListMachinesRequest createListMachinesRequest() {
        return new ListMachinesRequest();
    }

    /**
     * Create an instance of {@link ListAuthAccountsRequest }
     * 
     */
    public ListAuthAccountsRequest createListAuthAccountsRequest() {
        return new ListAuthAccountsRequest();
    }

    /**
     * Create an instance of {@link ListOptionsRequest }
     * 
     */
    public ListOptionsRequest createListOptionsRequest() {
        return new ListOptionsRequest();
    }

    /**
     * Create an instance of {@link SAVECONFIGERRORFault }
     * 
     */
    public SAVECONFIGERRORFault createSAVECONFIGERRORFault() {
        return new SAVECONFIGERRORFault();
    }

    /**
     * Create an instance of {@link SESSIONKEYEXPIREDFault }
     * 
     */
    public SESSIONKEYEXPIREDFault createSESSIONKEYEXPIREDFault() {
        return new SESSIONKEYEXPIREDFault();
    }

    /**
     * Create an instance of {@link MACHINEEXISTINGFault }
     * 
     */
    public MACHINEEXISTINGFault createMACHINEEXISTINGFault() {
        return new MACHINEEXISTINGFault();
    }

    /**
     * Create an instance of {@link UpdateLocalAccountResponse }
     * 
     */
    public UpdateLocalAccountResponse createUpdateLocalAccountResponse() {
        return new UpdateLocalAccountResponse();
    }

    /**
     * Create an instance of {@link USERALREADYLOCKEDFault }
     * 
     */
    public USERALREADYLOCKEDFault createUSERALREADYLOCKEDFault() {
        return new USERALREADYLOCKEDFault();
    }

    /**
     * Create an instance of {@link UNKNOWNUSERFault }
     * 
     */
    public UNKNOWNUSERFault createUNKNOWNUSERFault() {
        return new UNKNOWNUSERFault();
    }

    /**
     * Create an instance of {@link UNDEFINEDFault }
     * 
     */
    public UNDEFINEDFault createUNDEFINEDFault() {
        return new UNDEFINEDFault();
    }

    /**
     * Create an instance of {@link ROOTUSERONLYFault }
     * 
     */
    public ROOTUSERONLYFault createROOTUSERONLYFault() {
        return new ROOTUSERONLYFault();
    }

    /**
     * Create an instance of {@link UNKNOWNSESSIONIDFault }
     * 
     */
    public UNKNOWNSESSIONIDFault createUNKNOWNSESSIONIDFault() {
        return new UNKNOWNSESSIONIDFault();
    }

    /**
     * Create an instance of {@link ListMachinesResponse.Data }
     * 
     */
    public ListMachinesResponse.Data createListMachinesResponseData() {
        return new ListMachinesResponse.Data();
    }

    /**
     * Create an instance of {@link INVALIDMAILADRESSFault }
     * 
     */
    public INVALIDMAILADRESSFault createINVALIDMAILADRESSFault() {
        return new INVALIDMAILADRESSFault();
    }

    /**
     * Create an instance of {@link ListSessionsRequest }
     * 
     */
    public ListSessionsRequest createListSessionsRequest() {
        return new ListSessionsRequest();
    }

    /**
     * Create an instance of {@link ConfigureOptionResponse }
     * 
     */
    public ConfigureOptionResponse createConfigureOptionResponse() {
        return new ConfigureOptionResponse();
    }

    /**
     * Create an instance of {@link UpdateLocalAccountRequest }
     * 
     */
    public UpdateLocalAccountRequest createUpdateLocalAccountRequest() {
        return new UpdateLocalAccountRequest();
    }

    /**
     * Create an instance of {@link AUTHENTERRFault }
     * 
     */
    public AUTHENTERRFault createAUTHENTERRFault() {
        return new AUTHENTERRFault();
    }

    /**
     * Create an instance of {@link DIETFault }
     * 
     */
    public DIETFault createDIETFault() {
        return new DIETFault();
    }

    /**
     * Create an instance of {@link ListMachinesResponse.Data.Machine }
     * 
     */
    public ListMachinesResponse.Data.Machine createListMachinesResponseDataMachine() {
        return new ListMachinesResponse.Data.Machine();
    }

    /**
     * Create an instance of {@link UNKNOWNMACHINEFault }
     * 
     */
    public UNKNOWNMACHINEFault createUNKNOWNMACHINEFault() {
        return new UNKNOWNMACHINEFault();
    }

    /**
     * Create an instance of {@link DeleteLocalAccountResponse }
     * 
     */
    public DeleteLocalAccountResponse createDeleteLocalAccountResponse() {
        return new DeleteLocalAccountResponse();
    }

    /**
     * Create an instance of {@link ListAuthAccountsResponse.Data.Authaccount }
     * 
     */
    public ListAuthAccountsResponse.Data.Authaccount createListAuthAccountsResponseDataAuthaccount() {
        return new ListAuthAccountsResponse.Data.Authaccount();
    }

    /**
     * Create an instance of {@link CloseResponse }
     * 
     */
    public CloseResponse createCloseResponse() {
        return new CloseResponse();
    }

    /**
     * Create an instance of {@link MACHINELOCKEDFault }
     * 
     */
    public MACHINELOCKEDFault createMACHINELOCKEDFault() {
        return new MACHINELOCKEDFault();
    }

    /**
     * Create an instance of {@link UNKNOWNAUTHSYSTEMFault }
     * 
     */
    public UNKNOWNAUTHSYSTEMFault createUNKNOWNAUTHSYSTEMFault() {
        return new UNKNOWNAUTHSYSTEMFault();
    }

    /**
     * Create an instance of {@link ConnectResponse }
     * 
     */
    public ConnectResponse createConnectResponse() {
        return new ConnectResponse();
    }

    /**
     * Create an instance of {@link LOCALACCOUNTEXISTFault }
     * 
     */
    public LOCALACCOUNTEXISTFault createLOCALACCOUNTEXISTFault() {
        return new LOCALACCOUNTEXISTFault();
    }

    /**
     * Create an instance of {@link READONLYACCOUNTFault }
     * 
     */
    public READONLYACCOUNTFault createREADONLYACCOUNTFault() {
        return new READONLYACCOUNTFault();
    }

    /**
     * Create an instance of {@link ConfigureOptionRequest }
     * 
     */
    public ConfigureOptionRequest createConfigureOptionRequest() {
        return new ConfigureOptionRequest();
    }

    /**
     * Create an instance of {@link ListSessionsResponse.Data.Session }
     * 
     */
    public ListSessionsResponse.Data.Session createListSessionsResponseDataSession() {
        return new ListSessionsResponse.Data.Session();
    }

    /**
     * Create an instance of {@link ListAuthSystemsResponse.Data.Authsystem }
     * 
     */
    public ListAuthSystemsResponse.Data.Authsystem createListAuthSystemsResponseDataAuthsystem() {
        return new ListAuthSystemsResponse.Data.Authsystem();
    }

    /**
     * Create an instance of {@link SESSIONKEYNOTFOUNDFault }
     * 
     */
    public SESSIONKEYNOTFOUNDFault createSESSIONKEYNOTFOUNDFault() {
        return new SESSIONKEYNOTFOUNDFault();
    }

    /**
     * Create an instance of {@link UNKNOWNAUTHSYSTEMTYPEFault }
     * 
     */
    public UNKNOWNAUTHSYSTEMTYPEFault createUNKNOWNAUTHSYSTEMTYPEFault() {
        return new UNKNOWNAUTHSYSTEMTYPEFault();
    }

    /**
     * Create an instance of {@link ListSessionsResponse.Data }
     * 
     */
    public ListSessionsResponse.Data createListSessionsResponseData() {
        return new ListSessionsResponse.Data();
    }

    /**
     * Create an instance of {@link LOGINALREADYUSEDFault }
     * 
     */
    public LOGINALREADYUSEDFault createLOGINALREADYUSEDFault() {
        return new LOGINALREADYUSEDFault();
    }

    /**
     * Create an instance of {@link ListAuthSystemsResponse.Data }
     * 
     */
    public ListAuthSystemsResponse.Data createListAuthSystemsResponseData() {
        return new ListAuthSystemsResponse.Data();
    }

    /**
     * Create an instance of {@link UNKNOWNCLOSUREMODEFault }
     * 
     */
    public UNKNOWNCLOSUREMODEFault createUNKNOWNCLOSUREMODEFault() {
        return new UNKNOWNCLOSUREMODEFault();
    }

    /**
     * Create an instance of {@link ListHistoryCmdResponse.Data }
     * 
     */
    public ListHistoryCmdResponse.Data createListHistoryCmdResponseData() {
        return new ListHistoryCmdResponse.Data();
    }

    /**
     * Create an instance of {@link UpdateAuthAccountResponse }
     * 
     */
    public UpdateAuthAccountResponse createUpdateAuthAccountResponse() {
        return new UpdateAuthAccountResponse();
    }

    /**
     * Create an instance of {@link UNKNOWNENCRYPTIONMETHODFault }
     * 
     */
    public UNKNOWNENCRYPTIONMETHODFault createUNKNOWNENCRYPTIONMETHODFault() {
        return new UNKNOWNENCRYPTIONMETHODFault();
    }

    /**
     * Create an instance of {@link ChangePasswordResponse }
     * 
     */
    public ChangePasswordResponse createChangePasswordResponse() {
        return new ChangePasswordResponse();
    }

    /**
     * Create an instance of {@link INCORRECTTRANSFERCMDFault }
     * 
     */
    public INCORRECTTRANSFERCMDFault createINCORRECTTRANSFERCMDFault() {
        return new INCORRECTTRANSFERCMDFault();
    }

    /**
     * Create an instance of {@link COMMANDRUNNINGFault }
     * 
     */
    public COMMANDRUNNINGFault createCOMMANDRUNNINGFault() {
        return new COMMANDRUNNINGFault();
    }

    /**
     * Create an instance of {@link DBCONNFault }
     * 
     */
    public DBCONNFault createDBCONNFault() {
        return new DBCONNFault();
    }

    /**
     * Create an instance of {@link DeleteAuthAccountResponse }
     * 
     */
    public DeleteAuthAccountResponse createDeleteAuthAccountResponse() {
        return new DeleteAuthAccountResponse();
    }

    /**
     * Create an instance of {@link ChangePasswordRequest }
     * 
     */
    public ChangePasswordRequest createChangePasswordRequest() {
        return new ChangePasswordRequest();
    }

    /**
     * Create an instance of {@link AddLocalAccountRequest }
     * 
     */
    public AddLocalAccountRequest createAddLocalAccountRequest() {
        return new AddLocalAccountRequest();
    }

    /**
     * Create an instance of {@link ListHistoryCmdResponse.Data.Command }
     * 
     */
    public ListHistoryCmdResponse.Data.Command createListHistoryCmdResponseDataCommand() {
        return new ListHistoryCmdResponse.Data.Command();
    }

    /**
     * Create an instance of {@link ListAuthAccountsResponse }
     * 
     */
    public ListAuthAccountsResponse createListAuthAccountsResponse() {
        return new ListAuthAccountsResponse();
    }

    /**
     * Create an instance of {@link ListLocalAccountsResponse.Data.Localaccount }
     * 
     */
    public ListLocalAccountsResponse.Data.Localaccount createListLocalAccountsResponseDataLocalaccount() {
        return new ListLocalAccountsResponse.Data.Localaccount();
    }

    /**
     * Create an instance of {@link SYSTEMFault }
     * 
     */
    public SYSTEMFault createSYSTEMFault() {
        return new SYSTEMFault();
    }

    /**
     * Create an instance of {@link USERIDEXISTINGFault }
     * 
     */
    public USERIDEXISTINGFault createUSERIDEXISTINGFault() {
        return new USERIDEXISTINGFault();
    }

    /**
     * Create an instance of {@link AddLocalAccountResponse }
     * 
     */
    public AddLocalAccountResponse createAddLocalAccountResponse() {
        return new AddLocalAccountResponse();
    }

    /**
     * Create an instance of {@link NOADMINFault }
     * 
     */
    public NOADMINFault createNOADMINFault() {
        return new NOADMINFault();
    }

    /**
     * Create an instance of {@link ListHistoryCmdRequest }
     * 
     */
    public ListHistoryCmdRequest createListHistoryCmdRequest() {
        return new ListHistoryCmdRequest();
    }

    /**
     * Create an instance of {@link AddAuthAccountRequest }
     * 
     */
    public AddAuthAccountRequest createAddAuthAccountRequest() {
        return new AddAuthAccountRequest();
    }

    /**
     * Create an instance of {@link UNKNOWNLOCALACCOUNTFault }
     * 
     */
    public UNKNOWNLOCALACCOUNTFault createUNKNOWNLOCALACCOUNTFault() {
        return new UNKNOWNLOCALACCOUNTFault();
    }

    /**
     * Create an instance of {@link ListOptionsResponse.Data.Optionvalue }
     * 
     */
    public ListOptionsResponse.Data.Optionvalue createListOptionsResponseDataOptionvalue() {
        return new ListOptionsResponse.Data.Optionvalue();
    }

    /**
     * Create an instance of {@link UNKNOWNAUTHACCOUNTFault }
     * 
     */
    public UNKNOWNAUTHACCOUNTFault createUNKNOWNAUTHACCOUNTFault() {
        return new UNKNOWNAUTHACCOUNTFault();
    }

    /**
     * Create an instance of {@link DeleteAuthAccountRequest }
     * 
     */
    public DeleteAuthAccountRequest createDeleteAuthAccountRequest() {
        return new DeleteAuthAccountRequest();
    }

    /**
     * Create an instance of {@link AUTHSYSTEMALREADYLOCKEDFault }
     * 
     */
    public AUTHSYSTEMALREADYLOCKEDFault createAUTHSYSTEMALREADYLOCKEDFault() {
        return new AUTHSYSTEMALREADYLOCKEDFault();
    }

    /**
     * Create an instance of {@link INCORRECTTIMEOUTFault }
     * 
     */
    public INCORRECTTIMEOUTFault createINCORRECTTIMEOUTFault() {
        return new INCORRECTTIMEOUTFault();
    }

    /**
     * Create an instance of {@link UpdateAuthAccountRequest }
     * 
     */
    public UpdateAuthAccountRequest createUpdateAuthAccountRequest() {
        return new UpdateAuthAccountRequest();
    }

    /**
     * Create an instance of {@link ReconnectRequest }
     * 
     */
    public ReconnectRequest createReconnectRequest() {
        return new ReconnectRequest();
    }

    /**
     * Create an instance of {@link DBERRFault }
     * 
     */
    public DBERRFault createDBERRFault() {
        return new DBERRFault();
    }

    /**
     * Create an instance of {@link AUTHSYSTEMALREADYEXISTFault }
     * 
     */
    public AUTHSYSTEMALREADYEXISTFault createAUTHSYSTEMALREADYEXISTFault() {
        return new AUTHSYSTEMALREADYEXISTFault();
    }

    /**
     * Create an instance of {@link ConnectRequest }
     * 
     */
    public ConnectRequest createConnectRequest() {
        return new ConnectRequest();
    }

    /**
     * Create an instance of {@link ListAuthSystemsRequest }
     * 
     */
    public ListAuthSystemsRequest createListAuthSystemsRequest() {
        return new ListAuthSystemsRequest();
    }

    /**
     * Create an instance of {@link DeleteLocalAccountRequest }
     * 
     */
    public DeleteLocalAccountRequest createDeleteLocalAccountRequest() {
        return new DeleteLocalAccountRequest();
    }

}
