
package com.sysfera.vishnu.api.ims;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SeDType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SeDType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UNDEFINED"/>
 *     &lt;enumeration value="UMS"/>
 *     &lt;enumeration value="TMS"/>
 *     &lt;enumeration value="FMS"/>
 *     &lt;enumeration value="IMS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SeDType")
@XmlEnum
public enum SeDType {

    UNDEFINED,
    UMS,
    TMS,
    FMS,
    IMS;

    public String value() {
        return name();
    }

    public static SeDType fromValue(String v) {
        return valueOf(v);
    }

}
