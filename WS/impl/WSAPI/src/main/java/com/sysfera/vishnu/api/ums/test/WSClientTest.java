package com.sysfera.vishnu.api.ums.test;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ListIterator;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import com.sysfera.vishnu.api.ums.AUTHACCOUNTEXISTMessage;
import com.sysfera.vishnu.api.ums.AUTHENTERRMessage;
import com.sysfera.vishnu.api.ums.AddAuthAccountRequest;
import com.sysfera.vishnu.api.ums.DeleteAuthAccountRequest;
import com.sysfera.vishnu.api.ums.INCORRECTTRANSFERCMDMessage;
import com.sysfera.vishnu.api.ums.LOGINALREADYUSEDMessage;
import com.sysfera.vishnu.api.ums.ListAuthAccountsRequest;
import com.sysfera.vishnu.api.ums.ListAuthAccountsResponse;
import com.sysfera.vishnu.api.ums.ListAuthSystemsRequest;
import com.sysfera.vishnu.api.ums.ListAuthSystemsResponse;
import com.sysfera.vishnu.api.ums.ListLocalAccountsRequest;
import com.sysfera.vishnu.api.ums.ListLocalAccountsResponse;
import com.sysfera.vishnu.api.ums.ListMachinesRequest;
import com.sysfera.vishnu.api.ums.ListMachinesResponse;
import com.sysfera.vishnu.api.ums.READONLYACCOUNTMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNAUTHACCOUNTMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNAUTHSYSTEMMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNAUTHSYSTEMTYPEMessage;
import com.sysfera.vishnu.api.ums.UpdateAuthAccountRequest;
import com.sysfera.vishnu.api.ums.VishnuUMSPortType;
import com.sysfera.vishnu.api.ums.VishnuUMSService;
import com.sysfera.vishnu.api.ums.ConnectRequest;
import com.sysfera.vishnu.api.ums.SessionCloseType;
import com.sysfera.vishnu.api.ums.ConnectResponse;
import com.sysfera.vishnu.api.ums.COMMANDRUNNINGMessage;
import com.sysfera.vishnu.api.ums.DBCONNMessage;
import com.sysfera.vishnu.api.ums.DBERRMessage;
import com.sysfera.vishnu.api.ums.DIETMessage;
import com.sysfera.vishnu.api.ums.INCORRECTTIMEOUTMessage;
import com.sysfera.vishnu.api.ums.LOCALACCOUNTEXISTMessage;
import com.sysfera.vishnu.api.ums.MACHINELOCKEDMessage;
import com.sysfera.vishnu.api.ums.NOADMINMessage;
import com.sysfera.vishnu.api.ums.ReconnectRequest;
import com.sysfera.vishnu.api.ums.SESSIONKEYEXPIREDMessage;
import com.sysfera.vishnu.api.ums.SESSIONKEYNOTFOUNDMessage;
import com.sysfera.vishnu.api.ums.SYSTEMMessage;
import com.sysfera.vishnu.api.ums.UNDEFINEDMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNCLOSUREMODEMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNLOCALACCOUNTMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNMACHINEMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNOPTIONMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNSESSIONIDMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNUSERIDMessage;
import com.sysfera.vishnu.api.ums.UNKNOWNUSERMessage;
import com.sysfera.vishnu.api.ums.UNUSABLEMACHINEMessage;
import com.sysfera.vishnu.api.ums.USERLOCKEDMessage;
import com.sysfera.vishnu.api.ums.AddLocalAccountRequest;
import com.sysfera.vishnu.api.ums.UpdateLocalAccountRequest;
import com.sysfera.vishnu.api.ums.DeleteLocalAccountRequest;
import com.sysfera.vishnu.api.ums.ChangePasswordRequest;
import com.sysfera.vishnu.api.ums.ListSessionsRequest;
import com.sysfera.vishnu.api.ums.ListSessionsResponse;
import com.sysfera.vishnu.api.ums.ListHistoryCmdRequest;
import com.sysfera.vishnu.api.ums.ListHistoryCmdResponse;
import com.sysfera.vishnu.api.ums.ListOptionsRequest;
import com.sysfera.vishnu.api.ums.ListOptionsResponse;
import com.sysfera.vishnu.api.ums.ConfigureOptionRequest;
import com.sysfera.vishnu.api.ums.CloseRequest;

public class WSClientTest {
	
	private static final String 	JBOSS_SERVER_NAME = "localhost";
	private static final Integer 	JBOSS_SERVER_PORT = 8080;
	private static final String		TEST_USER_LOGIN = "user_1";
	private static final String		TEST_USER_PASSWORD = "toto";
	
	private static final String 	MACHINEID = "machine_1";
	private static final String 	ACLOGIN	  = "toto";
	private static final String 	AUTHSYS	  = "AUTH_1";
	
	private static VishnuUMSPortType 	UMSPort;
	private static String				sessionKey;
	private static String				sessionId;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("INIT TESTS");
		
		// Initialize web service interface
		UMSPort = getUMSPort("http://" + JBOSS_SERVER_NAME + ":" + JBOSS_SERVER_PORT + "/WSAPI/VishnuUMS?wsdl");
		
		// Initialize Vishnu session
		ConnectRequest request = new ConnectRequest();
		request.setUserId(TEST_USER_LOGIN);
		request.setPassword(TEST_USER_PASSWORD);
		request.setSessionInactivityDelay(new BigInteger("10"));
		request.setClosePolicy(SessionCloseType.valueOf(new String("CLOSE_ON_TIMEOUT")));
		ConnectResponse response = UMSPort.connect(request);
		sessionKey = response.getSessionKey();
		sessionId = response.getSessionId();
		System.out.println("SESSION KEY=" + sessionKey);
		System.out.println("SESSION ID=" + sessionId);
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test(expected=UNKNOWNUSERMessage.class)
	public void testConnect() throws UNKNOWNUSERMessage, NOADMINMessage, USERLOCKEDMessage, INCORRECTTIMEOUTMessage, DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage, UNKNOWNCLOSUREMODEMessage, SYSTEMMessage, DBERRMessage, AUTHENTERRMessage {
		ConnectRequest request = new ConnectRequest();
		request.setUserId(TEST_USER_LOGIN);
		request.setPassword(TEST_USER_PASSWORD + "1");	// test with wrong password
		UMSPort.connect(request);
	}
	
	@Test (expected=UNKNOWNUSERMessage.class)
	public void testConnectWithEmptyParameters() throws UNKNOWNUSERMessage, NOADMINMessage, USERLOCKEDMessage, INCORRECTTIMEOUTMessage, DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage, UNKNOWNCLOSUREMODEMessage, SYSTEMMessage, DBERRMessage, AUTHENTERRMessage {
		ConnectRequest request = new ConnectRequest();
		request.setUserId("");
		request.setPassword("");	// test with wrong password
		UMSPort.connect(request);
	}
	
	@Test
	public void testReconnect() throws UNKNOWNUSERMessage, UNKNOWNSESSIONIDMessage, SESSIONKEYNOTFOUNDMessage, USERLOCKEDMessage, DBCONNMessage, DIETMessage, UNUSABLEMACHINEMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, AUTHENTERRMessage {
		ReconnectRequest request = new ReconnectRequest();
		request.setUserId(TEST_USER_LOGIN);
		request.setPassword(TEST_USER_PASSWORD);
		request.setSessionId(sessionId);
		System.out.println("Reconnexion avec:"+sessionId);
		UMSPort.reconnect(request);
	}

	@Test
	public void testAddAuthAccount() throws LOCALACCOUNTEXISTMessage, UNKNOWNMACHINEMessage, SESSIONKEYNOTFOUNDMessage, DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNUSABLEMACHINEMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, MACHINELOCKEDMessage, LOGINALREADYUSEDMessage, UNKNOWNAUTHSYSTEMMessage, AUTHACCOUNTEXISTMessage, NOADMINMessage, USERLOCKEDMessage {
		AddAuthAccountRequest reqAcc = new AddAuthAccountRequest();
		reqAcc.setAcLogin(ACLOGIN);
		reqAcc.setAuthSystemId(AUTHSYS);
		reqAcc.setSessionKey(sessionKey);
		reqAcc.setUserId(TEST_USER_LOGIN);
		UMSPort.addAuthAccount(reqAcc);
	}

	@Test
	public void testListAuthAccount() throws SESSIONKEYNOTFOUNDMessage, DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, UNKNOWNAUTHACCOUNTMessage, UNKNOWNAUTHSYSTEMMessage, NOADMINMessage, USERLOCKEDMessage {
		ListAuthAccountsRequest request = new ListAuthAccountsRequest();
		request.setSessionKey(sessionKey);
		request.setAuthSystemId(AUTHSYS);

		ListAuthAccountsResponse response = UMSPort.listAuthAccounts(request);
		ListIterator<ListAuthAccountsResponse.Data.Authaccount> listA = response.getData().getAuthaccount().listIterator();
		ListAuthAccountsResponse.Data.Authaccount la;
		System.out.println("Test list auth account :");
		while(listA.hasNext()){
			la = listA.next();
			System.out.println ("User id : "+la.getUserId()+", auth system id = "+la.getAuthSystemId()+"log = "+la.getAcLogin());
			listA.remove();
		}
	}
	
	@Test
	public void testListAuthSys() throws SESSIONKEYNOTFOUNDMessage, DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, UNKNOWNAUTHACCOUNTMessage, UNKNOWNAUTHSYSTEMMessage, NOADMINMessage, USERLOCKEDMessage, UNKNOWNAUTHSYSTEMTYPEMessage {
		ListAuthSystemsRequest request = new ListAuthSystemsRequest();
		request.setSessionKey(sessionKey);
		request.setAuthSystemId(AUTHSYS);

		ListAuthSystemsResponse response = UMSPort.listAuthSystems(request);
		ListIterator<ListAuthSystemsResponse.Data.Authsystem> listA = response.getData().getAuthsystem().listIterator();
		ListAuthSystemsResponse.Data.Authsystem la;
		System.out.println("Test list auth account :");
		while(listA.hasNext()){
			la = listA.next();
			System.out.println ("Name : "+la.getName()+", auth system id = "+la.getAuthSystemId()+"log = "+la.getAuthLogin());
			listA.remove();
		}
	}
	
	@Test
	public void testUpdateAuthAccount() throws LOCALACCOUNTEXISTMessage, UNKNOWNMACHINEMessage, SESSIONKEYNOTFOUNDMessage, DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNUSABLEMACHINEMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, MACHINELOCKEDMessage, LOGINALREADYUSEDMessage, UNKNOWNAUTHSYSTEMMessage, AUTHACCOUNTEXISTMessage, NOADMINMessage, USERLOCKEDMessage, UNKNOWNAUTHACCOUNTMessage {
		UpdateAuthAccountRequest reqAcc = new UpdateAuthAccountRequest();
		reqAcc.setAcLogin("tutu");
		reqAcc.setAuthSystemId(AUTHSYS);
		reqAcc.setSessionKey(sessionKey);
		UMSPort.updateAuthAccount(reqAcc);
	}
	
	@Test
	public void testDeleteAuthAccount() throws LOCALACCOUNTEXISTMessage, UNKNOWNMACHINEMessage, SESSIONKEYNOTFOUNDMessage, DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNUSABLEMACHINEMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, MACHINELOCKEDMessage, LOGINALREADYUSEDMessage, UNKNOWNAUTHSYSTEMMessage, AUTHACCOUNTEXISTMessage, NOADMINMessage, USERLOCKEDMessage, UNKNOWNAUTHACCOUNTMessage {
		DeleteAuthAccountRequest reqAcc = new DeleteAuthAccountRequest();
		reqAcc.setAuthSystemId(AUTHSYS);
		reqAcc.setSessionKey(sessionKey);
		UMSPort.deleteAuthAccount(reqAcc);
	}
	
	@Test
	public void testAddLocalAccount() throws LOCALACCOUNTEXISTMessage, UNKNOWNMACHINEMessage, SESSIONKEYNOTFOUNDMessage, DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNUSABLEMACHINEMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, MACHINELOCKEDMessage, LOGINALREADYUSEDMessage {
		AddLocalAccountRequest reqAcc = new AddLocalAccountRequest();
		reqAcc.setSessionKey(sessionKey);
		reqAcc.setUserId(TEST_USER_LOGIN);
		reqAcc.setMachineId(MACHINEID);
		reqAcc.setAcLogin(ACLOGIN);
		reqAcc.setSshKeyPath("/home/keo/.ssh/id_dsa.pub");
		reqAcc.setHomeDirectory("/home/toto");
		UMSPort.addLocalAccount(reqAcc);
	}
	
	@Test
	public void testListLocalAccount() throws SESSIONKEYNOTFOUNDMessage, DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		ListLocalAccountsRequest request = new ListLocalAccountsRequest();
		request.setSessionKey(sessionKey);
		ListLocalAccountsResponse response = UMSPort.listLocalAccounts(request);
		ListIterator<ListLocalAccountsResponse.Data.Localaccount> listA = response.getData().getLocalaccount().listIterator();
		ListLocalAccountsResponse.Data.Localaccount la;
		System.out.println("Test list account :");
		while(listA.hasNext()){
			la = listA.next();
			System.out.println ("User id : "+la.getUserId()+", machine id = "+la.getMachineId()+" ssh = "+la.getSshKeyPath()+"log = "+la.getAcLogin());
			listA.remove();
		}
	}

	@Test
	public void testUpdateLocalAccount() throws UNKNOWNMACHINEMessage, SESSIONKEYNOTFOUNDMessage, DBCONNMessage, UNKNOWNLOCALACCOUNTMessage, UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, LOGINALREADYUSEDMessage {
		final String NEW_LOGIN = "titi";
		
		{
			// Update the local account login
			UpdateLocalAccountRequest request = new UpdateLocalAccountRequest();
			request.setSessionKey(sessionKey);
			request.setMachineId(MACHINEID);
			request.setAcLogin(NEW_LOGIN);
			UMSPort.updateLocalAccount(request);
		}
		
		{
			// Retrieve the new local account and check login
			ListLocalAccountsRequest request = new ListLocalAccountsRequest();
			request.setSessionKey(sessionKey);
			request.setUserId(TEST_USER_LOGIN);
			request.setMachineId(MACHINEID);
			ListLocalAccountsResponse response = UMSPort.listLocalAccounts(request);
			assertTrue(response.getData().getLocalaccount().size() == 1);
			assertTrue(response.getData().getLocalaccount().get(0).getAcLogin().equals(NEW_LOGIN));
		}
	}
	
	@Test
	public void testDeleteLocalAccount() throws SESSIONKEYNOTFOUNDMessage, DBCONNMessage, UNKNOWNLOCALACCOUNTMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, LOCALACCOUNTEXISTMessage, UNKNOWNMACHINEMessage, UNKNOWNUSERIDMessage, UNUSABLEMACHINEMessage, MACHINELOCKEDMessage {
		{
			// Delete local account
			DeleteLocalAccountRequest request = new DeleteLocalAccountRequest();
			request.setSessionKey(sessionKey);
			request.setUserId(TEST_USER_LOGIN);
			request.setMachineId(MACHINEID);			
			UMSPort.deleteLocalAccount(request);
		}
	}

	@Test
	public void testListMachine() throws SESSIONKEYNOTFOUNDMessage, DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		ListMachinesRequest request = new ListMachinesRequest();
		request.setSessionKey(sessionKey);
		request.setListAllMachine(true);
		ListMachinesResponse response = UMSPort.listMachines(request);
		ListIterator<ListMachinesResponse.Data.Machine> listm = response.getData().getMachine().listIterator();
		ListMachinesResponse.Data.Machine m;
		while(listm.hasNext()){
			m = listm.next();
			System.out.println ("Machine name : "+m.getName()+", machine id = "+m.getMachineId()+" lieu = "+m.getSite());
			listm.remove();
		}
	}

	@Test
	public void testListHistoryCmd() throws SESSIONKEYNOTFOUNDMessage, DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		ListHistoryCmdRequest request = new ListHistoryCmdRequest();
		request.setSessionKey(sessionKey);
		ListHistoryCmdResponse response = UMSPort.listHistoryCmd(request);
		ListIterator<ListHistoryCmdResponse.Data.Command> listH = response.getData().getCommand().listIterator();
		ListHistoryCmdResponse.Data.Command lh;
		while(listH.hasNext()){
			lh = listH.next();
			System.out.println ("Machine id : "+lh.getMachineId()+", session id  = "+lh.getSessionId()+", cmd = "+lh.getCommandId()+", description "+lh.getCmdDescription());
			listH.remove();
		}
	}

	@Test
	public void testListOptions() throws SESSIONKEYNOTFOUNDMessage, NOADMINMessage, DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, UNKNOWNOPTIONMessage {
		ListOptionsRequest request = new ListOptionsRequest();
		request.setSessionKey(sessionKey);
		request.setListAllDeftValue(true);
		ListOptionsResponse response = UMSPort.listOptions(request);
		ListIterator<ListOptionsResponse.Data.Optionvalue> listO = response.getData().getOptionvalue().listIterator();
		ListOptionsResponse.Data.Optionvalue lo;
		while(listO.hasNext()){
			lo = listO.next();
			System.out.println ("Name : "+lo.getOptionName()+", value : "+lo.getValue());
			listO.remove();
		}
	}

	@Test
	public void testConfigureOption() throws SESSIONKEYNOTFOUNDMessage, INCORRECTTIMEOUTMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNCLOSUREMODEMessage, SYSTEMMessage, DBERRMessage, UNKNOWNOPTIONMessage, NOADMINMessage, UNKNOWNUSERIDMessage, INCORRECTTRANSFERCMDMessage {
		{
			ConfigureOptionRequest request = new ConfigureOptionRequest();
			request.setSessionKey(sessionKey);
			request.setOptionName("VISHNU_CLOSE_POLICY");
			request.setValue("1");
			UMSPort.configureOption(request);
		}
		{
			ListOptionsRequest request = new ListOptionsRequest();
			request.setSessionKey(sessionKey);
			request.setOptionName("VISHNU_CLOSE_POLICY");
			ListOptionsResponse response = UMSPort.listOptions(request);
			assertTrue(response.getData().getOptionvalue().size() == 1);
			assertTrue(response.getData().getOptionvalue().get(0).getValue().equals("1"));
		}
	}

	@Test
	public void testListSessions() throws SESSIONKEYNOTFOUNDMessage, DBCONNMessage, UNKNOWNUSERIDMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNCLOSUREMODEMessage, SYSTEMMessage, DBERRMessage {
		ListSessionsRequest request = new ListSessionsRequest();
		request.setSessionKey(sessionKey);
		ListSessionsResponse response = UMSPort.listSessions(request);
		ListIterator<ListSessionsResponse.Data.Session> listS = response.getData().getSession().listIterator();
		ListSessionsResponse.Data.Session ls;
		while(listS.hasNext()){
			ls = listS.next();
			System.out.println ("User id : "+ls.getUserId()+", session key  = "+ls.getSessionKey()+" timeout = "+ls.getTimeout());
			listS.remove();
		}
	}

	@Test
	public void testChangePassword() throws UNKNOWNUSERMessage, USERLOCKEDMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SYSTEMMessage, DBERRMessage, READONLYACCOUNTMessage, AUTHENTERRMessage {
		ChangePasswordRequest request = new ChangePasswordRequest();
		request.setUserId(TEST_USER_LOGIN);
		request.setPassword(TEST_USER_PASSWORD);
		request.setPasswordNew("456");
		UMSPort.changePassword(request);
		request.setPassword("456");
		request.setPasswordNew(TEST_USER_PASSWORD);
		UMSPort.changePassword(request);
	}
	
	@Test
	public void testClose() throws SESSIONKEYNOTFOUNDMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, COMMANDRUNNINGMessage, SYSTEMMessage, DBERRMessage {
		CloseRequest request = new CloseRequest();
		request.setSessionKey(sessionKey);
		UMSPort.close(request);
	}
	
	private static VishnuUMSPortType getUMSPort(String endpointURI) throws MalformedURLException   {

		URL wsdlURL = new URL(endpointURI);
		VishnuUMSService service = new VishnuUMSService(wsdlURL);
		return service.getVishnuUMSPort();
	}

}
