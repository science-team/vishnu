
package com.sysfera.vishnu.api.ims;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="data" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="systeminfo" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                           &lt;attribute name="memory" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="diskSpace" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="machineId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data"
})
@XmlRootElement(name = "getSystemInfoResponse")
public class GetSystemInfoResponse {

    protected GetSystemInfoResponse.Data data;

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link GetSystemInfoResponse.Data }
     *     
     */
    public GetSystemInfoResponse.Data getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetSystemInfoResponse.Data }
     *     
     */
    public void setData(GetSystemInfoResponse.Data value) {
        this.data = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="systeminfo" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *                 &lt;attribute name="memory" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="diskSpace" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="machineId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "systeminfo"
    })
    public static class Data {

        @XmlElement(nillable = true)
        protected List<GetSystemInfoResponse.Data.Systeminfo> systeminfo;

        /**
         * Gets the value of the systeminfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the systeminfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSysteminfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GetSystemInfoResponse.Data.Systeminfo }
         * 
         * 
         */
        public List<GetSystemInfoResponse.Data.Systeminfo> getSysteminfo() {
            if (systeminfo == null) {
                systeminfo = new ArrayList<GetSystemInfoResponse.Data.Systeminfo>();
            }
            return this.systeminfo;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *       &lt;/sequence>
         *       &lt;attribute name="memory" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="diskSpace" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="machineId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Systeminfo {

            @XmlAttribute(name = "memory")
            protected Long memory;
            @XmlAttribute(name = "diskSpace")
            protected Long diskSpace;
            @XmlAttribute(name = "machineId")
            protected String machineId;

            /**
             * Gets the value of the memory property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getMemory() {
                return memory;
            }

            /**
             * Sets the value of the memory property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setMemory(Long value) {
                this.memory = value;
            }

            /**
             * Gets the value of the diskSpace property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getDiskSpace() {
                return diskSpace;
            }

            /**
             * Sets the value of the diskSpace property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setDiskSpace(Long value) {
                this.diskSpace = value;
            }

            /**
             * Gets the value of the machineId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMachineId() {
                return machineId;
            }

            /**
             * Sets the value of the machineId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMachineId(String value) {
                this.machineId = value;
            }

        }

    }

}
