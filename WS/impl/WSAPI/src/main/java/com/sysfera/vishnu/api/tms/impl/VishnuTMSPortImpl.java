package com.sysfera.vishnu.api.tms.impl;

import java.math.BigInteger;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.transaction.SystemException;

import com.sysfera.vishnu.api.tms.BATCHSCHEDULERERRORFault;
import com.sysfera.vishnu.api.tms.BATCHSCHEDULERERRORMessage;
import com.sysfera.vishnu.api.tms.CancelJobRequest;
import com.sysfera.vishnu.api.tms.CancelJobResponse;
import com.sysfera.vishnu.api.tms.DBCONNFault;
import com.sysfera.vishnu.api.tms.DBCONNMessage;
import com.sysfera.vishnu.api.tms.DBERRFault;
import com.sysfera.vishnu.api.tms.DBERRMessage;
import com.sysfera.vishnu.api.tms.DIETFault;
import com.sysfera.vishnu.api.tms.DIETMessage;
import com.sysfera.vishnu.api.tms.GetCompletedJobsOutputRequest;
import com.sysfera.vishnu.api.tms.GetCompletedJobsOutputResponse;
import com.sysfera.vishnu.api.tms.GetJobInfoRequest;
import com.sysfera.vishnu.api.tms.GetJobInfoResponse;
import com.sysfera.vishnu.api.tms.GetJobOutputRequest;
import com.sysfera.vishnu.api.tms.GetJobOutputResponse;
import com.sysfera.vishnu.api.tms.GetJobProgressRequest;
import com.sysfera.vishnu.api.tms.GetJobProgressResponse;
import com.sysfera.vishnu.api.tms.INVALIDPARAMMessage;
import com.sysfera.vishnu.api.tms.ListJobsRequest;
import com.sysfera.vishnu.api.tms.ListJobsResponse;
import com.sysfera.vishnu.api.tms.ListQueuesRequest;
import com.sysfera.vishnu.api.tms.ListQueuesResponse;
import com.sysfera.vishnu.api.tms.PERMISSIONDENIEDFault;
import com.sysfera.vishnu.api.tms.PERMISSIONDENIEDMessage;
import com.sysfera.vishnu.api.tms.SESSIONKEYEXPIREDFault;
import com.sysfera.vishnu.api.tms.SESSIONKEYEXPIREDMessage;
import com.sysfera.vishnu.api.tms.SYSTEMFault;
import com.sysfera.vishnu.api.tms.SYSTEMMessage;
import com.sysfera.vishnu.api.tms.SubmitJobRequest;
import com.sysfera.vishnu.api.tms.SubmitJobResponse;
import com.sysfera.vishnu.api.tms.UNDEFINEDFault;
import com.sysfera.vishnu.api.tms.UNDEFINEDMessage;
import com.sysfera.vishnu.api.tms.UNKNOWNBATCHSCHEDULERFault;
import com.sysfera.vishnu.api.tms.UNKNOWNBATCHSCHEDULERMessage;
import com.sysfera.vishnu.api.tms.UNKNOWNMACHINEFault;
import com.sysfera.vishnu.api.tms.UNKNOWNMACHINEMessage;
import com.sysfera.vishnu.api.tms.VishnuTMSPortType;
import com.sysfera.vishnu.api.vishnu.internal.InternalTMSException;
import com.sysfera.vishnu.api.vishnu.internal.Job;
import com.sysfera.vishnu.api.vishnu.internal.JobResult;
import com.sysfera.vishnu.api.vishnu.internal.ListJobResults;
import com.sysfera.vishnu.api.vishnu.internal.ListJobs;
import com.sysfera.vishnu.api.vishnu.internal.ListJobsOptions;
import com.sysfera.vishnu.api.vishnu.internal.ListProgression;
import com.sysfera.vishnu.api.vishnu.internal.ListQueues;
import com.sysfera.vishnu.api.vishnu.internal.LoadCriterion;
import com.sysfera.vishnu.api.vishnu.internal.ProgressOptions;
import com.sysfera.vishnu.api.vishnu.internal.Queue;
import com.sysfera.vishnu.api.vishnu.internal.Progression;
import com.sysfera.vishnu.api.vishnu.internal.SubmitOptions;
import com.sysfera.vishnu.api.vishnu.internal.VISHNU;
import com.sysfera.vishnu.api.tms.JobStatus;
import com.sysfera.vishnu.api.tms.JobPriority;
import com.sysfera.vishnu.api.tms.QueuePriority;
import com.sysfera.vishnu.api.tms.QueueStatus;
import com.sysfera.vishnu.api.vishnu.internal.InternalUMSException;

@WebService(targetNamespace = "urn:ResourceProxy", name = "VishnuTMS", serviceName = "VishnuTMSService")
    @Remote(VishnuTMSPortType.class)
    @Stateless
    public class VishnuTMSPortImpl implements VishnuTMSPortType {
	
	// Load the vishnu library
	//    static {
	//	LoadLibrary.LoadVishnuLibrary();
	//    }

	public GetJobInfoResponse getJobInfo(GetJobInfoRequest parameters)
	    throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage,
		   UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage,
		   DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage,	       
		   SYSTEMMessage, DBERRMessage {
	    GetJobInfoResponse res = new GetJobInfoResponse();
	    Job j = new Job();
	    if (parameters.getJobId() == null) {
		parameters.setJobId("");
	    }
	    if (parameters.getMachineId() == null) {
		parameters.setMachineId("");
	    }
	    if (parameters.getSessionKey() == null) {
		parameters.setSessionKey("");
	    }
	    try {
		VISHNU.getJobInfo(parameters.getSessionKey(), parameters.getMachineId(), parameters.getJobId(), j);
	    }catch (InternalTMSException e){
		String code="", msg="";                                                                                                                         
		String tmp[]={"", ""};                                                                                                                          
		splitExcep(e.getMessage(), tmp);                                                                                                                
		code = tmp[0];                                                                                                                                  
		msg = tmp[1];
		switch (new Integer(code)){
		case 1 :
		    throw new DIETMessage(msg, new DIETFault());
		case 2 :
		    throw new DBERRMessage(msg, new DBERRFault());
		case 3  :
		    throw new DBCONNMessage(msg, new DBCONNFault());
		case 4 :
		    throw new SYSTEMMessage(msg, new SYSTEMFault());
		case 29 :
		    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
		case 32 :
		    throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
		case 101 :
		    throw new UNKNOWNBATCHSCHEDULERMessage(msg, new UNKNOWNBATCHSCHEDULERFault());
		case 102 :
		    throw new BATCHSCHEDULERERRORMessage(msg, new BATCHSCHEDULERERRORFault());
		case 104 :
		    throw new PERMISSIONDENIEDMessage(msg, new PERMISSIONDENIEDFault());
		default :
		    throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
		}
	    }
	    res.setEndDate(j.getEndDate());
	    res.setErrorPath(j.getErrorPath());
	    res.setGroupName(j.getGroupName());
	    res.setJobDescription(j.getJobDescription());
	    res.setJobId(j.getJobId());
	    res.setJobName(j.getJobName());
	    res.setJobPath(j.getJobPath());
	    res.setJobPrio(JobPriority
			   .fromValue(com.sysfera.vishnu.api.tms.data.JobPrio.get(
										  j.getJobPrio()).getLiteral()));
	    res.setJobQueue(j.getJobQueue());
	    res.setJobWorkingDir(j.getJobWorkingDir());
	    // TODO IF POSSIBLE FIX DIRTY PARAM CHANGE
	    String l = new Long(j.getMemLimit()).toString();
	    BigInteger b = new BigInteger(l);
	    res.setMemLimit(b);
	    l = new Long(j.getNbCpus()).toString();
	    b = new BigInteger(l);
	    res.setNbCpus(b);
	    l = new Long(j.getNbNodes()).toString();
	    b = new BigInteger(l);
	    res.setNbNodes(b);
	    res.setNbNodesAndCpuPerNode(j.getNbNodesAndCpuPerNode());
	    res.setOutputPath(j.getOutputPath());
	    res.setOwner(j.getOwner());
	    res.setSessionId(j.getSessionId());
	    res.setStatus(JobStatus
			  .fromValue(com.sysfera.vishnu.api.tms.data.JobStatus.get(
										j.getStatus()).getLiteral()));
	    res.setSubmitDate(j.getSubmitDate());
	    res.setSubmitMachineId(j.getSubmitMachineId());
	    res.setSubmitMachineName(j.getSubmitMachineName());
	    res.setWallClockLimit(j.getWallClockLimit());
	    res.setBatchJobId(j.getBatchJobId());
	    return res;
	}

	public ListJobsResponse listJobs(ListJobsRequest parameters)
	    throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage,
		   UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage,
		   DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage,
		   SYSTEMMessage, DBERRMessage {
	    int i;
	    ListJobsResponse res = new ListJobsResponse();
	    ListJobsResponse.Data dat = new ListJobsResponse.Data();
	    ListJobs li = new ListJobs();
	    ListJobsOptions op = new ListJobsOptions();

	    if (parameters.getFromSubmitDate() != null) {
	    	op.setFromSubmitDate(parameters.getFromSubmitDate());
	    }
	    if(parameters.getJobId() != null) {
	    	op.setJobId(parameters.getJobId());
	    }
	    if (parameters.getMachineId() == null) {
	    	parameters.setMachineId("");
	    }
	    if (parameters.getNbCpu() != null) {
	    	op.setNbCpu(parameters.getNbCpu().intValue());
	    }
	    if (parameters.getOwner() != null) {
	    	op.setOwner(parameters.getOwner());
	    }
	    if (parameters.getPriority() != null) {
	    	op.setPriority(com.sysfera.vishnu.api.tms.data.JobPrio
			       .getByName(parameters.getPriority().value()).getValue());
	    }
	    if (parameters.getQueue() != null) {
	    	op.setQueue(parameters.getQueue());
	    }
	    if (parameters.getSessionKey() == null) {
	    	parameters.setSessionKey("");
	    }
	    if (parameters.getStatus() != null) {
	    	op.setStatus(com.sysfera.vishnu.api.tms.data.JobStatus
			     .getByName(parameters.getStatus().value()).getValue());
	    }
	    if (parameters.getToSubmitDate() != null) {
	    	op.setToSubmitDate(parameters.getToSubmitDate());
	    }
	    if (parameters.isBatchJob() != null) {
	    	op.setBatchJob(parameters.isBatchJob());
	    }
	    if (parameters.getMultipleStatus() != null) {
	    	op.setMultipleStatus(parameters.getMultipleStatus());
	    }
	    
	    try {
	    	VISHNU.listJobs(parameters.getSessionKey(), parameters.getMachineId(), li, op);
	    } catch (InternalTMSException e){
	        String code="", msg="";                                                                                                                         
	        String tmp[]={"", ""};                                                                                                                          
	        splitExcep(e.getMessage(), tmp);                                                                                                                
	        code = tmp[0];                                                                                                                                  
	        msg = tmp[1];
	        switch (new Integer(code)){
		case 1 :
		    throw new DIETMessage(msg, new DIETFault());
		case 2 :
		    throw new DBERRMessage(msg, new DBERRFault());
		case 3  :
		    throw new DBCONNMessage(msg, new DBCONNFault());
		case 4 :
		    throw new SYSTEMMessage(msg, new SYSTEMFault());
		case 29 :
		    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
		case 32 :
		    throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
		case 101 :
		    throw new UNKNOWNBATCHSCHEDULERMessage(msg, new UNKNOWNBATCHSCHEDULERFault());
		case 102 :
		    throw new BATCHSCHEDULERERRORMessage(msg, new BATCHSCHEDULERERRORFault());
		case 104 :
		    throw new PERMISSIONDENIEDMessage(msg, new PERMISSIONDENIEDFault());
		default :
		    throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
	        
	        }	
	    }
	    long running = 0;
	    long waiting = 0;
	    for (i = 0; i < li.getJobs().size(); i++) {
	    	ListJobsResponse.Data.Job opva = new ListJobsResponse.Data.Job();
	    	opva.setEndDate(li.getJobs().get(i).getEndDate());
	    	opva.setErrorPath(li.getJobs().get(i).getErrorPath());
	    	opva.setGroupName(li.getJobs().get(i).getGroupName());
	    	opva.setJobDescription(li.getJobs().get(i).getJobDescription());
	    	opva.setJobId(li.getJobs().get(i).getJobId());
	    	opva.setJobName(li.getJobs().get(i).getJobName());
	    	opva.setJobPath(li.getJobs().get(i).getJobPath());
	    	opva.setJobPrio(JobPriority
				.fromValue(com.sysfera.vishnu.api.tms.data.JobPrio.get(
										       li.getJobs().get(i).getJobPrio()).getLiteral()));
	    	opva.setJobQueue(li.getJobs().get(i).getJobQueue());
	    	opva.setJobWorkingDir(li.getJobs().get(i).getJobWorkingDir());
	    	Long l = new Long(li.getJobs().get(i).getMemLimit());
	    	BigInteger b = new BigInteger(l.toString());
	    	opva.setMemLimit(b);
	    	l = new Long(li.getJobs().get(i).getNbCpus());
	    	b = new BigInteger(l.toString());
	    	opva.setNbCpus(b);
	    	l = new Long(li.getJobs().get(i).getNbNodes());
	    	b = new BigInteger(l.toString());
	    	opva.setNbNodes(b);
	    	opva.setNbNodesAndCpuPerNode(li.getJobs().get(i).getNbNodesAndCpuPerNode());
	    	opva.setOutputPath(li.getJobs().get(i).getOutputPath());
	    	opva.setOwner(li.getJobs().get(i).getOwner());
	    	opva.setSessionId(li.getJobs().get(i).getSessionId());
	    	opva.setStatus(JobStatus
			       .fromValue(com.sysfera.vishnu.api.tms.data.JobStatus.get(
										     li.getJobs().get(i).getStatus()).getLiteral()));
	    	if (com.sysfera.vishnu.api.tms.data.JobStatus.get(
										     li.getJobs().get(i).getStatus()).getLiteral().compareTo("RUNNING")==0) {
	    		running ++;
	    	}
	    	if (com.sysfera.vishnu.api.tms.data.JobStatus.get(
				     li.getJobs().get(i).getStatus()).getLiteral().compareTo("SUBMITTED")==0 || com.sysfera.vishnu.api.tms.data.JobStatus.get(
						     li.getJobs().get(i).getStatus()).getLiteral().compareTo("QUEUED")==0 || com.sysfera.vishnu.api.tms.data.JobStatus.get(
								     li.getJobs().get(i).getStatus()).getLiteral().compareTo("WAITING")==0) {
	    		waiting ++;
}	    	
	    	opva.setSubmitDate(li.getJobs().get(i).getSubmitDate());
	    	opva.setSubmitMachineId(li.getJobs().get(i).getSubmitMachineId());
	    	opva.setSubmitMachineName(li.getJobs().get(i).getSubmitMachineName());
	    	opva.setWallClockLimit(li.getJobs().get(i).getWallClockLimit());
	    	opva.setBatchJobId(li.getJobs().get(i).getBatchJobId());
	    	dat.getJob().add(opva);
	    }
	    res.setNbJobs(li.getJobs().size());
	    res.setNbRunningJobs(running);
	    res.setNbWaitingJobs(waiting);
	    res.setData(dat);
	    return res;
	}

	public SubmitJobResponse submitJob(SubmitJobRequest parameters)
	    throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage,
		   UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage,
		   DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage,
		   SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
	    SubmitJobResponse res = new SubmitJobResponse();
	    SubmitOptions op = new SubmitOptions();
	    Job j = new Job();
	    if (parameters.getErrorPath() != null) {
		op.setErrorPath(parameters.getErrorPath());
	    }
	    if (parameters.getMachineId() == null) {
		parameters.setMachineId("");
	    }
	    if (parameters.getMemory() != null) {
		op.setMemory(parameters.getMemory().intValue());
	    }
	    if (parameters.getName() != null) {
		op.setName(parameters.getName());
	    }
	    if (parameters.getNbCpu() != null) {
		op.setNbCpu(parameters.getNbCpu().intValue());
	    }
	    if (parameters.getNbNodesAndCpuPerNode() != null) {
		op.setNbNodesAndCpuPerNode(parameters.getNbNodesAndCpuPerNode());
	    }
	    if (parameters.getOutputPath() != null) {
		op.setOutputPath(parameters.getOutputPath());
	    }
	    if (parameters.getQueue() != null) {
		op.setQueue(parameters.getQueue());
	    }
	    if (parameters.getMailNotification()!= null) {
	    	op.setMailNotification(parameters.getMailNotification());
	    }
	    if (parameters.getMailNotifyUser()!= null) {
	    	op.setMailNotifyUser(parameters.getMailNotifyUser());
	    }
	    if (parameters.getGroup()!=null) {
	    	op.setGroup(parameters.getGroup());
	    }
	    if (parameters.getWorkingDir()!=null) {
	    	op.setWorkingDir(parameters.getWorkingDir());
	    }
	    if (parameters.getCpuTime() != null) {
	    	op.setCpuTime(parameters.getCpuTime());
	    }
	    if (parameters.getWallTime() != null) {
	    	op.setWallTime(parameters.getWallTime().intValue());
	    }
	    if (parameters.isSelectQueueAutom()!=null) {
	    	op.setSelectQueueAutom(parameters.isSelectQueueAutom());
	    }
	    if(parameters.getData()!=null) {
	    	if (parameters.getData().getLoadcriterion()!=null) {
	    		LoadCriterion loadCriterion = new LoadCriterion();
	    		loadCriterion.setLoadType(com.sysfera.vishnu.api.tms.data.LoadType.getByName(
	    				parameters.getData().getLoadcriterion().get(0).getLoadType().value()).getValue());
	    		op.setCriterion(loadCriterion);
	    	}
	    }
	    
	    if (parameters.getScriptFilePath() == null) {
	    	parameters.setScriptFilePath("");
	    }
	    if (parameters.getSessionKey() == null) {
	    	parameters.setSessionKey("");
	    }
	  
	   
	    
	    try{
		VISHNU.submitJob(parameters.getSessionKey(), parameters.getMachineId(), parameters.getScriptFilePath(), j, op);
	    } catch ( InternalTMSException e) {
	        String code="", msg="";                                                                                                                         
	        String tmp[]={"", ""};                                                                                                                          
	        splitExcep(e.getMessage(), tmp);                                                                                                                
	        code = tmp[0];                                                                                                                                  
	        msg = tmp[1];
	        switch (new Integer(code)){
		case 1 :
		    throw new DIETMessage(msg, new DIETFault());
		case 2 :
		    throw new DBERRMessage(msg, new DBERRFault());
		case 3  :
		    throw new DBCONNMessage(msg, new DBCONNFault());
		case 4 :
		    throw new SYSTEMMessage(msg, new SYSTEMFault());
		case 29 :
		    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
		case 32 :
		    throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
		case 101 :
		    throw new UNKNOWNBATCHSCHEDULERMessage(msg, new UNKNOWNBATCHSCHEDULERFault());
		case 102 :
		    throw new BATCHSCHEDULERERRORMessage(msg, new BATCHSCHEDULERERRORFault());
		case 104 :
		    throw new PERMISSIONDENIEDMessage(msg, new PERMISSIONDENIEDFault());
		default :
		    throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
	        
	        }			
	    }
	    res.setEndDate(j.getEndDate());
	    res.setErrorPath(j.getErrorPath());
	    res.setGroupName(j.getGroupName());
	    res.setJobDescription(j.getJobDescription());
	    res.setJobId(j.getJobId());
	    res.setJobName(j.getJobName());
	    res.setJobPath(j.getJobPath());
	    res.setJobPrio(JobPriority
			   .fromValue(com.sysfera.vishnu.api.tms.data.JobPrio.get(
										  j.getJobPrio()).getLiteral()));
	    res.setJobQueue(j.getJobQueue());
	    res.setJobWorkingDir(j.getJobWorkingDir());
	    // TODO IF POSSIBLE FIX DIRTY PARAM CHANGE
	    String l = new Long(j.getMemLimit()).toString();
	    BigInteger b = new BigInteger(l);
	    res.setMemLimit(b);
	    l = new Long(j.getNbCpus()).toString();
	    b = new BigInteger(l);
	    res.setNbCpus(b);
	    l = new Long(j.getNbNodes()).toString();
	    b = new BigInteger(l);
	    res.setNbNodes(b);
	    res.setNbNodesAndCpuPerNode(j.getNbNodesAndCpuPerNode());
	    res.setOutputPath(j.getOutputPath());
	    res.setOwner(j.getOwner());
	    res.setSessionId(j.getSessionId());
	    res.setStatus(JobStatus
			  .fromValue(com.sysfera.vishnu.api.tms.data.JobStatus.get(
										j.getStatus()).getLiteral()));
	    res.setSubmitDate(j.getSubmitDate());
	    res.setSubmitMachineId(j.getSubmitMachineId());
	    res.setSubmitMachineName(j.getSubmitMachineName());
	    res.setWallClockLimit(j.getWallClockLimit());
	    res.setBatchJobId(j.getBatchJobId());
	    return res;
	}

	public ListQueuesResponse listQueues(ListQueuesRequest parameters)
	    throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage,
		   UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage,
		   DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage,
		   SYSTEMMessage, DBERRMessage {
	    ListQueuesResponse res = new ListQueuesResponse();
	    int i;
	    ListQueuesResponse.Data dat = new ListQueuesResponse.Data();
	    ListQueues li = new ListQueues();
	    if (parameters.getMachineId() == null) {
	    	parameters.setMachineId("");
	    }
	    if (parameters.getSessionKey() == null) {
	    	parameters.setSessionKey("");
	    }
	    if (parameters.getQueueName() == null) {
	    	parameters.setQueueName("");
	    }
	    try {
	    	VISHNU.listQueues(parameters.getSessionKey(), parameters.getMachineId(), li, parameters.getQueueName());
	    } catch (InternalTMSException e) {
	        String code="", msg="";                                                                                                                         
	        String tmp[]={"", ""};                                                                                                                          
	        splitExcep(e.getMessage(), tmp);                                                                                                                
	        code = tmp[0];                                                                                                                                  
	        msg = tmp[1];
	        switch (new Integer(code)){
		case 1 :
		    throw new DIETMessage(msg, new DIETFault());
		case 2 :
		    throw new DBERRMessage(msg, new DBERRFault());
		case 3  :
		    throw new DBCONNMessage(msg, new DBCONNFault());
		case 4 :
		    throw new SYSTEMMessage(msg, new SYSTEMFault());
		case 29 :
		    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
		case 32 :
		    throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
		case 101 :
		    throw new UNKNOWNBATCHSCHEDULERMessage(msg, new UNKNOWNBATCHSCHEDULERFault());
		case 102 :
		    throw new BATCHSCHEDULERERRORMessage(msg, new BATCHSCHEDULERERRORFault());
		case 104 :
		    throw new PERMISSIONDENIEDMessage(msg, new PERMISSIONDENIEDFault());
		default :
		    throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
	        
	        }
	    }
	    for (i = 0; i < li.getQueues().size(); i++) {
	    	ListQueuesResponse.Data.Queue opva = new ListQueuesResponse.Data.Queue();
	    	Queue q = li.getQueues().get(i);
	    	opva.setDescription(q.getDescription());
	    	Long l = new Long(q.getMaxJobCpu());
	    	BigInteger b = new BigInteger(l.toString());
	    	opva.setMaxJobCpu(b);
	    	l = new Long(q.getMaxProcCpu());
	    	b = new BigInteger(l.toString());
	    	opva.setMaxProcCpu(b);
	    	l = new Long(q.getMemory());
	    	b = new BigInteger(l.toString());
	    	opva.setMemory(b);
	    	opva.setName(q.getName());
	    	l = new Long(q.getNbJobsInQueue());
	    	b = new BigInteger(l.toString());
	    	opva.setNbJobsInQueue(b);
	    	l = new Long(q.getNbRunningJobs());
	    	b = new BigInteger(l.toString());
	    	opva.setNbRunningJobs(b);
	    	l = new Long(q.getNode());
	    	b = new BigInteger(l.toString());
	    	opva.setNode(b);
	    	
	    	opva.setPriority(QueuePriority
				 .fromValue(com.sysfera.vishnu.api.tms.data.QueuePrio.get(
											  q.getPriority()).getLiteral()));
	    	opva.setState(QueueStatus
			      .fromValue(com.sysfera.vishnu.api.tms.data.QueueStatus.get(
											 q.getState()).getLiteral()));
	    	opva.setWallTime(q.getWallTime());
	    	dat.getQueue().add(opva);
	    }
	    BigInteger b = new BigInteger(new Long(li.getQueues().size()).toString());
	    res.setNbQueues(b);
	    res.setData(dat);
	    return res;
	}

	public GetCompletedJobsOutputResponse getCompletedJobsOutput(
								     GetCompletedJobsOutputRequest parameters)
	    throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage,
		   UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage,
		   DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage,
		   SYSTEMMessage, DBERRMessage {
	    GetCompletedJobsOutputResponse res = new GetCompletedJobsOutputResponse();
	    ListJobResults li = new ListJobResults();
	    GetCompletedJobsOutputResponse.Data dat = new GetCompletedJobsOutputResponse.Data();
	    if (parameters.getMachineId() == null) {
		parameters.setMachineId("");
	    }
	    if (parameters.getSessionKey() == null) {
		parameters.setSessionKey("");
	    }
	    if (parameters.getOutDir() == null) {
		parameters.setOutDir("");
	    }

	    try {
		VISHNU.getCompletedJobsOutput(parameters.getSessionKey(), parameters.getMachineId(), li, parameters.getOutDir());
	    } catch(InternalTMSException e) {
	        String code="", msg="";                                                                                                                         
	        String tmp[]={"", ""};                                                                                                                          
	        splitExcep(e.getMessage(), tmp);                                                                                                                
	        code = tmp[0];                                                                                                                                  
	        msg = tmp[1];
	        switch (new Integer(code)){
		case 1 :
		    throw new DIETMessage(msg, new DIETFault());
		case 2 :
		    throw new DBERRMessage(msg, new DBERRFault());
		case 3  :
		    throw new DBCONNMessage(msg, new DBCONNFault());
		case 4 :
		    throw new SYSTEMMessage(msg, new SYSTEMFault());
		case 29 :
		    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
		case 32 :
		    throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
		case 101 :
		    throw new UNKNOWNBATCHSCHEDULERMessage(msg, new UNKNOWNBATCHSCHEDULERFault());
		case 102 :
		    throw new BATCHSCHEDULERERRORMessage(msg, new BATCHSCHEDULERERRORFault());
		case 104 :
		    throw new PERMISSIONDENIEDMessage(msg, new PERMISSIONDENIEDFault());
		default :
		    throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
	        }			
	    }
	    long i;
	    for (i = 0; i < li.getResults().size(); i++) {
	    	GetCompletedJobsOutputResponse.Data.Jobresult opva = new GetCompletedJobsOutputResponse.Data.Jobresult();
	    	JobResult r = li.getResults().get(i);
	    	opva.setErrorPath(r.getErrorPath());
	    	opva.setJobId(r.getJobId());
	    	opva.setOutputPath(r.getOutputPath());
	    	dat.getJobresult().add(opva);
	    }
	    res.setData(dat);
	    res.setNbJobs(new BigInteger(new Long(li.getResults().size()).toString()));
	    return res;
	}

	public GetJobProgressResponse getJobProgress(
						     GetJobProgressRequest parameters) throws UNKNOWNMACHINEMessage,
											      DBCONNMessage, DIETMessage, UNDEFINEDMessage,
											      SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
	    GetJobProgressResponse res = new GetJobProgressResponse();
	    GetJobProgressResponse.Data dat = new GetJobProgressResponse.Data();
	    ListProgression li = new ListProgression();
	    ProgressOptions op = new ProgressOptions();
	    int i;
	    if (parameters.getJobId() != null) {
		op.setJobId(parameters.getJobId());
	    }
	    if (parameters.getJobOwner() != null) {
		op.setJobOwner(parameters.getJobOwner());
	    }
	    if (parameters.getMachineId() == null) {
		parameters.setMachineId("");
	    }
	    if (parameters.getSessionKey() == null) {
		parameters.setSessionKey("");
	    }
	    try {
		VISHNU.getJobProgress(parameters.getSessionKey(), parameters.getMachineId(), li, op);
	    } catch(InternalTMSException e) {
	        String code="", msg="";                                                                                                                         
	        String tmp[]={"", ""};                                                                                                                          
	        splitExcep(e.getMessage(), tmp);                                                                                                                
	        code = tmp[0];                                                                                                                                  
	        msg = tmp[1];
	        switch (new Integer(code)){
		case 1 :
		    throw new DIETMessage(msg, new DIETFault());
		case 2 :
		    throw new DBERRMessage(msg, new DBERRFault());
		case 3  :
		    throw new DBCONNMessage(msg, new DBCONNFault());
		case 4 :
		    throw new SYSTEMMessage(msg, new SYSTEMFault());
		case 29 :
		    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
		case 32 :
		    throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
		default :
		    throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
	        
	        }
	    }
	    for (i = 0; i < li.getProgress().size(); i++) {
	    	GetJobProgressResponse.Data.Progression opva = new GetJobProgressResponse.Data.Progression();
	    	Progression p = li.getProgress().get(i);
	    	opva.setEndTime(p.getEndTime());
	    	opva.setJobId(p.getJobId());
	    	opva.setJobName(p.getJobName());
	    	Long lo = new Long(p.getPercent());
	    	BigInteger bi = new BigInteger(lo.toString());
	    	opva.setPercent(bi);
	    	opva.setStartTime(p.getStartTime());
	    	opva.setStatus(JobStatus
	  			  .fromValue(com.sysfera.vishnu.api.tms.data.JobStatus.get(
							p.getStatus()).getLiteral()));
	    	Long l = new Long(p.getWallTime());
	    	BigInteger b = new BigInteger(l.toString());
	    	opva.setWallTime(b);
	    	dat.getProgression().add(opva);
	    }
	    res.setData(dat);
	    res.setNbJobs(new BigInteger(new Long(li.getProgress().size()).toString()));
	    return res;
	}

	public GetJobOutputResponse getJobOutput(GetJobOutputRequest parameters)
	    throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage,
		   UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage,
		   DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage,
		   SYSTEMMessage, DBERRMessage {
	    GetJobOutputResponse res = new GetJobOutputResponse();
	    JobResult j = new JobResult();
	    if (parameters.getMachineId() == null) {
		parameters.setMachineId("");
	    }
	    if (parameters.getSessionKey() == null) {
		parameters.setSessionKey("");
	    }
	    if (parameters.getJobId() == null) {
		parameters.setJobId("");
	    }
	    
	    if (parameters.getOutDir() == null) {
	     parameters.setOutDir("");
	    }

	    try {
		VISHNU.getJobOutput(parameters.getSessionKey(), parameters.getMachineId(), parameters.getJobId(), j, parameters.getOutDir());
	    } catch(InternalTMSException e) {
	        String code="", msg="";                                                                                                                         
	        String tmp[]={"", ""};                                                                                                                          
	        splitExcep(e.getMessage(), tmp);                                                                                                                
	        code = tmp[0];                                                                                                                                  
	        msg = tmp[1];
	        switch (new Integer(code)){
		case 1 :
		    throw new DIETMessage(msg, new DIETFault());
		case 2 :
		    throw new DBERRMessage(msg, new DBERRFault());
		case 3  :
		    throw new DBCONNMessage(msg, new DBCONNFault());
		case 4 :
		    throw new SYSTEMMessage(msg, new SYSTEMFault());
		case 29 :
		    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
		case 32 :
		    throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
		case 101 :
		    throw new UNKNOWNBATCHSCHEDULERMessage(msg, new UNKNOWNBATCHSCHEDULERFault());
		case 102 :
		    throw new BATCHSCHEDULERERRORMessage(msg, new BATCHSCHEDULERERRORFault());
		case 104 :
		    throw new PERMISSIONDENIEDMessage(msg, new PERMISSIONDENIEDFault());
		default :
		    throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
	        
	        }
	    }
	    res.setErrorPath(j.getErrorPath());
	    res.setJobId(j.getJobId());
	    res.setOutputPath(j.getOutputPath());
	    return res;
	}

	public CancelJobResponse cancelJob(CancelJobRequest parameters)
	    throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage,
		   UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage,
		   DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage,
		   SYSTEMMessage, DBERRMessage {
	    CancelJobResponse res = new CancelJobResponse();
	    if (parameters.getJobId() == null) {
		parameters.setJobId("");
	    }
	    if (parameters.getMachineId() == null) {
		parameters.setMachineId("");
	    }
	    if (parameters.getSessionKey() == null) {
		parameters.setSessionKey("");
	    }
	    try {
		VISHNU.cancelJob(parameters.getSessionKey(), parameters.getMachineId(), parameters.getJobId());
	    } catch (InternalTMSException e){
	        String code="", msg="";                                                                                                                         
	        String tmp[]={"", ""};                                                                                                                          
	        splitExcep(e.getMessage(), tmp);                                                                                                                
	        code = tmp[0];                                                                                                                                  
	        msg = tmp[1];

	        switch (new Integer(code)){
		case 1 :
		    throw new DIETMessage(msg, new DIETFault());
		case 2 :
		    throw new DBERRMessage(msg, new DBERRFault());
		case 3  :
		    throw new DBCONNMessage(msg, new DBCONNFault());
		case 4 :
		    throw new SYSTEMMessage(msg, new SYSTEMFault());
		case 29 :
		    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
		case 32 :
		    throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
		case 101 :
		    throw new UNKNOWNBATCHSCHEDULERMessage(msg, new UNKNOWNBATCHSCHEDULERFault());
		case 102 :
		    throw new BATCHSCHEDULERERRORMessage(msg, new BATCHSCHEDULERERRORFault());
		case 104 :
		    throw new PERMISSIONDENIEDMessage(msg, new PERMISSIONDENIEDFault());
		default :
		    throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());

	        }
	    }
	    return res;
	}
	private void splitExcep (String s1, String[] array ){
	    
	    Integer i = s1.indexOf('#');
	    Integer j = s1.lastIndexOf('#');
	    
	    if(i!=-1){
		array[0] = s1.substring(0, i);
		array[1] = s1.substring(j+1);
		if(j!=i+1){
		    array[1] += ": ";
		    array[1] += s1.substring(i+1, j);
		}
	    }else{
		array[0] = "7"; // 7 is invalid system exception in C++
		array[1] = "Error ! Invalid exception gotten";
	    }
	}
	
    }

