
package com.sysfera.vishnu.api.fms;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transferId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{urn:ResourceProxy}Status"/>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientMachineId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sourceMachineId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="destinationMachineId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sourceFilePath" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="destinationFilePath" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="size" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="start_time" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="trCommand" type="{urn:ResourceProxy}TransferCommand"/>
 *         &lt;element name="errorMsg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transferId",
    "status",
    "userId",
    "clientMachineId",
    "sourceMachineId",
    "destinationMachineId",
    "sourceFilePath",
    "destinationFilePath",
    "size",
    "startTime",
    "trCommand",
    "errorMsg"
})
@XmlRootElement(name = "copyAsyncFileResponse")
public class CopyAsyncFileResponse {

    @XmlElement(required = true)
    protected String transferId;
    @XmlElement(required = true)
    protected Status status;
    @XmlElement(required = true)
    protected String userId;
    @XmlElement(required = true)
    protected String clientMachineId;
    @XmlElement(required = true)
    protected String sourceMachineId;
    @XmlElement(required = true)
    protected String destinationMachineId;
    @XmlElement(required = true)
    protected String sourceFilePath;
    @XmlElement(required = true)
    protected String destinationFilePath;
    protected long size;
    @XmlElement(name = "start_time")
    protected long startTime;
    @XmlElement(required = true)
    protected TransferCommand trCommand;
    @XmlElement(required = true)
    protected String errorMsg;

    /**
     * Gets the value of the transferId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferId() {
        return transferId;
    }

    /**
     * Sets the value of the transferId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferId(String value) {
        this.transferId = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setStatus(Status value) {
        this.status = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the clientMachineId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientMachineId() {
        return clientMachineId;
    }

    /**
     * Sets the value of the clientMachineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientMachineId(String value) {
        this.clientMachineId = value;
    }

    /**
     * Gets the value of the sourceMachineId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceMachineId() {
        return sourceMachineId;
    }

    /**
     * Sets the value of the sourceMachineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceMachineId(String value) {
        this.sourceMachineId = value;
    }

    /**
     * Gets the value of the destinationMachineId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationMachineId() {
        return destinationMachineId;
    }

    /**
     * Sets the value of the destinationMachineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationMachineId(String value) {
        this.destinationMachineId = value;
    }

    /**
     * Gets the value of the sourceFilePath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceFilePath() {
        return sourceFilePath;
    }

    /**
     * Sets the value of the sourceFilePath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceFilePath(String value) {
        this.sourceFilePath = value;
    }

    /**
     * Gets the value of the destinationFilePath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationFilePath() {
        return destinationFilePath;
    }

    /**
     * Sets the value of the destinationFilePath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationFilePath(String value) {
        this.destinationFilePath = value;
    }

    /**
     * Gets the value of the size property.
     * 
     */
    public long getSize() {
        return size;
    }

    /**
     * Sets the value of the size property.
     * 
     */
    public void setSize(long value) {
        this.size = value;
    }

    /**
     * Gets the value of the startTime property.
     * 
     */
    public long getStartTime() {
        return startTime;
    }

    /**
     * Sets the value of the startTime property.
     * 
     */
    public void setStartTime(long value) {
        this.startTime = value;
    }

    /**
     * Gets the value of the trCommand property.
     * 
     * @return
     *     possible object is
     *     {@link TransferCommand }
     *     
     */
    public TransferCommand getTrCommand() {
        return trCommand;
    }

    /**
     * Sets the value of the trCommand property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferCommand }
     *     
     */
    public void setTrCommand(TransferCommand value) {
        this.trCommand = value;
    }

    /**
     * Gets the value of the errorMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * Sets the value of the errorMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMsg(String value) {
        this.errorMsg = value;
    }

}
