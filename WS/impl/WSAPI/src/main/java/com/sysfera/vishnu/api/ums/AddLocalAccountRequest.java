
package com.sysfera.vishnu.api.ums;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="machineId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="acLogin" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sshKeyPath" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="homeDirectory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sessionKey",
    "userId",
    "machineId",
    "acLogin",
    "sshKeyPath",
    "homeDirectory"
})
@XmlRootElement(name = "addLocalAccountRequest")
public class AddLocalAccountRequest {

    @XmlElement(required = true)
    protected String sessionKey;
    @XmlElement(required = true)
    protected String userId;
    @XmlElement(required = true)
    protected String machineId;
    @XmlElement(required = true)
    protected String acLogin;
    @XmlElement(required = true)
    protected String sshKeyPath;
    @XmlElement(required = true)
    protected String homeDirectory;

    /**
     * Gets the value of the sessionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * Sets the value of the sessionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionKey(String value) {
        this.sessionKey = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the machineId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMachineId() {
        return machineId;
    }

    /**
     * Sets the value of the machineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMachineId(String value) {
        this.machineId = value;
    }

    /**
     * Gets the value of the acLogin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcLogin() {
        return acLogin;
    }

    /**
     * Sets the value of the acLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcLogin(String value) {
        this.acLogin = value;
    }

    /**
     * Gets the value of the sshKeyPath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSshKeyPath() {
        return sshKeyPath;
    }

    /**
     * Sets the value of the sshKeyPath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSshKeyPath(String value) {
        this.sshKeyPath = value;
    }

    /**
     * Gets the value of the homeDirectory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHomeDirectory() {
        return homeDirectory;
    }

    /**
     * Sets the value of the homeDirectory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHomeDirectory(String value) {
        this.homeDirectory = value;
    }

}
