
package com.sysfera.vishnu.api.ums;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sessionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dateLastConnect" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="dateCreation" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="dateClosure" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="status" type="{urn:ResourceProxy}StatusType"/>
 *         &lt;element name="closePolicy" type="{urn:ResourceProxy}SessionCloseType"/>
 *         &lt;element name="timeout" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="authenId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sessionId",
    "userId",
    "sessionKey",
    "dateLastConnect",
    "dateCreation",
    "dateClosure",
    "status",
    "closePolicy",
    "timeout",
    "authenId"
})
@XmlRootElement(name = "reconnectResponse")
public class ReconnectResponse {

    @XmlElement(required = true)
    protected String sessionId;
    @XmlElement(required = true)
    protected String userId;
    @XmlElement(required = true)
    protected String sessionKey;
    protected long dateLastConnect;
    protected long dateCreation;
    protected long dateClosure;
    @XmlElement(required = true)
    protected StatusType status;
    @XmlElement(required = true)
    protected SessionCloseType closePolicy;
    protected long timeout;
    @XmlElement(required = true)
    protected String authenId;

    /**
     * Gets the value of the sessionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets the value of the sessionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionId(String value) {
        this.sessionId = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the sessionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * Sets the value of the sessionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionKey(String value) {
        this.sessionKey = value;
    }

    /**
     * Gets the value of the dateLastConnect property.
     * 
     */
    public long getDateLastConnect() {
        return dateLastConnect;
    }

    /**
     * Sets the value of the dateLastConnect property.
     * 
     */
    public void setDateLastConnect(long value) {
        this.dateLastConnect = value;
    }

    /**
     * Gets the value of the dateCreation property.
     * 
     */
    public long getDateCreation() {
        return dateCreation;
    }

    /**
     * Sets the value of the dateCreation property.
     * 
     */
    public void setDateCreation(long value) {
        this.dateCreation = value;
    }

    /**
     * Gets the value of the dateClosure property.
     * 
     */
    public long getDateClosure() {
        return dateClosure;
    }

    /**
     * Sets the value of the dateClosure property.
     * 
     */
    public void setDateClosure(long value) {
        this.dateClosure = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link StatusType }
     *     
     */
    public StatusType getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusType }
     *     
     */
    public void setStatus(StatusType value) {
        this.status = value;
    }

    /**
     * Gets the value of the closePolicy property.
     * 
     * @return
     *     possible object is
     *     {@link SessionCloseType }
     *     
     */
    public SessionCloseType getClosePolicy() {
        return closePolicy;
    }

    /**
     * Sets the value of the closePolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionCloseType }
     *     
     */
    public void setClosePolicy(SessionCloseType value) {
        this.closePolicy = value;
    }

    /**
     * Gets the value of the timeout property.
     * 
     */
    public long getTimeout() {
        return timeout;
    }

    /**
     * Sets the value of the timeout property.
     * 
     */
    public void setTimeout(long value) {
        this.timeout = value;
    }

    /**
     * Gets the value of the authenId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthenId() {
        return authenId;
    }

    /**
     * Sets the value of the authenId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthenId(String value) {
        this.authenId = value;
    }

}
