
package com.sysfera.vishnu.api.tms;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for JobPriority.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="JobPriority">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UNDEFINED"/>
 *     &lt;enumeration value="VERY_LOW"/>
 *     &lt;enumeration value="LOW"/>
 *     &lt;enumeration value="NORMAL"/>
 *     &lt;enumeration value="HIGH"/>
 *     &lt;enumeration value="VERY_HIGH"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "JobPriority")
@XmlEnum
public enum JobPriority {

    UNDEFINED,
    VERY_LOW,
    LOW,
    NORMAL,
    HIGH,
    VERY_HIGH;

    public String value() {
        return name();
    }

    public static JobPriority fromValue(String v) {
        return valueOf(v);
    }

}
