
package com.sysfera.vishnu.api.ums;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="data" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="authsystem" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                           &lt;attribute name="authSystemId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="URI" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="authLogin" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="authPassword" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="userPasswordEncryption" use="required" type="{urn:ResourceProxy}EncryptionMethod" />
 *                           &lt;attribute name="type" type="{urn:ResourceProxy}AuthType" />
 *                           &lt;attribute name="status" type="{urn:ResourceProxy}StatusType" />
 *                           &lt;attribute name="ldapBase" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data"
})
@XmlRootElement(name = "listAuthSystemsResponse")
public class ListAuthSystemsResponse {

    protected ListAuthSystemsResponse.Data data;

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link ListAuthSystemsResponse.Data }
     *     
     */
    public ListAuthSystemsResponse.Data getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListAuthSystemsResponse.Data }
     *     
     */
    public void setData(ListAuthSystemsResponse.Data value) {
        this.data = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="authsystem" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *                 &lt;attribute name="authSystemId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="URI" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="authLogin" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="authPassword" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="userPasswordEncryption" use="required" type="{urn:ResourceProxy}EncryptionMethod" />
     *                 &lt;attribute name="type" type="{urn:ResourceProxy}AuthType" />
     *                 &lt;attribute name="status" type="{urn:ResourceProxy}StatusType" />
     *                 &lt;attribute name="ldapBase" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "authsystem"
    })
    public static class Data {

        @XmlElement(nillable = true)
        protected List<ListAuthSystemsResponse.Data.Authsystem> authsystem;

        /**
         * Gets the value of the authsystem property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the authsystem property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAuthsystem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ListAuthSystemsResponse.Data.Authsystem }
         * 
         * 
         */
        public List<ListAuthSystemsResponse.Data.Authsystem> getAuthsystem() {
            if (authsystem == null) {
                authsystem = new ArrayList<ListAuthSystemsResponse.Data.Authsystem>();
            }
            return this.authsystem;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *       &lt;/sequence>
         *       &lt;attribute name="authSystemId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="URI" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="authLogin" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="authPassword" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="userPasswordEncryption" use="required" type="{urn:ResourceProxy}EncryptionMethod" />
         *       &lt;attribute name="type" type="{urn:ResourceProxy}AuthType" />
         *       &lt;attribute name="status" type="{urn:ResourceProxy}StatusType" />
         *       &lt;attribute name="ldapBase" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Authsystem {

            @XmlAttribute(name = "authSystemId", required = true)
            protected String authSystemId;
            @XmlAttribute(name = "name", required = true)
            protected String name;
            @XmlAttribute(name = "URI", required = true)
            protected String uri;
            @XmlAttribute(name = "authLogin", required = true)
            protected String authLogin;
            @XmlAttribute(name = "authPassword", required = true)
            protected String authPassword;
            @XmlAttribute(name = "userPasswordEncryption", required = true)
            protected EncryptionMethod userPasswordEncryption;
            @XmlAttribute(name = "type")
            protected AuthType type;
            @XmlAttribute(name = "status")
            protected StatusType status;
            @XmlAttribute(name = "ldapBase")
            protected String ldapBase;

            /**
             * Gets the value of the authSystemId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAuthSystemId() {
                return authSystemId;
            }

            /**
             * Sets the value of the authSystemId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAuthSystemId(String value) {
                this.authSystemId = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the uri property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getURI() {
                return uri;
            }

            /**
             * Sets the value of the uri property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setURI(String value) {
                this.uri = value;
            }

            /**
             * Gets the value of the authLogin property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAuthLogin() {
                return authLogin;
            }

            /**
             * Sets the value of the authLogin property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAuthLogin(String value) {
                this.authLogin = value;
            }

            /**
             * Gets the value of the authPassword property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAuthPassword() {
                return authPassword;
            }

            /**
             * Sets the value of the authPassword property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAuthPassword(String value) {
                this.authPassword = value;
            }

            /**
             * Gets the value of the userPasswordEncryption property.
             * 
             * @return
             *     possible object is
             *     {@link EncryptionMethod }
             *     
             */
            public EncryptionMethod getUserPasswordEncryption() {
                return userPasswordEncryption;
            }

            /**
             * Sets the value of the userPasswordEncryption property.
             * 
             * @param value
             *     allowed object is
             *     {@link EncryptionMethod }
             *     
             */
            public void setUserPasswordEncryption(EncryptionMethod value) {
                this.userPasswordEncryption = value;
            }

            /**
             * Gets the value of the type property.
             * 
             * @return
             *     possible object is
             *     {@link AuthType }
             *     
             */
            public AuthType getType() {
                return type;
            }

            /**
             * Sets the value of the type property.
             * 
             * @param value
             *     allowed object is
             *     {@link AuthType }
             *     
             */
            public void setType(AuthType value) {
                this.type = value;
            }

            /**
             * Gets the value of the status property.
             * 
             * @return
             *     possible object is
             *     {@link StatusType }
             *     
             */
            public StatusType getStatus() {
                return status;
            }

            /**
             * Sets the value of the status property.
             * 
             * @param value
             *     allowed object is
             *     {@link StatusType }
             *     
             */
            public void setStatus(StatusType value) {
                this.status = value;
            }

            /**
             * Gets the value of the ldapBase property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLdapBase() {
                return ldapBase;
            }

            /**
             * Sets the value of the ldapBase property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLdapBase(String value) {
                this.ldapBase = value;
            }

        }

    }

}
