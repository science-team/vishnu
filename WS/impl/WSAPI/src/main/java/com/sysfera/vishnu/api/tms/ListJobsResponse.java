
package com.sysfera.vishnu.api.tms;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="data" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="job" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                           &lt;attribute name="sessionId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="submitMachineId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="submitMachineName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="jobId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="jobName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="jobPath" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="outputPath" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="errorPath" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="jobPrio" type="{urn:ResourceProxy}JobPriority" />
 *                           &lt;attribute name="nbCpus" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                           &lt;attribute name="jobWorkingDir" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="status" type="{urn:ResourceProxy}JobStatus" />
 *                           &lt;attribute name="submitDate" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="endDate" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="owner" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="jobQueue" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="wallClockLimit" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="groupName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="jobDescription" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="memLimit" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                           &lt;attribute name="nbNodes" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                           &lt;attribute name="nbNodesAndCpuPerNode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="batchJobId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="nbJobs" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="nbRunningJobs" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="nbWaitingJobs" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data",
    "nbJobs",
    "nbRunningJobs",
    "nbWaitingJobs"
})
@XmlRootElement(name = "listJobsResponse")
public class ListJobsResponse {

    protected ListJobsResponse.Data data;
    protected long nbJobs;
    protected long nbRunningJobs;
    protected long nbWaitingJobs;

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link ListJobsResponse.Data }
     *     
     */
    public ListJobsResponse.Data getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListJobsResponse.Data }
     *     
     */
    public void setData(ListJobsResponse.Data value) {
        this.data = value;
    }

    /**
     * Gets the value of the nbJobs property.
     * 
     */
    public long getNbJobs() {
        return nbJobs;
    }

    /**
     * Sets the value of the nbJobs property.
     * 
     */
    public void setNbJobs(long value) {
        this.nbJobs = value;
    }

    /**
     * Gets the value of the nbRunningJobs property.
     * 
     */
    public long getNbRunningJobs() {
        return nbRunningJobs;
    }

    /**
     * Sets the value of the nbRunningJobs property.
     * 
     */
    public void setNbRunningJobs(long value) {
        this.nbRunningJobs = value;
    }

    /**
     * Gets the value of the nbWaitingJobs property.
     * 
     */
    public long getNbWaitingJobs() {
        return nbWaitingJobs;
    }

    /**
     * Sets the value of the nbWaitingJobs property.
     * 
     */
    public void setNbWaitingJobs(long value) {
        this.nbWaitingJobs = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="job" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *                 &lt;attribute name="sessionId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="submitMachineId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="submitMachineName" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="jobId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="jobName" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="jobPath" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="outputPath" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="errorPath" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="jobPrio" type="{urn:ResourceProxy}JobPriority" />
     *                 &lt;attribute name="nbCpus" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *                 &lt;attribute name="jobWorkingDir" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="status" type="{urn:ResourceProxy}JobStatus" />
     *                 &lt;attribute name="submitDate" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="endDate" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="owner" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="jobQueue" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="wallClockLimit" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="groupName" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="jobDescription" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="memLimit" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *                 &lt;attribute name="nbNodes" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *                 &lt;attribute name="nbNodesAndCpuPerNode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="batchJobId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "job"
    })
    public static class Data {

        @XmlElement(nillable = true)
        protected List<ListJobsResponse.Data.Job> job;

        /**
         * Gets the value of the job property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the job property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getJob().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ListJobsResponse.Data.Job }
         * 
         * 
         */
        public List<ListJobsResponse.Data.Job> getJob() {
            if (job == null) {
                job = new ArrayList<ListJobsResponse.Data.Job>();
            }
            return this.job;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *       &lt;/sequence>
         *       &lt;attribute name="sessionId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="submitMachineId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="submitMachineName" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="jobId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="jobName" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="jobPath" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="outputPath" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="errorPath" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="jobPrio" type="{urn:ResourceProxy}JobPriority" />
         *       &lt;attribute name="nbCpus" type="{http://www.w3.org/2001/XMLSchema}integer" />
         *       &lt;attribute name="jobWorkingDir" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="status" type="{urn:ResourceProxy}JobStatus" />
         *       &lt;attribute name="submitDate" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="endDate" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="owner" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="jobQueue" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="wallClockLimit" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="groupName" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="jobDescription" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="memLimit" type="{http://www.w3.org/2001/XMLSchema}integer" />
         *       &lt;attribute name="nbNodes" type="{http://www.w3.org/2001/XMLSchema}integer" />
         *       &lt;attribute name="nbNodesAndCpuPerNode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="batchJobId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Job {

            @XmlAttribute(name = "sessionId")
            protected String sessionId;
            @XmlAttribute(name = "submitMachineId")
            protected String submitMachineId;
            @XmlAttribute(name = "submitMachineName")
            protected String submitMachineName;
            @XmlAttribute(name = "jobId")
            protected String jobId;
            @XmlAttribute(name = "jobName")
            protected String jobName;
            @XmlAttribute(name = "jobPath")
            protected String jobPath;
            @XmlAttribute(name = "outputPath")
            protected String outputPath;
            @XmlAttribute(name = "errorPath")
            protected String errorPath;
            @XmlAttribute(name = "jobPrio")
            protected JobPriority jobPrio;
            @XmlAttribute(name = "nbCpus")
            protected BigInteger nbCpus;
            @XmlAttribute(name = "jobWorkingDir")
            protected String jobWorkingDir;
            @XmlAttribute(name = "status")
            protected JobStatus status;
            @XmlAttribute(name = "submitDate")
            protected Long submitDate;
            @XmlAttribute(name = "endDate")
            protected Long endDate;
            @XmlAttribute(name = "owner")
            protected String owner;
            @XmlAttribute(name = "jobQueue")
            protected String jobQueue;
            @XmlAttribute(name = "wallClockLimit")
            protected Long wallClockLimit;
            @XmlAttribute(name = "groupName")
            protected String groupName;
            @XmlAttribute(name = "jobDescription")
            protected String jobDescription;
            @XmlAttribute(name = "memLimit")
            protected BigInteger memLimit;
            @XmlAttribute(name = "nbNodes")
            protected BigInteger nbNodes;
            @XmlAttribute(name = "nbNodesAndCpuPerNode")
            protected String nbNodesAndCpuPerNode;
            @XmlAttribute(name = "batchJobId")
            protected String batchJobId;

            /**
             * Gets the value of the sessionId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSessionId() {
                return sessionId;
            }

            /**
             * Sets the value of the sessionId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSessionId(String value) {
                this.sessionId = value;
            }

            /**
             * Gets the value of the submitMachineId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubmitMachineId() {
                return submitMachineId;
            }

            /**
             * Sets the value of the submitMachineId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubmitMachineId(String value) {
                this.submitMachineId = value;
            }

            /**
             * Gets the value of the submitMachineName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubmitMachineName() {
                return submitMachineName;
            }

            /**
             * Sets the value of the submitMachineName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubmitMachineName(String value) {
                this.submitMachineName = value;
            }

            /**
             * Gets the value of the jobId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getJobId() {
                return jobId;
            }

            /**
             * Sets the value of the jobId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setJobId(String value) {
                this.jobId = value;
            }

            /**
             * Gets the value of the jobName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getJobName() {
                return jobName;
            }

            /**
             * Sets the value of the jobName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setJobName(String value) {
                this.jobName = value;
            }

            /**
             * Gets the value of the jobPath property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getJobPath() {
                return jobPath;
            }

            /**
             * Sets the value of the jobPath property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setJobPath(String value) {
                this.jobPath = value;
            }

            /**
             * Gets the value of the outputPath property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOutputPath() {
                return outputPath;
            }

            /**
             * Sets the value of the outputPath property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOutputPath(String value) {
                this.outputPath = value;
            }

            /**
             * Gets the value of the errorPath property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getErrorPath() {
                return errorPath;
            }

            /**
             * Sets the value of the errorPath property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setErrorPath(String value) {
                this.errorPath = value;
            }

            /**
             * Gets the value of the jobPrio property.
             * 
             * @return
             *     possible object is
             *     {@link JobPriority }
             *     
             */
            public JobPriority getJobPrio() {
                return jobPrio;
            }

            /**
             * Sets the value of the jobPrio property.
             * 
             * @param value
             *     allowed object is
             *     {@link JobPriority }
             *     
             */
            public void setJobPrio(JobPriority value) {
                this.jobPrio = value;
            }

            /**
             * Gets the value of the nbCpus property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNbCpus() {
                return nbCpus;
            }

            /**
             * Sets the value of the nbCpus property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNbCpus(BigInteger value) {
                this.nbCpus = value;
            }

            /**
             * Gets the value of the jobWorkingDir property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getJobWorkingDir() {
                return jobWorkingDir;
            }

            /**
             * Sets the value of the jobWorkingDir property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setJobWorkingDir(String value) {
                this.jobWorkingDir = value;
            }

            /**
             * Gets the value of the status property.
             * 
             * @return
             *     possible object is
             *     {@link JobStatus }
             *     
             */
            public JobStatus getStatus() {
                return status;
            }

            /**
             * Sets the value of the status property.
             * 
             * @param value
             *     allowed object is
             *     {@link JobStatus }
             *     
             */
            public void setStatus(JobStatus value) {
                this.status = value;
            }

            /**
             * Gets the value of the submitDate property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getSubmitDate() {
                return submitDate;
            }

            /**
             * Sets the value of the submitDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setSubmitDate(Long value) {
                this.submitDate = value;
            }

            /**
             * Gets the value of the endDate property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getEndDate() {
                return endDate;
            }

            /**
             * Sets the value of the endDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setEndDate(Long value) {
                this.endDate = value;
            }

            /**
             * Gets the value of the owner property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOwner() {
                return owner;
            }

            /**
             * Sets the value of the owner property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOwner(String value) {
                this.owner = value;
            }

            /**
             * Gets the value of the jobQueue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getJobQueue() {
                return jobQueue;
            }

            /**
             * Sets the value of the jobQueue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setJobQueue(String value) {
                this.jobQueue = value;
            }

            /**
             * Gets the value of the wallClockLimit property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getWallClockLimit() {
                return wallClockLimit;
            }

            /**
             * Sets the value of the wallClockLimit property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setWallClockLimit(Long value) {
                this.wallClockLimit = value;
            }

            /**
             * Gets the value of the groupName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupName() {
                return groupName;
            }

            /**
             * Sets the value of the groupName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupName(String value) {
                this.groupName = value;
            }

            /**
             * Gets the value of the jobDescription property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getJobDescription() {
                return jobDescription;
            }

            /**
             * Sets the value of the jobDescription property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setJobDescription(String value) {
                this.jobDescription = value;
            }

            /**
             * Gets the value of the memLimit property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMemLimit() {
                return memLimit;
            }

            /**
             * Sets the value of the memLimit property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMemLimit(BigInteger value) {
                this.memLimit = value;
            }

            /**
             * Gets the value of the nbNodes property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNbNodes() {
                return nbNodes;
            }

            /**
             * Sets the value of the nbNodes property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNbNodes(BigInteger value) {
                this.nbNodes = value;
            }

            /**
             * Gets the value of the nbNodesAndCpuPerNode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNbNodesAndCpuPerNode() {
                return nbNodesAndCpuPerNode;
            }

            /**
             * Sets the value of the nbNodesAndCpuPerNode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNbNodesAndCpuPerNode(String value) {
                this.nbNodesAndCpuPerNode = value;
            }

            /**
             * Gets the value of the batchJobId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBatchJobId() {
                return batchJobId;
            }

            /**
             * Sets the value of the batchJobId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBatchJobId(String value) {
                this.batchJobId = value;
            }

        }

    }

}
