
package com.sysfera.vishnu.api.tms;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.2.9
 * Thu Mar 22 17:34:53 CET 2012
 * Generated source version: 2.2.9
 * 
 */

@WebFault(name = "DBERRFault", targetNamespace = "urn:ResourceProxy")
public class DBERRMessage extends Exception {
    public static final long serialVersionUID = 20120322173453L;
    
    private com.sysfera.vishnu.api.tms.DBERRFault dberrFault;

    public DBERRMessage() {
        super();
    }
    
    public DBERRMessage(String message) {
        super(message);
    }
    
    public DBERRMessage(String message, Throwable cause) {
        super(message, cause);
    }

    public DBERRMessage(String message, com.sysfera.vishnu.api.tms.DBERRFault dberrFault) {
        super(message);
        this.dberrFault = dberrFault;
    }

    public DBERRMessage(String message, com.sysfera.vishnu.api.tms.DBERRFault dberrFault, Throwable cause) {
        super(message, cause);
        this.dberrFault = dberrFault;
    }

    public com.sysfera.vishnu.api.tms.DBERRFault getFaultInfo() {
        return this.dberrFault;
    }
}
