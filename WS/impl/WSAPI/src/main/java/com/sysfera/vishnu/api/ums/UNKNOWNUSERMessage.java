
package com.sysfera.vishnu.api.ums;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.2.9
 * Fri Mar 09 11:53:42 CET 2012
 * Generated source version: 2.2.9
 * 
 */

@WebFault(name = "UNKNOWN_USERFault", targetNamespace = "urn:ResourceProxy")
public class UNKNOWNUSERMessage extends Exception {
    public static final long serialVersionUID = 20120309115342L;
    
    private com.sysfera.vishnu.api.ums.UNKNOWNUSERFault unknownUSERFault;

    public UNKNOWNUSERMessage() {
        super();
    }
    
    public UNKNOWNUSERMessage(String message) {
        super(message);
    }
    
    public UNKNOWNUSERMessage(String message, Throwable cause) {
        super(message, cause);
    }

    public UNKNOWNUSERMessage(String message, com.sysfera.vishnu.api.ums.UNKNOWNUSERFault unknownUSERFault) {
        super(message);
        this.unknownUSERFault = unknownUSERFault;
    }

    public UNKNOWNUSERMessage(String message, com.sysfera.vishnu.api.ums.UNKNOWNUSERFault unknownUSERFault, Throwable cause) {
        super(message, cause);
        this.unknownUSERFault = unknownUSERFault;
    }

    public com.sysfera.vishnu.api.ums.UNKNOWNUSERFault getFaultInfo() {
        return this.unknownUSERFault;
    }
}
