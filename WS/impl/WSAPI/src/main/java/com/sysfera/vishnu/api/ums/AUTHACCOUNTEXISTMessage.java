
package com.sysfera.vishnu.api.ums;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.2.9
 * Fri Mar 09 11:53:42 CET 2012
 * Generated source version: 2.2.9
 * 
 */

@WebFault(name = "AUTH_ACCOUNT_EXISTFault", targetNamespace = "urn:ResourceProxy")
public class AUTHACCOUNTEXISTMessage extends Exception {
    public static final long serialVersionUID = 20120309115342L;
    
    private com.sysfera.vishnu.api.ums.AUTHACCOUNTEXISTFault authACCOUNTEXISTFault;

    public AUTHACCOUNTEXISTMessage() {
        super();
    }
    
    public AUTHACCOUNTEXISTMessage(String message) {
        super(message);
    }
    
    public AUTHACCOUNTEXISTMessage(String message, Throwable cause) {
        super(message, cause);
    }

    public AUTHACCOUNTEXISTMessage(String message, com.sysfera.vishnu.api.ums.AUTHACCOUNTEXISTFault authACCOUNTEXISTFault) {
        super(message);
        this.authACCOUNTEXISTFault = authACCOUNTEXISTFault;
    }

    public AUTHACCOUNTEXISTMessage(String message, com.sysfera.vishnu.api.ums.AUTHACCOUNTEXISTFault authACCOUNTEXISTFault, Throwable cause) {
        super(message, cause);
        this.authACCOUNTEXISTFault = authACCOUNTEXISTFault;
    }

    public com.sysfera.vishnu.api.ums.AUTHACCOUNTEXISTFault getFaultInfo() {
        return this.authACCOUNTEXISTFault;
    }
}
