package com.sysfera.vishnu.api.ims.impl;

import java.math.BigInteger;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.transaction.SystemException;

import com.sysfera.vishnu.api.ims.DBERRFault;
import com.sysfera.vishnu.api.ims.DBERRMessage;
import com.sysfera.vishnu.api.ims.ExportCommandsRequest;
import com.sysfera.vishnu.api.ims.ExportCommandsResponse;
import com.sysfera.vishnu.api.ims.GetMetricCurrentValueRequest;
import com.sysfera.vishnu.api.ims.GetMetricCurrentValueResponse;
import com.sysfera.vishnu.api.ims.GetMetricCurrentValueResponse.Data.Metric;
import com.sysfera.vishnu.api.ims.GetMetricHistoryRequest;
import com.sysfera.vishnu.api.ims.GetMetricHistoryResponse;
import com.sysfera.vishnu.api.ims.GetSystemInfoRequest;
import com.sysfera.vishnu.api.ims.GetSystemInfoResponse;
import com.sysfera.vishnu.api.ims.GetUpdateFrequencyRequest;
import com.sysfera.vishnu.api.ims.GetUpdateFrequencyResponse;
import com.sysfera.vishnu.api.ims.INVALIDPARAMETERFault;
import com.sysfera.vishnu.api.ims.INVALIDPARAMETERMessage;
import com.sysfera.vishnu.api.ims.VishnuIMSPortType;
import com.sysfera.vishnu.api.ims.DBERRFault;
import com.sysfera.vishnu.api.ims.DBERRMessage;
import com.sysfera.vishnu.api.ims.SESSIONKEYEXPIREDFault;
import com.sysfera.vishnu.api.ims.SESSIONKEYEXPIREDMessage;
import com.sysfera.vishnu.api.ims.SESSIONKEYNOTFOUNDFault;
import com.sysfera.vishnu.api.ims.SESSIONKEYNOTFOUNDMessage;
import com.sysfera.vishnu.api.ims.UNDEFINEDFault;
import com.sysfera.vishnu.api.ims.UNDEFINEDMessage;
import com.sysfera.vishnu.api.vishnu.internal.CurMetricOp;
import com.sysfera.vishnu.api.vishnu.internal.ExportOp;
import com.sysfera.vishnu.api.vishnu.internal.InternalIMSException;
import com.sysfera.vishnu.api.vishnu.internal.ListMetric;
import com.sysfera.vishnu.api.vishnu.internal.ListSysInfo;
import com.sysfera.vishnu.api.vishnu.internal.MetricHistOp;
import com.sysfera.vishnu.api.vishnu.internal.SysInfoOp;
import com.sysfera.vishnu.api.vishnu.internal.VISHNU;



@SuppressWarnings({ "restriction", "unused" })
@WebService(targetNamespace = "urn:ResourceProxy", name = "VishnuIMS", serviceName = "VishnuIMSService")
    @Remote(VishnuIMSPortType.class)
    @Stateless
    public class VishnuIMSPortImpl implements VishnuIMSPortType {

	private void splitExcep (String s1, String[] array ){
	    
	    Integer i = s1.indexOf('#');
	    Integer j = s1.lastIndexOf('#');
	    
	    if(i!=-1){
		array[0] = s1.substring(0, i);
		array[1] = s1.substring(j+1);
		if(j!=i+1){
		    array[1] += ": ";
		    array[1] += s1.substring(i+1, j);
		}
	    }else{
		array[0] = "7"; // 7 is invalid system exception in C++
		array[1] = "Error ! Invalid exception gotten";
	    }
	}


	public ExportCommandsResponse exportCommands(
			ExportCommandsRequest parameters) throws INVALIDPARAMETERMessage, DBERRMessage, SESSIONKEYEXPIREDMessage, SESSIONKEYNOTFOUNDMessage, UNDEFINEDMessage {
		ExportCommandsResponse res = new ExportCommandsResponse();
		parameters.getOldSessionId();
		ExportOp op = new ExportOp();
		if (parameters.getSessionKey() == null) {
			parameters.setSessionKey("");
		}
		if (parameters.getOldSessionId() == null) {
			parameters.setOldSessionId("");
		}
		if (parameters.getExportType() != null) {
			op.setExportType(com.sysfera.vishnu.api.ims.data.ExportType
					.getByName(parameters.getExportType().value()).getValue());
		}
		try {
			VISHNU.exportCommands(parameters.getSessionKey(), parameters.getOldSessionId(),
									"", op);
			} catch (InternalIMSException e) {
				String code="", msg="";
				String tmp[]={"", ""};
				splitExcep(e.getMessage(), tmp);
				code = tmp[0];
				msg = tmp[1];
				switch (new Integer(code)){
				case 2 :
					throw new DBERRMessage(msg, new DBERRFault());
				case 28 :
					throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
				case 29 :
					throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
				case 10 :
					throw new INVALIDPARAMETERMessage(msg, new INVALIDPARAMETERFault());
				default :
					throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
				}
			}
		return res;
	}

	public GetMetricCurrentValueResponse getMetricCurrentValue(
			GetMetricCurrentValueRequest parameters) throws DBERRMessage, SESSIONKEYNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, INVALIDPARAMETERMessage, UNDEFINEDMessage {
		GetMetricCurrentValueResponse res = new GetMetricCurrentValueResponse();
		CurMetricOp op = new CurMetricOp();
		GetMetricCurrentValueResponse.Data dat = new GetMetricCurrentValueResponse.Data();
		ListMetric li = new ListMetric();
		if (parameters.getSessionKey() == null) {
			parameters.setSessionKey("");
		}
		if (parameters.getMachineId() == null) {
			parameters.setMachineId("");
		}
		if (parameters.getMetricType() != null) {
			op.setMetricType(com.sysfera.vishnu.api.ims.data.MetricType
					.getByName(parameters.getMetricType().value()).getValue());
		}
		try {
			VISHNU.getMetricCurrentValue(parameters.getSessionKey(), parameters.getMachineId(),
											li, op);
		} catch (InternalIMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 28 :
				throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
				throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 10 :
				throw new INVALIDPARAMETERMessage(msg, new INVALIDPARAMETERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		int i;
	    for (i = 0; i < li.getMetric().size(); i++) {
	    	GetMetricCurrentValueResponse.Data.Metric opva = new GetMetricCurrentValueResponse.Data.Metric();
	    	com.sysfera.vishnu.api.vishnu.internal.Metric m = li.getMetric().get(i);
	    	opva.setTime(m.getTime());
	    	opva.setValue(m.getValue());
	    	opva.setType(com.sysfera.vishnu.api.ims.MetricType
					 .fromValue(com.sysfera.vishnu.api.ims.data.MetricType.get(
							  m.getType()).getLiteral()));
	    	dat.getMetric().add(opva);
	    }
	    res.setData(dat);
		return res;
	}

	public GetMetricHistoryResponse getMetricHistory(
			GetMetricHistoryRequest parameters) throws DBERRMessage,  SESSIONKEYNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, INVALIDPARAMETERMessage, UNDEFINEDMessage {
		GetMetricHistoryResponse res = new GetMetricHistoryResponse();
		MetricHistOp op = new MetricHistOp();
		GetMetricHistoryResponse.Data dat = new GetMetricHistoryResponse.Data();
		ListMetric li = new ListMetric();

		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		if (parameters.getMachineId()==null) {
			parameters.setMachineId("");
		}
		
		if (parameters.getEndTime() != null) {
			op.setEndTime(parameters.getEndTime());
		}
		if (parameters.getStartTime() != null) {
			op.setStartTime(parameters.getStartTime());
		}
		if (parameters.getType() != null) {
			op.setType(com.sysfera.vishnu.api.ims.data.MetricType
					.getByName(parameters.getType().value()).getValue());
		}

		try {
			VISHNU.getMetricHistory(parameters.getSessionKey(),
										parameters.getMachineId(), li, op);
		} catch (InternalIMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 28 :
				throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
				throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 10 :
				throw new INVALIDPARAMETERMessage(msg, new INVALIDPARAMETERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}			
		}
		int i;
	    for (i = 0; i < li.getMetric().size(); i++) {
	    	GetMetricHistoryResponse.Data.Metric opva = new GetMetricHistoryResponse.Data.Metric();
	    	com.sysfera.vishnu.api.vishnu.internal.Metric m = li.getMetric().get(i);
	    	opva.setTime(m.getTime());
	    	opva.setValue(m.getValue());
	    	opva.setType(com.sysfera.vishnu.api.ims.MetricType
					 .fromValue(com.sysfera.vishnu.api.ims.data.MetricType.get(
							  m.getType()).getLiteral()));
	    	dat.getMetric().add(opva);
	    }
		res.setData(dat);
		return res;
	}

	public GetSystemInfoResponse getSystemInfo(GetSystemInfoRequest parameters) throws DBERRMessage, SESSIONKEYNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, INVALIDPARAMETERMessage, UNDEFINEDMessage {
		GetSystemInfoResponse res = new GetSystemInfoResponse();
		SysInfoOp op = new SysInfoOp();
		ListSysInfo li = new ListSysInfo();
		GetSystemInfoResponse.Data dat = new GetSystemInfoResponse.Data();
		
		if (parameters.getSessionKey() == null) {
			parameters.setSessionKey("");
		}
		if (parameters.getMachineId()!=null) {
			op.setMachineId(parameters.getMachineId());
		}

		try {
			VISHNU.getSystemInfo(parameters.getSessionKey(), li, op);
		} catch (InternalIMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 28 :
				throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
				throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 10 :
				throw new INVALIDPARAMETERMessage(msg, new INVALIDPARAMETERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		int i;
	    for (i = 0; i < li.getSysInfo().size(); i++) {
	    	GetSystemInfoResponse.Data.Systeminfo opva = new GetSystemInfoResponse.Data.Systeminfo();
	    	com.sysfera.vishnu.api.vishnu.internal.SystemInfo m = li.getSysInfo().get(i);
	    	opva.setDiskSpace(m.getDiskSpace());
	    	opva.setMachineId(m.getMachineId());
	    	opva.setMemory(m.getMemory());
	    	dat.getSysteminfo().add(opva);
	    }
		res.setData(dat);
		return res;
	}

	public GetUpdateFrequencyResponse getUpdateFrequency(
			GetUpdateFrequencyRequest parameters) throws DBERRMessage, SESSIONKEYNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, INVALIDPARAMETERMessage, UNDEFINEDMessage {
		GetUpdateFrequencyResponse res = new GetUpdateFrequencyResponse();
		int[] freq = {0};
		if (parameters.getSessionKey() == null) {
			parameters.setSessionKey("");
		}
		try {
			VISHNU.getUpdateFrequency(parameters.getSessionKey(), freq);
		} catch (InternalIMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 2 :
				throw new DBERRMessage(msg, new DBERRFault());
			case 28 :
				throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
				throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 10 :
				throw new INVALIDPARAMETERMessage(msg, new INVALIDPARAMETERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		Integer i = new Integer(freq[0]);
		String s = new String(i.toString());
		BigInteger b = new BigInteger(s);
		res.setFreq(b);
		return res;
	}
	
    }

