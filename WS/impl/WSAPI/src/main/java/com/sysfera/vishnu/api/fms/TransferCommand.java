
package com.sysfera.vishnu.api.fms;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransferCommand.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TransferCommand">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SCP"/>
 *     &lt;enumeration value="RSYNC"/>
 *     &lt;enumeration value="UNDEFINED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TransferCommand")
@XmlEnum
public enum TransferCommand {

    SCP,
    RSYNC,
    UNDEFINED;

    public String value() {
        return name();
    }

    public static TransferCommand fromValue(String v) {
        return valueOf(v);
    }

}
