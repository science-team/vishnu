
package com.sysfera.vishnu.api.tms;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="data" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="queue" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                           &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="maxJobCpu" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                           &lt;attribute name="maxProcCpu" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                           &lt;attribute name="memory" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                           &lt;attribute name="wallTime" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="node" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                           &lt;attribute name="nbRunningJobs" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                           &lt;attribute name="nbJobsInQueue" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                           &lt;attribute name="state" type="{urn:ResourceProxy}QueueStatus" />
 *                           &lt;attribute name="priority" type="{urn:ResourceProxy}QueuePriority" />
 *                           &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="nbQueues" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data",
    "nbQueues"
})
@XmlRootElement(name = "listQueuesResponse")
public class ListQueuesResponse {

    protected ListQueuesResponse.Data data;
    @XmlElement(required = true)
    protected BigInteger nbQueues;

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link ListQueuesResponse.Data }
     *     
     */
    public ListQueuesResponse.Data getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListQueuesResponse.Data }
     *     
     */
    public void setData(ListQueuesResponse.Data value) {
        this.data = value;
    }

    /**
     * Gets the value of the nbQueues property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNbQueues() {
        return nbQueues;
    }

    /**
     * Sets the value of the nbQueues property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNbQueues(BigInteger value) {
        this.nbQueues = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="queue" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *                 &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="maxJobCpu" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *                 &lt;attribute name="maxProcCpu" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *                 &lt;attribute name="memory" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *                 &lt;attribute name="wallTime" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="node" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *                 &lt;attribute name="nbRunningJobs" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *                 &lt;attribute name="nbJobsInQueue" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *                 &lt;attribute name="state" type="{urn:ResourceProxy}QueueStatus" />
     *                 &lt;attribute name="priority" type="{urn:ResourceProxy}QueuePriority" />
     *                 &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "queue"
    })
    public static class Data {

        @XmlElement(nillable = true)
        protected List<ListQueuesResponse.Data.Queue> queue;

        /**
         * Gets the value of the queue property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the queue property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getQueue().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ListQueuesResponse.Data.Queue }
         * 
         * 
         */
        public List<ListQueuesResponse.Data.Queue> getQueue() {
            if (queue == null) {
                queue = new ArrayList<ListQueuesResponse.Data.Queue>();
            }
            return this.queue;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *       &lt;/sequence>
         *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="maxJobCpu" type="{http://www.w3.org/2001/XMLSchema}integer" />
         *       &lt;attribute name="maxProcCpu" type="{http://www.w3.org/2001/XMLSchema}integer" />
         *       &lt;attribute name="memory" type="{http://www.w3.org/2001/XMLSchema}integer" />
         *       &lt;attribute name="wallTime" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="node" type="{http://www.w3.org/2001/XMLSchema}integer" />
         *       &lt;attribute name="nbRunningJobs" type="{http://www.w3.org/2001/XMLSchema}integer" />
         *       &lt;attribute name="nbJobsInQueue" type="{http://www.w3.org/2001/XMLSchema}integer" />
         *       &lt;attribute name="state" type="{urn:ResourceProxy}QueueStatus" />
         *       &lt;attribute name="priority" type="{urn:ResourceProxy}QueuePriority" />
         *       &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Queue {

            @XmlAttribute(name = "name")
            protected String name;
            @XmlAttribute(name = "maxJobCpu")
            protected BigInteger maxJobCpu;
            @XmlAttribute(name = "maxProcCpu")
            protected BigInteger maxProcCpu;
            @XmlAttribute(name = "memory")
            protected BigInteger memory;
            @XmlAttribute(name = "wallTime")
            protected Long wallTime;
            @XmlAttribute(name = "node")
            protected BigInteger node;
            @XmlAttribute(name = "nbRunningJobs")
            protected BigInteger nbRunningJobs;
            @XmlAttribute(name = "nbJobsInQueue")
            protected BigInteger nbJobsInQueue;
            @XmlAttribute(name = "state")
            protected QueueStatus state;
            @XmlAttribute(name = "priority")
            protected QueuePriority priority;
            @XmlAttribute(name = "description")
            protected String description;

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the maxJobCpu property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMaxJobCpu() {
                return maxJobCpu;
            }

            /**
             * Sets the value of the maxJobCpu property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMaxJobCpu(BigInteger value) {
                this.maxJobCpu = value;
            }

            /**
             * Gets the value of the maxProcCpu property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMaxProcCpu() {
                return maxProcCpu;
            }

            /**
             * Sets the value of the maxProcCpu property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMaxProcCpu(BigInteger value) {
                this.maxProcCpu = value;
            }

            /**
             * Gets the value of the memory property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMemory() {
                return memory;
            }

            /**
             * Sets the value of the memory property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMemory(BigInteger value) {
                this.memory = value;
            }

            /**
             * Gets the value of the wallTime property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getWallTime() {
                return wallTime;
            }

            /**
             * Sets the value of the wallTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setWallTime(Long value) {
                this.wallTime = value;
            }

            /**
             * Gets the value of the node property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNode() {
                return node;
            }

            /**
             * Sets the value of the node property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNode(BigInteger value) {
                this.node = value;
            }

            /**
             * Gets the value of the nbRunningJobs property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNbRunningJobs() {
                return nbRunningJobs;
            }

            /**
             * Sets the value of the nbRunningJobs property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNbRunningJobs(BigInteger value) {
                this.nbRunningJobs = value;
            }

            /**
             * Gets the value of the nbJobsInQueue property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNbJobsInQueue() {
                return nbJobsInQueue;
            }

            /**
             * Sets the value of the nbJobsInQueue property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNbJobsInQueue(BigInteger value) {
                this.nbJobsInQueue = value;
            }

            /**
             * Gets the value of the state property.
             * 
             * @return
             *     possible object is
             *     {@link QueueStatus }
             *     
             */
            public QueueStatus getState() {
                return state;
            }

            /**
             * Sets the value of the state property.
             * 
             * @param value
             *     allowed object is
             *     {@link QueueStatus }
             *     
             */
            public void setState(QueueStatus value) {
                this.state = value;
            }

            /**
             * Gets the value of the priority property.
             * 
             * @return
             *     possible object is
             *     {@link QueuePriority }
             *     
             */
            public QueuePriority getPriority() {
                return priority;
            }

            /**
             * Sets the value of the priority property.
             * 
             * @param value
             *     allowed object is
             *     {@link QueuePriority }
             *     
             */
            public void setPriority(QueuePriority value) {
                this.priority = value;
            }

            /**
             * Gets the value of the description property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Sets the value of the description property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

        }

    }

}
