
package com.sysfera.vishnu.api.tms;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="data" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="jobresult" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                           &lt;attribute name="jobId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="outputPath" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="errorPath" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="nbJobs" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data",
    "nbJobs"
})
@XmlRootElement(name = "getCompletedJobsOutputResponse")
public class GetCompletedJobsOutputResponse {

    protected GetCompletedJobsOutputResponse.Data data;
    @XmlElement(required = true)
    protected BigInteger nbJobs;

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link GetCompletedJobsOutputResponse.Data }
     *     
     */
    public GetCompletedJobsOutputResponse.Data getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCompletedJobsOutputResponse.Data }
     *     
     */
    public void setData(GetCompletedJobsOutputResponse.Data value) {
        this.data = value;
    }

    /**
     * Gets the value of the nbJobs property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNbJobs() {
        return nbJobs;
    }

    /**
     * Sets the value of the nbJobs property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNbJobs(BigInteger value) {
        this.nbJobs = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="jobresult" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *                 &lt;attribute name="jobId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="outputPath" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="errorPath" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "jobresult"
    })
    public static class Data {

        @XmlElement(nillable = true)
        protected List<GetCompletedJobsOutputResponse.Data.Jobresult> jobresult;

        /**
         * Gets the value of the jobresult property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the jobresult property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getJobresult().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GetCompletedJobsOutputResponse.Data.Jobresult }
         * 
         * 
         */
        public List<GetCompletedJobsOutputResponse.Data.Jobresult> getJobresult() {
            if (jobresult == null) {
                jobresult = new ArrayList<GetCompletedJobsOutputResponse.Data.Jobresult>();
            }
            return this.jobresult;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *       &lt;/sequence>
         *       &lt;attribute name="jobId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="outputPath" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="errorPath" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Jobresult {

            @XmlAttribute(name = "jobId")
            protected String jobId;
            @XmlAttribute(name = "outputPath")
            protected String outputPath;
            @XmlAttribute(name = "errorPath")
            protected String errorPath;

            /**
             * Gets the value of the jobId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getJobId() {
                return jobId;
            }

            /**
             * Sets the value of the jobId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setJobId(String value) {
                this.jobId = value;
            }

            /**
             * Gets the value of the outputPath property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOutputPath() {
                return outputPath;
            }

            /**
             * Sets the value of the outputPath property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOutputPath(String value) {
                this.outputPath = value;
            }

            /**
             * Gets the value of the errorPath property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getErrorPath() {
                return errorPath;
            }

            /**
             * Sets the value of the errorPath property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setErrorPath(String value) {
                this.errorPath = value;
            }

        }

    }

}
