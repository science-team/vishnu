
package com.sysfera.vishnu.api.ums;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="data" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="machine" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                           &lt;attribute name="machineId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="site" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="machineDescription" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="language" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="status" type="{urn:ResourceProxy}StatusType" />
 *                           &lt;attribute name="sshPublicKey" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data"
})
@XmlRootElement(name = "listMachinesResponse")
public class ListMachinesResponse {

    protected ListMachinesResponse.Data data;

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link ListMachinesResponse.Data }
     *     
     */
    public ListMachinesResponse.Data getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListMachinesResponse.Data }
     *     
     */
    public void setData(ListMachinesResponse.Data value) {
        this.data = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="machine" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *                 &lt;attribute name="machineId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="site" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="machineDescription" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="language" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="status" type="{urn:ResourceProxy}StatusType" />
     *                 &lt;attribute name="sshPublicKey" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "machine"
    })
    public static class Data {

        @XmlElement(nillable = true)
        protected List<ListMachinesResponse.Data.Machine> machine;

        /**
         * Gets the value of the machine property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the machine property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMachine().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ListMachinesResponse.Data.Machine }
         * 
         * 
         */
        public List<ListMachinesResponse.Data.Machine> getMachine() {
            if (machine == null) {
                machine = new ArrayList<ListMachinesResponse.Data.Machine>();
            }
            return this.machine;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *       &lt;/sequence>
         *       &lt;attribute name="machineId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="site" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="machineDescription" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="language" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="status" type="{urn:ResourceProxy}StatusType" />
         *       &lt;attribute name="sshPublicKey" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Machine {

            @XmlAttribute(name = "machineId", required = true)
            protected String machineId;
            @XmlAttribute(name = "name")
            protected String name;
            @XmlAttribute(name = "site")
            protected String site;
            @XmlAttribute(name = "machineDescription")
            protected String machineDescription;
            @XmlAttribute(name = "language")
            protected String language;
            @XmlAttribute(name = "status")
            protected StatusType status;
            @XmlAttribute(name = "sshPublicKey")
            protected String sshPublicKey;

            /**
             * Gets the value of the machineId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMachineId() {
                return machineId;
            }

            /**
             * Sets the value of the machineId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMachineId(String value) {
                this.machineId = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the site property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSite() {
                return site;
            }

            /**
             * Sets the value of the site property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSite(String value) {
                this.site = value;
            }

            /**
             * Gets the value of the machineDescription property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMachineDescription() {
                return machineDescription;
            }

            /**
             * Sets the value of the machineDescription property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMachineDescription(String value) {
                this.machineDescription = value;
            }

            /**
             * Gets the value of the language property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLanguage() {
                return language;
            }

            /**
             * Sets the value of the language property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLanguage(String value) {
                this.language = value;
            }

            /**
             * Gets the value of the status property.
             * 
             * @return
             *     possible object is
             *     {@link StatusType }
             *     
             */
            public StatusType getStatus() {
                return status;
            }

            /**
             * Sets the value of the status property.
             * 
             * @param value
             *     allowed object is
             *     {@link StatusType }
             *     
             */
            public void setStatus(StatusType value) {
                this.status = value;
            }

            /**
             * Gets the value of the sshPublicKey property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSshPublicKey() {
                return sshPublicKey;
            }

            /**
             * Sets the value of the sshPublicKey property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSshPublicKey(String value) {
                this.sshPublicKey = value;
            }

        }

    }

}
