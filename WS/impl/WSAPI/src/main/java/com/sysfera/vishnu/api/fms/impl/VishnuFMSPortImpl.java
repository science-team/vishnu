package com.sysfera.vishnu.api.fms.impl;

import java.math.BigInteger;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebService;

import com.sysfera.vishnu.api.fms.ChGrpRequest;
import com.sysfera.vishnu.api.fms.ChGrpResponse;
import com.sysfera.vishnu.api.fms.ChModRequest;
import com.sysfera.vishnu.api.fms.ChModResponse;
import com.sysfera.vishnu.api.fms.ContentOfFileRequest;
import com.sysfera.vishnu.api.fms.ContentOfFileResponse;
import com.sysfera.vishnu.api.fms.CopyAsyncFileRequest;
import com.sysfera.vishnu.api.fms.CopyAsyncFileResponse;
import com.sysfera.vishnu.api.fms.CopyFileRequest;
import com.sysfera.vishnu.api.fms.CopyFileResponse;
import com.sysfera.vishnu.api.fms.CreateDirRequest;
import com.sysfera.vishnu.api.fms.CreateDirResponse;
import com.sysfera.vishnu.api.fms.CreateFileRequest;
import com.sysfera.vishnu.api.fms.CreateFileResponse;
import com.sysfera.vishnu.api.fms.DBERRFault;
import com.sysfera.vishnu.api.fms.DBERRMessage;
import com.sysfera.vishnu.api.fms.GetFileInfoRequest;
import com.sysfera.vishnu.api.fms.GetFileInfoResponse;
import com.sysfera.vishnu.api.fms.HeadOfFileRequest;
import com.sysfera.vishnu.api.fms.HeadOfFileResponse;
import com.sysfera.vishnu.api.fms.INVALIDPARAMFault;
import com.sysfera.vishnu.api.fms.INVALIDPARAMMessage;
import com.sysfera.vishnu.api.fms.INVALIDPATHFault;
import com.sysfera.vishnu.api.fms.INVALIDPATHMessage;
import com.sysfera.vishnu.api.fms.ListDirRequest;
import com.sysfera.vishnu.api.fms.ListDirResponse;
import com.sysfera.vishnu.api.fms.ListFileTransfersRequest;
import com.sysfera.vishnu.api.fms.ListFileTransfersResponse;
import com.sysfera.vishnu.api.fms.MoveAsyncFileRequest;
import com.sysfera.vishnu.api.fms.MoveAsyncFileResponse;
import com.sysfera.vishnu.api.fms.MoveFileRequest;
import com.sysfera.vishnu.api.fms.MoveFileResponse;
import com.sysfera.vishnu.api.fms.RUNTIMEERRORMessage;
import com.sysfera.vishnu.api.fms.RUNTIMEERRORFault;
import com.sysfera.vishnu.api.fms.RemoveDirRequest;
import com.sysfera.vishnu.api.fms.RemoveDirResponse;
import com.sysfera.vishnu.api.fms.RemoveFileRequest;
import com.sysfera.vishnu.api.fms.RemoveFileResponse;
import com.sysfera.vishnu.api.fms.SESSIONKEYEXPIREDFault;
import com.sysfera.vishnu.api.fms.SESSIONKEYEXPIREDMessage;
import com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDFault;
import com.sysfera.vishnu.api.fms.SESSIONKEYNOTFOUNDMessage;
import com.sysfera.vishnu.api.fms.StopFileTransferRequest;
import com.sysfera.vishnu.api.fms.StopFileTransferResponse;
import com.sysfera.vishnu.api.fms.TailOfFileRequest;
import com.sysfera.vishnu.api.fms.TailOfFileResponse;
import com.sysfera.vishnu.api.fms.UNKNOWNUSERIDFault;
import com.sysfera.vishnu.api.fms.UNKNOWNUSERIDMessage;
import com.sysfera.vishnu.api.fms.VishnuFMSPortType;
import com.sysfera.vishnu.api.fms.UNKNOWNLOCALACCOUNTFault;
import com.sysfera.vishnu.api.fms.UNKNOWNLOCALACCOUNTMessage;
import com.sysfera.vishnu.api.fms.UNKNOWNMACHINEFault;
import com.sysfera.vishnu.api.fms.UNKNOWNMACHINEMessage;
import com.sysfera.vishnu.api.fms.CONFIGNOTFOUNDFault;
import com.sysfera.vishnu.api.fms.CONFIGNOTFOUNDMessage;
import com.sysfera.vishnu.api.fms.DIETFault;
import com.sysfera.vishnu.api.fms.DIETMessage;
import com.sysfera.vishnu.api.fms.UNDEFINEDFault;
import com.sysfera.vishnu.api.fms.UNDEFINEDMessage;
import com.sysfera.vishnu.api.fms.UNKNOWNFILETRANSFERMessage;
import com.sysfera.vishnu.api.fms.UNKNOWNFILETRANSFERFault;
import com.sysfera.vishnu.api.vishnu.internal.CpFileOptions;
import com.sysfera.vishnu.api.vishnu.internal.FileStat;
import com.sysfera.vishnu.api.vishnu.internal.FileTransfer;
import com.sysfera.vishnu.api.vishnu.internal.FileTransferList;
import com.sysfera.vishnu.api.vishnu.internal.HeadOfFileOptions;
import com.sysfera.vishnu.api.vishnu.internal.LsDirOptions;
import com.sysfera.vishnu.api.vishnu.internal.LsTransferOptions;
import com.sysfera.vishnu.api.vishnu.internal.RmFileOptions;
import com.sysfera.vishnu.api.vishnu.internal.StopTransferOptions;
import com.sysfera.vishnu.api.vishnu.internal.TailOfFileOptions;
import com.sysfera.vishnu.api.vishnu.internal.CreateDirOptions;
import com.sysfera.vishnu.api.vishnu.internal.VISHNU;
import com.sysfera.vishnu.api.vishnu.internal.InternalFMSException;
import com.sysfera.vishnu.api.vishnu.internal.DirEntry;
import com.sysfera.vishnu.api.vishnu.internal.DirEntryList;

@WebService(targetNamespace = "urn:ResourceProxy", name = "VishnuFMS", serviceName = "VishnuFMSService")
@Remote(VishnuFMSPortType.class)
@Stateless
public class VishnuFMSPortImpl implements VishnuFMSPortType {

	private void splitExcep (String s1, String[] array ){
	    
	    Integer i = s1.indexOf('#');
	    Integer j = s1.lastIndexOf('#');
	    
	    if(i!=-1){
		array[0] = s1.substring(0, i);
		array[1] = s1.substring(j+1);
		if(j!=i+1){
		    array[1] += ": ";
		    array[1] += s1.substring(i+1, j);
		}
	    }else{
		array[0] = "7"; // 7 is invalid system exception in C++
		array[1] = "Error ! Invalid exception gotten";
	    }
	}
	
	public HeadOfFileResponse headOfFile(HeadOfFileRequest parameters)
			throws  INVALIDPATHMessage,
		 DBERRMessage,
			SESSIONKEYNOTFOUNDMessage,
			INVALIDPARAMMessage, UNKNOWNLOCALACCOUNTMessage, UNKNOWNMACHINEMessage, CONFIGNOTFOUNDMessage, DIETMessage, RUNTIMEERRORMessage, SESSIONKEYEXPIREDMessage, UNDEFINEDMessage, UNKNOWNFILETRANSFERMessage {
		HeadOfFileResponse res = new HeadOfFileResponse();
		HeadOfFileOptions op = new HeadOfFileOptions();
		String[] content = {""};
		if (parameters.getSessionKey() == null) {
			parameters.setSessionKey("");
		}
		
		if (parameters.getPath()==null) {
			parameters.setPath("");
		}
		
		if (parameters.getNline()!=null) {
			op.setNline(parameters.getNline().intValue());
		}
		
		try {
			VISHNU.headOfFile(parameters.getSessionKey(), parameters.getPath(), content, op);
		}catch(InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		res.setFileContent(content[0]);
		return res;
	}

	public MoveAsyncFileResponse moveAsyncFile(MoveAsyncFileRequest parameters)
			throws INVALIDPATHMessage,
			DBERRMessage,
			SESSIONKEYNOTFOUNDMessage, DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage
			{
		MoveAsyncFileResponse res = new MoveAsyncFileResponse();
		FileTransfer ti = new FileTransfer();
		CpFileOptions op = new CpFileOptions();
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		if (parameters.getDest()==null) {
			parameters.setDest("");
		}
		if (parameters.getSrc()==null) {
			parameters.setSrc("");
		}
		if (parameters.isIsRecursive()) {
			op.setIsRecursive(parameters.isIsRecursive());
		}
		if (parameters.getTrCommand() != null) {
			op.setTrCommand(com.sysfera.vishnu.api.fms.data.TransferCommand
					.getByName(parameters.getTrCommand().value()).getValue());
		}
		try {
			VISHNU.moveAsyncFile(parameters.getSessionKey(), parameters.getSrc(), parameters.getDest(), ti, op);
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		res.setClientMachineId(ti.getClientMachineId());
		res.setDestinationFilePath(ti.getDestinationFilePath());
		res.setDestinationMachineId(ti.getDestinationMachineId());
		res.setErrorMsg(ti.getErrorMsg());
		res.setSize(ti.getSize());
		res.setSourceFilePath(ti.getSourceFilePath());
		res.setSourceMachineId(ti.getSourceMachineId());
		res.setStartTime(ti.getStart_time());
		res.setTransferId(ti.getTransferId());
		res.setUserId(ti.getUserId());
		res.setStatus(com.sysfera.vishnu.api.fms.Status
				 .fromValue(com.sysfera.vishnu.api.fms.data.StatusType.get(
						  ti.getStatus()).getLiteral()));
		res.setTrCommand(com.sysfera.vishnu.api.fms.TransferCommand
				 .fromValue(com.sysfera.vishnu.api.fms.data.TransferCommand.get(
						  ti.getTrCommand()).getLiteral()));
		return res;
	}

	public RemoveDirResponse removeDir(RemoveDirRequest parameters)
			throws INVALIDPATHMessage,
			DBERRMessage,
			 SESSIONKEYNOTFOUNDMessage, DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage {
		RemoveDirResponse res = new RemoveDirResponse();
		if (parameters.getPath()==null) {
			parameters.setPath("");
		}
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		try {
			VISHNU.removeDir(parameters.getSessionKey(), parameters.getPath());
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

	public ChModResponse chMod(ChModRequest parameters)
			throws INVALIDPATHMessage,
			DBERRMessage,
			DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage, SESSIONKEYNOTFOUNDMessage {
		ChModResponse res = new ChModResponse();
		if (parameters.getMode()==null) {
			parameters.setMode(new BigInteger("0"));
		}
		if (parameters.getPath()==null) {
			parameters.setPath("");
		}
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		try {
			VISHNU.chMod(parameters.getSessionKey(), parameters.getMode().intValue(), parameters.getPath());
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

	public GetFileInfoResponse getFileInfo(GetFileInfoRequest parameters)
			throws INVALIDPATHMessage,
			DBERRMessage,
			SESSIONKEYNOTFOUNDMessage, DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage{
		GetFileInfoResponse res = new GetFileInfoResponse();
		FileStat fi = new FileStat();
		if (parameters.getPath()==null) {
			parameters.setPath("");
		}
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		try {
			VISHNU.getFileInfo(parameters.getSessionKey(), parameters.getPath(), fi);
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		res.setAtime(fi.getAtime());
		res.setCtime(fi.getCtime());
		res.setGid(fi.getGid());
		res.setGroup(fi.getGroup());
		res.setMtime(fi.getMtime());
		res.setOwner(fi.getOwner());
		res.setPath(fi.getPath());
		Long l = new Long(fi.getPerms());
		BigInteger b = new BigInteger(l.toOctalString(l));
		res.setPerms(b);
		res.setSize(fi.getSize());
		res.setType(com.sysfera.vishnu.api.fms.FileType
				 .fromValue(com.sysfera.vishnu.api.fms.data.FileType.get(
						  fi.getType()).getLiteral()));
		res.setUid(fi.getUid());
		return res;
	}

	public CopyFileResponse copyFile(CopyFileRequest parameters)
			throws INVALIDPATHMessage,
			 DBERRMessage,
			SESSIONKEYNOTFOUNDMessage, DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage{
		CopyFileResponse res = new CopyFileResponse();
		CpFileOptions op = new CpFileOptions();
		if (parameters.getDest()==null) {
			parameters.setDest("");
		}
		if (parameters.getSessionKey()==null){
			parameters.setSessionKey("");
		}
		if (parameters.getSrc()==null) {
			parameters.setSrc("");
		}
		if (parameters.getTrCommand() != null) {
			op.setTrCommand(com.sysfera.vishnu.api.fms.data.TransferCommand
					.getByName(parameters.getTrCommand().value()).getValue());
		}
		if (parameters.isIsRecursive()) {
			op.setIsRecursive(parameters.isIsRecursive());
		}
		try {
			VISHNU.copyFile(parameters.getSessionKey(), parameters.getSrc(), parameters.getDest(), op);
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

	public CopyAsyncFileResponse copyAsyncFile(CopyAsyncFileRequest parameters)
			throws INVALIDPATHMessage,
			 DBERRMessage,
			 SESSIONKEYNOTFOUNDMessage, DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage{
		CopyAsyncFileResponse res = new CopyAsyncFileResponse();
		CpFileOptions op = new CpFileOptions();
		FileTransfer ti = new FileTransfer();
		if (parameters.getDest()==null) {
			parameters.setDest("");
		}
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		if (parameters.getSrc()==null) {
			parameters.setSrc("");
		}
		if (parameters.getTrCommand() != null) {
			op.setTrCommand(com.sysfera.vishnu.api.fms.data.TransferCommand
					.getByName(parameters.getTrCommand().value()).getValue());
		}
		if (parameters.isIsRecursive()) {
			op.setIsRecursive(parameters.isIsRecursive());
		}
		try {
			VISHNU.copyAsyncFile(parameters.getSessionKey(), parameters.getSrc(), parameters.getDest(), ti, op);
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		res.setClientMachineId(ti.getClientMachineId());
		res.setDestinationFilePath(ti.getDestinationFilePath());
		res.setDestinationMachineId(ti.getDestinationMachineId());
		res.setErrorMsg(ti.getErrorMsg());
		res.setSize(ti.getSize());
		res.setSourceFilePath(ti.getSourceFilePath());
		res.setSourceMachineId(ti.getSourceMachineId());
		res.setStartTime(ti.getStart_time());
		res.setStatus(com.sysfera.vishnu.api.fms.Status
				 .fromValue(com.sysfera.vishnu.api.fms.data.StatusType.get(
						  ti.getStatus()).getLiteral()));
		res.setTransferId(ti.getTransferId());
		res.setTrCommand(com.sysfera.vishnu.api.fms.TransferCommand
				 .fromValue(com.sysfera.vishnu.api.fms.data.TransferCommand.get(
						  ti.getTrCommand()).getLiteral()));
		res.setUserId(ti.getUserId());
		return res;
	}
//////////////////////////////////////////////////////////////////////////////////////////////////
	public ListDirResponse listDir(ListDirRequest parameters)
	throws INVALIDPATHMessage,
	DBERRMessage,
	SESSIONKEYNOTFOUNDMessage, DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage{
		ListDirResponse res = new ListDirResponse();
		ListDirResponse.Data dat = new ListDirResponse.Data();
		LsDirOptions op = new LsDirOptions();
		DirEntryList dir = new DirEntryList();
		
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		
		if (parameters.getPath()==null) {
			parameters.setPath("");
		}
		if (parameters.isAllFiles()) {
			op.setAllFiles(parameters.isAllFiles());
		}
		if (parameters.isLongFormat()) {
			op.setLongFormat(parameters.isLongFormat());
		}
 
		try  {
			VISHNU.listDir(parameters.getSessionKey(), parameters.getPath(), dir, op);
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		int i;
	    for (i = 0; i < dir.getDirEntries().size(); i++) {
	    	ListDirResponse.Data.Direntry opva = new ListDirResponse.Data.Direntry();
	    	com.sysfera.vishnu.api.vishnu.internal.DirEntry m = dir.getDirEntries().get(i);
	    	opva.setPath(m.getPath());
	    	Long l = new Long(m.getPerms());
			BigInteger b = new BigInteger(l.toOctalString(l));
	    	opva.setPerms(b);
	    	opva.setOwner(m.getOwner());
	    	opva.setGroup(m.getGroup());
	    	opva.setSize(m.getSize());
	    	opva.setCreationTime(m.getCreationTime());
	    	
	    	opva.setType(com.sysfera.vishnu.api.fms.FileType.fromValue(com.sysfera.vishnu.api.fms.data.FileType.get(
	    			m.getType()).getLiteral()));
	    	
	    	dat.getDirentry().add(opva);
	    }
	    res.setData(dat);
		return res;
	}
	
	

	
	public CreateFileResponse createFile(CreateFileRequest parameters)
			throws INVALIDPATHMessage,
			DBERRMessage,
			SESSIONKEYNOTFOUNDMessage, DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage {
		CreateFileResponse res = new CreateFileResponse();
		if (parameters.getPath()==null) {
			parameters.setPath("");
		}
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		try {
			VISHNU.createFile(parameters.getSessionKey(), parameters.getPath());
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

	public StopFileTransferResponse stopFileTransfer(
			StopFileTransferRequest parameters) throws
			INVALIDPATHMessage, DBERRMessage,
			SESSIONKEYNOTFOUNDMessage, DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage, UNKNOWNUSERIDMessage {
		StopFileTransferResponse res = new StopFileTransferResponse();
		StopTransferOptions op = new StopTransferOptions();
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		if (parameters.getFromMachineId()!=null) {
			op.setFromMachineId(parameters.getFromMachineId());
		}
		if (parameters.getTransferId()!=null) {
			op.setTransferId(parameters.getTransferId());
		}
		if (parameters.getUserId()!=null) {
			op.setUserId(parameters.getUserId());
		}
		try  {
			VISHNU.stopFileTransfer(parameters.getSessionKey(), op);
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 21 :
				throw new UNKNOWNUSERIDMessage(msg, new UNKNOWNUSERIDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

	public CreateDirResponse createDir(CreateDirRequest parameters)
			throws  INVALIDPATHMessage,
			DBERRMessage,
			SESSIONKEYNOTFOUNDMessage, DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage{
		CreateDirResponse res = new CreateDirResponse();
		CreateDirOptions op = new CreateDirOptions();
		
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		
		if (parameters.getPath()==null) {
			parameters.setSessionKey("");
		}
		if (parameters.isIsRecursive()!=null) {
			op.setIsRecursive(parameters.isIsRecursive());
		}
		try {
			VISHNU.createDir(parameters.getSessionKey(), parameters.getPath(),op);
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

	public ChGrpResponse chGrp(ChGrpRequest parameters)
			throws INVALIDPATHMessage,
			DBERRMessage,
			SESSIONKEYNOTFOUNDMessage, DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage{
		ChGrpResponse res = new ChGrpResponse();
		if (parameters.getGroup()==null) {
			parameters.setGroup("");
		}
		if (parameters.getPath()==null) {
			parameters.setPath("");
		}
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		try {
			VISHNU.chGrp(parameters.getSessionKey(), parameters.getGroup(), parameters.getPath());
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

	public TailOfFileResponse tailOfFile(TailOfFileRequest parameters)
			throws  INVALIDPATHMessage,
			 DBERRMessage,
			 SESSIONKEYNOTFOUNDMessage, DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage{
		TailOfFileResponse res = new TailOfFileResponse();
		String[] cont = {""};
		TailOfFileOptions op = new TailOfFileOptions();
		if (parameters.getNline()!=null) {
			op.setNline(parameters.getNline().intValue());
		}
		if (parameters.getPath()==null) {
			parameters.setPath("");
		}
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		try  {
			VISHNU.tailOfFile(parameters.getSessionKey(), parameters.getPath(), cont, op);
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		res.setFileContent(cont[0]);
		return res;
	}

	public ListFileTransfersResponse listFileTransfers(
			ListFileTransfersRequest parameters)
			throws INVALIDPATHMessage,
			DBERRMessage,
			SESSIONKEYNOTFOUNDMessage, DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage, UNKNOWNUSERIDMessage{
		ListFileTransfersResponse res = new ListFileTransfersResponse();
		ListFileTransfersResponse.Data dat = new ListFileTransfersResponse.Data();
		LsTransferOptions op = new LsTransferOptions();
		FileTransferList li = new FileTransferList();
		
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		if (parameters.getFromMachineId()!=null) {
			op.setFromMachineId(parameters.getFromMachineId());
		}
		if (parameters.getStatus()!=null) {
			op.setStatus(com.sysfera.vishnu.api.fms.data.StatusType
					.getByName(parameters.getStatus().value()).getValue());
		}
		if (parameters.getTransferId()!=null) {
                    op.setTransferId(parameters.getTransferId());
		}
                //  TODO COMMENTED BECAUSE OTHERWIZE USERPARAM PASSED, BUT ADMIN OPTION -> IMPOSSIBLE
//		if (parameters.getUserId()!=null) {
//			op.setUserId(parameters.getUserId());
//		}
		try  {
			VISHNU.listFileTransfers(parameters.getSessionKey(), li, op);
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 21 :
				throw new UNKNOWNUSERIDMessage(msg, new UNKNOWNUSERIDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		int i;
	    for (i = 0; i < li.getFileTransfers().size(); i++) {
	    	ListFileTransfersResponse.Data.Filetransfer opva = new ListFileTransfersResponse.Data.Filetransfer();
	    	com.sysfera.vishnu.api.vishnu.internal.FileTransfer m = li.getFileTransfers().get(i);
	    	opva.setClientMachineId(m.getClientMachineId());
	    	opva.setDestinationFilePath(m.getDestinationFilePath());
	    	opva.setDestinationMachineId(m.getDestinationMachineId());
	    	opva.setErrorMsg(m.getErrorMsg());
	    	opva.setSize(m.getSize());
	    	opva.setSourceFilePath(m.getSourceFilePath());
	    	opva.setSourceMachineId(m.getSourceMachineId());
	    	opva.setStartTime(m.getStart_time());
	    	opva.setStatus(com.sysfera.vishnu.api.fms.Status
					 .fromValue(com.sysfera.vishnu.api.fms.data.StatusType.get(
							  m.getStatus()).getLiteral()));
	    	opva.setTransferId(m.getTransferId());
	    	opva.setTrCommand(com.sysfera.vishnu.api.fms.TransferCommand
					 .fromValue(com.sysfera.vishnu.api.fms.data.TransferCommand.get(
							 m.getTrCommand()).getLiteral()));
	    	opva.setUserId(m.getUserId());
	    	dat.getFiletransfer().add(opva);
	    }
	    res.setData(dat);
		return res;
	}

	public ContentOfFileResponse contentOfFile(ContentOfFileRequest parameters)
			throws  INVALIDPATHMessage,
		 DBERRMessage,
		 SESSIONKEYNOTFOUNDMessage, DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage{
		ContentOfFileResponse res = new ContentOfFileResponse();
		String[] cont = {""};	
		if (parameters.getPath()==null) {
			parameters.setPath("");
		}
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		try  {
			VISHNU.contentOfFile(parameters.getSessionKey(), parameters.getPath(), cont);
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		res.setFileContent(cont[0]);
		return res;
	}

	public MoveFileResponse moveFile(MoveFileRequest parameters)
			throws INVALIDPATHMessage,
			DBERRMessage,
			 SESSIONKEYNOTFOUNDMessage, DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage {
		MoveFileResponse res = new MoveFileResponse();
		CpFileOptions op = new CpFileOptions();
		if (parameters.getDest()==null) {
			parameters.setDest("");
		}
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		if (parameters.getSrc()==null) {
			parameters.setSrc("");
		}
		if (parameters.isIsRecursive()) {
			op.setIsRecursive(parameters.isIsRecursive());
		}
		if (parameters.getTrCommand() != null) {
			op.setTrCommand(com.sysfera.vishnu.api.fms.data.TransferCommand
					.getByName(parameters.getTrCommand().value()).getValue());
		}
		try  {
			VISHNU.moveFile(parameters.getSessionKey(), parameters.getSrc(), parameters.getDest(), op);
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

	public RemoveFileResponse removeFile(RemoveFileRequest parameters)
			throws INVALIDPATHMessage,
			DBERRMessage,
			 SESSIONKEYNOTFOUNDMessage, DIETMessage, INVALIDPARAMMessage, CONFIGNOTFOUNDMessage, SESSIONKEYEXPIREDMessage, UNKNOWNMACHINEMessage, UNKNOWNLOCALACCOUNTMessage, RUNTIMEERRORMessage, UNKNOWNFILETRANSFERMessage, UNDEFINEDMessage {
		RemoveFileResponse res = new RemoveFileResponse();
		RmFileOptions op = new RmFileOptions();
		if (parameters.getPath()==null) {
			parameters.setPath("");
		}
		if (parameters.getSessionKey()==null) {
			parameters.setSessionKey("");
		}
		if (parameters.isIsRecursive()!=null) {
			op.setIsRecursive(parameters.isIsRecursive());
		}
		try {
			VISHNU.removeFile(parameters.getSessionKey(), parameters.getPath(), op);
		} catch (InternalFMSException e) {
			String code="", msg="";
			String tmp[]={"", ""};
			splitExcep(e.getMessage(), tmp);
			code = tmp[0];
			msg = tmp[1];
			switch (new Integer(code)){
			case 1 :
				throw new DIETMessage(msg, new DIETFault());
			case 2 :
			    throw new DBERRMessage(msg, new DBERRFault());
			case 10 :
			    throw new INVALIDPARAMMessage(msg, new INVALIDPARAMFault());
			case 12 :
				throw new CONFIGNOTFOUNDMessage(msg, new CONFIGNOTFOUNDFault());
			case 28 :
			    throw new SESSIONKEYNOTFOUNDMessage(msg, new SESSIONKEYNOTFOUNDFault());
			case 29 :
			    throw new SESSIONKEYEXPIREDMessage(msg, new SESSIONKEYEXPIREDFault());
			case 32 :
				throw new UNKNOWNMACHINEMessage(msg, new UNKNOWNMACHINEFault());
			case 38:
			    throw new UNKNOWNLOCALACCOUNTMessage(msg, new UNKNOWNLOCALACCOUNTFault());
			case 201 :
			    throw new INVALIDPATHMessage(msg, new INVALIDPATHFault());
			case 202:
				throw new RUNTIMEERRORMessage(msg, new RUNTIMEERRORFault());
			case 203 :
				throw new UNKNOWNFILETRANSFERMessage(msg, new UNKNOWNFILETRANSFERFault());
			default :
				throw new UNDEFINEDMessage(msg, new UNDEFINEDFault());
			}
		}
		return res;
	}

}
