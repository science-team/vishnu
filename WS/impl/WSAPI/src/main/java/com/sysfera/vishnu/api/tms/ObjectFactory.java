
package com.sysfera.vishnu.api.tms;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sysfera.vishnu.api.tms package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sysfera.vishnu.api.tms
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SSHFault }
     * 
     */
    public SSHFault createSSHFault() {
        return new SSHFault();
    }

    /**
     * Create an instance of {@link CONFIGNOTFOUNDFault }
     * 
     */
    public CONFIGNOTFOUNDFault createCONFIGNOTFOUNDFault() {
        return new CONFIGNOTFOUNDFault();
    }

    /**
     * Create an instance of {@link SubmitJobRequest.Data.Loadcriterion }
     * 
     */
    public SubmitJobRequest.Data.Loadcriterion createSubmitJobRequestDataLoadcriterion() {
        return new SubmitJobRequest.Data.Loadcriterion();
    }

    /**
     * Create an instance of {@link SESSIONKEYNOTFOUNDFault }
     * 
     */
    public SESSIONKEYNOTFOUNDFault createSESSIONKEYNOTFOUNDFault() {
        return new SESSIONKEYNOTFOUNDFault();
    }

    /**
     * Create an instance of {@link CancelJobRequest }
     * 
     */
    public CancelJobRequest createCancelJobRequest() {
        return new CancelJobRequest();
    }

    /**
     * Create an instance of {@link ALREADYTERMINATEDFault }
     * 
     */
    public ALREADYTERMINATEDFault createALREADYTERMINATEDFault() {
        return new ALREADYTERMINATEDFault();
    }

    /**
     * Create an instance of {@link GetJobInfoResponse }
     * 
     */
    public GetJobInfoResponse createGetJobInfoResponse() {
        return new GetJobInfoResponse();
    }

    /**
     * Create an instance of {@link GetCompletedJobsOutputRequest }
     * 
     */
    public GetCompletedJobsOutputRequest createGetCompletedJobsOutputRequest() {
        return new GetCompletedJobsOutputRequest();
    }

    /**
     * Create an instance of {@link ListJobsRequest }
     * 
     */
    public ListJobsRequest createListJobsRequest() {
        return new ListJobsRequest();
    }

    /**
     * Create an instance of {@link BATCHSCHEDULERERRORFault }
     * 
     */
    public BATCHSCHEDULERERRORFault createBATCHSCHEDULERERRORFault() {
        return new BATCHSCHEDULERERRORFault();
    }

    /**
     * Create an instance of {@link ListJobsResponse }
     * 
     */
    public ListJobsResponse createListJobsResponse() {
        return new ListJobsResponse();
    }

    /**
     * Create an instance of {@link PERMISSIONDENIEDFault }
     * 
     */
    public PERMISSIONDENIEDFault createPERMISSIONDENIEDFault() {
        return new PERMISSIONDENIEDFault();
    }

    /**
     * Create an instance of {@link GetJobProgressResponse.Data }
     * 
     */
    public GetJobProgressResponse.Data createGetJobProgressResponseData() {
        return new GetJobProgressResponse.Data();
    }

    /**
     * Create an instance of {@link GetJobInfoRequest }
     * 
     */
    public GetJobInfoRequest createGetJobInfoRequest() {
        return new GetJobInfoRequest();
    }

    /**
     * Create an instance of {@link GetJobProgressResponse }
     * 
     */
    public GetJobProgressResponse createGetJobProgressResponse() {
        return new GetJobProgressResponse();
    }

    /**
     * Create an instance of {@link UNKNOWNSESSIONIDFault }
     * 
     */
    public UNKNOWNSESSIONIDFault createUNKNOWNSESSIONIDFault() {
        return new UNKNOWNSESSIONIDFault();
    }

    /**
     * Create an instance of {@link SESSIONKEYEXPIREDFault }
     * 
     */
    public SESSIONKEYEXPIREDFault createSESSIONKEYEXPIREDFault() {
        return new SESSIONKEYEXPIREDFault();
    }

    /**
     * Create an instance of {@link GetCompletedJobsOutputResponse }
     * 
     */
    public GetCompletedJobsOutputResponse createGetCompletedJobsOutputResponse() {
        return new GetCompletedJobsOutputResponse();
    }

    /**
     * Create an instance of {@link GetCompletedJobsOutputResponse.Data.Jobresult }
     * 
     */
    public GetCompletedJobsOutputResponse.Data.Jobresult createGetCompletedJobsOutputResponseDataJobresult() {
        return new GetCompletedJobsOutputResponse.Data.Jobresult();
    }

    /**
     * Create an instance of {@link GetCompletedJobsOutputResponse.Data }
     * 
     */
    public GetCompletedJobsOutputResponse.Data createGetCompletedJobsOutputResponseData() {
        return new GetCompletedJobsOutputResponse.Data();
    }

    /**
     * Create an instance of {@link UNDEFINEDFault }
     * 
     */
    public UNDEFINEDFault createUNDEFINEDFault() {
        return new UNDEFINEDFault();
    }

    /**
     * Create an instance of {@link ListQueuesResponse }
     * 
     */
    public ListQueuesResponse createListQueuesResponse() {
        return new ListQueuesResponse();
    }

    /**
     * Create an instance of {@link ListQueuesResponse.Data.Queue }
     * 
     */
    public ListQueuesResponse.Data.Queue createListQueuesResponseDataQueue() {
        return new ListQueuesResponse.Data.Queue();
    }

    /**
     * Create an instance of {@link UNKNOWNJOBIDFault }
     * 
     */
    public UNKNOWNJOBIDFault createUNKNOWNJOBIDFault() {
        return new UNKNOWNJOBIDFault();
    }

    /**
     * Create an instance of {@link UNKNOWNBATCHSCHEDULERFault }
     * 
     */
    public UNKNOWNBATCHSCHEDULERFault createUNKNOWNBATCHSCHEDULERFault() {
        return new UNKNOWNBATCHSCHEDULERFault();
    }

    /**
     * Create an instance of {@link SubmitJobRequest }
     * 
     */
    public SubmitJobRequest createSubmitJobRequest() {
        return new SubmitJobRequest();
    }

    /**
     * Create an instance of {@link GetJobProgressResponse.Data.Progression }
     * 
     */
    public GetJobProgressResponse.Data.Progression createGetJobProgressResponseDataProgression() {
        return new GetJobProgressResponse.Data.Progression();
    }

    /**
     * Create an instance of {@link INVALIDPARAMFault }
     * 
     */
    public INVALIDPARAMFault createINVALIDPARAMFault() {
        return new INVALIDPARAMFault();
    }

    /**
     * Create an instance of {@link FILENOTFOUNDFault }
     * 
     */
    public FILENOTFOUNDFault createFILENOTFOUNDFault() {
        return new FILENOTFOUNDFault();
    }

    /**
     * Create an instance of {@link DBERRFault }
     * 
     */
    public DBERRFault createDBERRFault() {
        return new DBERRFault();
    }

    /**
     * Create an instance of {@link CancelJobResponse }
     * 
     */
    public CancelJobResponse createCancelJobResponse() {
        return new CancelJobResponse();
    }

    /**
     * Create an instance of {@link ALREADYCANCELEDFault }
     * 
     */
    public ALREADYCANCELEDFault createALREADYCANCELEDFault() {
        return new ALREADYCANCELEDFault();
    }

    /**
     * Create an instance of {@link ListQueuesResponse.Data }
     * 
     */
    public ListQueuesResponse.Data createListQueuesResponseData() {
        return new ListQueuesResponse.Data();
    }

    /**
     * Create an instance of {@link GetJobOutputResponse }
     * 
     */
    public GetJobOutputResponse createGetJobOutputResponse() {
        return new GetJobOutputResponse();
    }

    /**
     * Create an instance of {@link SubmitJobRequest.Data }
     * 
     */
    public SubmitJobRequest.Data createSubmitJobRequestData() {
        return new SubmitJobRequest.Data();
    }

    /**
     * Create an instance of {@link JOBISNOTTERMINATEDFault }
     * 
     */
    public JOBISNOTTERMINATEDFault createJOBISNOTTERMINATEDFault() {
        return new JOBISNOTTERMINATEDFault();
    }

    /**
     * Create an instance of {@link ListQueuesRequest }
     * 
     */
    public ListQueuesRequest createListQueuesRequest() {
        return new ListQueuesRequest();
    }

    /**
     * Create an instance of {@link ALREADYDOWNLOADEDFault }
     * 
     */
    public ALREADYDOWNLOADEDFault createALREADYDOWNLOADEDFault() {
        return new ALREADYDOWNLOADEDFault();
    }

    /**
     * Create an instance of {@link DBCONNFault }
     * 
     */
    public DBCONNFault createDBCONNFault() {
        return new DBCONNFault();
    }

    /**
     * Create an instance of {@link ListJobsResponse.Data.Job }
     * 
     */
    public ListJobsResponse.Data.Job createListJobsResponseDataJob() {
        return new ListJobsResponse.Data.Job();
    }

    /**
     * Create an instance of {@link SYSTEMFault }
     * 
     */
    public SYSTEMFault createSYSTEMFault() {
        return new SYSTEMFault();
    }

    /**
     * Create an instance of {@link GetJobProgressRequest }
     * 
     */
    public GetJobProgressRequest createGetJobProgressRequest() {
        return new GetJobProgressRequest();
    }

    /**
     * Create an instance of {@link GetJobOutputRequest }
     * 
     */
    public GetJobOutputRequest createGetJobOutputRequest() {
        return new GetJobOutputRequest();
    }

    /**
     * Create an instance of {@link ListJobsResponse.Data }
     * 
     */
    public ListJobsResponse.Data createListJobsResponseData() {
        return new ListJobsResponse.Data();
    }

    /**
     * Create an instance of {@link DIETFault }
     * 
     */
    public DIETFault createDIETFault() {
        return new DIETFault();
    }

    /**
     * Create an instance of {@link UNKNOWNMACHINEFault }
     * 
     */
    public UNKNOWNMACHINEFault createUNKNOWNMACHINEFault() {
        return new UNKNOWNMACHINEFault();
    }

    /**
     * Create an instance of {@link SubmitJobResponse }
     * 
     */
    public SubmitJobResponse createSubmitJobResponse() {
        return new SubmitJobResponse();
    }

}
