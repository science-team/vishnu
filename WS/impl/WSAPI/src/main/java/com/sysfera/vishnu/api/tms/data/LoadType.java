/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package com.sysfera.vishnu.api.tms.data;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>LoadType</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see TMS_Data.TMS_DataPackage#getLoadType()
 * @model instanceClass="TMS_Data.LoadType"
 *        annotation="Description content='represents the different Types of a VISHNU TMS LoadType object'"
 * @generated
 */
public enum LoadType implements Enumerator {
	/**
	 * The '<em><b>USE_NB_WAITING_JOBS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #USE_NB_WAITING_JOBS_VALUE
	 * @generated
	 * @ordered
	 */
	USE_NB_WAITING_JOBS(0, "USE_NB_WAITING_JOBS", "USE_NB_WAITING_JOBS"),
	/**
	 * The '<em><b></b>USE_NB_JOBS</em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #USE_NB_JOBS_VALUE
	 * @generated
	 * @ordered
	 */
	USE_NB_JOBS(1, "USE_NB_JOBS", "USE_NB_JOBS"),
	/**
	 * The '<em><b>USE_NB_RUNNING_JOBS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #USE_NB_RUNNING_JOBS_VALUE
	 * @generated
	 * @ordered
	 */
	USE_NB_RUNNING_JOBS(2, "USE_NB_RUNNING_JOBS", "USE_NB_RUNNING_JOBS");

	/**
	 * The '<em><b>USE_NB_WAITING_JOBS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>USE_NB_WAITING_JOBS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #USE_NB_WAITING_JOBS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int USE_NB_WAITING_JOBS_VALUE = 0;
	/**
	 * The '<em><b>USE_NB_JOBS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>USE_NB_JOBS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #USE_NB_JOBS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int USE_NB_JOBS_VALUE = 1;
	/**
	 * The '<em><b>USE_NB_RUNNING_JOBS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>USE_NB_RUNNING_JOBS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #USE_NB_RUNNING_JOBS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int USE_NB_RUNNING_JOBS_VALUE = 2;

	/**
	 * An array of all the '<em><b>Load Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final LoadType[] VALUES_ARRAY =
	    new LoadType[] {
		USE_NB_WAITING_JOBS,
		USE_NB_JOBS,
		USE_NB_RUNNING_JOBS
	};

	/**
	 * A public read-only list of all the '<em><b>Load Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<LoadType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Load Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LoadType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			LoadType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Load Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LoadType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			LoadType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Load Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LoadType get(int value) {
	    switch (value) {
	    case USE_NB_WAITING_JOBS_VALUE: return USE_NB_WAITING_JOBS;
	    case USE_NB_JOBS_VALUE: return USE_NB_JOBS;
	    case USE_NB_RUNNING_JOBS_VALUE: return USE_NB_RUNNING_JOBS;
	    }
	    return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private LoadType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //Load
