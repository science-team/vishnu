
package com.sysfera.vishnu.api.tms;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Job priority.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="JobPrio">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UNDEFINED"/>
 *     &lt;enumeration value="VERY_LOW"/>
 *     &lt;enumeration value="LOW"/>
 *     &lt;enumeration value="NORMAL"/>
 *     &lt;enumeration value="HIGH"/>
 *     &lt;enumeration value="VERY_HIGH"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "JobPrio")
@XmlEnum
public enum JobPrio {

    UNDEFINED,
	VERY_LOW,
	LOW,
	NORMAL,
	HIGH,
	VERY_HIGH;

    public String value() {
        return name();
    }

    public static JobPrio fromValue(String v) {
        return valueOf(v);
    }

}
