
package com.sysfera.vishnu.api.tms;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sessionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="submitMachineId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="submitMachineName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="jobId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="jobName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="jobPath" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="outputPath" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorPath" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="jobPrio" type="{urn:ResourceProxy}JobPriority"/>
 *         &lt;element name="nbCpus" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="jobWorkingDir" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{urn:ResourceProxy}JobStatus"/>
 *         &lt;element name="submitDate" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="owner" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="jobQueue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="wallClockLimit" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="groupName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="jobDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="memLimit" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="nbNodes" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="nbNodesAndCpuPerNode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="batchJobId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sessionId",
    "submitMachineId",
    "submitMachineName",
    "jobId",
    "jobName",
    "jobPath",
    "outputPath",
    "errorPath",
    "jobPrio",
    "nbCpus",
    "jobWorkingDir",
    "status",
    "submitDate",
    "endDate",
    "owner",
    "jobQueue",
    "wallClockLimit",
    "groupName",
    "jobDescription",
    "memLimit",
    "nbNodes",
    "nbNodesAndCpuPerNode",
    "batchJobId"
})
@XmlRootElement(name = "getJobInfoResponse")
public class GetJobInfoResponse {

    @XmlElement(required = true)
    protected String sessionId;
    @XmlElement(required = true)
    protected String submitMachineId;
    @XmlElement(required = true)
    protected String submitMachineName;
    @XmlElement(required = true)
    protected String jobId;
    @XmlElement(required = true)
    protected String jobName;
    @XmlElement(required = true)
    protected String jobPath;
    @XmlElement(required = true)
    protected String outputPath;
    @XmlElement(required = true)
    protected String errorPath;
    @XmlElement(required = true)
    protected JobPriority jobPrio;
    @XmlElement(required = true)
    protected BigInteger nbCpus;
    @XmlElement(required = true)
    protected String jobWorkingDir;
    @XmlElement(required = true)
    protected JobStatus status;
    protected long submitDate;
    protected long endDate;
    @XmlElement(required = true)
    protected String owner;
    @XmlElement(required = true)
    protected String jobQueue;
    protected long wallClockLimit;
    @XmlElement(required = true)
    protected String groupName;
    @XmlElement(required = true)
    protected String jobDescription;
    @XmlElement(required = true)
    protected BigInteger memLimit;
    @XmlElement(required = true)
    protected BigInteger nbNodes;
    @XmlElement(required = true)
    protected String nbNodesAndCpuPerNode;
    @XmlElement(required = true)
    protected String batchJobId;

    /**
     * Gets the value of the sessionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets the value of the sessionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionId(String value) {
        this.sessionId = value;
    }

    /**
     * Gets the value of the submitMachineId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubmitMachineId() {
        return submitMachineId;
    }

    /**
     * Sets the value of the submitMachineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubmitMachineId(String value) {
        this.submitMachineId = value;
    }

    /**
     * Gets the value of the submitMachineName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubmitMachineName() {
        return submitMachineName;
    }

    /**
     * Sets the value of the submitMachineName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubmitMachineName(String value) {
        this.submitMachineName = value;
    }

    /**
     * Gets the value of the jobId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobId() {
        return jobId;
    }

    /**
     * Sets the value of the jobId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobId(String value) {
        this.jobId = value;
    }

    /**
     * Gets the value of the jobName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * Sets the value of the jobName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobName(String value) {
        this.jobName = value;
    }

    /**
     * Gets the value of the jobPath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobPath() {
        return jobPath;
    }

    /**
     * Sets the value of the jobPath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobPath(String value) {
        this.jobPath = value;
    }

    /**
     * Gets the value of the outputPath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutputPath() {
        return outputPath;
    }

    /**
     * Sets the value of the outputPath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutputPath(String value) {
        this.outputPath = value;
    }

    /**
     * Gets the value of the errorPath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorPath() {
        return errorPath;
    }

    /**
     * Sets the value of the errorPath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorPath(String value) {
        this.errorPath = value;
    }

    /**
     * Gets the value of the jobPrio property.
     * 
     * @return
     *     possible object is
     *     {@link JobPriority }
     *     
     */
    public JobPriority getJobPrio() {
        return jobPrio;
    }

    /**
     * Sets the value of the jobPrio property.
     * 
     * @param value
     *     allowed object is
     *     {@link JobPriority }
     *     
     */
    public void setJobPrio(JobPriority value) {
        this.jobPrio = value;
    }

    /**
     * Gets the value of the nbCpus property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNbCpus() {
        return nbCpus;
    }

    /**
     * Sets the value of the nbCpus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNbCpus(BigInteger value) {
        this.nbCpus = value;
    }

    /**
     * Gets the value of the jobWorkingDir property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobWorkingDir() {
        return jobWorkingDir;
    }

    /**
     * Sets the value of the jobWorkingDir property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobWorkingDir(String value) {
        this.jobWorkingDir = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link JobStatus }
     *     
     */
    public JobStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link JobStatus }
     *     
     */
    public void setStatus(JobStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the submitDate property.
     * 
     */
    public long getSubmitDate() {
        return submitDate;
    }

    /**
     * Sets the value of the submitDate property.
     * 
     */
    public void setSubmitDate(long value) {
        this.submitDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     */
    public long getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     */
    public void setEndDate(long value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwner(String value) {
        this.owner = value;
    }

    /**
     * Gets the value of the jobQueue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobQueue() {
        return jobQueue;
    }

    /**
     * Sets the value of the jobQueue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobQueue(String value) {
        this.jobQueue = value;
    }

    /**
     * Gets the value of the wallClockLimit property.
     * 
     */
    public long getWallClockLimit() {
        return wallClockLimit;
    }

    /**
     * Sets the value of the wallClockLimit property.
     * 
     */
    public void setWallClockLimit(long value) {
        this.wallClockLimit = value;
    }

    /**
     * Gets the value of the groupName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Sets the value of the groupName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupName(String value) {
        this.groupName = value;
    }

    /**
     * Gets the value of the jobDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobDescription() {
        return jobDescription;
    }

    /**
     * Sets the value of the jobDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobDescription(String value) {
        this.jobDescription = value;
    }

    /**
     * Gets the value of the memLimit property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMemLimit() {
        return memLimit;
    }

    /**
     * Sets the value of the memLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMemLimit(BigInteger value) {
        this.memLimit = value;
    }

    /**
     * Gets the value of the nbNodes property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNbNodes() {
        return nbNodes;
    }

    /**
     * Sets the value of the nbNodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNbNodes(BigInteger value) {
        this.nbNodes = value;
    }

    /**
     * Gets the value of the nbNodesAndCpuPerNode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNbNodesAndCpuPerNode() {
        return nbNodesAndCpuPerNode;
    }

    /**
     * Sets the value of the nbNodesAndCpuPerNode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNbNodesAndCpuPerNode(String value) {
        this.nbNodesAndCpuPerNode = value;
    }

    /**
     * Gets the value of the batchJobId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBatchJobId() {
        return batchJobId;
    }

    /**
     * Sets the value of the batchJobId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBatchJobId(String value) {
        this.batchJobId = value;
    }

}
