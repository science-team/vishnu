
package com.sysfera.vishnu.api.ums;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.2.9
 * Fri Mar 09 11:53:42 CET 2012
 * Generated source version: 2.2.9
 * 
 */

@WebFault(name = "COMMAND_RUNNINGFault", targetNamespace = "urn:ResourceProxy")
public class COMMANDRUNNINGMessage extends Exception {
    public static final long serialVersionUID = 20120309115342L;
    
    private com.sysfera.vishnu.api.ums.COMMANDRUNNINGFault commandRUNNINGFault;

    public COMMANDRUNNINGMessage() {
        super();
    }
    
    public COMMANDRUNNINGMessage(String message) {
        super(message);
    }
    
    public COMMANDRUNNINGMessage(String message, Throwable cause) {
        super(message, cause);
    }

    public COMMANDRUNNINGMessage(String message, com.sysfera.vishnu.api.ums.COMMANDRUNNINGFault commandRUNNINGFault) {
        super(message);
        this.commandRUNNINGFault = commandRUNNINGFault;
    }

    public COMMANDRUNNINGMessage(String message, com.sysfera.vishnu.api.ums.COMMANDRUNNINGFault commandRUNNINGFault, Throwable cause) {
        super(message, cause);
        this.commandRUNNINGFault = commandRUNNINGFault;
    }

    public com.sysfera.vishnu.api.ums.COMMANDRUNNINGFault getFaultInfo() {
        return this.commandRUNNINGFault;
    }
}
