
package com.sysfera.vishnu.api.tms;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for JobStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="JobStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UNDEFINED"/>
 *     &lt;enumeration value="SUBMITTED"/>
 *     &lt;enumeration value="QUEUED"/>
 *     &lt;enumeration value="WAITING"/>
 *     &lt;enumeration value="RUNNING"/>
 *     &lt;enumeration value="TERMINATED"/>
 *     &lt;enumeration value="CANCELLED"/>
 *     &lt;enumeration value="ALREADY_DOWNLOADED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "JobStatus")
@XmlEnum
public enum JobStatus {

    UNDEFINED,
    SUBMITTED,
    QUEUED,
    WAITING,
    RUNNING,
    TERMINATED,
    CANCELLED,
    ALREADY_DOWNLOADED;

    public String value() {
        return name();
    }

    public static JobStatus fromValue(String v) {
        return valueOf(v);
    }

}
