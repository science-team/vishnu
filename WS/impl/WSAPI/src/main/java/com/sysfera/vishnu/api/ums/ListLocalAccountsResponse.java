
package com.sysfera.vishnu.api.ums;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="data" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="localaccount" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                           &lt;attribute name="userId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="machineId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="acLogin" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="sshKeyPath" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="homeDirectory" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data"
})
@XmlRootElement(name = "listLocalAccountsResponse")
public class ListLocalAccountsResponse {

    protected ListLocalAccountsResponse.Data data;

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link ListLocalAccountsResponse.Data }
     *     
     */
    public ListLocalAccountsResponse.Data getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListLocalAccountsResponse.Data }
     *     
     */
    public void setData(ListLocalAccountsResponse.Data value) {
        this.data = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="localaccount" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *                 &lt;attribute name="userId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="machineId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="acLogin" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="sshKeyPath" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="homeDirectory" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "localaccount"
    })
    public static class Data {

        @XmlElement(nillable = true)
        protected List<ListLocalAccountsResponse.Data.Localaccount> localaccount;

        /**
         * Gets the value of the localaccount property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the localaccount property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLocalaccount().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ListLocalAccountsResponse.Data.Localaccount }
         * 
         * 
         */
        public List<ListLocalAccountsResponse.Data.Localaccount> getLocalaccount() {
            if (localaccount == null) {
                localaccount = new ArrayList<ListLocalAccountsResponse.Data.Localaccount>();
            }
            return this.localaccount;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *       &lt;/sequence>
         *       &lt;attribute name="userId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="machineId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="acLogin" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="sshKeyPath" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="homeDirectory" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Localaccount {

            @XmlAttribute(name = "userId", required = true)
            protected String userId;
            @XmlAttribute(name = "machineId", required = true)
            protected String machineId;
            @XmlAttribute(name = "acLogin")
            protected String acLogin;
            @XmlAttribute(name = "sshKeyPath")
            protected String sshKeyPath;
            @XmlAttribute(name = "homeDirectory")
            protected String homeDirectory;

            /**
             * Gets the value of the userId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUserId() {
                return userId;
            }

            /**
             * Sets the value of the userId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUserId(String value) {
                this.userId = value;
            }

            /**
             * Gets the value of the machineId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMachineId() {
                return machineId;
            }

            /**
             * Sets the value of the machineId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMachineId(String value) {
                this.machineId = value;
            }

            /**
             * Gets the value of the acLogin property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcLogin() {
                return acLogin;
            }

            /**
             * Sets the value of the acLogin property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcLogin(String value) {
                this.acLogin = value;
            }

            /**
             * Gets the value of the sshKeyPath property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSshKeyPath() {
                return sshKeyPath;
            }

            /**
             * Sets the value of the sshKeyPath property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSshKeyPath(String value) {
                this.sshKeyPath = value;
            }

            /**
             * Gets the value of the homeDirectory property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHomeDirectory() {
                return homeDirectory;
            }

            /**
             * Sets the value of the homeDirectory property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHomeDirectory(String value) {
                this.homeDirectory = value;
            }

        }

    }

}
