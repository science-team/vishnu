package com.sysfera.vishnu.api.tms.test;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sysfera.vishnu.api.tms.ALREADYCANCELEDMessage;
import com.sysfera.vishnu.api.tms.ALREADYDOWNLOADEDMessage;
import com.sysfera.vishnu.api.tms.ALREADYTERMINATEDMessage;
import com.sysfera.vishnu.api.tms.BATCHSCHEDULERERRORMessage;
import com.sysfera.vishnu.api.tms.CancelJobRequest;
import com.sysfera.vishnu.api.tms.CancelJobResponse;
import com.sysfera.vishnu.api.tms.DBCONNMessage;
import com.sysfera.vishnu.api.tms.DBERRMessage;
import com.sysfera.vishnu.api.tms.DIETMessage;
import com.sysfera.vishnu.api.tms.GetCompletedJobsOutputRequest;
import com.sysfera.vishnu.api.tms.GetCompletedJobsOutputResponse;
import com.sysfera.vishnu.api.tms.GetJobInfoResponse;
import com.sysfera.vishnu.api.tms.GetJobOutputRequest;
import com.sysfera.vishnu.api.tms.GetJobOutputResponse;
import com.sysfera.vishnu.api.tms.GetJobProgressRequest;
import com.sysfera.vishnu.api.tms.GetJobProgressResponse;
import com.sysfera.vishnu.api.tms.INVALIDPARAMMessage;
import com.sysfera.vishnu.api.tms.JOBISNOTTERMINATEDMessage;
import com.sysfera.vishnu.api.tms.LoadType;
import com.sysfera.vishnu.api.tms.SubmitJobRequest.Data.Loadcriterion;
import com.sysfera.vishnu.api.tms.data.JobStatus;
import com.sysfera.vishnu.api.tms.ListJobsRequest;
import com.sysfera.vishnu.api.tms.ListJobsResponse;
import com.sysfera.vishnu.api.tms.ListQueuesRequest;
import com.sysfera.vishnu.api.tms.ListQueuesResponse;
import com.sysfera.vishnu.api.tms.PERMISSIONDENIEDMessage;
import com.sysfera.vishnu.api.tms.SESSIONKEYEXPIREDMessage;
import com.sysfera.vishnu.api.tms.SSHMessage;
import com.sysfera.vishnu.api.tms.SYSTEMMessage;
import com.sysfera.vishnu.api.tms.SubmitJobRequest;
import com.sysfera.vishnu.api.tms.SubmitJobResponse;
import com.sysfera.vishnu.api.tms.UNDEFINEDMessage;
import com.sysfera.vishnu.api.tms.UNKNOWNBATCHSCHEDULERMessage;
import com.sysfera.vishnu.api.tms.UNKNOWNMACHINEMessage;
import com.sysfera.vishnu.api.tms.VishnuTMSPortType;
import com.sysfera.vishnu.api.tms.VishnuTMSService;
import com.sysfera.vishnu.api.tms.GetJobInfoRequest;
import com.sysfera.vishnu.api.ums.ConnectRequest;
import com.sysfera.vishnu.api.ums.ConnectResponse;
import com.sysfera.vishnu.api.ums.SessionCloseType;
import com.sysfera.vishnu.api.ums.VishnuUMSPortType;
import com.sysfera.vishnu.api.ums.VishnuUMSService;
import com.sysfera.vishnu.api.vishnu.internal.LoadCriterion;

public class WSClientTMSTest {
	
	private static final String 	JBOSS_SERVER_NAME = "localhost";
	private static final Integer 	JBOSS_SERVER_PORT = 8080;
	private static final String		TEST_USER_LOGIN = "root";
	private static final String		TEST_USER_PASSWORD = "vishnu_user";
	private static final String		SCRIPTPATH = "/home/traore/VishnuProject/eclipse_1/WSAPI/src/main/java/com/sysfera/vishnu/api/tms/test/torque_script";
	private static final String		WAITING_SCRIPTPAT_GEN = "/home/traore/VishnuProject/eclipse_1/WSAPI/src/main/java/com/sysfera/vishnu/api/tms/test/testScriptGenWaiting";
	private static final String		SCRIPT_PATH_GEN = "/home/traore/VishnuProject/eclipse_1/WSAPI/src/main/java/com/sysfera/vishnu/api/tms/test/testScriptGen";
	private static final String		FASTSCRIPTPATH = "/home/traore/VishnuProject/eclipse_1/WSAPI/src/main/java/com/sysfera/vishnu/api/tms/test/fast_torque_script";
	
	private static final String 	MACHINEID = "MA_4";
	private static final String 	ACLOGIN	  = "traore";
	
	private static VishnuUMSPortType 	UMSPort;
	private static VishnuTMSPortType 	TMSPort;
	private static String				sessionKey;
	private static String				sessionId;
	private static String               jid;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("INIT TESTS");
		
		// Initialize web service interface
		UMSPort = getUMSPort("http://" + JBOSS_SERVER_NAME + ":" + JBOSS_SERVER_PORT + "/WSAPI/VishnuUMS?wsdl");
		TMSPort = getTMSPort("http://" + JBOSS_SERVER_NAME + ":" + JBOSS_SERVER_PORT + "/WSAPI/VishnuTMS?wsdl");
		
		// Initialize Vishnu session
		ConnectRequest request = new ConnectRequest();
		request.setUserId(TEST_USER_LOGIN);
		request.setPassword(TEST_USER_PASSWORD);
		request.setSessionInactivityDelay(new BigInteger("500"));
		System.out.println("Valeur du delai :"+request.getSessionInactivityDelay());
		request.setClosePolicy(SessionCloseType.valueOf(new String("CLOSE_ON_TIMEOUT")));
		ConnectResponse response = UMSPort.connect(request);
		sessionKey = response.getSessionKey();
		sessionId = response.getSessionId();
		System.out.println("SESSION KEY=" + sessionKey);
		System.out.println("SESSION ID=" + sessionId);
	}

	@Test
	public void testSubmitJob() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, SSHMessage {
		SubmitJobRequest req = new SubmitJobRequest();
		req.setErrorPath("/tmp/test_submit.err");
		req.setMachineId(MACHINEID);
		req.setOutputPath("/tmp/test_submit.out");
		req.setSessionKey(sessionKey);
		req.setScriptFilePath(SCRIPTPATH);
		SubmitJobResponse resp = TMSPort.submitJob(req);
		jid = resp.getJobId();
		System.out.println("Job Id: "+resp.getJobId());
		System.out.println("Owner: "+resp.getOwner());
	}
	
	@Test
	public void testGetJobInfo() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		GetJobInfoRequest req = new GetJobInfoRequest();
		req.setJobId(jid);
		req.setMachineId(MACHINEID);
		req.setSessionKey(sessionKey);
		GetJobInfoResponse resp = TMSPort.getJobInfo(req);
		System.out.println("job id: "+resp.getJobId());
		System.out.println("job name: "+resp.getJobName());
	}

	@Test
	public void testListJobs() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		ListJobsRequest req = new ListJobsRequest();
		req.setSessionKey(sessionKey);
		req.setMachineId(MACHINEID);
		req.setJobId(jid);
		ListJobsResponse resp = TMSPort.listJobs(req);
		System.out.println("Job id: "+resp.getData().getJob().get(0).getJobId());
		System.out.println("Job name: "+resp.getData().getJob().get(0).getJobName());
		System.out.println("Job owner: "+resp.getData().getJob().get(0).getOwner());
		System.out.println("nbjobs: "+resp.getNbJobs());
		System.out.println("Status du job: "+com.sysfera.vishnu.api.tms.data.JobStatus.get(
						     resp.getData().getJob().get(0).getStatus().toString()).getLiteral());
	}


	@Test
	public void testListQueues() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		ListQueuesRequest req = new ListQueuesRequest();
		req.setMachineId(MACHINEID);
		req.setSessionKey(sessionKey);
		ListQueuesResponse resp = TMSPort.listQueues(req);
		System.out.println("Queue1 name: "+resp.getData().getQueue().get(0).getName());
		System.out.println("Nombrede queues: "+resp.getNbQueues());	}


	@Test
	public void testGetJobProgress() throws UNKNOWNMACHINEMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		GetJobProgressRequest req = new GetJobProgressRequest();
		req.setMachineId(MACHINEID);
		req.setSessionKey(sessionKey);
		req.setJobId(jid);
		GetJobProgressResponse resp = TMSPort.getJobProgress(req);
	}

	@Test(expected=UNDEFINEDMessage.class)
	public void testGetJobOutput() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, SSHMessage, JOBISNOTTERMINATEDMessage, ALREADYDOWNLOADEDMessage {
		GetJobOutputRequest req = new GetJobOutputRequest();
		req.setMachineId(MACHINEID);
		req.setSessionKey(sessionKey);
		req.setJobId(jid);
		GetJobOutputResponse resp = TMSPort.getJobOutput(req);
	}

	@Test
	public void testGetCompletedJobsOutput() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, SSHMessage {
		SubmitJobRequest req1 = new SubmitJobRequest();
		req1.setErrorPath("/tmp/test_outpout.err");
		req1.setMachineId(MACHINEID);
		req1.setOutputPath("/tmp/test_outpout.out");
		req1.setSessionKey(sessionKey);
		req1.setScriptFilePath(FASTSCRIPTPATH);
		try {
			SubmitJobResponse resp1 = TMSPort.submitJob(req1);
		} catch (INVALIDPARAMMessage e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			Thread.currentThread().sleep(3);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		GetCompletedJobsOutputRequest req = new GetCompletedJobsOutputRequest();
		req.setMachineId(MACHINEID);
		req.setSessionKey(sessionKey);
		req.setOutDir("/tmp");
		GetCompletedJobsOutputResponse resp = TMSPort.getCompletedJobsOutput(req);
		System.out.println("Taille du resultat: "+resp.getData().getJobresult().size());
	}
	
	@Test
	public void testCancelJob() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, SSHMessage, ALREADYTERMINATEDMessage, ALREADYCANCELEDMessage {
		CancelJobRequest req = new CancelJobRequest();
		req.setJobId(jid);
		req.setMachineId(MACHINEID);
		req.setSessionKey(sessionKey);
		CancelJobResponse resp = TMSPort.cancelJob(req);
	}
	
	@Test
	public void testAutomaticSubmitJob() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, SSHMessage {
		SubmitJobRequest req = new SubmitJobRequest();
		req.setErrorPath("/tmp/test_autom.err");
		req.setOutputPath("/tmp/test_autom.out");
		req.setSessionKey(sessionKey);
		req.setScriptFilePath(WAITING_SCRIPTPAT_GEN);
		req.setMachineId(MACHINEID);
		SubmitJobResponse resp = TMSPort.submitJob(req);
		System.out.println("WAITING - Job Id: "+resp.getJobId());
		System.out.println("WAITING - Owner: "+resp.getOwner());
		System.out.println("WAITING - Job status: "+ resp.getStatus());
		System.out.println("WAITING - machineId: "+resp.getSubmitMachineId());


		req.setScriptFilePath(SCRIPT_PATH_GEN);
		req.setMachineId("autom");
		SubmitJobResponse resp1 = TMSPort.submitJob(req);
		jid = resp.getJobId();
		System.out.println("AutomaticSubmitJob - Job Id: "+resp1.getJobId());
		System.out.println("AutomaticSubmitJob - Job status: "+ resp1.getStatus());
		System.out.println("AutomaticSubmitJob - Owner: "+resp1.getOwner());
		System.out.println("AutomaticSubmitJob - machineId: "+resp1.getSubmitMachineId());

		SubmitJobResponse resp2 = TMSPort.submitJob(req);

		System.out.println("AutomaticSubmitJob2 - Job Id: "+resp2.getJobId());
		System.out.println("AutomaticSubmitJob2 - Owner: "+resp2.getOwner());
		System.out.println("AutomaticSubmitJob2 - Job status: "+ resp2.getStatus());
		System.out.println("AutomaticSubmitJob2 - machineId: "+resp2.getSubmitMachineId());

		SubmitJobResponse resp3 = TMSPort.submitJob(req);

		System.out.println("AutomaticSubmitJob3 - Job Id: "+resp3.getJobId());
		System.out.println("AutomaticSubmitJob3 - Owner: "+resp3.getOwner());
		System.out.println("AutomaticSubmitJob3 - Job status: "+ resp3.getStatus());
		System.out.println("AutomaticSubmitJob3 - machineId: "+resp3.getSubmitMachineId());

		
		
	}
	
	
	@Test
	public void testAutomaticSubmitJobWithMinimumRunningJobs() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, SSHMessage {
		SubmitJobRequest req = new SubmitJobRequest();
		req.setErrorPath("/tmp/test_autom3.err");
		req.setMachineId("autom");
		req.setOutputPath("/tmp/test_autom3.out");
		req.setSessionKey(sessionKey);
		req.setScriptFilePath(SCRIPT_PATH_GEN);
		
		
		SubmitJobRequest.Data data = new SubmitJobRequest.Data();
		SubmitJobRequest.Data.Loadcriterion loadCriterion = new SubmitJobRequest.Data.Loadcriterion();
		loadCriterion.setLoadType(LoadType.USE_NB_RUNNING_JOBS);
		data.getLoadcriterion().add(loadCriterion);
	    req.setData(data);
	    
		SubmitJobResponse resp = TMSPort.submitJob(req);
		jid = resp.getJobId();
		System.out.println("AutomaticSubmitJob with minimum running jobs - Job Id: "+resp.getJobId());
		System.out.println("AutomaticSubmitJob with minimum running jobs - Job status: "+ resp.getStatus());
		System.out.println("AutomaticSubmitJob with minimum running jobs - Owner: "+resp.getOwner());
		System.out.println("AutomaticSubmitJob with minimum running jobs - machineId: "+resp.getSubmitMachineId());
	}
	
	@Test
	public void testAutomaticSubmitJobWithMinimumTotalJobs() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, SSHMessage {
		SubmitJobRequest req = new SubmitJobRequest();
		req.setErrorPath("/tmp/test_autom2.err");
		req.setMachineId("autom");
		req.setOutputPath("/tmp/test_autom2.out");
		req.setSessionKey(sessionKey);
		req.setScriptFilePath(SCRIPT_PATH_GEN);
		
		
		SubmitJobRequest.Data data = new SubmitJobRequest.Data();
		SubmitJobRequest.Data.Loadcriterion loadCriterion = new SubmitJobRequest.Data.Loadcriterion();
		loadCriterion.setLoadType(LoadType.USE_NB_JOBS);
		data.getLoadcriterion().add(loadCriterion);
		req.setData(data);
		
		SubmitJobResponse resp = TMSPort.submitJob(req);
		jid = resp.getJobId();
		System.out.println("AutomaticSubmitJob with minimum total jobs - Job Id: "+resp.getJobId());
		System.out.println("AutomaticSubmitJob with minimum total jobs - Job status: "+ resp.getStatus());
		System.out.println("AutomaticSubmitJob with minimum total jobs - Owner: "+resp.getOwner());
		System.out.println("AutomaticSubmitJob with minimum total jobs - machineId: "+resp.getSubmitMachineId());
	}
	
	
	@Test
	public void testListALLJobs() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		ListJobsRequest req = new ListJobsRequest();
		req.setSessionKey(sessionKey);
		req.setMachineId("all");
		ListJobsResponse resp = TMSPort.listJobs(req);
		System.out.println("**********************BEGIN LIST OF JOBS ON ALL MACHINES********************");
		for(int i=0; i < resp.getData().getJob().size(); i++) {
			System.out.println("**********************INFORMATION of job "+resp.getData().getJob().get(i).getJobId());
			System.out.println("LIST ALL JOBS - Job id: "+resp.getData().getJob().get(i).getJobId());
			System.out.println("LIST ALL JOBS - machine id: "+resp.getData().getJob().get(i).getSubmitMachineId());
			System.out.println("LIST ALL JOBS - Job name: "+resp.getData().getJob().get(i).getJobName());
			System.out.println("LIST ALL JOBS - Job owner: "+resp.getData().getJob().get(i).getOwner());
			System.out.println("LIST ALL JOBS - Status du job: "+com.sysfera.vishnu.api.tms.data.JobStatus.get(
					resp.getData().getJob().get(i).getStatus().toString()).getLiteral());
			;
		}
		System.out.println("LIST ALL JOBS - number of total jobs: "+resp.getNbJobs());
		System.out.println("LIST ALL JOBS - number of running jobs: "+resp.getNbRunningJobs());
		System.out.println("LIST ALL JOBS - number of waiting jobs: "+resp.getNbWaitingJobs());
		System.out.println("**********************END LIST OF JOBS ON ALL MACHINES********************");
	}

	@Test
	public void testListBatchJobs() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		ListJobsRequest req = new ListJobsRequest();
		req.setSessionKey(sessionKey);
		req.setMachineId(MACHINEID);
		req.setBatchJob(true);
		ListJobsResponse resp = TMSPort.listJobs(req);
		System.out.println("**********************BEGIN LIST OF BATCH JOBS ON A MACHINES********************");
		for(int i=0; i < resp.getData().getJob().size(); i++) {
			System.out.println("**********************INFORMATION of job "+resp.getData().getJob().get(i).getJobId());
			System.out.println("LIST BATCH JOBS - Job id: "+resp.getData().getJob().get(i).getJobId());
			System.out.println("LIST BATCH JOBS - machine id: "+resp.getData().getJob().get(i).getSubmitMachineId());
			System.out.println("LIST BATCH JOBS - Job name: "+resp.getData().getJob().get(i).getJobName());
			System.out.println("LIST BATCH JOBS - Job owner: "+resp.getData().getJob().get(i).getOwner());
			System.out.println("LIST BATCH JOBS - Status du job: "+com.sysfera.vishnu.api.tms.data.JobStatus.get(
					resp.getData().getJob().get(i).getStatus().toString()).getLiteral());
			;
		}
		System.out.println("LIST BATCH JOBS - number of total jobs: "+resp.getNbJobs());
		System.out.println("LIST BATCH JOBS - number of running jobs: "+resp.getNbRunningJobs());
		System.out.println("LIST BATCH JOBS - number of waiting jobs: "+resp.getNbWaitingJobs());
		System.out.println("**********************END LIST OF BATCH JOBS ON A MACHINES********************");
	}
	
	@Test
	public void testListBatchJobInfo() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		ListJobsRequest req = new ListJobsRequest();
		req.setSessionKey(sessionKey);
		req.setMachineId(MACHINEID);
		req.setJobId(jid);
		req.setBatchJob(true);
		ListJobsResponse resp = TMSPort.listJobs(req);
		System.out.println("**********************BEGIN LIST OF BATCH JOBS ON A MACHINES********************");
		for(int i=0; i < resp.getData().getJob().size(); i++) {
			System.out.println("**********************INFORMATION of job "+resp.getData().getJob().get(i).getJobId());
			System.out.println("LIST BATCH JOBS - Job id: "+resp.getData().getJob().get(i).getJobId());
			System.out.println("LIST BATCH JOBS - machine id: "+resp.getData().getJob().get(i).getSubmitMachineId());
			System.out.println("LIST BATCH JOBS - Job name: "+resp.getData().getJob().get(i).getJobName());
			System.out.println("LIST BATCH JOBS - Job owner: "+resp.getData().getJob().get(i).getOwner());
			System.out.println("LIST BATCH JOBS - Status du job: "+com.sysfera.vishnu.api.tms.data.JobStatus.get(
					resp.getData().getJob().get(i).getStatus().toString()).getLiteral());
			;
		}
		System.out.println("LIST BATCH JOBS - number of total jobs: "+resp.getNbJobs());
		System.out.println("LIST BATCH JOBS - number of running jobs: "+resp.getNbRunningJobs());
		System.out.println("LIST BATCH JOBS - number of waiting jobs: "+resp.getNbWaitingJobs());
		System.out.println("**********************END LIST OF BATCH JOBS ON A MACHINES********************");
	}
	
	@Test
	public void testListJobsWithMultiStatus() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		ListJobsRequest req = new ListJobsRequest();
		req.setSessionKey(sessionKey);
		req.setMachineId("all");
		req.setMultipleStatus("R");
		ListJobsResponse resp = TMSPort.listJobs(req);
		System.out.println("**********************BEGIN LIST RUNNING JOBS WITH MULTISTATUS ON A MACHINE********************");
		for(int i=0; i < resp.getData().getJob().size(); i++) {
			System.out.println("**********************INFORMATION of job "+resp.getData().getJob().get(i).getJobId());
			System.out.println("LIST JOBS - Job id: "+resp.getData().getJob().get(i).getJobId());
			System.out.println("LIST JOBS - machine id: "+resp.getData().getJob().get(i).getSubmitMachineId());
			System.out.println("LIST JOBS - Job name: "+resp.getData().getJob().get(i).getJobName());
			System.out.println("LIST JOBS - Job owner: "+resp.getData().getJob().get(i).getOwner());
			System.out.println("LIST JOBS - Status du job: "+com.sysfera.vishnu.api.tms.data.JobStatus.get(
					resp.getData().getJob().get(i).getStatus().toString()).getLiteral());
			;
		}
		System.out.println("LIST JOBS - number of total jobs: "+resp.getNbJobs());
		System.out.println("LIST JOBS - number of running jobs: "+resp.getNbRunningJobs());
		System.out.println("LIST JOBS - number of waiting jobs: "+resp.getNbWaitingJobs());
		System.out.println("**********************END LIST JOBS WITH MULTISTATUS ON A MACHINE********************");
	}
	
	@Test
	public void testListJobsWithMultiStatus2() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		ListJobsRequest req = new ListJobsRequest();
		req.setSessionKey(sessionKey);
		req.setMachineId("all");
		req.setMultipleStatus("W");
		ListJobsResponse resp = TMSPort.listJobs(req);
		System.out.println("**********************BEGIN LIST WAITING JOBS WITH MULTISTATUS ON A MACHINE********************");
		for(int i=0; i < resp.getData().getJob().size(); i++) {
			System.out.println("**********************INFORMATION of job "+resp.getData().getJob().get(i).getJobId());
			System.out.println("LIST JOBS - Job id: "+resp.getData().getJob().get(i).getJobId());
			System.out.println("LIST JOBS - machine id: "+resp.getData().getJob().get(i).getSubmitMachineId());
			System.out.println("LIST JOBS - Job name: "+resp.getData().getJob().get(i).getJobName());
			System.out.println("LIST JOBS - Job owner: "+resp.getData().getJob().get(i).getOwner());
			System.out.println("LIST JOBS - Status du job: "+com.sysfera.vishnu.api.tms.data.JobStatus.get(
					resp.getData().getJob().get(i).getStatus().toString()).getLiteral());
			;
		}
		System.out.println("LIST JOBS - number of total jobs: "+resp.getNbJobs());
		System.out.println("LIST JOBS - number of running jobs: "+resp.getNbRunningJobs());
		System.out.println("LIST JOBS - number of waiting jobs: "+resp.getNbWaitingJobs());
		System.out.println("**********************END LIST JOBS WITH MULTISTATUS ON A MACHINE********************");
	}
	
	@Test
	public void testListJobsWithMultiStatus3() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage {
		ListJobsRequest req = new ListJobsRequest();
		req.setSessionKey(sessionKey);
		req.setMachineId("all");
		req.setMultipleStatus("RW");
		ListJobsResponse resp = TMSPort.listJobs(req);
		System.out.println("**********************BEGIN LIST RUNNING AND WAITING JOBS WITH MULTISTATUS ON A MACHINE********************");
		for(int i=0; i < resp.getData().getJob().size(); i++) {
			System.out.println("**********************INFORMATION of job "+resp.getData().getJob().get(i).getJobId());
			System.out.println("LIST JOBS - Job id: "+resp.getData().getJob().get(i).getJobId());
			System.out.println("LIST JOBS - machine id: "+resp.getData().getJob().get(i).getSubmitMachineId());
			System.out.println("LIST JOBS - Job name: "+resp.getData().getJob().get(i).getJobName());
			System.out.println("LIST JOBS - Job owner: "+resp.getData().getJob().get(i).getOwner());
			System.out.println("LIST JOBS - Status du job: "+com.sysfera.vishnu.api.tms.data.JobStatus.get(
					resp.getData().getJob().get(i).getStatus().toString()).getLiteral());
			;
		}
		System.out.println("LIST JOBS - number of total jobs: "+resp.getNbJobs());
		System.out.println("LIST JOBS - number of running jobs: "+resp.getNbRunningJobs());
		System.out.println("LIST JOBS - number of waiting jobs: "+resp.getNbWaitingJobs());
		System.out.println("**********************END LIST JOBS WITH MULTISTATUS ON A MACHINE********************");
	}
	
	@Test
	public void testSubmitJobWithAutomaticQueueOption() throws UNKNOWNBATCHSCHEDULERMessage, BATCHSCHEDULERERRORMessage, UNKNOWNMACHINEMessage, PERMISSIONDENIEDMessage, DBCONNMessage, DIETMessage, INVALIDPARAMMessage, UNDEFINEDMessage, SESSIONKEYEXPIREDMessage, SYSTEMMessage, DBERRMessage, SSHMessage {
		SubmitJobRequest req = new SubmitJobRequest();
		req.setErrorPath("/tmp/test_queueAutom.out");
		req.setMachineId(MACHINEID);
		req.setOutputPath("/tmp/test_queueAutom.err");
		req.setSessionKey(sessionKey);
		req.setScriptFilePath(SCRIPT_PATH_GEN);
		req.setSelectQueueAutom(true);
		
		SubmitJobResponse resp = TMSPort.submitJob(req);
		jid = resp.getJobId();
		System.out.println("SubmitJob with automatic queue option - Job Id: "+resp.getJobId());
		System.out.println("SubmitJob with automatic queue option - Job status: "+ resp.getStatus());
		System.out.println("SubmitJob automatic queue option - Owner: "+resp.getOwner());
		System.out.println("SubmitJob automatic queue option - Queue: "+resp.getJobQueue());
		System.out.println("SubmitJob automatic queue option - machineId: "+resp.getSubmitMachineId());
	}
	
	private static VishnuUMSPortType getUMSPort(String endpointURI) throws MalformedURLException   {

		URL wsdlURL = new URL(endpointURI);
		VishnuUMSService service = new VishnuUMSService(wsdlURL);
		return service.getVishnuUMSPort();
	}
	private static VishnuTMSPortType getTMSPort(String endpointURI) throws MalformedURLException   {

		URL wsdlURL = new URL(endpointURI);
		VishnuTMSService service = new VishnuTMSService(wsdlURL);
		return service.getVishnuTMSPort();
	}
}
