
package com.sysfera.vishnu.api.tms;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueueStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="QueueStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NOT_STARTED"/>
 *     &lt;enumeration value="STARTED"/>
 *     &lt;enumeration value="RUNNING"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "QueueStatus")
@XmlEnum
public enum QueueStatus {

    NOT_STARTED,
    STARTED,
    RUNNING;

    public String value() {
        return name();
    }

    public static QueueStatus fromValue(String v) {
        return valueOf(v);
    }

}
