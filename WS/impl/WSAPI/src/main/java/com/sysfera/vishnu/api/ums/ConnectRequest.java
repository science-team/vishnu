
package com.sysfera.vishnu.api.ums;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="closePolicy" type="{urn:ResourceProxy}SessionCloseType" minOccurs="0"/>
 *         &lt;element name="sessionInactivityDelay" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="substituteUserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userId",
    "password",
    "closePolicy",
    "sessionInactivityDelay",
    "substituteUserId"
})
@XmlRootElement(name = "connectRequest")
public class ConnectRequest {

    @XmlElement(required = true)
    protected String userId;
    @XmlElement(required = true)
    protected String password;
    protected SessionCloseType closePolicy;
    protected BigInteger sessionInactivityDelay;
    protected String substituteUserId;

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the closePolicy property.
     * 
     * @return
     *     possible object is
     *     {@link SessionCloseType }
     *     
     */
    public SessionCloseType getClosePolicy() {
        return closePolicy;
    }

    /**
     * Sets the value of the closePolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionCloseType }
     *     
     */
    public void setClosePolicy(SessionCloseType value) {
        this.closePolicy = value;
    }

    /**
     * Gets the value of the sessionInactivityDelay property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSessionInactivityDelay() {
        return sessionInactivityDelay;
    }

    /**
     * Sets the value of the sessionInactivityDelay property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSessionInactivityDelay(BigInteger value) {
        this.sessionInactivityDelay = value;
    }

    /**
     * Gets the value of the substituteUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubstituteUserId() {
        return substituteUserId;
    }

    /**
     * Sets the value of the substituteUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubstituteUserId(String value) {
        this.substituteUserId = value;
    }

}
