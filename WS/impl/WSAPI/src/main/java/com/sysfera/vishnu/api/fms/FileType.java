
package com.sysfera.vishnu.api.fms;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FileType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FileType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BLOCK"/>
 *     &lt;enumeration value="CHARACTER"/>
 *     &lt;enumeration value="DIRECTORY"/>
 *     &lt;enumeration value="SYMBOLICLINK"/>
 *     &lt;enumeration value="SCKT"/>
 *     &lt;enumeration value="FIFO"/>
 *     &lt;enumeration value="REGULAR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FileType")
@XmlEnum
public enum FileType {

    BLOCK,
    CHARACTER,
    DIRECTORY,
    SYMBOLICLINK,
    SCKT,
    FIFO,
    REGULAR;

    public String value() {
        return name();
    }

    public static FileType fromValue(String v) {
        return valueOf(v);
    }

}
