
package com.sysfera.vishnu.api.ims;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sysfera.vishnu.api.ims package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sysfera.vishnu.api.ims
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetMetricHistoryResponse.Data.Metric }
     * 
     */
    public GetMetricHistoryResponse.Data.Metric createGetMetricHistoryResponseDataMetric() {
        return new GetMetricHistoryResponse.Data.Metric();
    }

    /**
     * Create an instance of {@link GetMetricCurrentValueResponse.Data.Metric }
     * 
     */
    public GetMetricCurrentValueResponse.Data.Metric createGetMetricCurrentValueResponseDataMetric() {
        return new GetMetricCurrentValueResponse.Data.Metric();
    }

    /**
     * Create an instance of {@link GetMetricCurrentValueResponse }
     * 
     */
    public GetMetricCurrentValueResponse createGetMetricCurrentValueResponse() {
        return new GetMetricCurrentValueResponse();
    }

    /**
     * Create an instance of {@link GetSystemInfoResponse.Data.Systeminfo }
     * 
     */
    public GetSystemInfoResponse.Data.Systeminfo createGetSystemInfoResponseDataSysteminfo() {
        return new GetSystemInfoResponse.Data.Systeminfo();
    }

    /**
     * Create an instance of {@link INVALIDPARAMETERFault }
     * 
     */
    public INVALIDPARAMETERFault createINVALIDPARAMETERFault() {
        return new INVALIDPARAMETERFault();
    }

    /**
     * Create an instance of {@link GetMetricCurrentValueResponse.Data }
     * 
     */
    public GetMetricCurrentValueResponse.Data createGetMetricCurrentValueResponseData() {
        return new GetMetricCurrentValueResponse.Data();
    }

    /**
     * Create an instance of {@link GetMetricHistoryRequest }
     * 
     */
    public GetMetricHistoryRequest createGetMetricHistoryRequest() {
        return new GetMetricHistoryRequest();
    }

    /**
     * Create an instance of {@link COMPONENTERRORFault }
     * 
     */
    public COMPONENTERRORFault createCOMPONENTERRORFault() {
        return new COMPONENTERRORFault();
    }

    /**
     * Create an instance of {@link GetSystemInfoRequest }
     * 
     */
    public GetSystemInfoRequest createGetSystemInfoRequest() {
        return new GetSystemInfoRequest();
    }

    /**
     * Create an instance of {@link GetSystemInfoResponse }
     * 
     */
    public GetSystemInfoResponse createGetSystemInfoResponse() {
        return new GetSystemInfoResponse();
    }

    /**
     * Create an instance of {@link GetMetricHistoryResponse }
     * 
     */
    public GetMetricHistoryResponse createGetMetricHistoryResponse() {
        return new GetMetricHistoryResponse();
    }

    /**
     * Create an instance of {@link DBERRFault }
     * 
     */
    public DBERRFault createDBERRFault() {
        return new DBERRFault();
    }

    /**
     * Create an instance of {@link GetUpdateFrequencyRequest }
     * 
     */
    public GetUpdateFrequencyRequest createGetUpdateFrequencyRequest() {
        return new GetUpdateFrequencyRequest();
    }

    /**
     * Create an instance of {@link GetMetricCurrentValueRequest }
     * 
     */
    public GetMetricCurrentValueRequest createGetMetricCurrentValueRequest() {
        return new GetMetricCurrentValueRequest();
    }

    /**
     * Create an instance of {@link SESSIONKEYNOTFOUNDFault }
     * 
     */
    public SESSIONKEYNOTFOUNDFault createSESSIONKEYNOTFOUNDFault() {
        return new SESSIONKEYNOTFOUNDFault();
    }

    /**
     * Create an instance of {@link UNDEFINEDFault }
     * 
     */
    public UNDEFINEDFault createUNDEFINEDFault() {
        return new UNDEFINEDFault();
    }

    /**
     * Create an instance of {@link ExportCommandsResponse }
     * 
     */
    public ExportCommandsResponse createExportCommandsResponse() {
        return new ExportCommandsResponse();
    }

    /**
     * Create an instance of {@link GetSystemInfoResponse.Data }
     * 
     */
    public GetSystemInfoResponse.Data createGetSystemInfoResponseData() {
        return new GetSystemInfoResponse.Data();
    }

    /**
     * Create an instance of {@link GetUpdateFrequencyResponse }
     * 
     */
    public GetUpdateFrequencyResponse createGetUpdateFrequencyResponse() {
        return new GetUpdateFrequencyResponse();
    }

    /**
     * Create an instance of {@link GetMetricHistoryResponse.Data }
     * 
     */
    public GetMetricHistoryResponse.Data createGetMetricHistoryResponseData() {
        return new GetMetricHistoryResponse.Data();
    }

    /**
     * Create an instance of {@link SESSIONKEYEXPIREDFault }
     * 
     */
    public SESSIONKEYEXPIREDFault createSESSIONKEYEXPIREDFault() {
        return new SESSIONKEYEXPIREDFault();
    }

    /**
     * Create an instance of {@link ExportCommandsRequest }
     * 
     */
    public ExportCommandsRequest createExportCommandsRequest() {
        return new ExportCommandsRequest();
    }

}
