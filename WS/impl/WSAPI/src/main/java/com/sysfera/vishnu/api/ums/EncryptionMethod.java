
package com.sysfera.vishnu.api.ums;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EncryptionMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EncryptionMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UNDEFINED"/>
 *     &lt;enumeration value="SSHA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EncryptionMethod")
@XmlEnum
public enum EncryptionMethod {

    UNDEFINED,
    SSHA;

    public String value() {
        return name();
    }

    public static EncryptionMethod fromValue(String v) {
        return valueOf(v);
    }

}
