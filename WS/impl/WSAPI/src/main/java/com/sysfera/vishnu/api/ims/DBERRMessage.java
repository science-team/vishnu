
package com.sysfera.vishnu.api.ims;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.2.9
 * Tue Aug 23 15:40:52 CEST 2011
 * Generated source version: 2.2.9
 * 
 */

@WebFault(name = "DBERRFault", targetNamespace = "urn:ResourceProxy")
public class DBERRMessage extends Exception {
    public static final long serialVersionUID = 20110823154052L;
    
    private com.sysfera.vishnu.api.ims.DBERRFault dberrFault;

    public DBERRMessage() {
        super();
    }
    
    public DBERRMessage(String message) {
        super(message);
    }
    
    public DBERRMessage(String message, Throwable cause) {
        super(message, cause);
    }

    public DBERRMessage(String message, com.sysfera.vishnu.api.ims.DBERRFault dberrFault) {
        super(message);
        this.dberrFault = dberrFault;
    }

    public DBERRMessage(String message, com.sysfera.vishnu.api.ims.DBERRFault dberrFault, Throwable cause) {
        super(message, cause);
        this.dberrFault = dberrFault;
    }

    public com.sysfera.vishnu.api.ims.DBERRFault getFaultInfo() {
        return this.dberrFault;
    }
}
