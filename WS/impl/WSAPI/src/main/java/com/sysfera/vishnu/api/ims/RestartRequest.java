
package com.sysfera.vishnu.api.ims;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="machineId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="type" type="{urn:ResourceProxy}RestartType"/>
 *         &lt;element name="dietConfFile" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="vishnuConf" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sedType" type="{urn:ResourceProxy}SeDType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sessionKey",
    "machineId",
    "type",
    "dietConfFile",
    "vishnuConf",
    "sedType"
})
@XmlRootElement(name = "restartRequest")
public class RestartRequest {

    @XmlElement(required = true)
    protected String sessionKey;
    @XmlElement(required = true)
    protected String machineId;
    @XmlElement(required = true)
    protected RestartType type;
    @XmlElement(required = true)
    protected String dietConfFile;
    @XmlElement(required = true)
    protected String vishnuConf;
    @XmlElement(required = true)
    protected SeDType sedType;

    /**
     * Gets the value of the sessionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * Sets the value of the sessionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionKey(String value) {
        this.sessionKey = value;
    }

    /**
     * Gets the value of the machineId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMachineId() {
        return machineId;
    }

    /**
     * Sets the value of the machineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMachineId(String value) {
        this.machineId = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link RestartType }
     *     
     */
    public RestartType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestartType }
     *     
     */
    public void setType(RestartType value) {
        this.type = value;
    }

    /**
     * Gets the value of the dietConfFile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDietConfFile() {
        return dietConfFile;
    }

    /**
     * Sets the value of the dietConfFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDietConfFile(String value) {
        this.dietConfFile = value;
    }

    /**
     * Gets the value of the vishnuConf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVishnuConf() {
        return vishnuConf;
    }

    /**
     * Sets the value of the vishnuConf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVishnuConf(String value) {
        this.vishnuConf = value;
    }

    /**
     * Gets the value of the sedType property.
     * 
     * @return
     *     possible object is
     *     {@link SeDType }
     *     
     */
    public SeDType getSedType() {
        return sedType;
    }

    /**
     * Sets the value of the sedType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SeDType }
     *     
     */
    public void setSedType(SeDType value) {
        this.sedType = value;
    }

}
