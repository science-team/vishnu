
package com.sysfera.vishnu.api.fms;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="path" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="owner" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="group" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="perms" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="uid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="gid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="size" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="atime" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="mtime" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ctime" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="type" type="{urn:ResourceProxy}FileType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "path",
    "owner",
    "group",
    "perms",
    "uid",
    "gid",
    "size",
    "atime",
    "mtime",
    "ctime",
    "type"
})
@XmlRootElement(name = "getFileInfoResponse")
public class GetFileInfoResponse {

    @XmlElement(required = true)
    protected String path;
    @XmlElement(required = true)
    protected String owner;
    @XmlElement(required = true)
    protected String group;
    @XmlElement(required = true)
    protected BigInteger perms;
    protected long uid;
    protected long gid;
    protected long size;
    protected long atime;
    protected long mtime;
    protected long ctime;
    @XmlElement(required = true)
    protected FileType type;

    /**
     * Gets the value of the path property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPath() {
        return path;
    }

    /**
     * Sets the value of the path property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPath(String value) {
        this.path = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwner(String value) {
        this.owner = value;
    }

    /**
     * Gets the value of the group property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroup() {
        return group;
    }

    /**
     * Sets the value of the group property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroup(String value) {
        this.group = value;
    }

    /**
     * Gets the value of the perms property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPerms() {
        return perms;
    }

    /**
     * Sets the value of the perms property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPerms(BigInteger value) {
        this.perms = value;
    }

    /**
     * Gets the value of the uid property.
     * 
     */
    public long getUid() {
        return uid;
    }

    /**
     * Sets the value of the uid property.
     * 
     */
    public void setUid(long value) {
        this.uid = value;
    }

    /**
     * Gets the value of the gid property.
     * 
     */
    public long getGid() {
        return gid;
    }

    /**
     * Sets the value of the gid property.
     * 
     */
    public void setGid(long value) {
        this.gid = value;
    }

    /**
     * Gets the value of the size property.
     * 
     */
    public long getSize() {
        return size;
    }

    /**
     * Sets the value of the size property.
     * 
     */
    public void setSize(long value) {
        this.size = value;
    }

    /**
     * Gets the value of the atime property.
     * 
     */
    public long getAtime() {
        return atime;
    }

    /**
     * Sets the value of the atime property.
     * 
     */
    public void setAtime(long value) {
        this.atime = value;
    }

    /**
     * Gets the value of the mtime property.
     * 
     */
    public long getMtime() {
        return mtime;
    }

    /**
     * Sets the value of the mtime property.
     * 
     */
    public void setMtime(long value) {
        this.mtime = value;
    }

    /**
     * Gets the value of the ctime property.
     * 
     */
    public long getCtime() {
        return ctime;
    }

    /**
     * Sets the value of the ctime property.
     * 
     */
    public void setCtime(long value) {
        this.ctime = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link FileType }
     *     
     */
    public FileType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link FileType }
     *     
     */
    public void setType(FileType value) {
        this.type = value;
    }

}
