
package com.sysfera.vishnu.api.fms;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="data" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="filetransfer" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                           &lt;attribute name="transferId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="status" type="{urn:ResourceProxy}Status" />
 *                           &lt;attribute name="userId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="clientMachineId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="sourceMachineId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="destinationMachineId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="sourceFilePath" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="destinationFilePath" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="size" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="start_time" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="trCommand" type="{urn:ResourceProxy}TransferCommand" />
 *                           &lt;attribute name="errorMsg" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data"
})
@XmlRootElement(name = "listFileTransfersResponse")
public class ListFileTransfersResponse {

    protected ListFileTransfersResponse.Data data;

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link ListFileTransfersResponse.Data }
     *     
     */
    public ListFileTransfersResponse.Data getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListFileTransfersResponse.Data }
     *     
     */
    public void setData(ListFileTransfersResponse.Data value) {
        this.data = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="filetransfer" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *                 &lt;attribute name="transferId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="status" type="{urn:ResourceProxy}Status" />
     *                 &lt;attribute name="userId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="clientMachineId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="sourceMachineId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="destinationMachineId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="sourceFilePath" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="destinationFilePath" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="size" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="start_time" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="trCommand" type="{urn:ResourceProxy}TransferCommand" />
     *                 &lt;attribute name="errorMsg" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "filetransfer"
    })
    public static class Data {

        @XmlElement(nillable = true)
        protected List<ListFileTransfersResponse.Data.Filetransfer> filetransfer;

        /**
         * Gets the value of the filetransfer property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the filetransfer property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFiletransfer().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ListFileTransfersResponse.Data.Filetransfer }
         * 
         * 
         */
        public List<ListFileTransfersResponse.Data.Filetransfer> getFiletransfer() {
            if (filetransfer == null) {
                filetransfer = new ArrayList<ListFileTransfersResponse.Data.Filetransfer>();
            }
            return this.filetransfer;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *       &lt;/sequence>
         *       &lt;attribute name="transferId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="status" type="{urn:ResourceProxy}Status" />
         *       &lt;attribute name="userId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="clientMachineId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="sourceMachineId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="destinationMachineId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="sourceFilePath" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="destinationFilePath" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="size" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="start_time" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="trCommand" type="{urn:ResourceProxy}TransferCommand" />
         *       &lt;attribute name="errorMsg" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Filetransfer {

            @XmlAttribute(name = "transferId")
            protected String transferId;
            @XmlAttribute(name = "status")
            protected Status status;
            @XmlAttribute(name = "userId")
            protected String userId;
            @XmlAttribute(name = "clientMachineId")
            protected String clientMachineId;
            @XmlAttribute(name = "sourceMachineId")
            protected String sourceMachineId;
            @XmlAttribute(name = "destinationMachineId")
            protected String destinationMachineId;
            @XmlAttribute(name = "sourceFilePath")
            protected String sourceFilePath;
            @XmlAttribute(name = "destinationFilePath")
            protected String destinationFilePath;
            @XmlAttribute(name = "size")
            protected Long size;
            @XmlAttribute(name = "start_time")
            protected Long startTime;
            @XmlAttribute(name = "trCommand")
            protected TransferCommand trCommand;
            @XmlAttribute(name = "errorMsg")
            protected String errorMsg;

            /**
             * Gets the value of the transferId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTransferId() {
                return transferId;
            }

            /**
             * Sets the value of the transferId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTransferId(String value) {
                this.transferId = value;
            }

            /**
             * Gets the value of the status property.
             * 
             * @return
             *     possible object is
             *     {@link Status }
             *     
             */
            public Status getStatus() {
                return status;
            }

            /**
             * Sets the value of the status property.
             * 
             * @param value
             *     allowed object is
             *     {@link Status }
             *     
             */
            public void setStatus(Status value) {
                this.status = value;
            }

            /**
             * Gets the value of the userId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUserId() {
                return userId;
            }

            /**
             * Sets the value of the userId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUserId(String value) {
                this.userId = value;
            }

            /**
             * Gets the value of the clientMachineId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getClientMachineId() {
                return clientMachineId;
            }

            /**
             * Sets the value of the clientMachineId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setClientMachineId(String value) {
                this.clientMachineId = value;
            }

            /**
             * Gets the value of the sourceMachineId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSourceMachineId() {
                return sourceMachineId;
            }

            /**
             * Sets the value of the sourceMachineId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSourceMachineId(String value) {
                this.sourceMachineId = value;
            }

            /**
             * Gets the value of the destinationMachineId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDestinationMachineId() {
                return destinationMachineId;
            }

            /**
             * Sets the value of the destinationMachineId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDestinationMachineId(String value) {
                this.destinationMachineId = value;
            }

            /**
             * Gets the value of the sourceFilePath property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSourceFilePath() {
                return sourceFilePath;
            }

            /**
             * Sets the value of the sourceFilePath property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSourceFilePath(String value) {
                this.sourceFilePath = value;
            }

            /**
             * Gets the value of the destinationFilePath property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDestinationFilePath() {
                return destinationFilePath;
            }

            /**
             * Sets the value of the destinationFilePath property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDestinationFilePath(String value) {
                this.destinationFilePath = value;
            }

            /**
             * Gets the value of the size property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getSize() {
                return size;
            }

            /**
             * Sets the value of the size property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setSize(Long value) {
                this.size = value;
            }

            /**
             * Gets the value of the startTime property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getStartTime() {
                return startTime;
            }

            /**
             * Sets the value of the startTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setStartTime(Long value) {
                this.startTime = value;
            }

            /**
             * Gets the value of the trCommand property.
             * 
             * @return
             *     possible object is
             *     {@link TransferCommand }
             *     
             */
            public TransferCommand getTrCommand() {
                return trCommand;
            }

            /**
             * Sets the value of the trCommand property.
             * 
             * @param value
             *     allowed object is
             *     {@link TransferCommand }
             *     
             */
            public void setTrCommand(TransferCommand value) {
                this.trCommand = value;
            }

            /**
             * Gets the value of the errorMsg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getErrorMsg() {
                return errorMsg;
            }

            /**
             * Sets the value of the errorMsg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setErrorMsg(String value) {
                this.errorMsg = value;
            }

        }

    }

}
