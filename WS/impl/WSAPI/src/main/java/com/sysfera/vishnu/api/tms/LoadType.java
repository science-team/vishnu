
package com.sysfera.vishnu.api.tms;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoadType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LoadType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="USE_NB_WAITING_JOBS"/>
 *     &lt;enumeration value="USE_NB_JOBS"/>
 *     &lt;enumeration value="USE_NB_RUNNING_JOBS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LoadType")
@XmlEnum
public enum LoadType {

    USE_NB_WAITING_JOBS,
    USE_NB_JOBS,
    USE_NB_RUNNING_JOBS;

    public String value() {
        return name();
    }

    public static LoadType fromValue(String v) {
        return valueOf(v);
    }

}
