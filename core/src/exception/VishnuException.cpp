/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

#include "VishnuException.hpp"
#include <sstream>

template <class T>
std::string convertToString(T val) {
    std::ostringstream out;
    out << val;
    return out.str();
}

VishnuException::VishnuException(){
  mmsgc = "";
  mtype =VishnuException::NONE;
  mval = -1;
}

VishnuException::VishnuException(const VishnuException& e){
  mp = std::map<int, std::string>();
  mfullMsg = "";
  mmsgc = e.getMsgComp();
  mtype = e.getTypeI();
  mval  = e.getMsgI();
}

VishnuException::VishnuException(int msg, std::string msgComp){
  mval = msg;
  mmsgc = msgComp;
  mtype = VishnuException::NONE;
}

const char*
VishnuException::what() const throw()
{
  mfullMsg = "VISHNU Error " + convertToString(getMsgI());
  mfullMsg += " : ";
  mfullMsg += getMsg();
  mfullMsg += " [";
  mfullMsg += mmsgc;
  mfullMsg += " ]";
  return mfullMsg.c_str();
}

void
VishnuException::appendMsgComp(std::string s){
  mmsgc += s;
}

int
VishnuException::getMsgI() const{
  return mval;
}

/**
* \brief Function to get the string associated to SystemException
* \fn    buildExceptionString()
* \return a serialized format of the exception
*/
std::string
VishnuException::buildExceptionString() const {
  std::string errorInfo;

  //To get the error code associated to the exception follows by #
  errorInfo =  convertToString(getMsgI())+"#";

  //To get exception complementary message
  errorInfo.append(mmsgc);

  return errorInfo;
}

/**
* \brief Implementation of the equal operator
* \return the object exception with the new values
*/
VishnuException&
VishnuException::operator=(const VishnuException &e) {
  mmsgc = e.getMsgComp();
  mtype = e.getTypeI();
  mval  = e.getMsgI();
  mfullMsg = e.mfullMsg;
  mp = e.mp;
  return (*this);
}

