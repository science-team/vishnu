/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file LDAPUMSAuthenticator.cpp
 * \brief This file implements the LDAPUMSauthenticator
 * \author Eugène PAMBA CAPO-CHICHI (eugene.capochichi@sysfera.com)
 * \date 15/12/11
 */

#include <iostream>
#include "LDAPUMSAuthenticator.hpp"
#include "LDAPAuthenticator.hpp"
#include "UMSAuthenticator.hpp"
#include "UMSVishnuException.hpp"
#include "SystemException.hpp"

LDAPUMSAuthenticator::LDAPUMSAuthenticator(){
}

LDAPUMSAuthenticator::~LDAPUMSAuthenticator(){
}

bool
LDAPUMSAuthenticator::authenticate(UMS_Data::User& user) {
  bool authenticated = false;

  UMSAuthenticator umsAuthenticator;
  LDAPAuthenticator ldapAuthenticator;
  SystemException excep;
  UMSVishnuException umsexcep;
  bool umsexcepfound = false;
  bool excepfound = false;

  //To avoid to return an exception when the first authenticator failed
  try {
    authenticated = ldapAuthenticator.authenticate(user);
  } catch (UMSVishnuException& e) {
    //Do not throw exception
    umsexcep = e;
    umsexcepfound = true;
  } catch (SystemException& e) {
    //Do not throw exception
    excep = e;
    excepfound = true;
  }

  if (authenticated) {
    return authenticated;
  }
  else {
   authenticated = umsAuthenticator.authenticate(user);
    //if the user is not authenticated
    if (!authenticated) {
      //If an exception has been found
      if (excepfound) {
        throw excep;
      }
      if (umsexcepfound) {
        throw umsexcep;
      }
    }
    return authenticated;
  }
}

