/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file AuthenticatorConfiguration.hpp
 * \brief This file contains the declaration of the AuthenticatorConfiguration class
 * \author Eugène PAMBA CAPO-CHICHI
 * \date 3 Janvier 2012
*/

#ifndef _AUTHENTICATORCONFIGURATION_HPP_
#define _AUTHENTICATORCONFIGURATION_HPP_

#include "ExecConfiguration.hpp"

/**
 * \class AuthenticatorConfiguration
 * \brief Contains the configuration parameters for the Vishnu authenticator
 */
class AuthenticatorConfiguration {

public:

  /**
   * \brief The authenticator type
   */
  typedef enum {
    UMS,
    LDAP,
    UMSLDAP,
    LDAPUMS
  } authentype_t;

  /**
   * \brief Constructor
   * \param execConfig  the configuration of the program
   */
  AuthenticatorConfiguration(const ExecConfiguration& execConfig);

  /**
   * \brief Check that the configuration is correct
   */
  void
  check() throw (UserException);

  /**
   * \brief Get the authenticator type
   * \return authenticator type
   */
  authentype_t
  getAuthenType() const { return mauthType; }


protected:

  /////////////////////////////////
  // Attributes
  /////////////////////////////////

  /**
   * \brief Reference to the main program configuration
   */
  const ExecConfiguration& mexecConfig;

  /**
   * \brief Attribute type of authenticator
   */
  authentype_t mauthType;
};

#endif // _AUTHENTICATORCONFIGURATION_HPP_
