/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

// -*- mode: c++; c-basic-style: "bsd"; c-basic-offset: 4; -*-
/*
 * TMS_Data/SubmitOptionsImpl.cpp
 * Copyright (C) Cátedra SAES-UMU 2010 <andres.senac@um.es>
 *
 * EMF4CPP is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EMF4CPP is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SubmitOptions.hpp"
#include <TMS_Data/TMS_DataPackage.hpp>
#include <TMS_Data/LoadCriterion.hpp>
#include <ecore/EObject.hpp>
#include <ecore/EClass.hpp>
#include <ecore/EStructuralFeature.hpp>
#include <ecore/EReference.hpp>
#include <ecore/EObject.hpp>
#include <ecorecpp/mapping.hpp>

using namespace ::TMS_Data;

/*PROTECTED REGION ID(SubmitOptionsImpl.cpp) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

void SubmitOptions::_initialize()
{
    // Supertypes

    // Rerefences
    if (m_criterion)
    {
        m_criterion->_initialize();
        m_criterion->_setEContainer(
                this,
                ::TMS_Data::TMS_DataPackage::_instance()->getSubmitOptions__criterion());
    }

    /*PROTECTED REGION ID(SubmitOptionsImpl__initialize) START*/
    // Please, enable the protected region if you add manually written code.
    // To do this, add the keyword ENABLED before START.
    /*PROTECTED REGION END*/
}

// Operations


// EObject
::ecore::EJavaObject SubmitOptions::eGet(::ecore::EInt _featureID,
        ::ecore::EBoolean _resolve)
{
    ::ecore::EJavaObject _any;
    switch (_featureID)
    {
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__NAME:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::toAny(_any, m_name);
    }
        return _any;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__QUEUE:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::toAny(_any,
                m_queue);
    }
        return _any;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__WALLTIME:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EInt >::toAny(_any,
                m_wallTime);
    }
        return _any;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__MEMORY:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EInt >::toAny(_any, m_memory);
    }
        return _any;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__NBCPU:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EInt >::toAny(_any, m_nbCpu);
    }
        return _any;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__NBNODESANDCPUPERNODE:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::toAny(_any,
                m_nbNodesAndCpuPerNode);
    }
        return _any;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__OUTPUTPATH:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::toAny(_any,
                m_outputPath);
    }
        return _any;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__ERRORPATH:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::toAny(_any,
                m_errorPath);
    }
        return _any;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__MAILNOTIFICATION:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::toAny(_any,
                m_mailNotification);
    }
        return _any;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__MAILNOTIFYUSER:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::toAny(_any,
                m_mailNotifyUser);
    }
        return _any;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__GROUP:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::toAny(_any,
                m_group);
    }
        return _any;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__WORKINGDIR:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::toAny(_any,
                m_workingDir);
    }
        return _any;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__CPUTIME:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::toAny(_any,
                m_cpuTime);
    }
        return _any;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__SELECTQUEUEAUTOM:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EBoolean >::toAny(_any,
                m_selectQueueAutom);
    }
        return _any;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__CRITERION:
    {
        _any = static_cast< ::ecore::EObject* > (m_criterion);
    }
        return _any;

    }
    throw "Error";
}

void SubmitOptions::eSet(::ecore::EInt _featureID,
        ::ecore::EJavaObject const& _newValue)
{
    switch (_featureID)
    {
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__NAME:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::fromAny(_newValue,
                m_name);
    }
        return;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__QUEUE:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::fromAny(_newValue,
                m_queue);
    }
        return;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__WALLTIME:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EInt >::fromAny(_newValue,
                m_wallTime);
    }
        return;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__MEMORY:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EInt >::fromAny(_newValue,
                m_memory);
    }
        return;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__NBCPU:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EInt >::fromAny(_newValue,
                m_nbCpu);
    }
        return;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__NBNODESANDCPUPERNODE:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::fromAny(_newValue,
                m_nbNodesAndCpuPerNode);
    }
        return;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__OUTPUTPATH:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::fromAny(_newValue,
                m_outputPath);
    }
        return;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__ERRORPATH:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::fromAny(_newValue,
                m_errorPath);
    }
        return;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__MAILNOTIFICATION:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::fromAny(_newValue,
                m_mailNotification);
    }
        return;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__MAILNOTIFYUSER:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::fromAny(_newValue,
                m_mailNotifyUser);
    }
        return;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__GROUP:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::fromAny(_newValue,
                m_group);
    }
        return;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__WORKINGDIR:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::fromAny(_newValue,
                m_workingDir);
    }
        return;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__CPUTIME:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EString >::fromAny(_newValue,
                m_cpuTime);
    }
        return;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__SELECTQUEUEAUTOM:
    {
        ::ecorecpp::mapping::any_traits< ::ecore::EBoolean >::fromAny(
                _newValue, m_selectQueueAutom);
    }
        return;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__CRITERION:
    {
        ::ecore::EObject_ptr _t0 = ::ecorecpp::mapping::any::any_cast<
                ::ecore::EObject_ptr >(_newValue);
        ::TMS_Data::LoadCriterion_ptr _t1 =
                dynamic_cast< ::TMS_Data::LoadCriterion_ptr > (_t0);
        ::TMS_Data::SubmitOptions::setCriterion(_t1);
    }
        return;

    }
    throw "Error";
}

::ecore::EBoolean SubmitOptions::eIsSet(::ecore::EInt _featureID)
{
    switch (_featureID)
    {
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__NAME:
        return ::ecorecpp::mapping::set_traits< ::ecore::EString >::is_set(
                m_name);
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__QUEUE:
        return ::ecorecpp::mapping::set_traits< ::ecore::EString >::is_set(
                m_queue);
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__WALLTIME:
        return m_wallTime != -1;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__MEMORY:
        return m_memory != -1;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__NBCPU:
        return m_nbCpu != -1;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__NBNODESANDCPUPERNODE:
        return ::ecorecpp::mapping::set_traits< ::ecore::EString >::is_set(
                m_nbNodesAndCpuPerNode);
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__OUTPUTPATH:
        return ::ecorecpp::mapping::set_traits< ::ecore::EString >::is_set(
                m_outputPath);
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__ERRORPATH:
        return ::ecorecpp::mapping::set_traits< ::ecore::EString >::is_set(
                m_errorPath);
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__MAILNOTIFICATION:
        return ::ecorecpp::mapping::set_traits< ::ecore::EString >::is_set(
                m_mailNotification);
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__MAILNOTIFYUSER:
        return ::ecorecpp::mapping::set_traits< ::ecore::EString >::is_set(
                m_mailNotifyUser);
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__GROUP:
        return ::ecorecpp::mapping::set_traits< ::ecore::EString >::is_set(
                m_group);
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__WORKINGDIR:
        return ::ecorecpp::mapping::set_traits< ::ecore::EString >::is_set(
                m_workingDir);
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__CPUTIME:
        return ::ecorecpp::mapping::set_traits< ::ecore::EString >::is_set(
                m_cpuTime);
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__SELECTQUEUEAUTOM:
        return m_selectQueueAutom != false;
    case ::TMS_Data::TMS_DataPackage::SUBMITOPTIONS__CRITERION:
        return m_criterion;

    }
    throw "Error";
}

void SubmitOptions::eUnset(::ecore::EInt _featureID)
{
    switch (_featureID)
    {

    }
    throw "Error";
}

::ecore::EClass_ptr SubmitOptions::_eClass()
{
    static ::ecore::EClass_ptr
            _eclass =
                    dynamic_cast< ::TMS_Data::TMS_DataPackage_ptr > (::TMS_Data::TMS_DataPackage::_instance())->getSubmitOptions();
    return _eclass;
}

