/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file DatabaseResult.hpp
 * \brief This file presents the Database results class.
 * \author Eugène PAMBA CAPO-CHICHI (eugene.capochichi.sysfera.com)
 * \date 15/12/10 
 */

#ifndef _DATABASERESULT_H_
#define _DATABASERESULT_H_

#include <string>
#include <vector>

/**
 * \class DatabaseResult
 * \brief This class describes the object which encapsulates the database results
 */
class DatabaseResult{
public :
  /**
   * \brief Function to print the database results
   * \fn    print()
   */
  void 
  print();
  /**
   * \brief Function to print the attributes names
   * \fn    printAttributesNames()
   */
  void 
  printAttributesNames();
  /**
   * \brief To get the number of tuples 
   * \fn size_t getNbTuples() const
   * \return 0 on success, an error code otherwise
   */
  size_t 
  getNbTuples() const;
  /**
   * \brief To get the number of fields 
   * \fn size_t getNbFields() const
   * \return 0 on success, an error code otherwise
   */
  size_t 
  getNbFields() const;
  
  /**
   * \brief To get a specific results using its position
   * \fn std::vector<std::string> get(size_t position) const 
   * \param position The position of the request
   * \return the tuple associated to the postion
   */
  std::vector<std::string> 
  get(size_t position) const;
  /**
   * \brief To get the number of fields 
   * \fn std::vector<std::vector<std::string> > getResults() const
 
   * \return all results
   */
  std::vector<std::vector<std::string> >
  getResults() const;
  /**
   * \brief To get the first attribut value of the first element 
   * \fn std::string getFirstElement() const
   * \return the first attribut value of the first element
   */
   std::string
   getFirstElement() const;
  /**
   * \fn DatabaseResult(std::vector<std::vector<std::string> > res, std::vector<std::string> namesAttributes)
   * \brief Constructor, raises an exception on error
   */
  DatabaseResult(std::vector<std::vector<std::string> > res, std::vector<std::string> namesAttributes);
  /**
   * \fn ~DatabaseResult()
   * \brief Destructor, raises an exception on error
   */
 ~DatabaseResult();
private :
  /////////////////////////////////
  // Attributes
  /////////////////////////////////
  /**
   * \brief The number of fields
   */
   size_t fields;
  /**
   * \brief The number of tuples
   */
   size_t tuples;
  /**
   * \brief The database results
   */
  std::vector<std::vector<std::string> > results;
  /**
   * \brief The attributes names
   */
  std::vector<std::string> attributesNames;
};

#endif // DATABASERESULT
