/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file OracleDatabase.hpp
 * \brief This file presents a PostGreSQL database.
 * \author Kevin Coulomb (kevin.coulomb@sysfera.com)
 * \date 15/12/10
 */

#ifndef _ORACLEDATABASE_HH_
#define _ORACLEDATABASE_HH_

#include "InternalVishnuException.hpp"
#include "Database.hpp"
#include "occi.h"
#include <vector>

using namespace oracle::occi;
/**
 * \class OracleDatabase
 * \brief Oracle implementation of the Database class
 */
class OracleDatabase : public Database {
public :
  /**
   * \brief Function to process the request in the database
   * \fn    int process(std::string request)
   * \param request The request to process
   * \return raises an exception on error
   */
  int
  process(std::string request);

  /**
   * \brief To start a transaction with the database
   * \fn int startTransaction(std::string request)
   * \param request The series of requests to process
   * \return raises an exception on error
   */
  int
  startTransaction(std::string request);

  /**
  * \brief To make a connection to the database
  * \fn int connect()
  * \return raises an exception on error
  */
  int
  connect();
  /**
   * \fn OracleDatabase(std::string hostname,
   *		         std::string username,
   * 	 	         std::string pwd,
   *	 	         std::string database = "",
   *	 	         unsigned int port = 0)
   * \param hostname The name of the host to connect
   * \param username The username to connect to the database
   * \param pwd      The password to connect to the database
   * \param database The database to connect
   * \param port     The port to use
   * \brief Constructor, raises an exception on error
   */
  OracleDatabase(std::string hostname,
                 std::string username,
                 std::string pwd,
                 std::string database = "",
                 unsigned int port = 0);

  /**
   * \fn ~OracleDatabase()
   * \brief Destructor, raises an exception on error
   */
  ~OracleDatabase();

  /**
   * \brief To set the db to use
   * \fn int setDatabase(std::string db)
   * \param db The name of the database to use
   * \return 0 raises an exception on error
   */
  int
  setDatabase(std::string db);

  /**
  * \brief To get the result of a select request
  * \fn DatabaseResult* getResult(std::string request)
  * \param request The request to process
  * \return An object which encapsulates the database results
  */
  DatabaseResult*
  getResult(std::string request);

  /**
   * \brief To get the type of database
   * \return An enum identifying the type of database
   */
  DbConfiguration::db_type_t
  getDbType() { return DbConfiguration::ORACLE; };

private :

  /////////////////////////////////
  // Attributes
  /////////////////////////////////

  /**
   * \brief The oracle environnement
   */
  Environment *menvironment;
  /**
   * \brief The connection structure
   */
  Connection  *mcon;
  /**
   * \brief The statement (=request)
   */
  Statement   *mstmt;
  /**
   * \brief The result of a request
   */
  ResultSet   *mres;

  /**
   * \brief The host of the base
   */
  std::string mhost;
  /**
   * \brief The username to connect
   */
  std::string musername;
  /**
   * \brief The password to connect
   */
  std::string mpwd;
  /**
   * \brief The database to connect
   */
  std::string mdatabase;

  /////////////////////////////////
  // Functions
  /////////////////////////////////

  /**
   * \brief To disconnect from the database
   * \fn int disconnect()
   * \return raises an exception on error
   */
  int
  disconnect();
};

#endif // POSTGREDATABASE
