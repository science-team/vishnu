/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file DbConfiguration.hpp
 * \brief This file contains the declaration of the DbConfiguration class
 * \author Benjamin Isnard
 * \date April
*/

#ifndef _DBCONFIGURATION_HPP_
#define _DBCONFIGURATION_HPP_

#include "ExecConfiguration.hpp"

/**
 * \class DbConfiguration
 * \brief Contains the configuration parameters for the Vishnu database
 */
class DbConfiguration {

public:

  /**
   * \brief The database type
   */
  typedef enum {
    POSTGRESQL,
    ORACLE,
    MYSQL
  } db_type_t;

  /**
   * \brief Default value for the nb of db connections in the pool
   */
  static const int defaultDbPoolSize;

  /**
   * \brief Constructor
   * \param execConfig  the configuration of the program
   */
  DbConfiguration(const ExecConfiguration& execConfig);

  /**
   * \brief Check that the configuration is correct
   */
  void check() throw (UserException);

  /**
   * \brief Get the database type
   * \return database type
   */
  db_type_t getDbType() const { return mdbType; }

  /**
   * \brief Get the database host
   * \return database host
   */
  const std::string& getDbHost() const { return mdbHost; }

  /**
   * \brief Get the database port
   * \return database port
   */
  int getDbPort() const { return mdbPort; }

  /**
   * \brief Get the database name
   * \return database name
   */
  const std::string& getDbName() const { return mdbName; }

  /**
   * \brief Get the database user name to connect to the database
   * \return database user name
   */
  const std::string& getDbUserName() const { return mdbUserName; }

   /**
   * \brief Get the database user password to connect to the database
   * \return database user password
   */
  const std::string& getDbUserPassword() const { return mdbPassword; }

  /**
   * \brief Set the nb of connections to open simultaneously to the database
   * \param poolSize: nb of connections in the connection pool
   */
  void setDbPoolSize(unsigned poolSize) { mdbPoolSize = poolSize; }

  /**
   * \brief Get the nb of connections to open simultaneously to the database
   * \brief in order to process parallel requests.
   * \return nb of connections in the connection pool
   */
  int getDbPoolSize() { return mdbPoolSize; }

protected:

  /////////////////////////////////
  // Attributes
  /////////////////////////////////

  /**
   * \brief Reference to the main program configuration
   */
  const ExecConfiguration& mexecConfig;

  /**
   * \brief Attribute type of database
   */
  db_type_t mdbType;

  /**
   * \brief Attribute host of the database
   */
  std::string mdbHost;

  /**
   * \brief Attribute port of the database
   */
  unsigned mdbPort;

  /**
   * \brief Attribute name of the database
   */
  std::string mdbName;

  /**
   * \brief Attribute user name to connect to the database
   */
  std::string mdbUserName;

  /**
   * \brief Attribute user password to connect to the database
   */
  std::string mdbPassword;

  /**
   * \brief Attribute number of db connections in the pool
   */
  unsigned mdbPoolSize;

};

#endif // _DBCONFIGURATION_HPP_
