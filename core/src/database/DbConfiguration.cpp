/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file DbConfiguration.cpp
 * \brief This file contains the definitions of the DbConfiguration class
 * \author Benjamin Isnard
 * \date April
*/

#include "DbConfiguration.hpp"

using namespace std;

const int DbConfiguration::defaultDbPoolSize = 10;  //%RELAX<MISRA_0_1_3> Used in this file

/**
 * \brief Constructor
 * \param execConfig  the configuration of the program
 */
DbConfiguration::DbConfiguration(const ExecConfiguration& execConfig) : mexecConfig(execConfig),
mdbPort(0), mdbPoolSize(defaultDbPoolSize), mdbType(POSTGRESQL)
{
}

/**
 * \brief Check that the configuration is correct
 */
void DbConfiguration::check() throw (UserException)
{
  string dbTypeStr;
  mexecConfig.getRequiredConfigValue<std::string>(vishnu::DBTYPE, dbTypeStr);
  if (dbTypeStr == "oracle") {
    mdbType = DbConfiguration::ORACLE;
  } else if (dbTypeStr == "postgresql") {
    mdbType = DbConfiguration::POSTGRESQL;
  } else if (dbTypeStr == "mysql") {
    mdbType = DbConfiguration::MYSQL;
  } else {
    throw UserException(ERRCODE_INVALID_PARAM, "Configuration for database type is invalid (must be 'oracle' or 'postgresql' or 'mysql')");
  }
  mexecConfig.getRequiredConfigValue<std::string>(vishnu::DBHOST, mdbHost);
  mexecConfig.getConfigValue<unsigned>(vishnu::DBPORT, mdbPort);
  mexecConfig.getRequiredConfigValue<std::string>(vishnu::DBNAME, mdbName);
  mexecConfig.getRequiredConfigValue<std::string>(vishnu::DBUSERNAME, mdbUserName);
  mexecConfig.getRequiredConfigValue<std::string>(vishnu::DBPASSWORD, mdbPassword);
  mexecConfig.getConfigValue<unsigned>(vishnu::DBPOOLSIZE, mdbPoolSize);

}

