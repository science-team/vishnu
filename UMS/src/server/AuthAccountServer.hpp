/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
* \file AuthAccountServer.hpp
* \brief This file presents the Class which manipulates VISHNU authAccount data on server side.
* \author Eugène PAMBA CAPO-CHICHI (eugene.capochichi@sysfera.com)
* \date 31/01/2011
*/

#ifndef AUTH_ACCOUNT_SERVER_H
#define AUTH_ACCOUNT_SERVER_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include "SessionServer.hpp"
#include "UMS_Data.hpp"
#include "UMS_Data_forward.hpp"

/**
* \class AuthAccountServer
* \brief AuthAccountServer class implementation
*/
class AuthAccountServer {
public:
  /**
  * \brief Constructor
  * \fn AuthAccountServer(UMS_Data::AuthAccount*& account, SessionServer& session)
  * \param account The AuthAccount data structure
  * \param session The object which encapsulates session data
  */
  AuthAccountServer(UMS_Data::AuthAccount*& account, SessionServer& session);
  /**
  * \brief Function to add a new VISHNU authAccount
  * \fn int add()
  * \return raises an exception on error
  */
  int
  add();
  /**
  * \brief Function to update a VISHNU authAccount
  * \fn int update()
  * \return raises an exception on error
  */
  int
  update();
  /**
  * \brief Function to delete a VISHNU authAccount
  * \fn int deleteAuthAccount()
  * \return raises an exception on error
  */
  int
  deleteAuthAccount();
  /**
  * \fn ~AuthAccountServer
  * \brief Destructor,
  */
  ~AuthAccountServer();
  /**
  * \brief Function to get authAccount data structure
  * \fn UMS_Data::AuthAccount getData()
  * \return  The AuthAccount data structure
  */
  UMS_Data::AuthAccount*
  getData();
  /**
  * \brief Function to get authAccount information from the database vishnu
  * \fn getAttribut(std::string condition, std::string attrname);
  * \param condition The condition of the select request
  * \param attrname the name of the attribut to get
  * \return the value of the attribut or empty string if no results
  */
  std::string
  getAttribut(std::string condition, std::string attrname = "authaccountid");

private:
  /////////////////////////////////
  // Attributes
  /////////////////////////////////
  /**
  * \brief The authAccount data structure
  */
  UMS_Data::AuthAccount *mauthAccount;
  /**
  * \brief An object which encapsulates session data
  */
  SessionServer msessionServer;
  /**
  * \brief An instance of vishnu database
  */
  Database *mdatabaseVishnu;
  /////////////////////////////////
  // Functions
  /////////////////////////////////
  /**
  * \brief Function to check authAccount on database
  * \param idAuthSystem The internal database id of the user-authentication system
  * \param idUser The internal database id of the user
  * \return true if the authAccount exists else false
  */
  bool
  exist(std::string idAuthSystem, std::string idUser);
   /**
  * \brief Function to check if a given login is used on a user-authentication system
  * \param numAuthSystem the internal id of the user-authentication system
  * \param acLogin the account login
  * \return true if the login is already used on the machine
  */
  bool
  isLoginUsed(std::string numAuthSystem, std::string acLogin);
  /**
  * \brief Function to check the authAccount userId
  * \param userServer The user server to use
  * \return raises an exception on error
  */
  int
  checkAuthAccountUserId(UserServer& userServer);
};
#endif
