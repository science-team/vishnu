/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
* \file AuthSystemServer.hpp
* \brief This file presents the Class which manipulates VISHNU AuthSystem data on server side.
* \author Eugène PAMBA CAPO-CHICHI (eugene.capochichi@sysfera.com)
* \date 03/02/2012
*/

#ifndef AUTH_SYSTEM_SERVER_H
#define AUTH_SYSTEM_SERVER_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include "SessionServer.hpp"
#include "UMS_Data.hpp"
#include "UMS_Data_forward.hpp"


/**
* \class AuthSystemServer
* \brief AuthSystemServer class implementation
*/
class AuthSystemServer {
public:
  /**
  * \brief Constructor
  * \param authSystem The AuthSystem data structure
  */
  AuthSystemServer(UMS_Data::AuthSystem*& authSystem);
  /**
  * \brief Constructor
  * \param authSystem The AuthSystem data structure
  * \param session The object which encapsulates session data
  */
  AuthSystemServer(UMS_Data::AuthSystem*& authSystem, SessionServer& session);
  /**
  * \brief Function to add a new VISHNU localAccount
  * \param vishnuId The identifier of the vishnu instance
  * \return raises an exception on error
  */
  int
  add(int vishnuId);
  /**
  * \brief Function to update a VISHNU localAccount
  * \return raises an exception on error
  */
  int
  update();
  /**
   * \brief Function to delete a VISHNU localAccount
   * \return raises an exception on error
   */
  int
  deleteAuthSystem();
  /**
  * \fn ~AuthSystemServer
  * \brief Destructor,
  */
  ~AuthSystemServer();
  /**
  * \brief Function to get localAccount data structure
  * \return  The AuthSystem data structure
  */
  UMS_Data::AuthSystem*
  getData();
  /**
  * \brief Function to get localAccount information from the database vishnu
  * \param condition The condition of the select request
  * \param attrname the name of the attribut to get
  * \return the value of the attribut or empty string if no results
  */
  std::string
  getAttribut(std::string condition, std::string attrname = "numauthsystemid");

private:
  /////////////////////////////////
  // Attributes
  /////////////////////////////////
  /**
  * \brief The AuthSystem data structure
  */
  UMS_Data::AuthSystem *mauthsystem;
  /**
  * \brief An object which encapsulates session data
  */
  SessionServer msessionServer;
  /**
  * \brief An instance of vishnu database
  */
  Database *mdatabaseVishnu;

  /////////////////////////////////
  // Functions
  /////////////////////////////////
 /**
  * \brief Function to check the user-authentication system on database
  * \fn bool exist()
  * \return true if the user-authentication system exists else false
  */
  bool
  exist();
  /**
  * \brief Function to check the user-authentication system parameters values
  *\return raises an exception on error
  */
  int
  checkValues();
  /**
  * \brief Function to check the $USERNAME string on the ldap base
  *\return raises an exception on error
  */
  int
  checkLdapBase();
};
#endif
