/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file NetrcReader.hpp
 * \brief This file contains the netrc file reader class.
 * \author Daouda Traore (daouda.traore@sysfera.com) and
 * Eugene PAMBA CAPO-CHICHI (eugene.capochichi@sysfera.com)
 * \date December 2011
 */
#ifndef _NETRC_READER_H_
#define _NETRC_READER_H_

#include <iostream>
#include <vector>
#include <map>


using namespace std;

/**
 * \class NetrcReader
 * \brief NetrcReader class implementation
 */
class NetrcReader {

  public:
    /**
     * \fn NetrcReader(std::string path)
     * \brief Constructor
     */
    NetrcReader();

    /**
     * \fn NetrcReader(std::string path)
     * \param path  The path until the netrc file
     * \brief Constructor
     */
    NetrcReader(std::string path);

    /**
     * \brief Function to get the login and the password associated to a specific machine
     * defined on the netrc file
     * \param auth all the login and password returned which is associated to the machine
     * \param machineName the name of the machine defined on the netrc file
     * \return the corresponding couple (login, password)
     */
    void
    read(map<size_t, pair<string,string> >& auth, const string& machineName = "vishnu");

    /**
     * \fn ~NetrcReader()
     * \brief Destructor
     */
    ~NetrcReader();

  private:

    /////////////////////////////////
    // Functions
    /////////////////////////////////
    /**
      * \brief Function to get value associated to a key
      * \param key the key associated to a value on the file
      * \param value  the value corresponding to the key on the file
      * \param tokens the parsed tokens
      * \param tab a table which allows to register the couple key value
      * \param machine_pos
      * \return true if the value has found else found
      */
    bool
    searchValueFromKey( const string& key,
                        string& value,
                        vector<string>& tokens,
                        std::map<size_t, pair<string,string> >& tab,
                        const size_t machine_pos);

    /**
    * \brief Function to get the login and the password associated to a specific machine from a map
    * \param tab a table in which the couple key and value are registered
    * \param machineName the machine name used for searching the couple
    * \return the login and password associated to the corresponding machineName
    */
    pair<string,string>
    getIdentifiers(std::map<size_t, pair<string,string> >& tab, const string& machineName);

    /**
     * \brief Function to get the login and the password associated to a specific machine
     * by parsing the netrc file
     * \param machineName the name of the machine
     * \return all the corresponding couple (login, password)
     */
    map<size_t, pair<string,string> >&
    getNetrcInfo(const string& machineName);

    /**
     * \brief Function to check the validity of the netrc file
     * \fn  void check()
     * \return raises an exception on error
     */
    void
    check();

    /**
     * \brief Function to analyze the couple key and value table 
     * \param tab a table in which the couple key and value are registered
     * \param machineName the machine name used for searching the couple
     * \return The analyzed tab
     */
    map<size_t, pair<string,string> >&
     analyze(std::map<size_t, pair<string,string> >& tab, const string& machineName);

    /////////////////////////////////
    // Attributes
    /////////////////////////////////

    /**
     * \brief The path until the netrc file
     */
    string mpath;

};
#endif
