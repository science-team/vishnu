/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file ConfigurationProxy.hpp
 * \brief This file contains the VISHNU configurationProxy class.
 * \author Daouda Traore (daouda.traore@sysfera.com)
 * \date February 2011
 */
#ifndef _CONFIGURATION_PROXY_H_
#define _CONFIGURATION_PROXY_H_

#include <string>
#include <iostream>
#include <fstream>

#include "SessionProxy.hpp"
#include "Configuration.hpp"

/**
 * \class ConfigurationProxy 
 * \brief ConfigurationProxy class implementation 
 */
class ConfigurationProxy
{

  public:

    /**
     * \fn ConfigurationProxy(const std::string& filePath,
     *                        const SessionProxy& session)
     * \param filePath The file containing the configuration (serialized)
     * \param session The object which encapsulates the session information (ex: identifier of the session)
     * \brief Constructor, raises an exception on error
     */
    ConfigurationProxy(const std::string& filePath, const SessionProxy& session);
    /**
     * \fn ConfigurationProxy(UMS_Data::Configuration* config,
     *                        const SessionProxy& session)
     * \param config The object which encapsulates the configuration description 
     * \param session The object which encapsulates the session information (ex: identifier of the session)
     * \brief Constructor, raises an exception on error
     */
    ConfigurationProxy(UMS_Data::Configuration* config, const SessionProxy& session);
    /**
     * \brief Function to save the configuration of VISHNU 
     * \fn  int save()
     * \return raises an exception on error
     */
    int save();
    /**
     * \brief Function to restore the configuration of VISHNU from a file 
     * \fn  int restoreFromFile()
     * \return raises an exception on error
     */
    int restoreFromFile();
    /**
     * \brief Function to restore the configuration of VISHNU from an object (Data) 
     * \fn  int restoreFromData()
     * \return raises an exception on error
     */
    int restoreFromData();
    /**
     * \brief Function get the saved configuration of VISHNU 
     * \fn  UMS_Data::Configuration* getData()
     * \return saved configuration 
     * \return raises an exception on error
     */
    UMS_Data::Configuration* getData();
    /**
     * \brief Function get SessionProxy object which contains the VISHNU session identifier 
     * \fn SessionProxy getSessionProxy() 
     * \return a SessionProy object which contains the VISHNU session information 
     * \return raises an exception on error
     */
    SessionProxy getSessionProxy();
    /**
     * \fn ~ConfigurationProxy()
     * \brief Destructor, raises an exception on error
     */
    ~ConfigurationProxy();

  private:
    /**
     * \brief Function to combine restoreFromFile() and restoreFromData() into one function 
     * \fn  int restore(bool fromFile=true)
     * \param fromFile To select the call of restoreFromFile or restoreFromData function 
     * \return raises an exception on error
     */
    int restore(bool fromFile=true);

    /////////////////////////////////
    // Attributes
    /////////////////////////////////

    /**
     * \brief A pointer on the Configuration object
     */ 
    UMS_Data::Configuration *mconfiguration;
    /**
     * \brief The SessionProxy object containing the encrypted identifier of the session
     *  generated by VISHNU
     */
    SessionProxy msessionProxy;
    /**
     * \brief the file containing the configuration (serialized)
     */
    std::string mfilePath;
};
#endif
