/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file connectUtils.hpp
 * \brief This file declare useful functions and types for the VISHNU connexion
 * command
 * \author Ibrahima Cissé (ibrahima.cisse@sysfera.com)
 */



#ifndef CONNECTUTILS__HH
#define CONNECTUTILS__HH


#include<iostream>
#include<boost/shared_ptr.hpp>

#include <ecore.hpp> // Ecore metamodel
#include <ecorecpp.hpp> // EMF4CPP utils
#include "UMS_Data.hpp"


class Options;



/**
 * \brief a environment variable name mapper
 *
 * For each environment variable, the name_mapper function
 * is called to obtain the option name. If it returns empty string
 * the variable is ignored.
 */
struct UMS_env_name_mapper{

/**
 * Mapper operator for environnement variable
 * \param name The environment variable name
 * \return The name used in the program
 */

	std::string operator ()(std::string name){

		std::string result= "";

                if (name=="VISHNU_CONFIG_FILE") {

                        result="dietConfig";
                }

                if (name=="VISHNU_CLOSE_POLICY") {

                        result="closePolicy";
                }

		return result;
	}
};




/**
 * \brief To build options for the VISHNU authentication (connect, reconnect)command
 * command
 * \param pgName: Represents the name of the command
 * \param userId : A callback for user identifier
 * \param req    : for required parameters, 0 optional paramter whereas 1 means
 * mandatory parameter
 * \param dietConfig      : Represents the VISHNU config file
 * \param userIdGroup : represents the userId option group type, it may be HIDDEN,CONFIG,or ENV
 * \return The description of all options allowed by the command
 */


template <typename userIdType>

boost::shared_ptr<Options>
makeConnectOptions(std::string pgName,userIdType& userId,int req,std::string& dietConfig, const Group_type& userIdGroup=HIDDEN){

  boost::shared_ptr<Options> opt(new Options(pgName));


  opt->add("dietConfig,c",
           "The diet config file",
           ENV,
           dietConfig);

  opt->add("userId,u",
           "represents the VISHNU user identifier",
           userIdGroup,
           userId,
           req);


  return opt;


}


#endif



