/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file authenticationAccountUtils.cpp
 * \brief This file defines useful functions and types for the VISHNU add
 * update and delete authentication account commands
 * \author Ibrahima Cisse (ibrahima.cisse@sysfera.com)
 */

#include<iostream>
#include "authenticationAccountUtils.hpp"
#include "Options.hpp"
#include <string>
using namespace std;



/**
 * \brief To build options for the VISHNU system authentication commands
 * \param pgName : The name of the command
 * \param dietConfig : Represents the VISHNU config file
 * \param fAuthSystemId : the identifier of the user-authentication system
 * \param fUserId :represents an admin option to add a local user-authentication configuration of a specific user
 * \param fAcLogin : represents the login of the user on the associated user-authentication system
 * \return The description of all options allowed by the command
 */


boost::shared_ptr<Options> makeAuthAccountOptions(std::string pgName,std::string & dietConfig,StringcallBackType& fAuthSystemId,StringcallBackType& fUserId, StringcallBackType& fAcLogin, int type) {


  boost::shared_ptr<Options> opt(new Options(pgName));


  opt->add("dietConfig,c",
      "The diet config file",
      ENV,
      dietConfig);


  Group_type group=CONFIG;


  if(type){// type =0 for "update function" and type=1 for "add function"

    group=HIDDEN;
  }

  opt->add("authSystemId",
      "the identifier of the user-authentication system",
      HIDDEN,
      fAuthSystemId,
      1);

    opt->setPosition("authSystemId",1);


  opt->add("acLogin,l",
      " represents the login of the user on the associated "
      "user-authentication system",
      group,
      fAcLogin,
      type);

  if (type){
    opt->setPosition("acLogin",1);
  }


  opt->add("userId,u",
      "represents an admin option to add a local"
      "user-authentication configuration of a specific user",
      CONFIG,
      fUserId);


  return opt;

}




