/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file list_authentication_accounts.cpp
 * This file defines the VISHNU list authentication accounts command
 * \author Ibrahima Cisse (ibrahima.cisse@sysfera.com)
 */

#include "common.hpp"
#include "cliUtil.hpp"
#include "utils.hpp"
#include "sessionUtils.hpp"
#include <boost/bind.hpp>
#include "utilVishnu.hpp"

#include "GenericCli.hpp"

namespace po = boost::program_options;

using namespace std;
using namespace vishnu;

struct ListAuthenticationAccountsFunc {

  UMS_Data::ListAuthAccounts mlsAuthAccounts;
  UMS_Data::ListAuthAccOptions mlistOptions;
  bool mfull;

  ListAuthenticationAccountsFunc(UMS_Data::ListAuthAccounts lsAuthAccounts, UMS_Data::ListAuthAccOptions listOptions, bool full):
    mlsAuthAccounts(lsAuthAccounts), mlistOptions(listOptions), mfull(full)
  {};

  int operator()(std::string sessionKey) {
    int res =  listAuthAccounts(sessionKey,mlsAuthAccounts,mlistOptions);
    // Display the list
    if(mfull) {
      cout << mlsAuthAccounts << endl;
    }
    else {
      for(unsigned int i = 0; i < mlsAuthAccounts.getAuthAccounts().size(); i++) {
        cout << mlsAuthAccounts.getAuthAccounts().get(i) ;
      }
    }

    return res;
  }
};

int main (int ac, char* av[]){


  /******* Parsed value containers ****************/

  string dietConfig;

  /********** EMF data ************/

  UMS_Data::ListAuthAccounts lsAuthAccounts;

  UMS_Data::ListAuthAccOptions listOptions;

  /******** Callback functions ******************/

  boost::function1<void,string> fUserId( boost::bind(&UMS_Data::ListAuthAccOptions::setUserId,boost::ref(listOptions),_1));

  boost::function1<void,string> fAuthSystemId( boost::bind(&UMS_Data::ListAuthAccOptions::setAuthSystemId,boost::ref(listOptions),_1));


  /**************** Describe options *************/

  boost::shared_ptr<Options> opt(new Options(av[0]));


  opt->add("dietConfig,c",
      "The diet config file",
      ENV,
      dietConfig);


  opt->add("userId,u",
      "is an admin option for listing all user-authentication "
      "accounts of a specific user identified by his/her userId",
      CONFIG,
      fUserId);

  opt->add("authSystemId,i",
      "is an option for listing user-authentication account of "
       "a specific user-authentication system",
      CONFIG,
      fAuthSystemId);

  opt->add("listAll,a",
      "is an admin option for listing all"
      " user-authentication accounts of VISHNU",
      CONFIG
      );

  bool isEmpty;
  //To process list options
  GenericCli().processListOpt(opt, isEmpty, ac, av);

  if (opt->count("listAll")){
    listOptions.setListAll(true);
  }

  bool full = false;
  // Display the list
  if(isEmpty|| (opt->count("listAll"))) {
    full = true;
  }

  ListAuthenticationAccountsFunc listAuthenticationAccountsFunc (lsAuthAccounts, listOptions, full);
  return GenericCli().run(listAuthenticationAccountsFunc, dietConfig, ac, av);
}// end of main


