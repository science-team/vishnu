/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file reconnect_m.cpp
 * This file defines the VISHNU reconnect command
 * \author Ibrahima Cisse (ibrahima.cisse@sysfera.com)
 */

#include <stdlib.h>
#include "common.hpp"
#include "cliUtil.hpp"
#include "utils.hpp"
#include "connectUtils.hpp"
#include "sessionUtils.hpp"
#include "daemon_cleaner.hpp"
#include "utilVishnu.hpp"
#include "UserException.hpp"

using namespace std;
using namespace vishnu;


int main (int ac, char* av[]){

  int counter;

  string sessionId;

  string dietConfig;

  string userId;

  string password;

  /*********** In parameters **********************/

  UMS_Data::ListUsers listUsers;
  UMS_Data::UMS_DataFactory_ptr ecoreFactory = UMS_Data::UMS_DataFactory::_instance();

  boost::shared_ptr<Options> opt=makeConnectOptions(av[0],userId,0, dietConfig,CONFIG);



  opt->add("sessionId,s",
           "represents the identifier of the session",
           HIDDEN,
           sessionId,1);

  opt->setPosition("sessionId",1);


  opt->add("password,w","To give the password ",CONFIG,password );

/********* Out parameters     *****************/

  UMS_Data::Session session;



  try {

    dietConfig = string(getenv("VISHNU_CONFIG_FILE"));
    if (ac<2) {
      helpUsage(*opt,"[option] ");
      return ERRCODE_CLI_ERROR_MISSING_PARAMETER;
    }

    sessionId = string(av[1]);

    /************** Call UMS reconnect service *******************************/

    cleaner(const_cast<char*>(dietConfig.c_str()), ac, av);// lauch the daemon cleaner if it is not already running

    /********* Get all the couples user/pwd ******************/
    counter = 2;
    while (counter<ac) {
      string tmp(av[counter]);
      if (counter+1>=ac) {
        helpUsage(*opt,"[option] ");
        return ERRCODE_CLI_ERROR_MISSING_PARAMETER;
      }
      UMS_Data::User_ptr user = ecoreFactory->createUser();
      user->setUserId(tmp);
      user->setPassword(string(av[counter+1]));
      listUsers.getUsers().push_back(user);
      counter += 2;
    }

    // initializing DIET

    if (vishnuInitialize(const_cast<char*>(dietConfig.c_str()), ac, av)) {

      errorUsage(av[0],dietErrorMsg,EXECERROR);

      return  ERRCODE_CLI_ERROR_DIET ;

    }

    reconnect(listUsers, sessionId, session);

    // store the session information

    storeLastSession(session,getppid());

    printSuccessMessage();


  }// End of try bloc

  catch(VishnuException& e){// catch all Vishnu runtime error

    std::string  msg = e.getMsg()+" ["+e.getMsgComp()+"]";

    errorUsage(av[0], msg,EXECERROR);

    return e.getMsgI() ;

  }

  catch(std::exception& e){//// catch all other command line runtime error

    errorUsage(av[0], e.what()) ;

    return ERRCODE_CLI_ERROR_RUNTIME;
  }

  return 0;

  // }}RELAX<CODEREDUCER>

}// end of main


