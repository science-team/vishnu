/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

#ifndef UMS_TEST_UTILS_HPP
#define UMS_TEST_UTILS_HPP

#include <fstream>
#include <sstream>

/**
 * \brief To put the a string content on a file
 * \param path  the file path
 * \param content the file content
 * \return true if all names are found within the directory long content
 */
void
putOnFile(const std::string& path, const std::string& content) {
  std::ofstream ofile(path.c_str());
  ofile << content;
  ofile.close();
}

/**
 * \brief To change the content of a file by replacing a specific values
 * \param path  the file path
 * \param oldValue the value which will be replaced
 * \param newValue the new value
 */
void
replaceAllOccurences(const std::string& path,
                     const std::string& oldValue,
                     const std::string& newValue) {

  std::string fileContent;

  std::ifstream ifs (path.c_str());
  if (ifs.is_open()) {
    std::ostringstream oss;
    oss << ifs.rdbuf();
    fileContent = oss.str();
    ifs.close();
  }
  size_t pos = fileContent.find(oldValue);
  while(pos!=std::string::npos) {
    fileContent.replace(pos, oldValue.size(), newValue, 0, newValue.size());
    pos = fileContent.find(oldValue, pos+newValue.size());
  }
  putOnFile(path, fileContent);
}

/**
 * \brief To add a line on a file
 * \param path  the file path
 * \param content the line which will be added
 * \param the old file content
 */
void
addLineToFile(const std::string& path,
              const std::string& line,
              std::string& oldfileContent) {

  std::ifstream ifs (path.c_str());
  if (ifs.is_open()) {
    std::ostringstream oss;
    oss << ifs.rdbuf();
    oldfileContent = oss.str();
    ifs.close();
  }
  putOnFile(path, line);
}
#endif