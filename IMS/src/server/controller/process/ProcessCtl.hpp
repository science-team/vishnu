/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/


/**
 * \file ProcessCtl.hpp
 * \brief This file defines the class to control the processes
 * \author Kevin Coulomb (kevin.coulomb@sysfera.com)
 * \date 10/05/11
 */

#ifndef __PROCESSCTL__HH__
#define __PROCESSCTL__HH__

#include "data/ProcessServer.hpp"


/**
 * \class ProcessCtl
 * \brief Class dealing wwith the control of the processes
 */
class ProcessCtl{
public:
  /**
   * \brief Constructor
   */
  ProcessCtl(string mid, UserServer user);
  /**
   * \brief Destructor
   */
  ~ProcessCtl();
  /**
   * \brief Return true if the process with a diet name Pname is of type IMS
   * \param Pname: DIET name of the process
   * \return True if Pname represents an IMS SeD
   */
  bool
  isIMSSeD(string Pname);
  /**
   * \brief To relaunch a process
   * \param op: Options for restarting
   * \param machineTo: the host of the process
   * \param isAPI: If called with the external API or not
   */
  void
  restart(IMS_Data::RestartOp_ptr op, string machineTo, bool isAPI = true);
  /**
   * \brief To stop a process
   */
  void
  stop(IMS_Data::Process_ptr p);
  /**
   * To perform load schedding operations
   * \param type: The type of loadShedding
   */
  void
  loadShed(int type);
protected:
private:
  /**
   * \brief To stop all the vishnu processes on a machine
   */
  void
  stopAll();
  /**
   * \brief The process server
   */
  ProcessServer mp;
  /**
   * \brief The option to restart
   */
  IMS_Data::RestartOp mop;
  /**
   * \brief The machine id
   */
  string mmid;
  /**
   * \brief A user server
   */
  UserServer muser;
};

#endif

