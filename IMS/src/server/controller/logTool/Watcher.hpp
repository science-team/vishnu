/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file Watcher.hpp
 * \brief This file defines the class that creates a tool and use it
 * \author Kevin Coulomb (kevin.coulomb@sysfera.com)
 * \date 18/04/11
 */

#ifndef __WATCHER__HH__
#define __WATCHER__HH__


#include "LogTool.hpp"


/**
 * \class Watcher
 * \brief Class that contain the main function to run the log tool
 */
class Watcher{
public:
  /**
   * \brief Constructor
   * \param toolType: The type of the tool used by the watcher
   * \param argc: Argc from main
   * \param argv: Argv from main
   * \param mid: The machine Id the watcher is running
   */
  Watcher(int toolType, int argc, char** argv, string mid);
  /**
   * \brief Destructor
   */
  ~Watcher();
  /**
   * \brief To run the function
   */
  void 
  run();
protected:
private:
  /**
   * \brief The log tool to use
   */
  LogTool* mtool;
};


#endif


