/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file LogTool.hpp
 * \brief This file defines the generic log tool class
 * \author Kevin Coulomb (kevin.coulomb@sysfera.com)
 * \date 18/04/11
 */

#ifndef __LOGTOOL__HH__
#define __LOGTOOL__HH__

#include "LogORBMgr.hh"

using namespace std;

/**
 * \class LogTool
 * \brief Root class to make a log tool in VISHNU
 */
class LogTool:  public POA_ToolMsgReceiver,
		public PortableServer::RefCountServantBase{
public:
  /**
   * \brief Constructor
   * \param mid: The machine id
   */
  LogTool(string mid);
  /**
   * \brief Destructor
   */
  ~LogTool();
  /**
   * \brief To run the program
   */
  virtual void run() = 0;
  /**
   * \brief To disconnect the tool
   * \return The disconnexion value
   */
  virtual int disconnect() = 0;
  /**
   * \brief Function called when a message is received
   * \param msg: The message to send
   */
  virtual void sendMsg(const log_msg_buf_t& msg) = 0;
  /**
   * \brief To set the filter to use
   * \param description_file: The path to the description file
   */
  virtual void setFilter(string description_file) = 0;
  /**
   * \brief To set the name of the output file
   * \param name: The name of the output file
   */
  virtual void setFilename(string name) = 0;
protected:
  /**
   * \brief The log central tool
   */
  LogCentralTool_ptr mLCT;
  /**
   * \brief The name of the output log file
   */
  string mfilename;
  /**
   * \brief The filter
   */
  filter_t mfilter;
  /**
   * \brief The name of the component
   */
  string mname;
  /**
   * \brief The machineid
   */
  string mmid;
private:
  
};

#endif

