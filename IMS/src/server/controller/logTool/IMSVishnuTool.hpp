/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file IMSVishnuTool.hpp
 * \brief This file defines the IMS log tool implementation
 * \author Kevin Coulomb (kevin.coulomb@sysfera.com)
 * \date 18/04/11
 */

#ifndef __IMSVISHNUTOOL__HH__
#define __IMSVISHNUTOOL__HH__

#include "LogTool.hpp"
#include "data/ProcessServer.hpp"

/**
 * \class IMSVishnuTool
 * \brief Class that implements the log tool for the IMS SeD in VISHNU
 */
class IMSVishnuTool: public LogTool{
public:
  /**
   * \brief Constructor
   * \param argc: The argc of the main function
   * \param argv: The argv of the main function
   * \param mid: The machine id
   */
  IMSVishnuTool(int argc, char** argv, string mid);
  /**
   * \brief Destructor
   */
  ~IMSVishnuTool();
  /**
   * \brief To run the program
   */
  void 
  run();
  /**
   * \brief To disconnect the tool
   * \return The disconnexion value
   */
  int 
  disconnect();
  /**
   * \brief Function called when a message is received
   * \param msg: The message to send
   */
  void 
  sendMsg(const log_msg_buf_t& msg);
  /**
   * \brief To set the filter to use
   * \param description_file: The path to the description file
   */
  void 
  setFilter(string description_file);
  /**
   * \brief To set the name of the output file
   * \param name: The name of the file filter
   */
  void 
  setFilename(string name);
  /**
   * \brief Return true if the imssed is the last actif imssed
   * \return True if the process is elected, false otherwise
   */
  bool
  elect();
protected:
private:
  /**
   * \brief Extract the machine if from log message (out message)
   * \param msg: The message received from the log
   * \return The hostname of the machine
   */
  string
  getMidFromOutLog(string msg);
  /**
   * \brief The process server
   */
  ProcessServer mproc;
};


#endif
