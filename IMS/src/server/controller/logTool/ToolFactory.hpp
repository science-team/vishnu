/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file ToolFactory.hpp
 * \brief This file defines the factory to get log tool
 * \author Kevin Coulomb (kevin.coulomb@sysfera.com)
 * \date 18/04/11
 */

#ifndef __TOOLFACTORY__HH__
#define __TOOLFACTORY__HH__

#include "LogTool.hpp"

/**
 * \brief Type of tool to create
 */
typedef enum toolType{
  IMSVishnuTool_v1
}toolType;

/**
 * \class ToolFactory
 * \brief Factory to generate tools
 */
class ToolFactory{
public:
  /**
   * \brief To get the singleton instance
   * \return The only instance of the factory
   */
  static ToolFactory* getInstance();
  /**
   * \brief To get a tool of type 'type'
   * \param type: The type of the tool to create
   * \param argc: Argc from main
   * \param argv: Argv from main
   * \param mid: The machine id the log tool will run
   * \return The created tool
   */
  LogTool*
  getTool(int type, int argc,  char** argv, string mid);
  /**
   * \brief Destructor
   */
  ~ToolFactory();
protected:
private:
  /**
   * \brief Constructor
   */
  ToolFactory();
  /**
   * \brief The singleton instance
   */
  static ToolFactory* mfact;
};


#endif
