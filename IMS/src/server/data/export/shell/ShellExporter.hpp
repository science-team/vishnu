/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file ShellExporter.hpp
 * \brief This file defines the processes interaction with the database
 * \author Kevin Coulomb (kevin.coulomb@sysfera.com)
 * \date 19/04/11
 */

#ifndef __SHELLEXPORTER__HH__
#define __SHELLEXPORTER__HH__

#include <ecore.hpp> // Ecore metamodel
#include <ecorecpp.hpp> // EMF4CPP utils
#include "IMS_Data.hpp"

#include "data/ExportServer.hpp"

static const std::string UMSSHELLMAPPERNAME = "UMSSHELL";
static const std::string TMSSHELLMAPPERNAME = "TMSSHELL";
static const std::string IMSSHELLMAPPERNAME = "IMSSHELL";
static const std::string FMSSHELLMAPPERNAME = "FMSSHELL";


/**
 * \class ShellExporter
 * \brief Class to export in shell format
 */
class ShellExporter: public ExportServer{
public:
  /**
   * \brief Constructor
   * \param u: The user server
   * \param op: Options to restart
   */
  ShellExporter(UserServer u, IMS_Data::ExportOp_ptr op);
  /**
   * \brief Destructor
   */
  ~ShellExporter();
  /**
   * \brief To export the commands made in the oldSession in the file filename with the options op in the shell format
   * \param oldSession: Session id of the old session to export
   * \param content: The content of the export (OUT)
   * \return Succes, an error code otherwise
   */
  int
  exporte(string oldSession, string &content);
protected:
private:
  /**
   * \brief Return true if the session with the session sid sid is closed
   * \param sid: A session id
   * \return False is the session does not exist or is open
   */
  bool
  isClosed(string sid);
  /**
   * \brief Check if the user is allowed to get the list of commands
   */
  bool
  isAllowed(string oldSession, UserServer muser);
  /**
   * \brief To get the name of the mapper for the shell
   */
  string
  getMapperName(int type);

};


#endif

