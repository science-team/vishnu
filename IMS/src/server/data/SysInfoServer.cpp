/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

#include "SysInfoServer.hpp"
#include "DbFactory.hpp"
#include "IMSVishnuException.hpp"

SysInfoServer::SysInfoServer(const UserServer session):msession(session) {
  DbFactory factory;
  mdatabase = factory.getDatabaseInstance();
  mvishnuId = 1;
}

SysInfoServer::SysInfoServer(const UserServer session, IMS_Data::SysInfoOp op):msession(session)  {
  DbFactory factory;
  mdatabase = factory.getDatabaseInstance();
  mop = op;
  mvishnuId = 1;
}

SysInfoServer::~SysInfoServer() {
}

IMS_Data::ListSysInfo_ptr
SysInfoServer::getSysInfo() {
  string req = "SELECT * from machine WHERE vishnu_vishnuid='"+convertToString(mvishnuId)+"'";
  vector<string> results = vector<string>();
  vector<string>::iterator iter;

  if(mop.getMachineId().compare("")) {
    string reqnmid = "SELECT * from machine where  machineid ='"+mop.getMachineId()+"'";
    boost::scoped_ptr<DatabaseResult> result(mdatabase->getResult(reqnmid.c_str()));
    if(result->getNbTuples() == 0) {
      throw IMSVishnuException(ERRCODE_INVPROCESS, "Unknown machine id");
    }
    req += " AND  machineid ='"+mop.getMachineId()+"'";
  }
  IMS_Data::IMS_DataFactory_ptr ecoreFactory = IMS_Data::IMS_DataFactory::_instance();
  IMS_Data::ListSysInfo_ptr mlistObject = ecoreFactory->createListSysInfo();

  try {
    boost::scoped_ptr<DatabaseResult> result(mdatabase->getResult(req.c_str()));
      for (size_t i = 0; i < result->getNbTuples(); i++){
	results.clear();
	results = result->get(i);
	iter = results.begin();
	IMS_Data::SystemInfo_ptr sys = ecoreFactory->createSystemInfo();
        if ((*(iter+4)).empty()) {
          sys->setDiskSpace(-1);
        } else {
          sys->setDiskSpace(convertToInt(*(iter+4)));
        }
        if ((*(iter+5)).empty()) {
          sys->setMemory(-1);
        } else {
          sys->setMemory(convertToInt(*(iter+5)));
        }
	sys->setMachineId(*(iter+7));
	mlistObject->getSysInfo().push_back(sys);
      }
  } catch (SystemException& e) {
    throw (e);
  }
  return mlistObject;
}

void
SysInfoServer::setSysInfo(IMS_Data::SystemInfo_ptr sys) {
  bool added = false;
  if (!msession.isAdmin()){
    throw UMSVishnuException(ERRCODE_NO_ADMIN, "set system info is an admin function. A user cannot call it");
  }
  // No update needed
  if (sys->getMemory()==0 && sys->getDiskSpace()==0) {
    return;
  }
  if (sys->getMachineId().compare("")==0) {
    throw UserException(ERRCODE_INVALID_PARAM, "Error missing the machine id. ");
  }
  string reqnmid = "SELECT * from machine where  machineid ='"+sys->getMachineId()+"'";
  boost::scoped_ptr<DatabaseResult> result(mdatabase->getResult(reqnmid.c_str()));
  if(result->getNbTuples() == 0) {
    throw IMSVishnuException(ERRCODE_INVPROCESS, "Unknown machine id");
  }

  string request = "update machine set ";
  if (sys->getDiskSpace() < 0 || sys->getMemory() < 0) {
    throw UserException(ERRCODE_INVALID_PARAM, "Invalid negative value");
  }
  if (sys->getDiskSpace()>0) {
    request += "  diskspace ="+convertToString(sys->getDiskSpace());
    added = true;
  }
  if (sys->getMemory()>0) {
    if (added) {
      request += ",";
    }
    request += "  memory ="+convertToString(sys->getMemory());
  }
  request += " where  machineid ='"+sys->getMachineId()+"'";

  request += " AND vishnu_vishnuid=";
  request += convertToString(mvishnuId);

  try{
    mdatabase->process(request.c_str());
  }catch(SystemException& e){
    e.appendMsgComp(" Error inserting new system states ");
    throw(e);
  }
}

