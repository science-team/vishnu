/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file MetricServer.hpp
 * \brief This file defines the metrics interaction with the database
 * \author Kevin Coulomb (kevin.coulomb@sysfera.com)
 * \date 19/04/11
 */

#ifndef __METRICSERVER__HH__
#define __METRICSERVER__HH__

#include <ecore.hpp> // Ecore metamodel
#include <ecorecpp.hpp> // EMF4CPP utils
#include "IMS_Data.hpp"
#include "SessionServer.hpp"

using namespace std;

/**
 * \class MetricServer
 * \brief Server that link the metrics and the database
 */
class MetricServer{
public:
  /**
   * \brief Constructor
   * \param session: A session to use for the call
   * \param mail: The script to send mails
   */
  MetricServer(const UserServer session, string mail);
  /**
   * \brief Constructor
   * \param session: A session to use for the call
   * \param op: Options for the get metric history call
   * \param mail: The script to send mails
   */
  MetricServer(const UserServer session, IMS_Data::MetricHistOp_ptr op, string mail);
  /**
   * \brief Constructor
   * \param session: A session to use for the call
   * \param op: Options for the get current metric value call
   * \param mail: The script to send mails
   */
  MetricServer(const UserServer session, IMS_Data::CurMetricOp_ptr op, string mail);
  /**
   * \brief Destructor
   */
  ~MetricServer();
  /**
   * \brief To set the update frequency value
   * \param freq: The new frequency value
   */
  void
  setUpFreq(int freq);
  /**
   * \brief Update the mfreq member with the value from the database
   * \return The new mfreq value
   */
  unsigned int
  checkUpFreq();
  /**
   * \brief Return the current metric values, gotten directly with CoRI
   * \return The current list of metrics
   */
  IMS_Data::ListMetric*
  getCurMet();
  /**
   * \brief Return the old values of the metrics, gotten from the database
   * \return The old list of metrics
   * \param machineId: The vishnu id of the machine
   */
  IMS_Data::ListMetric*
  getHistMet(string machineId);

  /**
   * \brief To add all the monitored types of metric in the database at a time t
   * \param set: A list containing the monitored
   * \param mid: The machine id of the machine to add the set
   */
  void
  addMetricSet(IMS_Data::ListMetric* set, string mid);


protected:
private:
  /**
   * \brief To send a mail 
   * \param val: The value of the metric
   * \param threshold: The value of the threshold
   * \param type: The type of the threshold
   * \param email: The email adress
   * \param uid: The user id of the user target
   * \param mid: The machine id of the machine where the threshold is reached
   */
  void
  sendMail(int val, int threshold, int type, string email, string uid, string mid);
  /**
   * \brief The position of the frequency in the vishnu table
   */
  static const int FREQPOS = 1;
  /**
   * \brief The id of vishnu
   */
  int mvishnuId;
  /**
   * \brief The update frequency
   */
  unsigned int mfreq;
  /**
   * \brief The session
   */
  UserServer msession;
  /**
  * \brief An instance of vishnu database
  */
  Database *mdatabase;
  /**
   * \brief The name of the command
   */
  string mcommandName;
  /**
   * \brief The process to send mails
   */
  string msendmail;
  /**
   * \brief The options for the history of metrics
   */
  IMS_Data::MetricHistOp_ptr mhop;
  /**
   * \brief The options for the current metrics
   */
  IMS_Data::CurMetricOp_ptr mcop;
};

#endif
