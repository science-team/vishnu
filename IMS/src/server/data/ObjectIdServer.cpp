/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

#include "ObjectIdServer.hpp"
#include "DbFactory.hpp"

ObjectIdServer::ObjectIdServer(const UserServer session):msession(session) {
  DbFactory factory;
  mdatabase = factory.getDatabaseInstance();
  mvishnuId  = 1;
}

ObjectIdServer::~ObjectIdServer() {
}

void
ObjectIdServer::setUID(string fmt) {
  if (!msession.isAdmin()){
    throw UMSVishnuException(ERRCODE_NO_ADMIN, "define user id  format is an admin function. A user cannot call it");
  }
  if (!containCpt(fmt)) {
    throw UserException(10, "Invalid format, it does not contain any counter. ");
  }
  if (fmt.find_first_of('@')!=string::npos) {
    throw UserException(10, "Invalid format, it cannot contain the @ character. ");
  }
  string request = "update  vishnu set  formatiduser ='"+fmt+"' where  vishnuid ='";
  request += convertToString(mvishnuId);
  request += "'";
  try{
    mdatabase->process(request.c_str());
  }catch(SystemException& e){
    e.appendMsgComp("Failed to set the user format to "+fmt);
    throw(e);
  }
}

void
ObjectIdServer::setFID(string fmt) {
  if (!msession.isAdmin()){
    throw UMSVishnuException(ERRCODE_NO_ADMIN, "define file transfer id  format is an admin function. A user cannot call it");
  }
  if (!containCpt(fmt)) {
    throw UserException(10, "Invalid format, it does not contain any counter. ");
  }
  if (fmt.find_first_of('@')!=string::npos) {
    throw UserException(10, "Invalid format, it cannot contain the @ character. ");
  }
  string request = "update  vishnu set  formatidfiletransfer ='"+fmt+"' where  vishnuid ='";
  request += convertToString(mvishnuId);
  request += "'";
  try{
    mdatabase->process(request.c_str());
  }catch(SystemException& e){
    e.appendMsgComp("Failed to set the file format to "+fmt);
    throw(e);
  }
}

void
ObjectIdServer::setTID(string fmt) {
  if (!msession.isAdmin()){
    throw UMSVishnuException(ERRCODE_NO_ADMIN, "define task id  format is an admin function. A user cannot call it");
  }
  if (!containCpt(fmt)) {
    throw UserException(10, "Invalid format, it does not contain any counter. ");
  }
  if (fmt.find_first_of('@')!=string::npos) {
    throw UserException(10, "Invalid format, it cannot contain the @ character. ");
  }
  string request = "update  vishnu set  formatidjob ='"+fmt+"' where  vishnuid ='";
  request += convertToString(mvishnuId);
  request += "'";
  try{
    mdatabase->process(request.c_str());
  }catch(SystemException& e){
    e.appendMsgComp("Failed to set the task format to "+fmt);
    throw(e);
  }
}

void
ObjectIdServer::setMID(string fmt) {
  if (!msession.isAdmin()){
    throw UMSVishnuException(ERRCODE_NO_ADMIN, "define machine id  format is an admin function. A user cannot call it");
  }
  if (!containCpt(fmt)) {
    throw UserException(10, "Invalid format, it does not contain any counter. ");
  }
  if (fmt.find_first_of('@')!=string::npos) {
    throw UserException(10, "Invalid format, it cannot contain the @ character. ");
  }
  string request = "update  vishnu set  formatidmachine ='"+fmt+"' where  vishnuid='";
  request += convertToString(mvishnuId);
  request += "'";
  try{
    mdatabase->process(request.c_str());
  }catch(SystemException& e){
    e.appendMsgComp("Failed to set the machine format to "+fmt);
    throw(e);
  }
}

void
ObjectIdServer::setAID(string fmt) {
  if (!msession.isAdmin()){
    throw UMSVishnuException(ERRCODE_NO_ADMIN, "define authentication id  format is an admin function. A user cannot call it");
  }
  if (!containCpt(fmt)) {
    throw UserException(10, "Invalid format, it does not contain any counter. ");
  }
  if (fmt.find_first_of('@')!=string::npos) {
    throw UserException(10, "Invalid format, it cannot contain the @ character. ");
  }
  string request = "update  vishnu set  formatidauth ='"+fmt+"' where  vishnuid='";
  request += convertToString(mvishnuId);
  request += "'";
  try{
    mdatabase->process(request.c_str());
  }catch(SystemException& e){
    e.appendMsgComp("Failed to set the machine format to "+fmt);
    throw(e);
  }
}


bool
ObjectIdServer::containCpt(string fmt) {
  return (fmt.find("$CPT")!=string::npos);
}

