/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file ObjectIdServer.hpp
 * \brief This file defines the object id format server
 * \author Kevin Coulomb (kevin.coulomb@sysfera.com)
 * \date 19/04/11
 */

#ifndef __OBJECTIDSERVER__HH__
#define __OBJECTIDSERVER__HH__

#include <ecore.hpp> // Ecore metamodel
#include <ecorecpp.hpp> // EMF4CPP utils
#include "IMS_Data.hpp"
#include "SessionServer.hpp"
#include "Database.hpp"

using namespace std;

/**
 * \class ObjectIdServer
 * \brief Class that implements the object id format server
 */
class ObjectIdServer{
public:
  /**
   * \brief Constructor
   */
  ObjectIdServer(const UserServer session);
  /**
   * \brief Destructor
   */
  ~ObjectIdServer();
  /**
   * \brief To set the format of a user
   */
  void
  setUID(string fmt);
  /**
   * \brief To set the format of a file transfer
   */
  void
  setFID(string fmt);
  /**
   * \brief To set the format of a task
   */
  void
  setTID(string fmt);
  /**
   * \brief To set the format of a machine
   */
  void
  setMID(string fmt);
  /**
   * \brief To set the format of an authentication system id
   */
  void
  setAID(string fmt);
protected:
private:
  /**
   * \brief Test if fmt contains '$CPT' as a substring
   * \param fmt: The format string to test
   * \return true if the string fmt contains the '$cpt' substring
   */
  bool
  containCpt(string fmt);
  /**
   * \brief The id of vishnu
   */
  int mvishnuId;
  /**
  * \brief An instance of vishnu database
  */
  Database *mdatabase;
  /**
   * \brief The session
   */
  UserServer msession;
};

#endif // OBJECTIDSERVER
