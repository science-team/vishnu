/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file ExportFactory.hpp
 * \brief This file defines the export factory
 * \author Kevin Coulomb (kevin.coulomb@sysfera.com)
 * \date 23/05/10
 */

#ifndef _EXPORTFACTORY_H_
#define _EXPORTFACTORY_H_

#include "data/ExportServer.hpp"
#include "UserServer.hpp"
#include <ecore.hpp> // Ecore metamodel
#include <ecorecpp.hpp> // EMF4CPP utils
#include "IMS_Data.hpp"


/**
 * \class ExportFactory
 * \brief Export factory
 */
class ExportFactory{
public:
  /**
   * \brief Constructor
   */
  ExportFactory();
  /**
   * \brief Destructor
   */
  ~ExportFactory();
  /**
   * \brief To get an export server
   */
  static ExportServer*
  getExporter(UserServer u, IMS_Data::ExportOp_ptr expOp);

protected:
private:

};

#endif
