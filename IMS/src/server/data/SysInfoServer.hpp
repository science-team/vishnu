/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file SysInfoServer.hpp
 * \brief This file defines the metrics interaction with the database
 * \author Kevin Coulomb (kevin.coulomb@sysfera.com)
 * \date 19/04/11
 */

#ifndef __SYSINFOSERVER__HH__
#define __SYSINFOSERVER__HH__

#include <ecore.hpp> // Ecore metamodel
#include <ecorecpp.hpp> // EMF4CPP utils
#include "IMS_Data.hpp"
#include "SessionServer.hpp"

using namespace std;

/**
 * \class SysInfoServer
 * \brief Class that links the database and the system information about the machines
 */
class SysInfoServer{
public:
  /**
   * \brief Constructor
   * \param session: A session to use for the call
   */
  SysInfoServer(const UserServer session);
  /**
   * \brief Constructor
   * \param session: A session to use for the call
   * \param op: The option of the sysinfo
   */
  SysInfoServer(const UserServer session, IMS_Data::SysInfoOp op);
  /**
   * \brief Destructor
   */
  ~SysInfoServer();
  /**
   * \brief To get system information
   * \return A list of system info
   */
  IMS_Data::ListSysInfo_ptr
  getSysInfo();
  /**
   * \brief To set system information
   */
  void
  setSysInfo(IMS_Data::SystemInfo_ptr sys);
protected:
private:
  /**
   * \brief The id of vishnu
   */
  int mvishnuId;
  /**
   * \brief The session
   */
  UserServer msession;
  /**
  * \brief An instance of vishnu database
  */
  Database *mdatabase;
  /**
   * \brief Option for getting sysinfo
   */
  IMS_Data::SysInfoOp mop;

}; // end sysinfoserver

#endif
