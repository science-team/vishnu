/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

#include "ObjectIdProxy.hpp"
#include "UserException.hpp"
#include "utilClient.hpp"
#include "utilVishnu.hpp"
using namespace vishnu;

ObjectIdProxy::ObjectIdProxy(const SessionProxy& session): msessionProxy(session) {
}

ObjectIdProxy::~ObjectIdProxy() {
}

void
ObjectIdProxy::setUID(string fmt) {
  try {
    defineCall("int_defineUserIdentifier", fmt);
  } catch (UserException& e) {
    e.appendMsgComp("Bad user format");
    throw (e);
  }
}

void
ObjectIdProxy::setMID(string fmt) {
  try {
    defineCall("int_defineMachineIdentifier", fmt);
  } catch (UserException& e) {
    e.appendMsgComp("Bad machine format");
    throw (e);
  }
}

void
ObjectIdProxy::setTID(string fmt) {
  try {
    defineCall ("int_defineJobIdentifier", fmt);
  } catch (UserException& e) {
    e.appendMsgComp("Bad task format");
    throw (e);
  }
}

void
ObjectIdProxy::setFID(string fmt) {
  try {
    defineCall ("int_defineTransferIdentifier", fmt);
  } catch (UserException& e) {
    e.appendMsgComp("Bad file transfert format");
    throw (e);
  }
}

void
ObjectIdProxy::setAID(string fmt) {
  try {
    defineCall ("int_defineAuthIdentifier", fmt);
  } catch (UserException& e) {
    e.appendMsgComp("Bad authentication format");
    throw (e);
  }
}

void
ObjectIdProxy::defineCall(string name, string fmt) {
  diet_profile_t* profile = NULL;
  string sessionKey;
  char* errorInfo = NULL;
  string serviceName = name;
  string msgErrorDiet;

  profile = diet_profile_alloc(serviceName.c_str(), 1, 1, 2);
  sessionKey = msessionProxy.getSessionKey();
  //IN Parameters
  if (diet_string_set(diet_parameter(profile,0), strdup(sessionKey.c_str()), DIET_VOLATILE)) {
    msgErrorDiet += "with sessionKey parameter "+sessionKey;
    raiseDietMsgException(msgErrorDiet);
  }
  if (diet_string_set(diet_parameter(profile,1), strdup(fmt.c_str()), DIET_VOLATILE)) {
    msgErrorDiet += "with format parameter "+fmt;
    raiseDietMsgException(msgErrorDiet);
  }

  //OUT Parameters
  diet_string_set(diet_parameter(profile,2), NULL, DIET_VOLATILE);
  if(!diet_call(profile)) {
    if(diet_string_get(diet_parameter(profile,2), &errorInfo, NULL)){
      msgErrorDiet += " by receiving User serialized  message";
      raiseDietMsgException(msgErrorDiet);
    }
  }
  else {
    raiseDietMsgException("DIET call failure");
  }
  /*To raise a vishnu exception if the receiving message is not empty*/
  raiseExceptionIfNotEmptyMsg(errorInfo);
}
