/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/


#include <iostream>
#include <string>
#include "ThresholdProxy.hpp"
#include "utilClient.hpp"
#include "utilVishnu.hpp"
#include "QueryProxy.hpp"

using namespace vishnu;

/**
* \param session The object which encapsulates the session information
* \brief Constructor
*/
ThresholdProxy::ThresholdProxy(const SessionProxy& session): msessionProxy(session) {
}


/**
* \brief Function to set a system threshold
* \param systemThreshold the system threshold
* \return raises an exception on error
*/
void
ThresholdProxy::setSystemThreshold(IMS_Data::Threshold systemThreshold) {

  diet_profile_t* profile = NULL;
  std::string sessionKey;
  char* errorInfo = NULL;
  std::string objectToString;

  std::string serviceName = "int_setSystemThreshold";

  profile = diet_profile_alloc(serviceName.c_str(), 1, 1, 2);
  sessionKey = msessionProxy.getSessionKey();

  std::string msgErrorDiet = "call of function diet_string_set is rejected ";
  //IN Parameters
  if (diet_string_set(diet_parameter(profile,0), strdup(sessionKey.c_str()), DIET_VOLATILE)) {
    msgErrorDiet += "with sessionKey parameter "+sessionKey;
    raiseDietMsgException(msgErrorDiet);
  }

  ::ecorecpp::serializer::serializer _ser;
  //To serialize the options object in to optionsInString
  objectToString =  strdup(_ser.serialize_str(const_cast<IMS_Data::Threshold_ptr>(&systemThreshold)).c_str());

  if (diet_string_set(diet_parameter(profile,1), strdup(objectToString.c_str()),  DIET_VOLATILE)) {
    msgErrorDiet += "with SystemInfo parameter ";
    raiseDietMsgException(msgErrorDiet);
  }

  //OUT Parameters
  diet_string_set(diet_parameter(profile,2), NULL, DIET_VOLATILE);

  if(!diet_call(profile)) {
    if(diet_string_get(diet_parameter(profile,2), &errorInfo, NULL)){
      msgErrorDiet += " by receiving errorInfo message";
      raiseDietMsgException(msgErrorDiet);
    }
  }
  else {
    raiseDietMsgException("DIET call failure");
  }

  /*To raise a vishnu exception if the receiving message is not empty*/
  raiseExceptionIfNotEmptyMsg(errorInfo);

  diet_profile_free(profile);
}

/**
* \brief Function to get a system threshold
* \param options the options data structure to get system threshold
* \param listSysThreshold the list of system threshold returned
* \return raises an exception on error
*/
int
ThresholdProxy::getSystemThreshold(IMS_Data::ListThreshold& listSysThreshold,
                   const IMS_Data::ThresholdOp& options) {

  std::string name = "int_getSystemThreshold";
  QueryProxy<IMS_Data::ThresholdOp, IMS_Data::ListThreshold>
  query(options, msessionProxy, name);

  IMS_Data::ListThreshold_ptr li = query.list();

  if(li != NULL) {
    IMS_Data::IMS_DataFactory_ptr ecoreFactory = IMS_Data::IMS_DataFactory::_instance();
    for(unsigned int i = 0; i < li->getThreshold().size(); i++) {
      IMS_Data::Threshold_ptr threshold = ecoreFactory->createThreshold();
       //To copy the content and not the pointer
      *threshold = *li->getThreshold().get(i);
      listSysThreshold.getThreshold().push_back(threshold);
    }
    delete li;
  }
  return 0;
}

/**
* \brief Destructor
*/
ThresholdProxy::~ThresholdProxy() {

}
