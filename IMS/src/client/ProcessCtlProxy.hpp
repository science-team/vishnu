/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
  * \file ProcessCtlProxy.hpp
  * \brief This file contains the VISHNU ProcessCtlProxy class.
  * \author Eugène PAMBA CAPO-CHICHI (eugene.capochichi@sysfera.com)
  * \date May 2011
  */

#ifndef _PROCESSCTL_PROXY_H
#define _PROCESSCTL_PROXY_H

#include "IMS_Data.hpp"
#include "SessionProxy.hpp"
#include "MachineProxy.hpp"

/**
 * \class ProcessCtlProxy
 * \brief ProcessCtlProxy class implementation
 */
class ProcessCtlProxy {

  public:

   /**
  * \param session The object which encapsulates the session information
  * \brief Constructor
  */
  explicit ProcessCtlProxy( const SessionProxy& session);

  /**
  * \param session The object which encapsulates the session information
  * \param machineId The id of the machine
  * \brief Constructor
  */
  explicit ProcessCtlProxy( const SessionProxy& session,
                            const std::string& machineId);

  /**
  * \brief Function to to restart a SeD or a MA
  * \param options the options for the restart
  * \return raises an exception on error
  */
  int
  restart(const IMS_Data::RestartOp& options);

  /**
  * \brief Function to get the refresh period
  * \param process The data structure containing information about the process to stop
  * \return raises an exception on error
  */
  int
  stop(IMS_Data::Process process);

  /**
  * \brief Function to get the refresh period
  * \param loadShedType the type of the load shedding mode (SOFT or HARD)
  * \return raises an exception on error
  */
  int
  loadShed(IMS_Data::LoadShedType loadShedType);

  /**
    * \brief Destructor, raises an exception on error
    */
  ~ProcessCtlProxy();

  private:
  /**
   * \brief Call the FMS sed to cancel all
   */
  void
  cancelFMS();
  /**
   * \brief Call the TMS sed to cancel all
   */
  void
  cancelTMS();

  /////////////////////////////////
  // Attributes
  /////////////////////////////////

  /**
  * \brief The object to manipulate the session data
  */
  SessionProxy msessionProxy;
  /**
  * \brief The id of the machine
  */
  std::string mmachineId;

};
#endif //_PROCESSCTL_PROXY_H
