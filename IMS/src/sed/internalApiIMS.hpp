/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file internalApiIMS.hpp
 * \brief This file presents the internal api of UMS
 * \author Eugène PAMBA CAPO-CHICHI (eugene.capochichi@sysfera.com)
 * \date 31/01/2001
*/

#ifndef __INTERNALAPIIMS__HH__
#define __INTERNALAPIIMS__HH__

#include "DIET_server.h"


/**
* \brief Function to solve the service export
* \fn    int solveExport(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveExport(diet_profile_t* pb);
/**
* \brief Function to solve the service to get hte current metric
* \fn    int solveCurMetric(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveCurMetric(diet_profile_t* pb);
/**
* \brief Function to solve the service to get the metric history
* \fn    int solveOldMetric(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveOldMetric(diet_profile_t* pb);
/**
* \brief Function to solve the service to get the processes
* \fn    int solvePS(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solvePS(diet_profile_t* pb);
/**
* \brief Function to solve the service to set the system info
* \fn    int solveSetSysInfo(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveSetSysInfo(diet_profile_t* pb);
/**
* \brief Function to solve the service to set a threshold
* \fn    int solveSetThreshold(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveSetThreshold(diet_profile_t* pb);
/**
* \brief Function to solve the service get a threshold
* \fn    int solveGetThreshold(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveGetThreshold(diet_profile_t* pb);
/**
* \brief Function to solve the service to set the user id format
* \fn    int solveSetUID(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveSetUID(diet_profile_t* pb);
/**
* \brief Function to solve the service to set the job id format
* \fn    int solveSetJID(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveSetJID(diet_profile_t* pb);
/**
* \brief Function to solve the service to the the transfer id format
* \fn    int solveSetTID(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveSetTID(diet_profile_t* pb);
/**
* \brief Function to solve the service to set the machine id format
* \fn    int solveSetMID(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveSetMID(diet_profile_t* pb);
/**
* \brief Function to solve the service to set the machine id format
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveSetAID(diet_profile_t* pb);
/**
* \brief Function to solve the service to load shed
* \fn    int solveLoadShed(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveLoadShed(diet_profile_t* pb);
/**
* \brief Function to solve the service to set the update frequency
* \fn    int solveSetUpFreq(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveSetUpFreq(diet_profile_t* pb);
/**
* \brief Function to solve the service export
* \fn    int solveGetUpFreq(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveGetUpFreq(diet_profile_t* pb);
/**
* \brief Function to solve the service export
* \fn    int solveRestart(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveRestart(diet_profile_t* pb);
/**
* \brief Function to solve the service export
* \fn    int solveStop(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveStop(diet_profile_t* pb);
/**
* \brief Function to solve the service export
* \fn    int solveGetSysInfo(diet_profile_t* pb)
* \param pb is a structure which corresponds to the descriptor of a profile
* \return raises an exception on error
*/
int
solveGetSysInfo(diet_profile_t* pb);

#endif // Internal api IMS
