/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file ServerIMS.hpp
 * \brief This file presents the implementation of the IMS server.
 * \author Kevin COULOMB (kevin.coulomb@sysfera.com)
 * \date 12/03/2011
*/

#ifndef _SERVERIMS_H_
#define _SERVERIMS_H_

#include <string>
#include "DbConfiguration.hpp"
#include "Database.hpp"
#include "UMSMapper.hpp"
#include "TMSMapper.hpp"
#include "FMSMapper.hpp"
#include "IMSMapper.hpp"

/**
 * \brief Number of service in IMS
 */
#define NB_SRV 18
#include "DIET_server.h"

static const char* SRV[NB_SRV] = {
  "int_exportCommands",
  "int_getMetricCurentValue",
  "int_getMetricHistory",
  "int_getProcesses",
  "int_setSystemInfo",
  "int_setSystemThreshold",
  "int_getSystemThreshold",
  "int_defineUserIdentifier",
  "int_defineJobIdentifier",
  "int_defineTransferIdentifier",
  "int_defineMachineIdentifier",
  "int_loadShed",
  "int_setUpdateFrequency",
  "int_getUpdateFrequency",
  "int_restart",
  "int_stop",
  "int_getSystemInfo",
  "int_defineAuthIdentifier"};
/**
 * \class ServerIMS
 * \brief This class describes the IMS server
 */
class ServerIMS {
public :

  /**
   * \brief To get the unique instance of the server
   */
  static ServerIMS*  getInstance();

  /**
  * \brief To get the vishnuId
  * \fn int getVishnuId()
  * \return the path of the configuration file
  */
  int
  getVishnuId();

  /**
  * \brief To get the path to the sendmail script
  * \fn std::string getSendmailScriptPath()
  * \return the path of the configuration file
  */
  std::string
  getSendmailScriptPath();

   /**
   * \brief To initialize the IMS server
   * \param vishnuId The password of the root user vishnu_user for the connection with the database
   * \param dbConfig  The configuration of the database
   * \param sendmailScriptPath The path to the script for sending emails
   * \param mid: The machine id
   * \return an error code (0 if success and 1 if an error occurs)
   */
  int
  init(int vishnuId, DbConfiguration dbConfig, string sendmailScriptPath, string mid);

  /**
   * \brief Destructor, raises an exception on error
   * \fn ~ServerIMS()
   */
  ~ServerIMS();

private :

  /**
   * \brief Constructor, private because class is singleton
   */
  ServerIMS();

  /////////////////////////////////
  // Attributes
  /////////////////////////////////
  /**
  * \brief The path to the sendmail script
  */
  std::string msendmailScriptPath;
  /**
   * \brief The singleton reference
   */
  static ServerIMS *minstance;
  /**
  * \brief The vishnu id
  */
  int mvishnuId;
  /**
  * \brief Structure representing a profile description
  */
  diet_profile_desc_t* mprofile;
  /**
  * \brief Instance of Database
  */
  static Database *mdatabaseVishnu;
  /**
  * \brief Instance of UMSMapper
  */
  static UMSMapper *mmapperUMS;
  /**
  * \brief Instance of UMSMapper
  */
  static TMSMapper *mmapperTMS;
  /**
  * \brief Instance of UMSMapper
  */
  static FMSMapper *mmapperFMS;
  /**
  * \brief Instance of UMSMapper
  */
  static IMSMapper *mmapperIMS;
};
#endif // SERVERIMS
