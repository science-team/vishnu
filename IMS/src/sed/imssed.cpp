/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

#include <csignal>
#include <sys/wait.h>
#include <fstream>
#include "ServerIMS.hpp"
#include "ExecConfiguration.hpp"
#include "DbConfiguration.hpp"
#include "utilServer.hpp"
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include "controller/logTool/Watcher.hpp"
#include "controller/logTool/ToolFactory.hpp"
#include "controller/HM/HM.hpp"

#include <boost/thread.hpp>

#include <sys/types.h>
#include <signal.h>

using namespace vishnu;
using namespace boost;
/**
 * \mainpage IMS
 *
 * <center><b>IMS is the module that deals with the management of the information in the vishnu system</b></center>
 *
 * \image html logoSysFera.jpg
 *
 * \authors CAPOCHICHI Eugene
 * \authors CISSE      Ibrahima
 * \authors COULOMB    Kevin
 * \authors ISNARD     Benjamin
 * \authors TRAORE     Daouda
 *
 * \file imssed.cpp
 * \brief The main function offering the set of services
 */

/**
 * \brief To show how to use the sed
 * \fn int usage(char* cmd)
 * \param cmd The name of the program
 * \return Always 1
 */
int
usage(char* cmd) {
  std::cout << "Usage: %s <vishnu_config.cfg> \n"+ std::string(cmd);
  return 1;
}

/**
 * \brief To catch a signal
 * \param signum is the signal to catch
 */
void
controlSignal (int signum) {
  int res;
  switch (signum) {
    case SIGCHLD:
      res = waitpid (-1, NULL, WNOHANG);
      while (res > 0) {
        res = waitpid (-1, NULL, WNOHANG);
      }
      break;
    default:
     break;
  }
}


/**
 * \brief The main function
 * \fn int main(int argc, char* argv[], char* envp[])
 * \param argc Number of parameter
 * \param argv List of argument
 * \param envp Array of environment variables
 * \return The result of the diet sed call
 */
int main(int argc, char* argv[], char* envp[]) {

  int res = 0;
  int vishnuId = 0;
  ExecConfiguration config;
  DbConfiguration dbConfig(config);
  string sendmailScriptPath;
  string dietConfigFile;
  string IMSTYPE = "IMS";
  struct sigaction action;
  string mid;
  string cfg;

  if (argc < 2) {
    return usage(argv[0]);
  }

  // Read the configuration
  try {
    config.initFromFile(argv[1]);
    config.getRequiredConfigValue<std::string>(vishnu::DIETCONFIGFILE, dietConfigFile);
    config.getRequiredConfigValue<int>(vishnu::VISHNUID, vishnuId);
    dbConfig.check();
    config.getRequiredConfigValue<std::string>(vishnu::SENDMAILSCRIPT, sendmailScriptPath);
    config.getRequiredConfigValue<std::string>(vishnu::MACHINEID, mid);
  } catch (UserException& e) {
    std::cerr << e.what() << std::endl;
    exit(1);
  }catch (std::exception& e) {
    std::cerr << argv[0] << " : "<< e.what() << std::endl;
    exit(1);
  }
  // Check DIET Configuration file
  if(!boost::filesystem::is_regular_file(sendmailScriptPath)) {
    std::cerr << "Error: cannot open DIET configuration file" << std::endl;
    exit(1);
  }

  // Initialize the IMS Server (Opens a connection to the database)
  ServerIMS* server = ServerIMS::getInstance();
  res = server->init(vishnuId, dbConfig, sendmailScriptPath, mid);

  registerSeD(IMSTYPE, config, cfg);

  // Watcher thread
  Watcher w(IMSVishnuTool_v1, argc, argv, mid);
  thread thr1(bind(&Watcher::run, &w));//%RELAX<MISRA_0_1_3> Because it used to launch a thread


  // History maker thread
  HM hm = HM(sendmailScriptPath, mid);
  thread thr2(bind(&HM::run, &hm));//%RELAX<MISRA_0_1_3> Because it used to launch a thread

  //Declaration of signal handler, to remove script children
   action.sa_handler = controlSignal;
   sigemptyset (&(action.sa_mask));
   action.sa_flags = 0;
   sigaction (SIGCHLD, &action, NULL);

  // Initialize the DIET SeD
  if (!res) {
    diet_print_service_table();
    res = diet_SeD(cfg.c_str(), argc, argv);
    unregisterSeD(IMSTYPE, mid);
    pid_t pid = getpid();
    kill(pid, SIGINT);
  } else {
    std::cerr << "There was a problem during services initialization" << std::endl;
    exit(1);
  }
  // To avoid quitting to fast in case of problems
  while(1){
    sleep(1000);
  }
  return res;
}
