/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file get_processes.cpp
 * This file defines the VISHNU command to get the processes
 * \author Coulomb Kevin (kevin.coulomb@sysfera.com)
 */


#include "CLICmd.hpp"
#include "utilVishnu.hpp"
#include "cliError.hpp"
#include "cliUtil.hpp"
#include "api_ums.hpp"
#include "api_ims.hpp"
#include "sessionUtils.hpp"
#include "displayer.hpp"
#include <boost/bind.hpp>

#include "GenericCli.hpp"

namespace po = boost::program_options;

using namespace std;
using namespace vishnu;

struct GetProcessesFunc {

  IMS_Data::ProcessOp mop; 

  GetProcessesFunc(const IMS_Data::ProcessOp& op):
   mop(op)
  {};

  int operator()(std::string sessionKey) {
    IMS_Data::ListProcesses processes;
    int res = getProcesses(sessionKey, processes, mop);
    displayListProc(&processes);
    return res;
  }
};

boost::shared_ptr<Options>
makeGProcOp(string pgName, 
	     boost::function1<void, string>& fmid,
	     string& dietConfig){
  boost::shared_ptr<Options> opt(new Options(pgName));

  // Environement option
  opt->add("dietConfig,c",
           "The diet config file",
           ENV,
           dietConfig);

  // All cli options
  opt->add("machineId,p",
	   "The id of the machine",
	   CONFIG,
	   fmid);
  return opt;
}

int main (int argc, char* argv[]){
  
  /******* Parsed value containers ****************/
  string dietConfig;

  /********** EMF data ************/
  IMS_Data::ProcessOp op;

  /******** Callback functions ******************/
  boost::function1<void,string> fmid(boost::bind(&IMS_Data::ProcessOp::setMachineId,boost::ref(op),_1));

  /**************** Describe options *************/
  boost::shared_ptr<Options> opt = makeGProcOp(argv[0], fmid, dietConfig);

  bool isEmpty;
  //To process list options
  GenericCli().processListOpt(opt, isEmpty, argc, argv);

  //call of the api function
  GetProcessesFunc getProcessesFunc(op);
  return GenericCli().run(getProcessesFunc, dietConfig, argc, argv);
}
