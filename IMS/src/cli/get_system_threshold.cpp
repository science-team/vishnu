/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file get_system_threshold.cpp
 * This file defines the VISHNU command to get the system threshold
 * \author Eugène PAMBA CAPO-CHICHI(eugene.capochichi@sysfera.com)
 */



#include "CLICmd.hpp"
#include "utilVishnu.hpp"
#include "cliError.hpp"
#include "cliUtil.hpp"
#include "api_ums.hpp"
#include "api_ims.hpp"
#include "sessionUtils.hpp"
#include <boost/bind.hpp>
#include "displayer.hpp"

#include "GenericCli.hpp"

using namespace std;
using namespace vishnu;

struct GetSysThresholdFunc {

  IMS_Data::ThresholdOp mthresholdOp;

  GetSysThresholdFunc(const IMS_Data::ThresholdOp thresholdOp):
   mthresholdOp(thresholdOp)
  {};

  int operator()(std::string sessionKey) {
    IMS_Data::ListThreshold listThreshold;    
    int res = getSystemThreshold(sessionKey, listThreshold, mthresholdOp);
    displayListSysThreshold(&listThreshold);    
    return res;
  }
};

boost::shared_ptr<Options>
makeGetystemThresholdOpt(string pgName,
                  boost::function1<void, string>& fMachineId,
                  boost::function1<void, IMS_Data::MetricType>& fType,
                  string& dietConfig) {

  boost::shared_ptr<Options> opt(new Options(pgName));

  // Environement option
  opt->add("dietConfig,c",
           "The diet config file",
           ENV,
           dietConfig);

  opt->add("machineId,m",
            "represents the id of the machine",
            CONFIG,
            fMachineId);

  opt->add("metricType,t",
            "the type of the metric",
            CONFIG,
            fType);

  return opt;
}


int main (int argc, char* argv[]){

  /******* Parsed value containers ****************/
  string dietConfig;

   /********** EMF data ************/
  IMS_Data::ThresholdOp thresholdOp;

  /******** Callback functions ******************/
  boost::function1<void,string> fMachineId (boost::bind(&IMS_Data::ThresholdOp::setMachineId,boost::ref(thresholdOp),_1));
  boost::function1<void,IMS_Data::MetricType> fType (boost::bind(&IMS_Data::ThresholdOp::setMetricType,boost::ref(thresholdOp),_1));

  /**************** Describe options *************/
  boost::shared_ptr<Options> opt=makeGetystemThresholdOpt(argv[0], fMachineId, fType, dietConfig);

  bool isEmpty;
  //To process list options
  GenericCli().processListOpt(opt, isEmpty, argc, argv);

  //call of the api function
  GetSysThresholdFunc getSysThresholdFunc(thresholdOp);
  return GenericCli().run(getSysThresholdFunc, dietConfig, argc, argv);

}

