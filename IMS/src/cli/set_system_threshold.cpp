/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file set_system_threshold.cpp
 * This file defines the VISHNU command to set the system threshold
 * \author Eugène PAMBA CAPO-CHICHI(eugene.capochichi@sysfera.com)
 */


#include "CLICmd.hpp"
#include "utilVishnu.hpp"
#include "cliError.hpp"
#include "cliUtil.hpp"
#include "api_ums.hpp"
#include "api_ims.hpp"
#include "sessionUtils.hpp"

#include "GenericCli.hpp"

using namespace std;
using namespace vishnu;

struct SetSysThresholdFunc {

  IMS_Data::Threshold msystemThreshold;

  SetSysThresholdFunc(const IMS_Data::Threshold& systemThreshold):
   msystemThreshold(systemThreshold)
  {};

  int operator()(std::string sessionKey) {
    return setSystemThreshold(sessionKey, msystemThreshold);
  }
};


int main (int argc, char* argv[]){

  /******* Parsed value containers ****************/
  string dietConfig;
  string machineId;
  string handler;
  int value;
  IMS_Data::MetricType type;
   /********** EMF data ************/
  IMS_Data::Threshold systemThreshold;

  /**************** Describe options *************/
  boost::shared_ptr<Options> opt(new Options(argv[0]));

  //Environement option
  opt->add("dietConfig,c",
           "The diet config file",
           ENV,
           dietConfig);

  opt->add("machineId,i",
            "represents the id of the machine",
            HIDDEN,
            machineId,1);
  opt->setPosition("machineId",1);

  opt->add("value,v",
            "represents the value of the threshold",
            HIDDEN,
            value,1);
  opt->setPosition("value",1);

  opt->add("type,t",
            "represents the type of the threshold",
            HIDDEN,
            type,1);
  opt->setPosition("type",1);

  opt->add( "handler,u",
            "represents the userId of the administrator that handles the threshold",
            HIDDEN,
            handler,1);
  opt->setPosition("handler",1);

  bool isEmpty;
  //To process list options
  GenericCli().processListOpt(opt, isEmpty, argc, argv, "machineId value type handler");

  //To set the corresponding values to the object systemThreshold
  systemThreshold.setMachineId(machineId);
  systemThreshold.setValue(value);
  systemThreshold.setType(type);
  systemThreshold.setHandler(handler);
  //call of the api function
  SetSysThresholdFunc setSysThresholdFunc(systemThreshold);
  return GenericCli().run(setSysThresholdFunc, dietConfig, argc, argv);
}

