/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file stop.cpp
 * This file defines the VISHNU command to stop a vishnu process
 * \author Eugène PAMBA CAPO-CHICHI(eugene.capochichi@sysfera.com)
 */


#include "CLICmd.hpp"
#include "utilVishnu.hpp"
#include "cliError.hpp"
#include "cliUtil.hpp"
#include "api_ums.hpp"
#include "api_ims.hpp"
#include "sessionUtils.hpp"
#include <boost/bind.hpp>

#include "GenericCli.hpp"

using namespace std;
using namespace vishnu;

struct ExportFunc {

  string moldSessionId;
  string mfilename;
  IMS_Data::ExportOp mexportOp;

  ExportFunc(const std::string& oldSessionId, const std::string& filename,
             const IMS_Data::ExportOp& exportOp):
   moldSessionId(oldSessionId), mfilename(filename), mexportOp(exportOp)
  {};

  int operator()(std::string sessionKey) {
    return exportCommands(sessionKey, moldSessionId, mfilename, mexportOp);
  }
};


boost::shared_ptr<Options>
makeExportOpt(string pgName,
            boost::function1<void, IMS_Data::ExportType>& fExportType,
            string& dietConfig) {

  boost::shared_ptr<Options> opt(new Options(pgName));

  // Environement option
  opt->add("dietConfig,c",
           "The diet config file",
           ENV,
           dietConfig);

    // All cli options
   opt->add("exportType,t",
            "the type to export\n"
            "1 for SHELL",
            CONFIG,
            fExportType);

  return opt;
}


int main (int argc, char* argv[]){

  /******* Parsed value containers ****************/
  string dietConfig;
  string oldSessionId;
  string filename;

   /********** EMF data ************/
  IMS_Data::ExportOp exportOp;

  /******** Callback functions ******************/
  boost::function1<void, IMS_Data::ExportType> fExportType (boost::bind(&IMS_Data::ExportOp::setExportType,boost::ref(exportOp),_1));


  /**************** Describe options *************/
  boost::shared_ptr<Options> opt=makeExportOpt(argv[0], fExportType, dietConfig);

  opt->add( "oldSessionId,o",
            "represents the id of the session to export",
            HIDDEN,
            oldSessionId,1);

  opt->setPosition("oldSessionId",1);

  opt->add( "filename,f",
            "represents the file in which the data will be exported",
            HIDDEN,
            filename,1);

  opt->setPosition("filename",1);

  bool isEmpty;
  //To process list options
  GenericCli().processListOpt(opt, isEmpty, argc, argv, "oldSessionId filename");

  //call of the api function
  ExportFunc exportFunc(oldSessionId, filename, exportOp);
  return GenericCli().run(exportFunc, dietConfig, argc, argv);

}

