/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file defineIdentifierUtils.hpp
 * \brief This file declare useful functions and types for the VISHNU define
 * identifier command
 * \author Daouda Traoré (daouda.traore@sysfera.com)
 */



#ifndef DEFINEIDENTIFIER_HH
#define DEFINEIDENTIFIER_HH


#include<iostream>
#include<boost/shared_ptr.hpp>

#include <ecore.hpp> // Ecore metamodel
#include <ecorecpp.hpp> // EMF4CPP utils
#include "IMS_Data.hpp"

#include "utilVishnu.hpp"
#include "cliError.hpp"
#include "cliUtil.hpp"
#include "CLICmd.hpp"
class Options;

using namespace std;
using namespace vishnu;

/**
 * \brief To build options for the VISHNU copy of file command
 * \param pgName The name of the command
 * \param dietConfig Represents the VISHNU config file
 * \param newFormat Represents the new format to use
 * \return The built option
 */
boost::shared_ptr<Options>
makeDefineIdentifierOptions(string pgName,
    string& dietConfig,
    string& newFormat){

  boost::shared_ptr<Options> opt(new Options(pgName));

  // Environement option
  opt->add("dietConfig,c",
      "The diet config file",
      ENV,
      dietConfig);

  // All cli obligatory parameters
  opt->add("format,f",
      "The new format to use",
      HIDDEN,
      newFormat,1);
  opt->setPosition("format",1);

  return opt;
}

/**
 * \brief  A define type
 */

typedef enum{
  JOB,
  MACHINE,
  TRANSFER,
  USER,
  AUTH
} DefineIdentifierType;


/**
 * \brief A functor to handle define identifier api function
 */

template<DefineIdentifierType defineIdentifierType>
struct DefineIdentifierFunc {
  /**
   * \brief The new format
   */
  std::string mnewFormat;

  /**
   * \brief Constructor with parameters
   * \param newFormat The new format
   */
  DefineIdentifierFunc(const std::string& newFormat):mnewFormat(newFormat){};

  /**
   * \brief () operator
   * \param sessionKey The session key
   * \return 0 if it succeeds or an error code otherwise
   */
  int operator()(const std::string& sessionKey) {

    int res = -1;
    switch(defineIdentifierType) {
      case JOB:
        res = defineJobIdentifier(sessionKey, mnewFormat);
        break;
      case MACHINE:
        res = defineMachineIdentifier(sessionKey, mnewFormat);
        break;
      case TRANSFER:
        res = defineTransferIdentifier(sessionKey, mnewFormat);
        break;
      case USER:
        res = defineUserIdentifier(sessionKey, mnewFormat);
        break;
      case AUTH:
        res = defineAuthIdentifier(sessionKey, mnewFormat);
        break;
      default:
        break;
    }

    return res;
  }
};


#endif



