/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

#include "utilVishnu.hpp"
#include "displayer.hpp"

using namespace std;
using namespace vishnu;

void
displayListProc(IMS_Data::ListProcesses* li){
  for (unsigned int i = 0 ; i < li->getProcess().size() ; i++){
    displayProc(li->getProcess().get(i));
  }
}

void
displayProc(IMS_Data::Process* p){
  cout << " ------------------------ " << endl;
  cout << " Vishnu name : " << p->getProcessName() << endl;
  cout << " Diet id     : " << p->getDietId() << endl;
  cout << " Machine id  : " << p->getMachineId() << endl;
}

void
displayListMetric(IMS_Data::ListMetric* li){
  for (unsigned int i = 0 ; i < li->getMetric().size() ; i++){
    displayMetric(li->getMetric().get(i));
  }
}

void
displayMetric(IMS_Data::Metric* m){
  string type;
  string unity;
  switch(m->getType()){
  case 1 :
    type = "Free CPU";
    unity = " %";
    break;
  case 2 :
    type = "Free diskspace";
    unity = " Mb";
    break;
  case 3 :
    type = "Free memory";
    unity = " Mb";
    break;
  default:
    type = "unknown ";
    break;
  }
  cout << " ------------------------ " << endl;
  cout << " type : " << type << endl;
  cout << " value: " << m->getValue() << unity << endl;
  string date = boost::posix_time::to_simple_string(boost::posix_time::from_time_t(convertUTCtimeINLocaltime(m->getTime())));
  cout << " time : " << date << endl;
}

void
displayFreq(int freq) {
  cout << "The frequency of update of the history maker is " << freq << endl;
}


void
displaySystemInfo(IMS_Data::SystemInfo* sysInfo){
  cout << " ------------------------ " << endl;
  cout << " machineId : " << sysInfo->getMachineId() << endl;
  cout << " Disk space: " << sysInfo->getDiskSpace() << endl;
  cout << " Memory : " << sysInfo->getMemory() << endl;
}


void
displayListSysInfo(IMS_Data::ListSysInfo* li) {
  for (unsigned int i = 0 ; i < li->getSysInfo().size() ; i++){
    displaySystemInfo(li->getSysInfo().get(i));
  }
}


/**
 * \brief  function to convert the type of metric into string
 * \param metricType: The type of metric
 * \return The converted state value
 */
std::string
convertMetricToString(const int& metricType) {
  string res = "";
  switch(metricType) {
    case 0:
      res = "UNDEFINED";
      break;
    case 1:
      res = "CPUUSE";
      break;
    case 2:
      res = "FREEDISKSPACE";
      break;
    case 3:
      res = "FREEMORY";
      break;
    default:
      res = "UNDEFINED";
      break;
  }
  return res;
}



void
displaySystemThreshold(IMS_Data::Threshold* systemThreshold) {
  cout << " ------------------------ " << endl;
  cout << " machineId : " << systemThreshold->getMachineId() << endl;
  cout << " value : " << systemThreshold->getValue() << endl;
  cout << " handler : " << systemThreshold->getHandler() << endl;
  cout << " type : " << convertMetricToString(systemThreshold->getType()) << endl;
}


void
displayListSysThreshold(IMS_Data::ListThreshold* li) {
  for (unsigned int i = 0 ; i < li->getThreshold().size() ; i++){
    displaySystemThreshold(li->getThreshold().get(i));
  }
}

