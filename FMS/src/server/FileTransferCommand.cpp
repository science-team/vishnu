/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \brief Implementation of Helper class to build a transfer command
 * \author Ibrahima Cisse (ibrahima.cisse@sysfera.com)
 */

#include "FileTransferCommand.hpp"
#include "ScpCommand.hpp"
#include "RsyncCommand.hpp"

FileTransferCommand::FileTransferCommand ():mname("UNKNOWN"), 
    mlocation("UNKNOWN"),
    mrecursive(false),
    mcompression(false),
    mcommand("UNKNOWN"){}

FileTransferCommand::FileTransferCommand (const std::string& name,
                                  const std::string& location,
                                  bool recursive,
                                  bool compression,
                                  const std::string& command
                                   ):mname(name), 
                                     mlocation(location),
                                     mrecursive(recursive),
                                     mcompression(compression),
                                     mcommand(command){}


std::string FileTransferCommand::getName()const{

  return mname; 
}


std::string FileTransferCommand::getLocation()const{
  return mlocation;
}

bool FileTransferCommand::isRecursive() const{
  return mrecursive;
}

bool FileTransferCommand::useCompression() const{
  return mcompression;
}

std::string FileTransferCommand::getCommand()const{
  return mcommand;
}

void FileTransferCommand::setLocation (const std::string& location){

  mlocation=location;
}

void FileTransferCommand::setName(const std::string& name){

mname=name;
}


void FileTransferCommand::setCommand(const std::string& command){
  mcommand=command;
}



void FileTransferCommand::addOptions (const std::string& options){

  mcommand.append(options);
}


FileTransferCommand* FileTransferCommand::getCopyCommand(const SessionServer& sessionServer,const FMS_Data::CpFileOptions& options){

  if (options.getTrCommand()==1) {// Rsync

    return new RsyncCommand(options.isIsRecursive());
  }
  else {

    return new ScpCommand(options.isIsRecursive());

  }

}
