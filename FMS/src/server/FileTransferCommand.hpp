/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \brief Declaration of Helper class to build a transfer command
 * \author Ibrahima Cisse (ibrahima.cisse@sysfera.com)
 */


#ifndef TRANSFERCOMMAND_HPP
#define TRANSFERCOMMAND_HPP

#include <string>
#include "FMS_Data.hpp"
#include "SessionServer.hpp"

/**
 * \brief The generic file transfer command class
 */

class FileTransferCommand{

  public:
    /**
     * \brief The default constructor
     */

    FileTransferCommand ();
    /**
     * \brief A constructor by value
     * \param name the name of the command
     * \param location the path of the command
     * \param recursive a flag for command recursivity
     * \param compression a flag for use compression
     * \param command the command 
     */

    FileTransferCommand (const std::string& name,
        const std::string& location,
        bool recursive,
        bool compression,
        const std::string& command=""
        );

    /**
     * \brief Get the command
     * \return the command
     */
    std::string getCommand()const;

    /**
     * \brief Get the command name
     * \return the command name
     */
    std::string getName()const;

    /**
     * \brief Get the command location
     * \return the command location
     */
    std::string getLocation()const;
    /**
     * \brief Check if the command is recursive
     * \return true if the command is recursive false otherwise
     */
    bool isRecursive() const;
    /**
     * \brief Check if the command uses the compression
     * \return true if the command uses compression false otherwise
     */
    bool useCompression() const;

    /**
     * \brief A factory to build a copy command
     * \param sessionServer the session object server
     * \param options the copy options
     * \return a new file transfer command
     */
    static FileTransferCommand* getCopyCommand(const SessionServer& sessionServer, const FMS_Data::CpFileOptions& options);

  protected:

    /**
     * \brief To update the command location
     * \param location the new command location
     */

    void setLocation (const std::string& location);

    /**
     * \brief To update the command name
     * \param name the new command name
     */
    void setName(const std::string& name);
    /**
     * \brief To update the command 
     * \param command the new command 
     */
    void setCommand(const std::string& command);

    /**
     * \brief To add options to the command 
     * \param options the new options to add
     */
    void addOptions (const std::string& options);
    /**
     * \brief To build the command using options
     */
    virtual void build()=0;

  private:
    /**
     * \brief The name
     */
    std::string mname;
    /**
     * \brief The location
     */
    std::string mlocation;
    /**
     * \brief The flag for recursivity
     */
    bool mrecursive;
    /**
     * \brief The flag for compression
     */
    bool mcompression;
    /**
     * \brief The command
     */
    std::string mcommand;

};


#endif
