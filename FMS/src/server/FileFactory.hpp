/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/


/**
 * \file FileFactory.hpp
 * This file contains a  file server class factory declarartion
 * \author Ibrahima Cisse (ibrahima.cisse@sysfera.com)
 */

#ifndef FILEFACTORY_HH
#define FILEFACTORY_HH

#include <string>

#include "SessionServer.hpp"
#include "File.hpp"
#include "SSHFile.hpp"

/**
 * \brief  A factory class for the Files classes.
 */

class FileFactory {

  public:
    /**
     * \brief Update a ssh server host
     * \param sshServer the new ssh server host
     */
    static void setSSHServer(const std::string& sshServer);
    /**
     * \brief Update a ssh port
     * \param sshPort the new ssh port
     */
    static void setSSHPort(const unsigned int sshPort);
    /**
     * \brief Update a ssh command path
     * \param sshCommand the new ssh command path
     */
    static void setSSHCommand(const std::string& sshCommand);
    /**
     * \brief Update a scp command path
     * \param scpCommand the new scp command path
     */
    static void setSCPCommand(const std::string& scpCommand);

    /**
     * \brief Get the ssh file implementation
     * \param sessionServer the session server object
     * \param path the path of the file
     * \param user the file user
     * \param key the ssh private key path
     * \return The new built file
     */
    static File* getFileServer(const SessionServer& sessionServer,
        const std::string& path,
        const std::string& user,
        const std::string& key);
  private :


    /**
     * \brief The ssh host server
     */
    static std::string sshServer;
    /**
     * \brief The ssh port
     */
    static unsigned int sshPort;
    /**
     * \brief The ssh command path
     */
    static std::string sshCommand;
    /**
     * \brief The scp command path
     */
    static  std::string scpCommand;

};

#endif
