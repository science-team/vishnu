/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file list_dir.cpp
 * This file defines the VISHNU list dir command 
 * \author Daouda Traore (daouda.traore@sysfera.com)
 */


#include "CLICmd.hpp"
#include "utilVishnu.hpp"
#include "cliError.hpp"
#include "cliUtil.hpp"
#include "api_ums.hpp"
#include "api_fms.hpp"
#include "sessionUtils.hpp"
#include "FMS_Data.hpp"
#include "cmdArgs.hpp"
#include <boost/bind.hpp>
#include "FMSDisplayer.hpp"
#include "GenericCli.hpp"
#include "remoteCommandUtils.hpp"

namespace po = boost::program_options;

using namespace std;
using namespace vishnu;
using namespace FMS_Data;

struct ListDirFunc {

  std::string mpath;
  LsDirOptions moptions;

  ListDirFunc(const std::string& path,const LsDirOptions& options):mpath(path),moptions(options){}

  int operator()(std::string sessionKey) {

    DirEntryList dirContent;

    int res= listDir(sessionKey, mpath, dirContent, moptions);

    if (false ==moptions.isLongFormat()) {

      for(unsigned int i = 0; i < dirContent.getDirEntries().size(); i++) {

        std::cout <<  (dirContent.getDirEntries().get(i))->getPath() <<"\n";
      } 

    }else{

      cout << dirContent << "\n"; 
    }
    return res;
  }
};


int main (int ac, char* av[]){
  
  /******* Parsed value containers ****************/
  string dietConfig;
  string path;
   
  /********** EMF data ************/
  FMS_Data::LsDirOptions lsDirOptions;
  
  /**************** Describe options *************/


  boost::shared_ptr<Options> opt(makeRemoteCommandOpt(av[0],dietConfig,path));

 opt->add("longFormat,l",
      "It specifies the long display format (all available file information",
      CONFIG);

  opt->add("allFiles,a",
      "Allows to display all files including hidden file",
      CONFIG);


  bool isEmpty;
  GenericCli().processListOpt( opt, isEmpty,ac,av," path");


 if ( opt->count("allFiles")){
     lsDirOptions.setAllFiles(true);
  }

 if ( opt->count("longFormat")){
     lsDirOptions.setLongFormat(true);
  }

 ListDirFunc apiFunc(path,lsDirOptions);
  
 return GenericCli().run(apiFunc, dietConfig, ac, av);

}
