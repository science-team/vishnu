/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file ch_mod.cpp
 * This file defines the VISHNU ch_mod command 
 * \author Daouda Traore (daouda.traore@sysfera.com)
 */

#include "CLICmd.hpp"
#include "utilVishnu.hpp"
#include "cliError.hpp"
#include "cliUtil.hpp"
#include "api_ums.hpp"
#include "api_fms.hpp"
#include "sessionUtils.hpp"
#include "FMS_Data.hpp"
#include "cmdArgs.hpp"
#include <boost/bind.hpp>
#include <sys/types.h>
#include "FMSDisplayer.hpp"
#include "GenericCli.hpp"
namespace po = boost::program_options;

using namespace std;
using namespace vishnu;

struct ChangeFileModeFunc {

  std::string mpath;
  mode_t  mmode;
  
  ChangeFileModeFunc(const std::string& path,const mode_t&  mode):mpath(path),mmode(mode){}

  int operator()(std::string sessionKey) {
    
      int res =chMod(sessionKey, mmode, mpath);
    return res;
  }
};


int main (int argc, char* argv[]){
  

  /******* Parsed value containers ****************/
  string dietConfig;
  string path;
  mode_t  mode;

  /**************** Describe options *************/

  boost::shared_ptr<Options> opt=processOpt(argv[0], dietConfig);

  opt->add("mode,m",
      "The acces rights of file/directory in octal sytem",
      HIDDEN,
      mode,1);
  opt->setPosition("mode",1); 

 opt->add("path,p",
      "The file/directory following the pattern [host:]file path",
      HIDDEN,
      path,1);
  opt->setPosition("path",1); 

  bool isEmpty;
  GenericCli().processListOpt( opt, isEmpty,argc,argv,"mode path");
  ChangeFileModeFunc apiFunc(path,mode);
  return GenericCli().run(apiFunc, dietConfig, argc, argv);

}
