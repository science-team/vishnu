/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file remove_file.cpp
 * This file defines the VISHNU remove file command 
 * \author Daouda Traore (daouda.traore@sysfera.com)
 */


#include "CLICmd.hpp"
#include "utilVishnu.hpp"
#include "cliError.hpp"
#include "cliUtil.hpp"
#include "api_ums.hpp"
#include "api_fms.hpp"
#include "sessionUtils.hpp"
#include "cmdArgs.hpp"
#include <boost/bind.hpp>
#include "GenericCli.hpp"
#include "remoteCommandUtils.hpp"

namespace po = boost::program_options;

using namespace std;
using namespace vishnu;

using namespace FMS_Data;

int main (int ac, char* av[]){
  

  /******* Parsed value containers ****************/
  string dietConfig;
  string path;

  /********** EMF data ************/
  FMS_Data::RmFileOptions rmFileOptions;

  /**************** Describe options *************/
  boost::shared_ptr<Options> opt(makeRemoteCommandOpt(av[0],dietConfig,path));
  
  opt->add("isRecursive,r",
      "It specifies when the remove command is recursive (case of directory) or not.",
      CONFIG);
  
  bool isEmpty;
  GenericCli().processListOpt( opt, isEmpty,ac,av," path");
 
  // Parse the cli and setting the options found

  if(opt->count("isRecursive")){
    rmFileOptions.setIsRecursive(true);
  }

  FileActionFunc<REMOVEFILE,FMS_Data::RmFileOptions> apiFunc(path,rmFileOptions);
  return GenericCli().run(apiFunc, dietConfig, ac, av);

}
