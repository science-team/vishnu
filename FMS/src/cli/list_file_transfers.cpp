/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
 * \file list_file_transfers.cpp
 * This file defines the VISHNU list file transfers command 
 * \author Daouda Traore (daouda.traore@sysfera.com)
 */


#include "CLICmd.hpp"
#include "utilVishnu.hpp"
#include "cliError.hpp"
#include "cliUtil.hpp"
#include "api_ums.hpp"
#include "api_fms.hpp"
#include "sessionUtils.hpp"
#include "FMS_Data.hpp"
#include <boost/bind.hpp>
#include "FMSDisplayer.hpp"
#include "GenericCli.hpp"

namespace po = boost::program_options;

using namespace std;
using namespace vishnu;
using namespace FMS_Data;

/**
 * \brief To build options for the VISHNU list directory of file command
 * \param pgName : The name of the command
 * \param dietConfig: Represents the VISHNU config file
 * \param ftransferId: The file transfer identifier
 * \param ffromMachineId: The machine that is the source of the file transfer
 * \param fuserId: The user identifier
 * \param fstatus: The file transfer status
 */
boost::shared_ptr<Options>
makeListFileTransferTrOpt(string pgName, 
    string& dietConfig,
    boost::function1<void, string>& ftransferId,
    boost::function1<void, string>& ffromMachineId,
    boost::function1<void, string>& fuserId,
    string& statusStr){

  boost::shared_ptr<Options> opt(new Options(pgName));

  // Environement option
  opt->add("dietConfig,c",
      "The diet config file",
      ENV,
      dietConfig);

  opt->add("transferId,t",
      "A given transfer id",
      CONFIG,
      ftransferId);

 opt->add("fromMachineId,m",
      "The machine that is the source of the file transfer",
      CONFIG,
      ffromMachineId);

 opt->add("userId,u",
      "Allows an admin to stop file transfers of a specific user",
      CONFIG,
      fuserId);

 opt->add("status,s",
      "The file transfer status. The different  available status are:\n"
      "0 or I: for INPROGRESS file transfer\n"
      "1 or T: for COMPLETED file transfer\n"
      "2 or C: for CANCELLED file transfer\n"
      "3 or F: for FAILED file transfer\n",
      CONFIG,
      statusStr);

  return opt;
}



// List file transfer functor
struct ListFileTransferFunc {

  FMS_Data::LsTransferOptions mlsFileTransferOptions;



  ListFileTransferFunc(const FMS_Data::LsTransferOptions& lsFileTransferOptions):mlsFileTransferOptions(lsFileTransferOptions)
  {};

  int operator()(std::string sessionKey) {

    FileTransferList fileTransferList;

    int res=listFileTransfers(sessionKey, fileTransferList,mlsFileTransferOptions);

    //To display the file transfer list
    std::cout << fileTransferList << std::endl;

    return res;
  }
};



int main (int ac, char* av[]){
  
  int ret; // Return value

  /******* Parsed value containers ****************/
  string dietConfig;
  string statusStr;
  int status;

   /********** EMF data ************/
  FMS_Data::LsTransferOptions lsFileTransferOptions;
  
  /******** Callback functions ******************/
  boost::function1<void, string> ftranferId(boost::bind(&FMS_Data::LsTransferOptions::setTransferId, boost::ref(lsFileTransferOptions),_1));
  boost::function1<void, string> ffromMachineId(boost::bind(&FMS_Data::LsTransferOptions::setFromMachineId, boost::ref(lsFileTransferOptions),_1));
  boost::function1<void, string> fuserId(boost::bind(&FMS_Data::LsTransferOptions::setUserId, boost::ref(lsFileTransferOptions),_1));

  /**************** Describe options *************/
  boost::shared_ptr<Options> opt= makeListFileTransferTrOpt(av[0], dietConfig, ftranferId, ffromMachineId, fuserId, statusStr);

  CLICmd cmd = CLICmd (ac, av, opt);

 // Parse the cli and setting the options found
    ret = cmd.parse(env_name_mapper());


  if(statusStr.size()!=0) {
    size_t pos = statusStr.find_first_not_of("0123456789");
    if(pos!=std::string::npos) {
      if(statusStr.size()==1) {
        switch(statusStr[0]) {
          case 'I' :
            status = 0;
            break;
          case 'T' :
            status = 1;
            break;
          case 'C' :
            status = 2;
            break;
          case 'F' :
            status = 3;
            break;
          default:
            status = -1;
            break;
        }
      }
      if( ( statusStr.size() > 1 ) || ( status==-1) ){
        std::cerr << "Unknown file transfer status " << statusStr << std::endl;
        return 0;
      }
    } else {
      status = convertToInt(statusStr);
    }
    lsFileTransferOptions.setStatus(status);
  }
  
  if (ret != CLI_SUCCESS){
    helpUsage(*opt,"[options]");
    return ret;
  }

  // PreProcess (adapt some parameters if necessary)
  checkVishnuConfig(*opt);  
  if ( opt->count("help")){
    helpUsage(*opt,"[options] ");
    return 0;
  }

  // Process command
 ListFileTransferFunc apiFunc( lsFileTransferOptions); 
 return GenericCli().run(apiFunc, dietConfig, ac, av);

}
