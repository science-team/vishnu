/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

#include <boost/scoped_ptr.hpp>

#include "FileTransferProxy.hpp"
#include "FileProxyFactory.hpp"
#include "SessionProxy.hpp"
#include "utilClient.hpp"
using namespace FMS_Data;
using namespace UMS_Data;
using namespace std;

FileTransferProxy::FileTransferProxy(const std::string& sessionKey):msessionKey(sessionKey){
}

FileTransferProxy::FileTransferProxy(const std::string& sessionKey,
                                     const std::string& srcFilePath,
                                     const std::string& destFilePath): msessionKey(sessionKey),
 msrcFilePath(srcFilePath), mdestFilePath(destFilePath){
}

int FileTransferProxy::addCpThread(const CpFileOptions& options){
  
  SessionProxy sessionProxy(msessionKey);

  boost::scoped_ptr<FileProxy> f (FileProxyFactory::getFileProxy(sessionProxy,msrcFilePath));

  int result= f->cp(mdestFilePath,options);

  return result;

}

int FileTransferProxy::addMvThread(const CpFileOptions& options){

  SessionProxy sessionProxy(msessionKey);

  boost::scoped_ptr<FileProxy> f (FileProxyFactory::getFileProxy(sessionProxy,msrcFilePath));

  int result= f->mv(mdestFilePath,options);

  return result;
}

int FileTransferProxy::addCpAsyncThread(const CpFileOptions& options){
  
  SessionProxy sessionProxy(msessionKey);

  boost::scoped_ptr<FileProxy> f (FileProxyFactory::getFileProxy(sessionProxy,msrcFilePath));

  int result= f->cpAsync(mdestFilePath,options,mtransferInfo);

  return result;

}

int FileTransferProxy::addMvAsyncThread(const CpFileOptions& options){

  SessionProxy sessionProxy(msessionKey);

  boost::scoped_ptr<FileProxy> f (FileProxyFactory::getFileProxy(sessionProxy,msrcFilePath));

  int result= f->mvAsync(mdestFilePath,options,mtransferInfo);

  return result;
}



int FileTransferProxy::stopThread(const StopTransferOptions& options) {

  diet_profile_t* profile = NULL;
  
  char* optionsToString = NULL;
  char* errorInfo = NULL;
  std::string serviceName = "FileTransferStop";

  profile = diet_profile_alloc(serviceName.c_str(), 1, 1, 2);

  std::string msgErrorDiet = "call of function diet_string_set is rejected ";
  //IN Parameters
  if (diet_string_set(diet_parameter(profile,0), strdup(msessionKey.c_str()), DIET_VOLATILE)) {
    msgErrorDiet += "with sessionKey parameter "+msessionKey;
    raiseDietMsgException(msgErrorDiet);
  }


  ::ecorecpp::serializer::serializer _ser;
  //To serialize the option object in to optionsInString
  optionsToString =  strdup(_ser.serialize_str(const_cast<FMS_Data::StopTransferOptions_ptr>(&options)).c_str());

  if (diet_string_set(diet_parameter(profile,1), optionsToString, DIET_VOLATILE)) {
    msgErrorDiet += "with jobInString parameter "+std::string(optionsToString);
    raiseDietMsgException(msgErrorDiet);
  }

  //OUT Parameters
  diet_string_set(diet_parameter(profile,2), NULL, DIET_VOLATILE);

  if(!diet_call(profile)) {
    if(diet_string_get(diet_parameter(profile,2), &errorInfo, NULL)){
      msgErrorDiet += " by receiving errorInfo message";
      raiseDietMsgException(msgErrorDiet);
    }
  }
  else {
    raiseDietMsgException("DIET call failure");
  }

  /*To raise a vishnu exception if the receiving message is not empty*/
  raiseExceptionIfNotEmptyMsg(errorInfo);
  diet_profile_free(profile);
  return 0;

}



const FMS_Data::FileTransfer& FileTransferProxy::getFileTransfer() const {
  return mtransferInfo;
}

FileTransferProxy::~FileTransferProxy(){

}
