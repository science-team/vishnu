/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/


/**
 * \file FileTransferProxy.hpp
 * \brief Contains a declaration of all File transfer services
 */

#ifndef FILETRANSFERPROXY
#define FILETRANSFERPROXY

#include "FMS_Data_forward.hpp"
#include "FMS_Data.hpp"

/**
 * \brief A proxy class to handle file transfer
 */

class FileTransferProxy{

  public:
    /**
     * \brief A constructor with one parameter
     * \param sessionKey The session key
     */ 
    explicit FileTransferProxy(const std::string& sessionKey);
    /**
     * \brief Another constructor with parameters
     * \param sessionKey The session key
     * \param srcFilePath The source file path
     * \param destFilePath The destination file path
     */ 
    FileTransferProxy(const std::string& sessionKey,
        const std::string& srcFilePath,
        const std::string& destFilePath);
    /**
     * \brief Add a new copy file thread 
     * \param options the copy options
     * \return 0 if the function succeeds or an error code otherwise
     */
    int addCpThread(const FMS_Data::CpFileOptions& options);

    /**
     * \brief Add a new move file thread 
     * \param options the move file options
     * \return 0 if the function succeeds or an error code otherwise
     */
    int addMvThread(const FMS_Data::CpFileOptions& options);

    /**
     * \brief Add a new asynchronous copy file thread 
     * \param options the copy options
     * \return 0 if the function succeeds or an error code otherwise
     */
    int addCpAsyncThread(const FMS_Data::CpFileOptions& options);

    /**
     * \brief Add a new asynchronous move file thread 
     * \param options the move options
     * \return 0 if the function succeeds or an error code otherwise
     */
    int addMvAsyncThread(const FMS_Data::CpFileOptions& options);
    /**
     * \brief Stop a file transfer
     * \param options The stop options
     * \return 0 if the function succeeds or an error code otherwise
     */


    int stopThread(const FMS_Data::StopTransferOptions& options);
    /**
     * \brief Get The file transfer information
     * \return The file transfer information
     */
    const FMS_Data::FileTransfer& getFileTransfer() const;
    /**
     * \brief The default destructor
     */
    ~FileTransferProxy(); 

  private:
    /**
     * \brief The session key
     */
    std::string msessionKey;
    /**
     * \brief The source file path
     */
    std::string msrcFilePath;
    /**
     * \brief The destination file path
     */
    std::string mdestFilePath;
    /**
     * \brief The file transfer information
     */
    FMS_Data::FileTransfer mtransferInfo;
};

#endif
