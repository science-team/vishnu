/* This file is a part of VISHNU.

* Copyright SysFera SA (2011) 

* contact@sysfera.com

* This software is a computer program whose purpose is to provide 
* access to distributed computing resources.
*
* This software is governed by the CeCILL  license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 

* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

/**
* \file ServerFMS.cpp
* \brief This file presents the implementation of the FMS server.
* \author Ibrahima Cisse (ibrahima.cisse@sysfera.com)
* \date 16/05/2011
*/

#include "ServerFMS.hpp"
#include "utilVishnu.hpp"
#include "DbFactory.hpp"
#include <boost/scoped_ptr.hpp>
#include "SystemException.hpp"
#include "internalApiFMS.hpp"

using namespace vishnu;
using namespace FMS_Data;

// {{RELAX<MISRA_0_1_3> Three static variables 
Database *ServerFMS::mdatabaseVishnu = NULL;
ServerFMS* ServerFMS::minstance = NULL;
FMSMapper *ServerFMS::mmapper = NULL;
// }}RELAX<MISRA_0_1_3>



/**
 * \brief To get the unique instance of the server
 */
ServerFMS*
ServerFMS::getInstance() {
  if (minstance == NULL) {
    minstance = new ServerFMS();
  }
  return minstance;
}

/**
* \brief To get the vishnuId
* \return the path of the configuration file
*/
int
ServerFMS::getVishnuId() const {
  return mvishnuId;
}


/**
* \brief Constructor (private)
*/
ServerFMS::ServerFMS() : mprofile(NULL) {
}

/**
* \brief To initialize the FMS server with individual parameters
* \fn int init(std::string vishnuid)
* \param vishnuId The id of the vishnu configuration registered in the database
* \param dbConfig  The configuration of the database
* \return an error code (0 if success and 1 if an error occurs)
*/
int
ServerFMS::init(int vishnuId,
                DbConfiguration dbConfig) {


  DbFactory factory;

  try {
    mdatabaseVishnu = factory.createDatabaseInstance(dbConfig);

    mvishnuId = vishnuId;

    std::string sqlCommand("SELECT * FROM vishnu where vishnuid="+convertToString(mvishnuId));

    /*connection to the database*/
    mdatabaseVishnu->connect();

    mmapper = new FMSMapper(MapperRegistry::getInstance(), FMSMAPPERNAME);
    mmapper->registerMapper();

    /* Checking of vishnuid on the database */
    boost::scoped_ptr<DatabaseResult> result(mdatabaseVishnu->getResult(sqlCommand.c_str()));
    if (result->getResults().size() == 0) {
      SystemException e(ERRCODE_DBERR, "The vishnuid is unrecognized");
      throw e;
    }

  } catch (VishnuException& e) {
      std::cout << e.what() << std::endl;
      exit(0);
  }

  // initialization of the service table
  diet_service_table_init(NB_SRV);

  // getFileInfos

  mprofile = getInfosProfile();

  if(diet_service_table_add(mprofile, NULL, get_infos)){

    return 1;
  }
  diet_profile_desc_free(mprofile);
  
  // content of file

  mprofile = getContentProfile();

  if ( diet_service_table_add(mprofile, NULL, contentFile)){
    return 1;

  }

  diet_profile_desc_free(mprofile);


  // Head of file

  mprofile = getHeadProfile();

  if ( diet_service_table_add(mprofile, NULL, headFile)){
    return 1;

  }

  diet_profile_desc_free(mprofile);

  // Tail of file

  mprofile = getTailProfile();

  if ( diet_service_table_add(mprofile, NULL, tailFile)){

    return 1;

  }

  diet_profile_desc_free(mprofile);


  // Create file

  mprofile = getCreateFileProfile();

  if ( diet_service_table_add(mprofile, NULL, solveCreateFile)){

    return 1;

  }

  diet_profile_desc_free(mprofile);

// Create dir

  mprofile = getCreateDirProfile();

  if ( diet_service_table_add(mprofile, NULL, solveCreateDir)){

    return 1;

  }

  diet_profile_desc_free(mprofile);


  // Remove file

  mprofile = getRemoveFileProfile();

  if ( diet_service_table_add(mprofile, NULL, solveRemoveFile)){

    return 1;

  }

  diet_profile_desc_free(mprofile);

// Remove dir

  mprofile = getRemoveDirProfile();

  if ( diet_service_table_add(mprofile, NULL, solveRemoveDir)){

    return 1;

  }

  diet_profile_desc_free(mprofile);

// Change group file

  mprofile = getChangeGroupProfile();

  if ( diet_service_table_add(mprofile, NULL, solveChangeGroup)){

    return 1;

  }

  diet_profile_desc_free(mprofile);

// Change mode file

  mprofile = getChangeModeProfile();

  if ( diet_service_table_add(mprofile, NULL, solveChangeMode)){

    return 1;

  }

  diet_profile_desc_free(mprofile);

// List the directory content

  mprofile = getListDirProfile();

  if ( diet_service_table_add(mprofile, NULL, solveListDir)){

    return 1;

  }

  diet_profile_desc_free(mprofile);


// Copy file

  mprofile = getTransferFileProfile("FileCopy");

  
  if ( diet_service_table_add(mprofile, NULL, solveTransferFile<File::copy,File::sync>)){

    return 1;
  }

  diet_profile_desc_free(mprofile);

// Copy Remote file

  mprofile = getTransferRemoteFileProfile("RemoteFileCopy");

  if ( diet_service_table_add(mprofile, NULL, solveTransferRemoteFile<File::copy,File::sync>)){

    return 1;
  }

  diet_profile_desc_free(mprofile);

// Move file

  mprofile = getTransferFileProfile("FileMove");

  
  if ( diet_service_table_add(mprofile, NULL, solveTransferFile<File::move,File::sync>)){

    return 1;
  }

  diet_profile_desc_free(mprofile);

// Move Remote file

  mprofile = getTransferRemoteFileProfile("RemoteFileMove");

  if ( diet_service_table_add(mprofile, NULL, solveTransferRemoteFile<File::move,File::sync>)){

    return 1;
  }

  diet_profile_desc_free(mprofile);

// Asynchronous command area

// CopyAsync file

  mprofile = getTransferFileAsyncProfile("FileCopyAsync");

  if ( diet_service_table_add(mprofile, NULL, solveTransferFile<File::copy,File::async>)){

    return 1;
  }

  diet_profile_desc_free(mprofile);

  // CopyAsync Remote file

  mprofile = getTransferRemoteFileAsyncProfile("RemoteFileCopyAsync");

  if ( diet_service_table_add(mprofile, NULL, solveTransferRemoteFile<File::copy,File::async>)){

    return 1;
  }

  diet_profile_desc_free(mprofile);


  // Move Async file

  mprofile = getTransferFileAsyncProfile("FileMoveAsync");

  if ( diet_service_table_add(mprofile, NULL, solveTransferFile<File::move,File::async>)){

    return 1;
  }

  diet_profile_desc_free(mprofile);




  // MoveAsync Remote file

  mprofile = getTransferRemoteFileAsyncProfile("RemoteFileMoveAsync");


  if ( diet_service_table_add(mprofile, NULL, solveTransferRemoteFile<File::move,File::async>)){

    return 1;
  }



  // List file transfer

  mprofile = getFileTransfersListProfile();

  if ( diet_service_table_add(mprofile, NULL, solveGetListOfFileTransfers)){

    return 1;
  }

  diet_profile_desc_free(mprofile);

  // Stop file transfer

  mprofile = getFileTransferStopProfile();

  if ( diet_service_table_add(mprofile, NULL,solveFileTransferStop ) ){

    return 1;
  }

  diet_profile_desc_free(mprofile);



  return 0;
}

/**
* \brief Destructor, raises an exception on error
*/
ServerFMS::~ServerFMS() {
  if (mmapper != NULL) {
    delete mmapper;
  }
  if (mdatabaseVishnu != NULL) {
    delete mdatabaseVishnu;
  }
}

